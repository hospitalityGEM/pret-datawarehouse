SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vDimAssessorJoined]
AS
SELECT d.[DinerUID]
      ,[Forename]
      ,[Surname]
      ,[Address1]
      ,[Address2]
      ,[Address3]
      ,[Address4]
      ,[Address5]
      ,[Postcode]
      ,[HomeTel]
      ,[WorkTel]
      ,[Mobile]
      ,[Fax]
      ,[Email]
      ,[DinerStatusUID]
      ,[BankName]
      ,[DOB]
      ,[OccupationUID]
	  ,CONVERT(NVARCHAR(10),
		CASE [Gender]
			WHEN 'F' THEN 'Female'
			WHEN 'M' THEN 'Male'
			ELSE 'Unknown'
		END  
	  ) AS Gender
	 
	  ,ISNULL(ddl.DateKey,19000101) AS LastLoginDateKey
	  ,ISNULL(TotalWarnings.WarningCount, 0) AS TotalWarnings
	  ,ISNULL(RecentWarnings.WarningCount, 0) AS RecentWarnings -- Past 12 Months
	  ,ISNULL(ddlw.DateKey, 19000101) AS LatestWarningDateKey
	  ,ISNULL(dda.DateKey, 19000101) AS ApplicationDateKey

FROM 
	MysteryDiningCopy.dbo.[Diner] d
LEFT  JOIN MysteryDiningDW.dbo.DimDate ddl ON CONVERT(DATETIME,CONVERT(DATE,d.LastLogIn)) = ddl.[Date]
LEFT  JOIN MysteryDiningDW.dbo.DimDate dda ON CONVERT(DATETIME,CONVERT(DATE,d.ApplicationDate)) = dda.[Date]
LEFT  JOIN
	(
	SELECT
		DinerUID
	,	COUNT(*) AS WarningCount
	,	MAX(LogDate) AS MostRecent
	FROM
		MysteryDiningCopy.dbo.DinerLog
	WHERE
		Activity = 'Warning'
	GROUP BY
		DinerUID
	) TotalWarnings ON d.DinerUID = TotalWarnings.DinerUID
LEFT  JOIN
	(
	SELECT
		DinerUID
	,	COUNT(*)	 AS WarningCount
	FROM
		MysteryDiningCopy.dbo.DinerLog
	WHERE
		Activity = 'Warning'
	AND
		LogDate > DATEADD(YEAR,-1,GETDATE())
	GROUP BY
		DinerUID
	) RecentWarnings ON d.DinerUID = RecentWarnings.DinerUID
LEFT  JOIN MysteryDiningDW.dbo.DimDate ddlw ON CONVERT(DATETIME,CONVERT(DATE,TotalWarnings.MostRecent)) = ddlw.[Date]


-- Test Case
-- SELECT * FROM vDimAssessorJoined

GO
