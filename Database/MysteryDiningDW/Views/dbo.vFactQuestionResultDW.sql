
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vFactQuestionResultDW]
AS
SELECT TOP 100 PERCENT
	[QuestionResultKey]
      ,[QuestionKey]
      ,[AssessorKey]
      ,[Questionnaire]
      ,[VisitKey]
      ,[AssessmentArea]
      ,[BranchKey]
      ,[GeographyKey]
      ,[PeriodKey]
      ,[VisitDateKey]
      ,[QuestionSection]
      ,[SectionNumber]
      ,[ManagementCategory]
      ,[Benchmark]
      ,[QuestionDepartment]
      ,[QuestionNumber]
      ,[AnswerText]
      ,[Score]
      ,[MaxScore]
      ,[AnsweredDateKey]
      ,[CheckedByAdminKey]
      ,[Comments]
      ,[Source]
      ,[TableSource]
      ,[FeedbackType]
      ,[IncludeInAnalysis]
      ,[ValidFromDate]
      ,[ValidToDate]
      ,[Active]
      ,[QQID]
      ,[QuestionResultID]
      ,[SubQuestionnaire]
      ,[Factor]
      ,[TimeStamp]
      ,[DishKey]
      ,[VisitID]
      ,[AnswerValue]
      ,[AnswerMaxValue]
	  ,[IncludeScoreInTotal]
     
FROM
	FactQuestionResult
	INNER JOIN			dbo.StagingLegacyVisitIDs AS slv			ON FactQuestionResult.VisitID = slv.VisitUID AND slv.ConfigInfoID = 4
WHERE
	[Source] = 'Legacy'
AND [Active] = 1

ORDER BY
	QuestionResultID
	
-- Test
-- SELECT * FROM vFactQuestionResultDW



GO
