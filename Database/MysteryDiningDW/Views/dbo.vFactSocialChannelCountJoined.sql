SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vFactSocialChannelCountJoined]
AS
SELECT
	dsc.SocialChannelKey
,	ddc.DateKey								AS CountedDateKey
,	scauc.DateTimeCaptured					AS DateTimeCounted
,	scauc.UserCount							AS ChannelCount
,	scauc.TalkingAboutCount					AS TalkingAboutCount
,	CONVERT(NVARCHAR(10), 'Legacy')			AS [Source]
,	scauc.SocialChannelAssignedUserCountUID AS SocialChannelCountID
FROM
	[MysteryDiningCopy].dbo.SocialChannelAssignedUserCount scauc
	INNER JOIN [MysteryDiningCopy].dbo.SocialChannelAssigned sca ON scauc.SocialChannelAssignedUID	= sca.SocialChannelAssignedUID
	INNER JOIN [MysteryDiningCopy].dbo.SocialAdvocacyChannel sac ON sca.SocialAdvocacyChannelUID	= sac.SocialAdvocacyChannelUID

	LEFT  JOIN MysteryDiningDW.dbo.DimSocialChannel dsc ON sca.ExternalID  = dsc.SocialChannelID AND dsc.Active = 1
													   AND sac.ChannelName = dsc.SocialNetwork
    LEFT  JOIN MysteryDiningDW.dbo.DimDate ddc ON CONVERT(DATETIME,CONVERT(DATE,scauc.DateTimeCaptured)) = ddc.[Date]
WHERE
	scauc.DateTimeCaptured > 
		(
			SELECT
				LastUpdated
			FROM
				MysteryDiningDW.dbo.ConfigInfo
			WHERE
				[Key] = 'FactSocialChannelCount'
		)

-- Test Case
-- SELECT * FROM vFactSocialChannelCountJoined
GO
