
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vFactSectionResultDW]
AS
SELECT TOP 100 PERCENT
	[SectionResultKey]
      ,[VisitKey]
      ,[BranchKey]
      ,[GeographyKey]
      ,[PeriodKey]
      ,[VisitDateKey]
      ,[AssessorKey]
      ,[Questionnaire]
      ,[SubQuestionnaire]
      ,[SectionTitle]
      ,[SectionNumber]
      ,[Score]
      ,[MaxScore]
      ,[IncludeInAnalysis]
      ,[ValidFromDate]
      ,[ValidToDate]
      ,[Active]
      ,[Source]
      ,[SectionID]
      ,[VisitID]
      ,[TimeStamp]
      ,[SectionDepartment]
      ,[MenuSection]
      ,[HiddenSection]
	  ,[IncludeScoreInTotal]
FROM 
	FactSectionResult
	INNER JOIN MysteryDiningDW.dbo.StagingLegacyVisitIDs slv ON FactSectionResult.VisitID		= slv.VisitUID AND slv.ConfigInfoID = 6
WHERE
	[Source] = 'Legacy'
AND [Active] = 1

ORDER BY
	VisitID
	
-- Test Case
-- SELECT * FROM vFactSectionResultDW


GO
