SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].vFactFeedbackResultDW
AS
SELECT TOP 100 PERCENT
	   [FeedbackResultKey]
      ,[VisitKey]
      ,[BranchKey]
      ,[GeographyKey]
      ,[PeriodKey]
      ,[VisitDateKey]
      ,[FeedbackKey]
      ,[Questionnaire]
      ,[SubQuestionnaire]
      ,[Score]
      ,[MaxScore]
      ,[ManagerComments]
      ,[IncludeInAnalysis]
      ,[TimeStarted]
      ,[TimeEnded]
      ,[IPAddress]
      ,[CookieGUID]
      ,[CompletedDateKey]
      ,[DiscardReason]
      ,[DiscardValue]
      ,[ValidFromDate]
      ,[ValidToDate]
      ,[Active]
      ,[Source]
      ,[FeedbackResultID]
      ,[TimeStamp]
      ,[ChangeReason]
	  ,[FeedbackType]
	  ,[OfferedAdvocacy]
      ,[AcceptedAdvocacy]
      ,[PostedAdvocacy]
	  ,[SubscribeAdvocacy]
	  ,[GuestMarketingKey]
FROM 
	[FactFeedbackResult]
WHERE
	[Source] = 'Legacy'
AND [Active] = 1
AND	[FeedbackResultID] IN (SELECT DISTINCT VisitUID FROM MysteryDiningCopy.dbo.VisitUpdate WHERE [TimeStamp] >
					(SELECT [LastUpdated] 
					FROM MysteryDiningDW.dbo.ConfigInfo
					WHERE ConfigInfoID = 11)
								AND	[TimeStamp] > '2005-01-01'
								AND [TimeStamp] IS NOT NULL)
ORDER BY
	FeedbackResultID

-- Test Case
-- SELECT * FROM vFactFeedbackResultDW
GO
