SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vFactFeedbackResultJoined]
AS
SELECT
	V.VisitUID																			AS FeedbackResultID
,	CONVERT(DECIMAL(18,2),ISNULL(bvs.Score, 0))											AS Score
,	CONVERT(DECIMAL(18,2),ISNULL(bvs.MaximumScore, 0))									AS MaxScore
,	CONVERT(NVARCHAR(3000),V.AdditionalComments)										AS ManagerComments
,	CONVERT(NVARCHAR(10),'Legacy')			AS [Source]
,	dV.VisitKey
,	dV.[Type]								AS FeedbackType
,	dV.VisitDateKey
,	ISNULL(dA.AdminKey, 0)					AS AdminKey
,	dV.VisitStatus
,	dV.PeriodKey							AS PeriodKey
,	dV.BranchKey							AS BranchKey
,	ISNULL(df.FeedbackKey, 0)				AS FeedbackKey
,	dV.Questionnaire
,	dV.SubQuestionnaire
,	cast(IIF(dP.IncludeInAnalysis = 'Exclude' OR qc.ExcludeFromAnalysis = 1, 'Exclude','Include') as nvarchar(10)) as IncludeInAnalysis
,	dB.GeographyKey
,	df.StartTime							AS TimeStarted
,	df.EndTime								AS TimeEnded
,	df.IPAddress
,	df.CookieGUID
,	df.DiscardReason
,	df.DiscardValue
,	df.CompletedDateKey
,	CONVERT(NVARCHAR(10),
	CASE
		WHEN ofqm.SocialAdvocacyEnabled = 0 THEN 'N/A'
		WHEN ofd.SocialAdvocacyOffered = 1	THEN 'Yes'
		ELSE 'No'
	END)									AS OfferedAdvocacy
,	CONVERT(NVARCHAR(10),
	CASE
		WHEN ofqm.SocialAdvocacyEnabled = 0 THEN 'N/A'
		WHEN ofd.SocialAdvocacyShareIntended = 1 
			AND ofd.SocialAdvocacyLanded =1	THEN 'Yes'
		ELSE 'No'
	END)									AS AcceptedAdvocacy
,	CONVERT(NVARCHAR(10),
	CASE
		WHEN ofqm.SocialAdvocacyEnabled = 0 THEN 'N/A'
		WHEN ofd.DateTimeCompleted < '2012-07-05' THEN 'N/A'
		ELSE ISNULL(Posted.PostedAdvocacy, 'No')
	END)									AS PostedAdvocacy
,	CONVERT(NVARCHAR(10),
	CASE
		WHEN ofqm.SocialAdvocacyEnabled = 0 THEN 'N/A'
		WHEN ofd.DateTimeCompleted < '2012-07-05' THEN 'N/A'
		ELSE ISNULL(Subscribe.SubscribeAdvocacy, 'No')
	END)									AS SubscribeAdvocacy
,	ISNULL(dgm.GuestMarketingKey, 0)		AS GuestMarketingKey
FROM
	MysteryDiningCopy.dbo.Visit V
	INNER JOIN MysteryDiningCopy.dbo.BatchedVisitScores bvs ON V.VisitUID = bvs.VisitUID
	INNER JOIN MysteryDiningCopy.dbo.Questionnaire q ON V.QuestionnaireUID = q.QuestionnaireUID AND q.VisitType IN ('OF', 'MF', 'CC')
	LEFT  JOIN MysteryDiningCopy.dbo.OnlineFeedbackDetails ofd ON V.VisitUID = ofd.VisitUID
	LEFT  JOIN MysteryDiningDW.dbo.DimVisit dV    ON V.VisitUID = dV.VisitID AND dV.[Source] = 'Legacy' AND dV.Active = 1
    LEFT  JOIN MysteryDiningDW.dbo.DimPeriod dP   ON dV.PeriodKey = dP.PeriodKey
    LEFT  JOIN MysteryDiningDW.dbo.DimBranch dB   ON dV.BranchKey = dB.BranchKey
    LEFT  JOIN MysteryDiningDW.dbo.DimAdmin dA    ON dV.AdminOwner = dA.AdminName AND dA.[Source] = 'Legacy'
	INNER JOIN MysteryDiningCopy.dbo.Questionnaire qn on v.QuestionnaireUID		= qn.QuestionnaireUID
	LEFT JOIN MysteryDiningCopy.dbo.QuestionnaireCategories qc on qn.CategoryUID = qc.CategoryUID
	LEFT JOIN MysteryDiningDW.dbo.DimFeedback df ON 
													(
													CASE
														WHEN q.VisitType = 'CC' THEN 0
														ELSE v.VisitUID
													END
													) = df.FeedbackID
	INNER JOIN MysteryDiningCopy.dbo.OnlineFeedbackQuestionnaireMeta ofqm ON ofd.OnlineFeedbackQuestionnaireMetaUID = ofqm.OnlineFeedbackQuestionnaireMetaUID
	LEFT JOIN
		(
			SELECT
				ofd.VisitUID
			,	CombinedPostCount.PostedAdvocacy
			FROM
				MysteryDiningCopy.dbo.OnlineFeedbackDetails ofd
			INNER JOIN MysteryDiningCopy.dbo.OnlineFeedbackRecommendation ofr ON ofd.OnlineFeedbackDetailsUID = ofr.OnlineFeedbackDetailsUID
			INNER JOIN				
				(
				SELECT
					RecommendationUID
				,	CASE 
						WHEN SUM(PostCount) > 0 THEN 'Yes'
						ELSE 'No' -- It will never be No because there won't be any records!
					END	AS PostedAdvocacy
				FROM
					(
						SELECT 
							rf.OnlineFeedbackRecommendationUID				  AS RecommendationUID
						,	COUNT(rf.OnlineFeedbackRecommendationFacebookUID) AS PostCount
						FROM
							MysteryDiningCopy.dbo.OnlineFeedbackRecommendationFacebook rf
						WHERE
							rf.IsPrimary = 1
						AND
							rf.fbPosted = 1
						GROUP BY
							rf.OnlineFeedbackRecommendationUID
			
						UNION

						SELECT 
							rt.OnlineFeedbackRecommendationUID				 AS RecommendationUID
						,	COUNT(rt.OnlineFeedbackRecommendationTwitterUID) AS PostCount
						FROM
							MysteryDiningCopy.dbo.OnlineFeedbackRecommendationTwitter rt
						WHERE
							rt.IsPrimary = 1
						AND
							rt.twTweeted = 1
						GROUP BY
							rt.OnlineFeedbackRecommendationUID
		
						UNION

						SELECT 
							re.OnlineFeedbackRecommendationUID			   AS RecommendationUID
						,	COUNT(re.OnlineFeedbackRecommendationEmailUID) AS PostCount
						FROM
							MysteryDiningCopy.dbo.OnlineFeedbackRecommendationEmail re
						WHERE
							re.IsPrimary = 1
						GROUP BY
							re.OnlineFeedbackRecommendationUID

						UNION

						SELECT 
							rta.OnlineFeedbackRecommendationUID
						,	count(rta.OnlineFeedbackRecommendationTripAdvisorUID) as PostCount

						 FROM MysteryDiningCopy.dbo.OnlineFeedbackRecommendationTripAdvisor rta

						 where 
							rta.IsPrimary=1 
						AND
							rta.taReviewPosted=1
						AND
							rta.TripAdvisorReviewVerificationStatusUID IN ('4','5')

						GROUP BY rta.OnlineFeedbackRecommendationUID
					) Posts
				GROUP BY
					RecommendationUID
				) CombinedPostCount ON ofr.OnlineFeedbackRecommendationUID = CombinedPostCount.RecommendationUID	
		) Posted ON V.VisitUID = Posted.VisitUID

	LEFT JOIN
		(
			SELECT
				ofd.VisitUID
			,	CombinedPostCount.SubscribeAdvocacy
			FROM
				MysteryDiningCopy.dbo.OnlineFeedbackDetails ofd
			INNER JOIN MysteryDiningCopy.dbo.OnlineFeedbackRecommendation ofr ON ofd.OnlineFeedbackDetailsUID = ofr.OnlineFeedbackDetailsUID
			INNER JOIN				
				(
				SELECT
					RecommendationUID
				,	CASE 
						WHEN SUM(SubscribeCount) > 0 THEN 'Yes'
						ELSE 'No'
					END	AS SubscribeAdvocacy
				FROM
					(
						SELECT 
							rf.OnlineFeedbackRecommendationUID				  AS RecommendationUID
						,	COUNT(rf.OnlineFeedbackRecommendationFacebookUID) AS SubscribeCount
						FROM
							MysteryDiningCopy.dbo.OnlineFeedbackRecommendationFacebook rf
						WHERE
							rf.IsPrimary = 1
						AND
							rf.fbConvertedBrandSubscriber = 1
						GROUP BY
							rf.OnlineFeedbackRecommendationUID
			
						UNION

						SELECT 
							rt.OnlineFeedbackRecommendationUID				 AS RecommendationUID
						,	COUNT(rt.OnlineFeedbackRecommendationTwitterUID) AS SubscribeCount
						FROM
							MysteryDiningCopy.dbo.OnlineFeedbackRecommendationTwitter rt
						WHERE
							rt.IsPrimary = 1
						AND
							rt.twConvertedFollower = 1
						GROUP BY
							rt.OnlineFeedbackRecommendationUID

					) Posts
				GROUP BY
					RecommendationUID
				) CombinedPostCount ON ofr.OnlineFeedbackRecommendationUID = CombinedPostCount.RecommendationUID	
		) Subscribe ON V.VisitUID = Subscribe.VisitUID

	LEFT  JOIN MysteryDiningDW.dbo.DimGuestMarketing dgm ON V.VisitUID = dgm.GuestMarketingID 
														 AND dgm.GuestMarketingDetailID = 0

    INNER JOIN MysteryDiningDW.dbo.StagingLegacyVisitIDs slv ON v.VisitUID		= slv.VisitUID AND slv.ConfigInfoID = 11

WHERE
	v.VisitDate < GETDATE()




GO
