SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vDimGeography-DinerJoined]
AS
SELECT [DinerUID]
      ,[Address1]
      ,[Address2]
      ,[Address3]
      ,[Address4]
      ,[Address5]
      ,[Postcode]
	  ,CONVERT(NVARCHAR(50), 'Diner')	AS TableSource
	  ,CONVERT(NVARCHAR(50), 'Legacy')	AS [Source]
FROM MysteryDiningCopy.dbo.[Diner]
GO
