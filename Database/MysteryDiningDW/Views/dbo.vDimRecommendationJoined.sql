SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vDimRecommendationJoined]
AS
SELECT

	ofr.RecommendationText				AS Recommendation
,	ISNULL(RV.ViewCount, 0)				AS CountOfViews
,	CONVERT(NVARCHAR(10),
	CASE
		WHEN RF.PostCount > 0 THEN 'Yes'
		ELSE 'No'
	END)								AS PostedToFacebook
,	CONVERT(NVARCHAR(10),
	CASE
		WHEN RT.PostCount > 0 THEN 'Yes'
		ELSE 'No'
	END)								AS PostedToTwitter
,	CONVERT(NVARCHAR(10),	
	CASE
		WHEN RE.EmailCount > 0 THEN 'Yes'
		ELSE 'No'
	END)								AS Emailed
,	ofrs.AnalysisName					AS RecommendationStatus
,	CONVERT(NVARCHAR(10), 'Legacy')		AS [Source]
,	ofr.OnlineFeedbackRecommendationUID AS RecommendationID

FROM
	MysteryDiningCopy.dbo.OnlineFeedbackRecommendation ofr
	INNER JOIN MysteryDiningCopy.dbo.OnlineFeedbackRecommendationStatus ofrs ON ofr.StatusUID = ofrs.OnlineFeedbackRecommendationStatusUID
	LEFT JOIN
		(
			SELECT 
				rv.OnlineFeedbackRecommendationUID
			,	COUNT(rv.OnlineFeedbackRecommendationReturnVisitorUID) AS ViewCount
			FROM
				MysteryDiningCopy.dbo.OnlineFeedbackRecommendationReturnVisitor rv
			--WHERE
				-- rv.OnlineFeedbackRecommendationUID IN !!!
			GROUP BY
				rv.OnlineFeedbackRecommendationUID
		) RV ON ofr.OnlineFeedbackRecommendationUID = RV.OnlineFeedbackRecommendationUID
	LEFT JOIN
		(
			SELECT 
				rf.OnlineFeedbackRecommendationUID
			,	COUNT(rf.OnlineFeedbackRecommendationFacebookUID) AS PostCount
			FROM
				MysteryDiningCopy.dbo.OnlineFeedbackRecommendationFacebook rf
			WHERE
				rf.IsPrimary = 1
			--AND
				-- rv.OnlineFeedbackRecommendationUID IN !!!
			GROUP BY
				rf.OnlineFeedbackRecommendationUID
		) RF ON ofr.OnlineFeedbackRecommendationUID = RF.OnlineFeedbackRecommendationUID
	LEFT JOIN
		(
			SELECT 
				rt.OnlineFeedbackRecommendationUID
			,	COUNT(rt.OnlineFeedbackRecommendationTwitterUID) AS PostCount
			FROM
				MysteryDiningCopy.dbo.OnlineFeedbackRecommendationTwitter rt
			WHERE
				rt.IsPrimary = 1
			--AND
				-- rv.OnlineFeedbackRecommendationUID IN !!!
			GROUP BY
				rt.OnlineFeedbackRecommendationUID
		) RT ON ofr.OnlineFeedbackRecommendationUID = RT.OnlineFeedbackRecommendationUID
	LEFT JOIN
		(
			SELECT 
				re.OnlineFeedbackRecommendationUID
			,	COUNT(re.OnlineFeedbackRecommendationEmailUID) AS EmailCount
			FROM
				MysteryDiningCopy.dbo.OnlineFeedbackRecommendationEmail re
			WHERE
				re.IsPrimary = 1
			--AND
				-- rv.OnlineFeedbackRecommendationUID IN !!!
			GROUP BY
				re.OnlineFeedbackRecommendationUID
		) RE ON ofr.OnlineFeedbackRecommendationUID = RE.OnlineFeedbackRecommendationUID

-- Runs quickly on initial version so didn't restrict by recommendation, if this slows down we should probably restrict it to the past 6 - 12 months


GO
