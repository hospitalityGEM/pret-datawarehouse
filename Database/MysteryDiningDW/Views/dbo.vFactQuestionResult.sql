SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vFactQuestionResult]
AS
SELECT TOP 100 PERCENT
	a.AnswerUID
,	a.VisitUID
,	a.QuestionUID
,	a.Answer
,	a.Score
,	a.[Weight]
,	a.Comments
,	a.[TimeStamp]
,	Substring(f.Factor,1,1) + Lower(Substring(f.Factor,2,Len(f.Factor))) AS Factor
FROM         
	MysteryDiningCopy.dbo.Answer a
	INNER JOIN
      (
      SELECT
        (CASE WHEN (Substring(q.Question,1, CHARINDEX(' ', q.Question))) IN ('WELCOME','RECOMMEND','PACE','INTEREST','CONFIDENCE','KNOWLEDGE','VALUE','WARMTH','TRAINING ')
        THEN (Substring(q.Question,1, CHARINDEX(' ', q.Question)))
        ELSE 'NONE'
        END) AS Factor
       ,	q.QuestionUID
		FROM
        MysteryDiningCopy.dbo.Question q
        INNER JOIN MysteryDiningCopy.dbo.Section s ON q.SectionUID = s.SectionUID
       ) f ON a.QuestionUID = f.QuestionUID
    INNER JOIN MysteryDiningCopy.dbo.Visit v ON a.VisitUID = v.VisitUID
WHERE
	v.VisitDate > (SELECT [LastUpdated] 
					FROM MysteryDiningDW.dbo.ConfigInfo
					WHERE [Key] = 'FactQuestionResultLatestRecord')
	AND a.[TimeStamp] IS NOT NULL
	AND v.VisitDate < '2020-01-01'
ORDER BY
	a.AnswerUID


GO
