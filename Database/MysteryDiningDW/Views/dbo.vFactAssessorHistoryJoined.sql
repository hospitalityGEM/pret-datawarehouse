SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vFactAssessorHistoryJoined]
AS

SELECT
	dl.Activity								AS Activity
,	dl.[Status]								AS [Status]
,	CASE WHEN LEN(CONVERT(NVARCHAR,dl.Comments))	   = 0	THEN 'No Comments' ELSE CONVERT(NVARCHAR(200),dl.Comments)		END	AS SystemComments
,	CASE WHEN LEN(CONVERT(NVARCHAR,dl.DinerComments))  = 0	THEN 'No Comments' ELSE CONVERT(NVARCHAR(200),dl.DinerComments)	END	AS AssessorComments
,	CASE WHEN LEN(CONVERT(NVARCHAR,dl.AdminComments))  = 0	THEN 'No Comments' ELSE CONVERT(NVARCHAR(200),dl.AdminComments)	END	AS AdminComments
,	ISNULL(dV.VisitKey, 0)					AS VisitKey
,	dl.Score								AS Points
,	CONVERT(NVARCHAR(10),'Legacy')			AS [Source]
,	ddl.DateKey								AS LogDateKey
,	ISNULL(dA.AdminKey, 0)					AS AdminKey
,	dl.DinerUID								AS AssessorID
,	dl.DinerLogUID							AS AssessorHistoryID
,	CONVERT(NVARCHAR(10),'New Row')			AS ChangeReason
FROM
	MysteryDiningCopy.dbo.DinerLog dl
	
    LEFT  JOIN
		(
			SELECT DISTINCT TOP 100 PERCENT
				MAX(AdminKey) AS AdminKey
			,	SUBSTRING(dA.AdminName,0,10) AS AdminName
			FROM
				MysteryDiningDW.dbo.DimAdmin dA
			WHERE
				[Source] = 'Legacy'
			GROUP BY
				SUBSTRING(dA.AdminName,0,10)
			ORDER BY
				MAX(AdminKey) DESC
		) dA  ON 
		(CASE 
			WHEN dl.[Owner] = 'System'	THEN 'THE SYSTE'
			WHEN dl.[Owner] = '99'		THEN 'THE SYSTE'
			ELSE dl.[Owner]
		END) = dA.AdminName
    LEFT  JOIN MysteryDiningDW.dbo.DimDate ddl ON CONVERT(DATETIME,CONVERT(DATE,dl.LogDate)) = ddl.[Date]
    LEFT  JOIN MysteryDiningDW.dbo.DimVisit dV ON dl.VisitUID = dV.VisitID AND dV.Active = 1

WHERE
	dl.DinerLogUID > 
	(
		SELECT 
			ci.Value
		FROM
			MysteryDiningDW.dbo.ConfigInfo ci
		WHERE	
			ci.ConfigInfoID = 21
	)


GO
