SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vDimQuestion]
AS
SELECT [QuestionUID]
      ,[SectionUID]
      ,[Question]
      ,[QuestCatagory]
      ,[MultiAnsType]
      ,[BenchmarkUID]
      ,[TrackingUID]
      ,AnalysisTag
FROM MysteryDiningCopy.dbo.[Question]
GO
