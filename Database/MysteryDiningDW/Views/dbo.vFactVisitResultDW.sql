
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vFactVisitResultDW]
AS
SELECT TOP 100 PERCENT
	[VisitResultKey]
      ,[VisitKey]
      ,[BranchKey]
      ,[GeographyKey]
      ,[PeriodKey]
      ,[VisitDateKey]
      ,[AssessorKey]
      ,[Questionnaire]
      ,[SubQuestionnaire]
      ,[Status]
      ,[AdminKey]
      ,[Score]
      ,[MaxScore]
      ,[ScoreText]
      ,[ScoreMethod]
      ,[Comment]
      ,[IncludeInAnalysis]
      ,[ValidFromDate]
      ,[ValidToDate]
      ,[Active]
      ,[Source]
      ,[VisitID]
      ,[TimeStamp]
	  ,[AdjustedScore]
FROM 
	FactVisitResult
	INNER JOIN		MysteryDiningDW.dbo.StagingLegacyVisitIDs slv		ON FactVisitResult.VisitID = slv.VisitUID AND slv.ConfigInfoID = 7 
WHERE
	[Source] = 'Legacy'
AND [Active] = 1

ORDER BY
	VisitID

GO
