SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vDimFeedbackJoined]
AS
SELECT
	ofd.VisitUID							AS FeedbackID
,	CONVERT(NVARCHAR(10),'Legacy')			AS [Source]
,	ofd.DateTimeStarted						AS TimeStarted
,	ofd.DateTimeCompleted					AS TimeEnded
,	DATEDIFF(SECOND, ofd.DateTimeStarted, ofd.DateTimeCompleted)	AS ElapsedTime
,	CONVERT(NVARCHAR(30),ofd.IPAddress)		AS IPAddress
,	CONVERT(NVARCHAR(50),ofd.CookieGUID)	AS CookieGUID
,	CONVERT(NVARCHAR(50),
	CASE
		WHEN SUBSTRING(ofd.DiscardReason, 1, 30)  = 'Feedback completed too quickly'
			THEN 'Too Fast'
		WHEN SUBSTRING(ofd.DiscardReason, 1, 54)  = 'Feedback completed too many times in the Client Period'
			THEN 'Too Many in Period'
		WHEN SUBSTRING(ofd.DiscardReason, 1, 51)  = 'Feedback completed too many times in the last 24hrs'
			THEN 'Too Many in 24h'
		WHEN SUBSTRING(ofd.DiscardReason, 1, 18)  = 'Invalid Visit Data'
			THEN 'Invalid Visit Data'
		WHEN LEN(ofd.DiscardReason) = 0
			THEN 'Valid'
		ELSE 'Unknown'
	END)									AS DiscardReason
,	CASE 
		WHEN SUBSTRING(ofd.DiscardReason, 1, 30)  = 'Feedback completed too quickly'
			THEN CONVERT(INT, SUBSTRING(ofd.DiscardReason, 33, LEN(ofd.DiscardReason) - 41))
		WHEN SUBSTRING(ofd.DiscardReason, 1, 54)  = 'Feedback completed too many times in the Client Period'
			THEN CONVERT(INT, SUBSTRING(ofd.DiscardReason, 57, LEN(ofd.DiscardReason) - 57))
		WHEN SUBSTRING(ofd.DiscardReason, 1, 51)  = 'Feedback completed too many times in the last 24hrs'
			THEN CONVERT(INT, SUBSTRING(ofd.DiscardReason, 54, LEN(ofd.DiscardReason) - 54))
		ELSE NULL
	END									AS DiscardValue
,	ISNULL(ddc.DateKey, 19000101)			AS CompletedDateKey
,	ofd.OnlineFeedbackDetailsUID			AS OFDID

FROM
	MysteryDiningCopy.dbo.OnlineFeedbackDetails ofd
	LEFT  JOIN MysteryDiningDW.dbo.DimDate ddc ON CONVERT(DATETIME,CONVERT(DATE,ofd.DateTimeCompleted)) = ddc.[Date]


GO
