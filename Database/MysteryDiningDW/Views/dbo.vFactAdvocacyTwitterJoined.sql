SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vFactAdvocacyTwitterJoined]
AS
SELECT
	dv.CurrentScore								AS VisitScore
,	dv.CurrentMaxScore							AS VisitMaxScore
,	df.CompletedDateKey							AS CompletionDateKey
,	dv.VisitDateKey								AS VisitDateKey
,	dv.BranchKey								AS BranchKey
,	dv.PeriodKey								AS PeriodKey
,	ISNULL(dr.RecommendationKey, 0)				AS RecommendationKey
,	CONVERT(NVARCHAR(10),
	CASE ofrt.twExistingBrandFollower
		WHEN 1 THEN 'Yes'
		WHEN 0 THEN 'No'
		ELSE 'N/A'
	END)										AS FollowingBefore
,	CONVERT(NVARCHAR(10),
	CASE 
		WHEN ofrt.twConvertedFollower = 1 THEN 'Yes'
		WHEN ofrt.twExistingBrandFollower = 1 THEN 'Yes'
		WHEN ofrt.twConvertedFollower = 0 AND ofrt.twExistingBrandFollower = 0 THEN 'No'
		ELSE 'N/A'
	END)										AS FollowingAfter
,	CONVERT(NVARCHAR(10),
	CASE 
		WHEN ofrt.twConvertedFollower = 1 THEN 'Yes'
		WHEN ofrt.twConvertedFollower = 0 THEN 'No'
		ELSE 'N/A'
	END)										AS ConvertedToFollower
,	CONVERT(NVARCHAR(10),
	CASE
		WHEN ofrt.twScreenName IS NULL THEN 'N/A'
		ELSE ofrt.twScreenName
	END)										AS UserAccount
,	CASE
		WHEN LEN(ofrt.twTweet) < 1 THEN 'N/A'
		ELSE ofrt.twTweet
	END											AS Tweet
,	ofrt.twFollowerCount						AS Followers
,	ofrt.twClientScreenName						AS FollowedAccount
,	CONVERT(NVARCHAR(10),
	CASE ofrt.twTweeted
		WHEN 1 THEN 'Yes'
		WHEN 0 THEN 'No'
		ELSE 'N/A'
	END)										AS Tweeted
,	CONVERT(NVARCHAR(10),
	CASE ofrt.IsPrimary
		WHEN 1 THEN 'Advocate'
		WHEN 0 THEN 'Friend'
		ELSE 'Unknown'
	END)										AS Advocate
,	ofrs.AnalysisName							AS RecommendationStatus
,	ISNULL(dgm.GuestMarketingKey, 0)			AS GuestMarketingKey
,	ISNULL(dsc.SocialChannelKey,0)				AS SocialChannelKey
,	CONVERT(NVARCHAR(10), 'Legacy')				AS [Source]		
,	ofrt.OnlineFeedbackRecommendationTwitterUID	AS AdvocacyTwitterID
FROM
	MysteryDiningCopy.dbo.OnlineFeedbackRecommendationTwitter ofrt
	INNER JOIN MysteryDiningCopy.dbo.OnlineFeedbackRecommendation ofr ON ofrt.OnlineFeedbackRecommendationUID = ofr.OnlineFeedbackRecommendationUID
	INNER JOIN MysteryDiningCopy.dbo.OnlineFeedbackDetails ofd ON ofr.OnlineFeedbackDetailsUID = ofd.OnlineFeedbackDetailsUID
	INNER JOIN MysteryDiningCopy.dbo.OnlineFeedbackRecommendationStatus ofrs ON ofr.StatusUID = ofrs.OnlineFeedbackRecommendationStatusUID
	
	LEFT JOIN MysteryDiningDW.dbo.DimSocialChannel dsc ON ofrt.twClientScreenName = dsc.SocialChannelID AND dsc.Active = 1 AND dsc.SocialNetwork = 'Twitter'
	LEFT JOIN MysteryDiningDW.dbo.DimRecommendation dr ON ofr.OnlineFeedbackRecommendationUID = dr.RecommendationID AND dr.Active = 1
	LEFT JOIN MysteryDiningDW.dbo.DimVisit dv ON ofd.VisitUID = dv.VisitID AND dv.Active = 1
	LEFT JOIN MysteryDiningDW.dbo.DimFeedback df ON ofd.VisitUID = df.FeedbackID
	LEFT JOIN MysteryDiningDW.dbo.DimGuestMarketing dgm ON ofd.VisitUID = dgm.GuestMarketingID 
														 AND ofrt.OnlineFeedbackRecommendationReturnVisitorUID = dgm.GuestMarketingDetailID


	INNER JOIN MysteryDiningDW.dbo.ConfigInfo ci ON ci.ConfigInfoID = 26 AND ci.LastUpdated < ofrt.[DateTimeAdded]

-- Test Case
-- SELECT * FROM vFactAdvocacyTwitterJoined
GO
