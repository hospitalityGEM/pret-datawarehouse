SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vFactRecommendationViewJoined]
AS
SELECT
	CAST(ofrv.IPAddress	AS NVARCHAR(30))		AS IPAddress
,	ofrv.CookieGUID								AS CookieGUID
,	ddvd.DateKey								AS ViewDateKey
,	ofrv.DateTimeAdded							AS ViewTimestamp
,	ISNULL(dr.RecommendationKey, 0)				AS RecommendationKey
,	sac.ChannelName								AS GuestSource
,	dv.VisitKey									AS VisitKey
,	ISNULL(dgm.GuestMarketingKey, 0)			AS GuestMarketingKey
,	df.FeedbackKey								AS FeedbackKey
,	dv.BranchKey								AS BranchKey
,	dv.PeriodKey								AS PeriodKey
,	CONVERT(NVARCHAR(10),
	CASE ofrf.fbConvertedBrandSubscriber
		WHEN 1 THEN 'Yes'
		WHEN 0 THEN 'No'
		ELSE 'N/A'
	END)										AS FacebookLiked
,	ISNULL(dscf.SocialChannelKey, 0)			AS FacebookSocialChannelKey
,	CONVERT(NVARCHAR(10),
	CASE ofrt.twConvertedFollower
		WHEN 1 THEN 'Yes'
		WHEN 0 THEN 'No'
		ELSE 'N/A'
	END)										AS TwitterFollowed
,	ISNULL(dsct.SocialChannelKey, 0)			AS TwitterSocialChannelKey
,	ofrs.AnalysisName							AS RecommendationStatus
,	CONVERT(NVARCHAR(10), 'Legacy')				AS [Source]		
,	ofrv.OnlineFeedbackRecommendationReturnVisitorUID	AS RecommendationViewID
FROM
	[MysteryDiningCopy].dbo.OnlineFeedbackRecommendationReturnVisitor ofrv
	INNER JOIN [MysteryDiningCopy].dbo.OnlineFeedbackRecommendation ofr ON ofrv.OnlineFeedbackRecommendationUID = ofr.OnlineFeedbackRecommendationUID
	INNER JOIN [MysteryDiningCopy].dbo.OnlineFeedbackDetails ofd ON ofr.OnlineFeedbackDetailsUID = ofd.OnlineFeedbackDetailsUID
	INNER JOIN [MysteryDiningCopy].dbo.OnlineFeedbackRecommendationStatus ofrs ON ofr.StatusUID = ofrs.OnlineFeedbackRecommendationStatusUID
	LEFT JOIN [MysteryDiningCopy].dbo.OnlineFeedbackRecommendationFacebook ofrf ON ofrv.OnlineFeedbackRecommendationReturnVisitorUID = ofrf.OnlineFeedbackRecommendationReturnVisitorUID
	LEFT JOIN [MysteryDiningCopy].dbo.OnlineFeedbackRecommendationTwitter ofrt ON ofrv.OnlineFeedbackRecommendationReturnVisitorUID = ofrt.OnlineFeedbackRecommendationReturnVisitorUID
	INNER JOIN [MysteryDiningCopy].dbo.SocialAdvocacyChannel sac ON ofrv.ViaSocialAdvocacyChannelUID = sac.SocialAdvocacyChannelUID

	LEFT  JOIN MysteryDiningDW.dbo.DimDate ddvd ON CONVERT(DATETIME,CONVERT(DATE,ofrv.DateTimeAdded)) = ddvd.[Date]
	LEFT  JOIN MysteryDiningDW.dbo.DimRecommendation dr ON ofrv.OnlineFeedbackRecommendationUID = dr.RecommendationID AND dr.Active = 1
	INNER JOIN MysteryDiningDW.dbo.DimVisit dv ON ofd.VisitUID = dv.VisitID AND dv.Active = 1
	INNER JOIN MysteryDiningDW.dbo.DimFeedback df ON ofd.VisitUID = df.FeedbackID
	LEFT  JOIN MysteryDiningDW.dbo.DimSocialChannel dsct ON ofrt.twClientScreenName = dsct.SocialChannelID
	LEFT  JOIN MysteryDiningDW.dbo.DimSocialChannel dscf ON ofrf.fbClientId = dscf.SocialChannelID
	LEFT  JOIN MysteryDiningDW.dbo.DimGuestMarketing dgm ON ofd.VisitUID = dgm.GuestMarketingID 
														 AND ofrv.OnlineFeedbackRecommendationReturnVisitorUID = dgm.GuestMarketingDetailID

	INNER JOIN MysteryDiningDW.dbo.ConfigInfo ci ON ci.ConfigInfoID = 28 AND ci.Value < ofrv.OnlineFeedbackRecommendationReturnVisitorUID

-- Test Case
-- SELECT * FROM vFactRecommendationViewJoined
GO
