SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vFactAdvocacyEmailJoined]
AS
SELECT
	dv.CurrentScore								AS VisitScore
,	dv.CurrentMaxScore							AS VisitMaxScore
,	df.CompletedDateKey							AS CompletionDateKey
,	dv.VisitDateKey								AS VisitDateKey
,	dv.BranchKey								AS BranchKey
,	dv.PeriodKey								AS PeriodKey
,	dr.RecommendationKey						AS RecommendationKey
,	ISNULL(dgm.GuestMarketingKey, 0)			AS GuestMarketingKey
,	ofre.RecipientCount							AS RecipientCount
,	ofre.SentCount								AS SentCount
,	ofre.[From]									AS FromAddress
,	ofre.[Subject]								AS [Subject]
,	ofre.[Message]								AS [Message]
,	CONVERT(NVARCHAR(10),
	CASE ofre.IsPrimary
		WHEN 1 THEN 'Advocate'
		WHEN 0 THEN 'Friend'
		ELSE 'Unknown'
	END)										AS Advocate
,	ofrs.AnalysisName							AS RecommendationStatus
,	CONVERT(NVARCHAR(10), 'Legacy')				AS [Source]		
,	ofre.OnlineFeedbackRecommendationEmailUID	AS AdvocacyEmailID
FROM
	MysteryDiningCopy.dbo.OnlineFeedbackRecommendationEmail ofre
	INNER JOIN MysteryDiningCopy.dbo.OnlineFeedbackRecommendation ofr ON ofre.OnlineFeedbackRecommendationUID = ofr.OnlineFeedbackRecommendationUID
	INNER JOIN MysteryDiningCopy.dbo.OnlineFeedbackDetails ofd ON ofr.OnlineFeedbackDetailsUID = ofd.OnlineFeedbackDetailsUID
	INNER JOIN MysteryDiningCopy.dbo.OnlineFeedbackRecommendationStatus ofrs ON ofr.StatusUID = ofrs.OnlineFeedbackRecommendationStatusUID

	INNER JOIN MysteryDiningDW.dbo.DimRecommendation dr ON ofr.OnlineFeedbackRecommendationUID = dr.RecommendationID AND dr.Active = 1
	INNER JOIN MysteryDiningDW.dbo.DimVisit dv ON ofd.VisitUID = dv.VisitID AND dv.Active = 1
	INNER JOIN MysteryDiningDW.dbo.DimFeedback df ON ofd.VisitUID = df.FeedbackID
	LEFT  JOIN MysteryDiningDW.dbo.DimGuestMarketing dgm ON ofd.VisitUID = dgm.GuestMarketingID 
														 AND ofre.OnlineFeedbackRecommendationReturnVisitorUID = dgm.GuestMarketingDetailID

	INNER JOIN MysteryDiningDW.dbo.ConfigInfo ci ON ci.ConfigInfoID = 25 AND ci.LastUpdated < ofre.[DateTimeAdded]

-- Test Case
-- SELECT * FROM vFactAdvocacyEmailJoined
GO
