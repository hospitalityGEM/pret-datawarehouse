
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vDimQuestionJoined]
AS
SELECT       
		q.[QuestionUID] AS QuestionID
,		c.Name AS Client
,		c.ClientUID as ClientID
,		ISNULL(mc.CategoryName, 'Not Set') AS ManagementCategory
,		CONVERT(NVARCHAR(100), ISNULL(CASE bm.BenchmarkUID
										 WHEN 'GWR' THEN 'Customer Loyalty Score'
										 WHEN 'B1' THEN 'Greeting' 
										 WHEN 'B2' THEN 'Upsold Water' 
										 WHEN 'B3' THEN 'Upsold Dishes' 
										 WHEN 'B4' THEN 'Second Drinks' 
										 WHEN 'B5' THEN 'Checkback' 
										 WHEN 'B6' THEN 'Manager Presence' 
										 WHEN 'B7' THEN 'Return/Recommend'
										 WHEN 'B8' THEN 'Goodbye' 
										 WHEN 'B9' THEN 'Warmth' 
										 WHEN 'B10' THEN 'Product Knowledge'
										 WHEN 'B11' THEN 'Food Quality'
										 WHEN 'B12' THEN 'Bill Payment' 
										 WHEN 'B13' THEN 'Pace Of service' 
										 WHEN 'B14' THEN 'Telephone Enquiry' 
										 WHEN 'B15' THEN 'Value For Money' 
										 WHEN 'B16' THEN 'Cleanliness' 
										 WHEN 'B17' THEN 'Toilet Standards'
										 WHEN 'B18' THEN 'Upsold Coffee/Desserts' 
										 WHEN 'B19' THEN 'Coffee Quality' 
										 ELSE NULL END, 'Not Set')) AS Benchmark
,		ISNULL(mq.Question, q.Question) AS MasterQuestion
,		ISNULL(dep.SectionCategoryName, 'Not Set') AS Department
,		ISNULL(sat.Name, 'Unknown') AS [Type]
,		ISNULL(at.AnalysisName, 'Not Set') AS AnalysisTag
,		IIF(quc.QuestionCatName = '', 'Standard', ISNULL(quc.QuestionCatName, 'Standard')) AS QuestionnaireCategory
,		CONVERT(NVARCHAR(200),REPLACE(REPLACE(se.Name, CHAR(10), ''), CHAR(13), '')) AS Section
,		CONVERT(NVARCHAR(2000),REPLACE(REPLACE(q.Question, CHAR(10), ''), CHAR(13), '')) AS Question
,		qu.[Description] AS Questionnaire
,		ISNULL(qu.[Description], 'Unknown') AS SubQuestionnaire
,		CONVERT(NVARCHAR(50), IIF(q.DishUID <> 0, 'Food', 'N/A')) AS MenuQuestion
,		CONVERT(NVARCHAR(10), IIF(se.HideSectionFromBody = 1, 'Hidden', 'Visible')) AS HiddenSection
,		CONVERT(NVARCHAR(10), CASE q.InternalUse 
								WHEN 1 THEN 'Hidden'
								WHEN 0 THEN 'Visible'
							    ELSE 'Unknown' END) AS HiddenQuestion
,		IIF(se.IsFoodQuality = 1, N'Food', N'N/A')  AS MenuSection
,		CASE	
			WHEN LEN(RTRIM(q.QuestionCode)) = 0 THEN 'N/A'
			ELSE ISNULL(q.QuestionCode, 'N/A') 
        END AS QuestionCode
,		CASE	
			WHEN LEN(RTRIM(q.QuestionSubject)) = 0 THEN 'Unknown'
			ELSE ISNULL(q.QuestionSubject, 'Unknown') 
        END AS QuestionSubject
,		qu.QuestionnaireUID as QuestionnaireID
,		quc.ExcludeFromAnalysis
FROM            
		MysteryDiningCopy.dbo.[Question] q 
			INNER JOIN		MysteryDiningCopy.dbo.Section se					ON q.SectionUID = se.SectionUID 
			INNER JOIN		MysteryDiningCopy.dbo.Questionnaire qu				ON se.QuestionnaireUID = qu.QuestionnaireUID 
			INNER JOIN		MysteryDiningCopy.dbo.Client c						ON qu.ClientUID = c.ClientUID 
			LEFT JOIN		MysteryDiningCopy.dbo.Category mc					ON q.QuestCatagory = mc.CategoryUID 
			LEFT JOIN		MysteryDiningCopy.dbo.Benchmark bm					ON q.BenchmarkUID = bm.BenchmarkUID
			LEFT JOIN		MysteryDiningCopy.dbo.Question mq					ON q.TrackingUID = mq.QuestionUID 
			LEFT JOIN		MysteryDiningCopy.dbo.SectionCategory dep			ON se.SectionCategoryUID = dep.SectionCategoryUID 
			LEFT JOIN		MysteryDiningDW.dbo.StagingAnswerType sat			ON q.MultiAnsType = sat.StagingAnswerTypeID 
			LEFT JOIN		MysteryDiningCopy.dbo.AnalysisTags at				ON q.AnalysisTag = at.AnalysisTagUID 
			LEFT JOIN		MysteryDiningCopy.dbo.QuestionnaireCategories quc	ON qu.CategoryUID = quc.CategoryUID



GO

EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[25] 4[9] 2[47] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vDimQuestionJoined', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vDimQuestionJoined', NULL, NULL
GO
