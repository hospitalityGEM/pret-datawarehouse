SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vFactLinkTrackingJoined]
AS
SELECT
	CAST(ofrv.IPAddress AS NVARCHAR(30))		AS IPAddress
,	ofrv.CookieGUID								AS CookieGUID
,	ddcd.DateKey								AS ClickedDateKey
,	oflt.DateTimeAdded							AS ClickedTimestamp
,	dr.RecommendationKey						AS RecommendationKey
,	CONVERT(NVARCHAR(10),
	ISNULL(sac.ChannelName, 'Unknown'))			AS GuestSource
,	dv.VisitKey									AS VisitKey
,	0											AS GuestMarketingKey
,	df.FeedbackKey								AS FeedbackKey
,	dv.BranchKey								AS BranchKey
,	dv.PeriodKey								AS PeriodKey
,	CASE
		WHEN SUBSTRING(oftl.LinkURL,1,7) = 'http://'  THEN SUBSTRING(oftl.LinkURL,8,LEN(oftl.LinkURL))
		WHEN SUBSTRING(oftl.LinkURL,1,8) = 'https://' THEN SUBSTRING(oftl.LinkURL,9,LEN(oftl.LinkURL))						
		ELSE oftl.LinkURL
	END											AS Destination
,	oflty.TypeName								AS DestinationSource
,	CONVERT(NVARCHAR(10), 'Legacy OF')			AS [Source]		
,	oflt.OnlineFeedbackLinkTrackingUID			AS LinkTrackingID
FROM
	MysteryDiningCopy.dbo.OnlineFeedbackLinkTracking oflt
	INNER JOIN MysteryDiningCopy.dbo.OnlineFeedbackRecommendationReturnVisitor ofrv ON oflt.OnlineFeedbackRecommendationReturnVisitorUID = ofrv.OnlineFeedbackRecommendationReturnVisitorUID
	INNER JOIN MysteryDiningCopy.dbo.OnlineFeedbackLinkType oflty ON oflt.OnlineFeedbackLinkTypeUID = oflty.OnlineFeedbackLinkTypeUID
	INNER JOIN MysteryDiningCopy.dbo.OnlineFeedbackTrackedLink oftl ON oflt.OnlineFeedbackTrackedLinkUID = oftl.OnlineFeedbackTrackedLinkUID
	INNER JOIN MysteryDiningCopy.dbo.OnlineFeedbackRecommendation ofr ON ofrv.OnlineFeedbackRecommendationUID = ofr.OnlineFeedbackRecommendationUID
	INNER JOIN MysteryDiningCopy.dbo.OnlineFeedbackDetails ofd ON ofr.OnlineFeedbackDetailsUID = ofd.OnlineFeedbackDetailsUID
	LEFT  JOIN MysteryDiningCopy.dbo.SocialAdvocacyChannel sac ON ofrv.ViaSocialAdvocacyChannelUID = sac.SocialAdvocacyChannelUID
	
	LEFT  JOIN MysteryDiningDW.dbo.DimDate ddcd ON CONVERT(DATETIME,CONVERT(DATE,oflt.DateTimeAdded)) = ddcd.[Date]
	LEFT JOIN MysteryDiningDW.dbo.DimRecommendation dr ON ofrv.OnlineFeedbackRecommendationUID = dr.RecommendationID AND dr.Active = 1
	INNER JOIN MysteryDiningDW.dbo.DimVisit dv ON ofd.VisitUID = dv.VisitID AND dv.Active = 1
	INNER JOIN MysteryDiningDW.dbo.DimFeedback df ON ofd.VisitUID = df.FeedbackID

	INNER JOIN MysteryDiningDW.dbo.ConfigInfo ci ON ci.ConfigInfoID = 20 AND ci.LastUpdated < oflt.[DateTimeAdded]

-- Test Case
-- SELECT * FROM vFactLinkTrackingJoined
GO
