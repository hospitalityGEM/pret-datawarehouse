SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vDimDishJoined]
AS
SELECT
	D.DishUID
,	D.Name										AS DishName
,	CONVERT(NVARCHAR(200),
	ISNULL(D.[Description], 'No Description'))	AS DishDescription
,	MS.Name										AS MenuSection
,	M.Name										AS MenuName
,	C.Name										AS Client
,	CONVERT(NVARCHAR(50),
	CASE M.StatusUID
		WHEN 'T' THEN 'Test'
		WHEN 'L' THEN 'Live'
		WHEN 'A' THEN 'Archive'
		WHEN 'B' THEN 'Rubbish'
		ELSE 'Unknown'
	END)										AS MenuStatus
,	MS.MenuSectionUID
,	M.MenuUID
,	CONVERT(NVARCHAR(10),'Legacy')				AS [Source]
FROM
	MysteryDiningCopy.dbo.Dish D
	INNER JOIN MysteryDiningCopy.dbo.MenuSection MS ON D.MenuSectionUID = MS.MenuSectionUID
    INNER JOIN MysteryDiningCopy.dbo.Menu M			ON MS.MenuUID		= M.MenuUID
    INNER JOIN MysteryDiningCopy.dbo.Client C		ON M.ClientUID		= C.ClientUID



-- Test Case
-- SELECT * FROM vDimDishJoined
GO
