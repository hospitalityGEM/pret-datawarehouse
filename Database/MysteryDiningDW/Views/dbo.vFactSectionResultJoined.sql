SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vFactSectionResultJoined]
AS
SELECT
	A.VisitUID
,	Q.SectionUID
,	CONVERT(DECIMAL(18,2),SUM(A.Score))		AS Score
,	CONVERT(DECIMAL(18,2),SUM(CASE WHEN A.Answer ='Na' then 0 else A.[Weight] end))	AS MaxScore
,	CONVERT(NVARCHAR(200),s.Name)									AS Section
,	s.SectionNo
,	dV.VisitKey
,	dV.PeriodKey
,	dV.BranchKey
,	dV.AssessorKey
,	dV.VisitDateKey
,	dV.Questionnaire
,	dV.SubQuestionnaire
,	cast(IIF(dP.IncludeInAnalysis = 'Exclude' OR qc.ExcludeFromAnalysis = 1, 'Exclude','Include') as nvarchar(10)) as IncludeInAnalysis
,	dB.GeographyKey
,	ISNULL(SC.SectionCategoryName, 'N/A')	AS Department
,	CONVERT(NVARCHAR(10),'Legacy')			AS [Source]
,	CONVERT(NVARCHAR(50),
	CASE
		WHEN s.IsFoodQuality = 1 THEN 'Food'
		ELSE 'N/A'
	END)									AS MenuSection
,	CONVERT(NVARCHAR(10),
	CASE s.HideSectionFromBody
		WHEN 1 THEN 'Hidden'
		ELSE 'Visible'
	END)									AS HiddenSection
,	s.IncludeScoreInTotal
FROM
	MysteryDiningCopy.dbo.Answer A
	INNER JOIN MysteryDiningCopy.dbo.Question Q ON A.QuestionUID				= Q.QuestionUID
    INNER JOIN MysteryDiningCopy.dbo.Visit v	ON A.VisitUID					= v.VisitUID
    INNER JOIN MysteryDiningCopy.dbo.Section s	ON Q.SectionUID					= s.SectionUID
    LEFT  JOIN MysteryDiningDW.dbo.DimVisit dV	ON A.VisitUID					= dV.VisitID AND dV.[Source] = 'Legacy' AND dV.Active = 1
    LEFT  JOIN MysteryDiningDW.dbo.DimPeriod dP ON dV.PeriodKey					= dP.PeriodKey
    LEFT  JOIN MysteryDiningDW.dbo.DimBranch dB ON dV.BranchKey					= dB.BranchKey
    LEFT  JOIN MysteryDiningCopy.dbo.SectionCategory SC ON s.SectionCategoryUID = SC.SectionCategoryUID
    INNER JOIN MysteryDiningDW.dbo.StagingLegacyVisitIDs slv ON v.VisitUID		= slv.VisitUID AND slv.ConfigInfoID = 6
	INNER JOIN MysteryDiningCopy.dbo.Questionnaire qn on v.QuestionnaireUID		= qn.QuestionnaireUID
	LEFT JOIN MysteryDiningCopy.dbo.QuestionnaireCategories qc on qn.CategoryUID = qc.CategoryUID
    
WHERE
	
	 VisitDate < GETDATE()

GROUP BY
	Q.SectionUID
,	A.VisitUID
,	s.Name
,	s.SectionNo
,	s.IsFoodQuality
,	dV.VisitKey
,	dV.PeriodKey
,	dV.BranchKey
,	dV.AssessorKey
,	dV.VisitDateKey
,	dV.Questionnaire
,	dV.SubQuestionnaire
,	dB.GeographyKey
,	SC.SectionCategoryName
,	s.HideSectionFromBody
,	s.IncludeScoreInTotal
,	IIF(dP.IncludeInAnalysis = 'Exclude' OR qc.ExcludeFromAnalysis = 1, 'Exclude','Include')

-- Test Case
-- SELECT * FROM vFactSectionResultJoined





GO
