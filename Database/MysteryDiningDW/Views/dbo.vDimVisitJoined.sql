
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE VIEW [dbo].[vDimVisitJoined]
AS

SELECT
	v.VisitUID
,	da.AssessorKey
,	CONVERT(DATETIME, '1900-01-01 ' + CAST(DATEPART([HOUR], v.[Time]) AS NVARCHAR(2)) + ':' + CAST(DATEPART([MINUTE], v.[Time]) AS NVARCHAR(2)) + ':00.000') AS [Time]
,	CONVERT(DATETIME, '1900-01-01 ' + CAST(DATEPART([HOUR], v.VisitEndTime) AS NVARCHAR(2)) + ':' + CAST(DATEPART([MINUTE], v.VisitEndTime) AS NVARCHAR(2)) + ':00.000') AS VisitEndTime
,	CONVERT(NVARCHAR(50), CASE qu.VisitType
		WHEN 'MV' THEN 'Mystery Visit'
		WHEN 'RC' THEN 'Real Customer'
		WHEN 'CC' THEN 'Comment Card'
		WHEN 'OF' THEN 'Online Feedback'
		ELSE 'Unknown'
	END) AS VisitType
,	qu.[Description] AS Questionnaire
,	CASE
		WHEN quc.QuestionCatName = '' THEN 'Standard'
		ELSE quc.QuestionCatName 
	END AS QuestionnaireCategory
,	vs.[Status] AS VisitStatus
,	ISNULL(v.Adults, 0) AS Adults
,	ISNULL(v.Children, 0) AS Children
,	ISNULL(v.ChildrenNotEating, 0) AS ChildrenNotEating
,	v.ReceiptNo
,	ISNULL(tl.[Description], 'Unknown') AS TradeLevel
,	CONVERT(NVARCHAR(50),v.SeatingLevel) AS SeatingLevel
,   ISNULL(w.[Description], 'Unknown') AS Weather
,	v.Comments
,	v.AdditionalComments
,	ISNULL(AO.Name, 'N/A') AS [Owner]
,	CONVERT(NVARCHAR(50),ISNULL(pf.PassFail, 'N/A'))			AS PassFail
,	CONVERT(NVARCHAR(300),ISNULL(pf.PassFailMessage, 'N/A'))	AS PassFailMessage
,	ISNULL(CASE 
		WHEN (bvs.MaximumScore IS NULL) OR (bvs.MaximumScore = 0) THEN 0 
		ELSE CONVERT(DECIMAL(18,2),CONVERT(INT, ROUND(CONVERT(DECIMAL(20,4), bvs.Score) /CONVERT(DECIMAL(20,4), bvs.MaximumScore) * 100, 0))) 
	END,0) AS CurrentVisitScore
,	CONVERT(DECIMAL(20,4), bvs.Score)			AS CurrentScore
,	CONVERT(DECIMAL(20,4), bvs.MaximumScore)	AS CurrentMaxScore
,	CONVERT(DECIMAL(20,4),	
	CASE
		WHEN pf.PassFail IN ('0%','5%','10%') THEN  bvs.Score - 0.01 *(Cast(REPLACE(pf.PassFail,'%','') AS INT) * bvs.MaximumScore)
		ELSE NULL 
	END) as AdjustedScore
,	ISNULL(slt.VisitScoreText,'Error')											AS VisitScoreText
,	CONVERT(NVARCHAR(50),MysteryDiningCopy.dbo.fnVisitScoreMethod(v.VisitUID))	AS VisitScoreMethod
,	CONVERT(NVARCHAR(10), 'Legacy') AS [Source]
,	ISNULL(ddvd.DateKey, 19000101)	AS VisitDateKey
,	ISNULL(ddat.DateKey, 19000101)	AS AvailableToDateKey
,	ISNULL(ddaf.DateKey, 19000101)	AS AvailableFromDateKey
,	ISNULL(ddda.DateKey, 19000101)	AS AddedDateKey
,	ISNULL(ddal.DateKey, 19000101)	AS AssessorBookDateKey
,	ISNULL(ddrd.DateKey, 19000101)	AS ReimbursementDateKey
,	ISNULL(dded.DateKey, 19000101)	AS VisitExitDateKey
,	dp.PeriodKey
,	db.BranchKey
,	db.GeographyKey
,	VAA.AssignedAdmin				AS AssignedAdmin
,	NULL							AS PeriodBranchVisitCount
,	NULL							AS PeriodBranchCategoryVisitCount
,	cast(ISNULL(SD.staffdesc,'N/A') as nvarchar(2000))		AS StaffDescription
,	ISNULL(ddir.DateKey, 19000101)	AS InitialReviewDateKey
,	ISNULL(ddlr.DateKey, 19000101)	AS LatestReviewDateKey
,	d.DiscardReasonOrigin
,	d.DiscardReason
,	d.DiscardReasonDetail
,	qu.QuestionnaireUID
,	cast(IIF(nb.BranchID is not null OR (dv.IsNBVisit=1 AND bvs.visitUID is not null),1,0) as bit) as IsNBVisit

FROM         
	MysteryDiningCopy.dbo.Visit v
	LEFT JOIN MysteryDiningCopy.dbo.BatchedVisitScores bvs ON v.VisitUID = bvs.VisitUID
	LEFT JOIN MysteryDiningCopy.dbo.PassFailMeta pf ON v.VisitUID = pf.VisitUID
    INNER JOIN MysteryDiningDW.dbo.StagingLegacyVisitIDs slv ON v.VisitUID		= slv.VisitUID AND slv.ConfigInfoID = 5
    LEFT  JOIN MysteryDiningDW.dbo.StagingLegacyVisitScoreText slt ON v.VisitUID	= slt.VisitUID
    LEFT  JOIN MysteryDiningCopy.dbo.VisitStatus vs ON v.VisitStatusUID = vs.VisitStatusUID
    LEFT  JOIN MysteryDiningCopy.dbo.Questionnaire qu ON v.QuestionnaireUID = qu.QuestionnaireUID
    LEFT  JOIN MysteryDiningCopy.dbo.QuestionnaireCategories quc ON qu.CategoryUID = quc.CategoryUID
    LEFT  JOIN MysteryDiningCopy.dbo.TradeLevel tl ON v.TradeLevelUID = tl.TradeLevelUID
    LEFT  JOIN MysteryDiningCopy.dbo.Weather w ON v.WeatherUID = w.WeatherUID
    LEFT  JOIN MysteryDiningCopy.dbo.Branch br ON v.BranchUID = br.BranchUID
	LEFT JOIN MysteryDiningCopy.dbo.NBBranchManager nb ON nb.BranchID=v.BranchUID and v.VisitDate between NB.NBStartDate and NB.NBEndDate
	LEFT JOIN 
		(Select 
				drd.VisitUID
			,	DiscardReasonType	AS DiscardReasonOrigin
			,	DiscardReason
			,	DiscardReasonDetail
			from MysteryDiningCopy.dbo.DiscardReasonDetail drd

			INNER JOIN
			(
			select VisitUID,max(DiscardReasonLogDate) as MaxLogDate

			from MysteryDiningCopy.dbo.DiscardReasonDetail 

			group by VisitUID)aux ON aux.VisitUID=drd.VisitUID and drd.DiscardReasonLogDate=aux.MaxLogDate
			INNER  JOIN MysteryDiningCopy.dbo.DiscardReason dir  on drd.DiscardReasonID = dir.DiscardReasonID
			INNER JOIN MysteryDiningCopy.dbo.DiscardReasonType drt on dir.DiscardReasonTypeID = drt.DiscardReasonTypeID
			) d ON d.visituid=v.VisitUID
	
	LEFT JOIN
		(
			SELECT	
				v.VisitUID	
			,	ISNULL(au.Name, 'Unassigned') AS AssignedAdmin
			FROM
				MysteryDiningCopy.dbo.Visit v
				INNER JOIN MysteryDiningCopy.dbo.Questionnaire q ON v.QuestionnaireUID = q.QuestionnaireUID
				LEFT  JOIN 
					(		
					SELECT
						vaa.VisitUID
					,	MAX(vaa.VAAUID) AS VAAUID
					FROM
						(
						SELECT
							VisitUID
						,	MAX(VAAUID) AS VAAUID
						FROM
							MysteryDiningCopy.dbo.VisitAssignedAdmin
						WHERE
							TimeUnassigned IS NULL
						GROUP BY
							AdminUID
						,	VisitUID
						) x
					INNER JOIN MysteryDiningCopy.dbo.VisitAssignedAdmin vaa	ON  x.VAAUID	= vaa.VAAUID
					GROUP BY
						vaa.VisitUID	
					) vaa ON v.VisitUID = vaa.VisitUID
				LEFT  JOIN MysteryDiningCopy.dbo.VisitAssignedAdmin vaaU ON vaa.VAAUID = vaaU.VAAUID AND vaa.VisitUID = v.VisitUID	
				LEFT  JOIN MysteryDiningCopy.dbo.[User] au ON vaaU.AdminUID = au.UserUID 
		) VAA ON v.VisitUID = VAA.VisitUID
    LEFT  JOIN MysteryDiningCopy.dbo.[User] AO ON v.[Owner] = AO.UserUID
    LEFT  JOIN MysteryDiningDW.dbo.DimDate ddvd ON CONVERT(DATETIME,CONVERT(DATE,v.VisitDate)) = ddvd.[Date]
    LEFT  JOIN MysteryDiningDW.dbo.DimDate ddat ON CONVERT(DATETIME,CONVERT(DATE,v.DateAvailableTo)) = ddat.[Date]
    LEFT  JOIN MysteryDiningDW.dbo.DimDate ddaf ON CONVERT(DATETIME,CONVERT(DATE,v.DateAvailableFr)) = ddaf.[Date]
    LEFT  JOIN MysteryDiningDW.dbo.DimDate ddda ON CONVERT(DATETIME,CONVERT(DATE,v.DateAdded)) = ddda.[Date]
    LEFT  JOIN MysteryDiningDW.dbo.DimDate ddal ON CONVERT(DATETIME,CONVERT(DATE,v.DateAllocated)) = ddal.[Date]
    LEFT  JOIN MysteryDiningDW.dbo.DimDate ddrd ON CONVERT(DATETIME,CONVERT(DATE,v.ReimbursementDate)) = ddrd.[Date]
	LEFT  JOIN MysteryDiningDW.dbo.DimDate dded ON CONVERT(DATETIME,CONVERT(DATE, v.VisitEndTime)) = dded.[Date]
	LEFT  JOIN MysteryDiningDW.dbo.DimDate ddir ON CONVERT(DATETIME,CONVERT(DATE, v.InitialReviewDate)) = ddir.[Date]
	LEFT  JOIN MysteryDiningDW.dbo.DimDate ddlr ON CONVERT(DATETIME,CONVERT(DATE, v.LastReviewDate)) = ddlr.[Date]
    LEFT  JOIN MysteryDiningDW.dbo.DimAssessor da ON v.DinerUID = da.AssessorID AND da.[Source] = 'Legacy' AND da.Active = 1
    LEFT  JOIN MysteryDiningDW.dbo.DimPeriod dp ON v.Period = dp.PeriodID 
												AND br.brandUID = dp.BrandID
												AND dp.[Source] = 'Legacy'
												AND dp.Active = 1
	LEFT  JOIN MysteryDiningDW.dbo.DimBranch db ON v.BranchUID = db.BranchID
												AND db.[Source] = 'Legacy'
												AND db.Active = 1
	LEFT  JOIN MysteryDiningDW.dbo.DimVisit dv ON v.VisitUID = dv.VisitID
												AND dv.[Source] = 'Legacy'
										        AND dv.Active = 1
	LEFT JOIN 

			(SELECT DISTINCT  s.VisitUID
						,	substring((select(' STAFF '+ cast(ROW_NUMBER () OVER (PARTITION BY sd.VisitUID ORDER BY sd.StaffDescriptionUID) as nvarchar(3))
						+  ': Gender: ' + sg.StaffGender 
						+	'. Hair Colour: ' + shc.StaffHairColour
						+	'. Hair Length: ' + shl.StaffHairLength
						+	'. Age: ' + RTRIM(sa.StaffAge)
						+	'. Approx Height: ' + RTRIM(sh.StaffApproxHeight)
						+'. Clothes: ' + SUBSTRING((SELECT ', ' + sc.StaffClothingItem as [text()]
								FROM  [MysteryDiningCopy].[dbo].[StaffClothingMAPPING] scm
								INNER JOIN [MysteryDiningCopy].[dbo].[StaffClothingItem] sc ON scm.ClothingItemUID=sc.StaffClothingItemUID
								where scm.StaffDescriptionUID=sd.StaffDescriptionUID
								order by scm.StaffDescriptionUID
								for XML PATH ('')),3,1000) 
						+	'. Features: ' + SUBSTRING((SELECT ', ' + sf.StaffFeatureItem as [text()] 
								FROM  [MysteryDiningCopy].[dbo].[StaffFeatureMAPPING] sfm
								INNER JOIN [MysteryDiningCopy].[dbo].[StaffFeatureItem] sf ON sf.StaffFeatureItemUID=sfm.StaffFeatureItemUID
								where sfm.StaffDescriptionUID=sd.StaffDescriptionUID
								order by sfm.StaffDescriptionUID
								for XML PATH ('')),3,1000)
						+ '.') as [text()]
					FROM [MysteryDiningCopy].[dbo].[StaffDescription] sd
							INNER JOIN [MysteryDiningCopy].[dbo].[StaffGender] sg ON sd.Gender=sg.StaffGenderUID
							INNER JOIN [MysteryDiningCopy].[dbo].[StaffHairColour] shc ON sd.HairColour=shc.StaffHairColourUID
							INNER JOIN [MysteryDiningCopy].[dbo].[StaffHairLength] shl ON sd.HairLength=shl.StaffHairLengthUID
							INNER JOIN [MysteryDiningCopy].[dbo].[StaffAge] sa ON sd.age=sa.StaffAgeUID
							INNER JOIN [MysteryDiningCopy].[dbo].[StaffApproxHeight] sh ON sd.ApproxHeight=sh.StaffApproxHeightUID 
					WHERE s.visitUID=sd.VisitUID 
					order by sd.VisitUID
					for XML PATH ('')),2,2000) [staffdesc]
				FROM [MysteryDiningCopy].[dbo].[StaffDescription] s )SD ON SD.VisitUID=v.VisitUID


WHERE
	v.VisitDate IS NOT NULL
AND
	v.VisitDate < '2020-01-01'		

GO
