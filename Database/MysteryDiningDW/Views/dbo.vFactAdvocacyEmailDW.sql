SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vFactAdvocacyEmailDW]
AS
SELECT
	*
FROM
	FactAdvocacyEmail
WHERE
	AdvocacyEmailID IN
	(
		SELECT
			AdvocacyEmailID
		FROM
			vFactAdvocacyEmailJoined
	)
	

-- Test Case
-- SELECT * FROM vFactAdvocacyEmailDW
GO
