
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vFactPerformanceAlertResultDW]
AS
SELECT TOP 100 PERCENT
	   [PerformanceAlertResultKey]
      ,[VisitKey]
      ,[PeriodKey]
      ,[BranchKey]
      ,[GeographyKey]
      ,[VisitDateKey]
      ,[AssessorKey]
      ,[Questionnaire]
      ,[SubQuestionnaire]
      ,[AlertName]
      ,[AlertType]
      ,[Score]
      ,[MaxScore]
      ,[ScoreText]
      ,[ScoreMethod]
      ,[IncludeInAnalysis]
      ,[VisitStatus]
      ,[RaisedDateKey]
      ,[CompletedDateKey]
      ,[ValidFromDate]
      ,[ValidToDate]
      ,[Active]
      ,[Source]
      ,[AlertID]
      ,[VisitID]
      ,[TimeStamp]
      ,[ChangeReason]
FROM 
	[FactPerformanceAlertResult]
	INNER JOIN MysteryDiningDW.dbo.StagingLegacyVisitIDs slv ON [FactPerformanceAlertResult].VisitID		= slv.VisitUID AND slv.ConfigInfoID = 18
WHERE
	[Source] = 'Legacy'
AND [Active] = 1

ORDER BY
	VisitID
,	AlertID

GO
