SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vDimGeography-Diner]
AS
SELECT [DinerUID]
      ,[Address1]
      ,[Address2]
      ,[Address3]
      ,[Address4]
      ,[Address5]
      ,[Postcode]
FROM MysteryDiningCopy.dbo.[Diner]
GO
