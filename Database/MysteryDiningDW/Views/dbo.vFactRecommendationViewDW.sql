SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vFactRecommendationViewDW]
AS
SELECT
	*
FROM
	FactRecommendationView
WHERE
	RecommendationViewID IN
	(
		SELECT
			RecommendationViewID
		FROM
			vFactRecommendationViewJoined
	)
	

-- Test Case
-- SELECT * FROM vFactRecommendationViewDW

GO
