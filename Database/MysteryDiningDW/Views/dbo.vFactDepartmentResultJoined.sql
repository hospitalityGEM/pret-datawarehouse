SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE VIEW [dbo].[vFactDepartmentResultJoined]
AS
SELECT
	A.VisitUID
,	CONVERT(DECIMAL(18,2),SUM(A.Score))							AS Score
,	CONVERT(DECIMAL(18,2),SUM(CASE WHEN A.Answer='Na' then 0 else A.[Weight] end))						AS MaxScore
,	SC.SectionCategoryName										AS Department
,	dV.VisitKey
,	dV.PeriodKey
,	dV.BranchKey
,	dV.AssessorKey
,	dV.VisitDateKey
,	dV.Questionnaire
,	dV.SubQuestionnaire
,	dV.CurrentScore												AS VisitScore
,	dV.CurrentMaxScore											AS VisitMaxScore
,	dV.VisitScoreMethod											AS VisitScoreMethod
,	cast(IIF(dP.IncludeInAnalysis = 'Exclude' OR qc.ExcludeFromAnalysis = 1, 'Exclude','Include') as nvarchar(10)) as IncludeInAnalysis
,	dB.GeographyKey
,	CONVERT(NVARCHAR(10),'Legacy')								AS [Source]
,	SC.SectionCategoryUID										AS DepartmentID
FROM
	MysteryDiningCopy.dbo.Answer A
	INNER JOIN MysteryDiningCopy.dbo.Question Q ON A.QuestionUID				= Q.QuestionUID
    INNER JOIN MysteryDiningCopy.dbo.Visit v	ON A.VisitUID					= v.VisitUID
    INNER JOIN MysteryDiningCopy.dbo.Section s	ON Q.SectionUID					= s.SectionUID
    INNER JOIN MysteryDiningCopy.dbo.SectionCategory SC ON s.SectionCategoryUID = SC.SectionCategoryUID  
    LEFT  JOIN MysteryDiningDW.dbo.DimVisit dV	ON A.VisitUID					= dV.VisitID AND dV.[Source] = 'Legacy' AND dV.Active = 1
    LEFT  JOIN MysteryDiningDW.dbo.DimPeriod dP ON dV.PeriodKey					= dP.PeriodKey
    LEFT  JOIN MysteryDiningDW.dbo.DimBranch dB ON dV.BranchKey					= dB.BranchKey
    INNER JOIN MysteryDiningDW.dbo.StagingLegacyVisitIDs slv ON v.VisitUID		= slv.VisitUID AND slv.ConfigInfoID = 9
	INNER JOIN MysteryDiningCopy.dbo.Questionnaire qn on v.QuestionnaireUID		= qn.QuestionnaireUID
	LEFT JOIN MysteryDiningCopy.dbo.QuestionnaireCategories qc on qn.CategoryUID = qc.CategoryUID

WHERE
		
		VisitDate < GETDATE()
	--AND v.VisitUID IN 
	--(SELECT DISTINCT VisitUID FROM MysteryDiningCopy.dbo.VisitUpdate WHERE [TimeStamp] >
	--(SELECT [LastUpdated] 
	--				FROM MysteryDiningDW.dbo.ConfigInfo
	--				WHERE ConfigInfoID = 9))
GROUP BY
	A.VisitUID
,	dV.VisitKey
,	dV.PeriodKey
,	dV.BranchKey
,	dV.AssessorKey
,	dV.VisitDateKey
,	dV.Questionnaire
,	dV.SubQuestionnaire
,	dV.VisitScoreText
,	dV.VisitScoreMethod	
,	dB.GeographyKey
,	SC.SectionCategoryName
,	dV.CurrentMaxScore
,	dV.CurrentScore
,	SC.SectionCategoryUID
,	IIF(dP.IncludeInAnalysis = 'Exclude' OR qc.ExcludeFromAnalysis = 1, 'Exclude','Include')

-- Test Case
-- SELECT * FROM vFactDepartmentResultJoined ORDER BY VisitUID, DepartmentID





GO
