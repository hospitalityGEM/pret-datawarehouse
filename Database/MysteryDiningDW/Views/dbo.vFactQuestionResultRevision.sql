SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vFactQuestionResultRevision]
AS
SELECT TOP 100 PERCENT
	ar.AnswerRevisionUID
,	ar.VisitUID
,	ar.QuestionUID
,	ar.Answer
,	ar.Score
,	ar.[Weight]
,	ar.Comments
,	ar.[TimeStamp]
,	Substring(f.Factor,1,1) + Lower(Substring(f.Factor,2,Len(f.Factor))) AS Factor
FROM         
	[MysteryDiningCopy].dbo.AnswerRevision ar
	INNER JOIN
	      (SELECT
            (CASE WHEN (Substring(q.Question,1, CHARINDEX(' ', q.Question))) IN ('WELCOME','RECOMMEND','PACE','INTEREST','CONFIDENCE','KNOWLEDGE','VALUE','WARMTH','TRAINING ')
            THEN (Substring(q.Question,1, CHARINDEX(' ', q.Question)))
            ELSE 'NONE'
            END) AS Factor
           ,	q.QuestionUID

      FROM

            MysteryDiningCopy.dbo.Question q
            INNER JOIN MysteryDiningcopy.dbo.Section s ON q.SectionUID = s.SectionUID) f ON ar.QuestionUID = f.QuestionUID
    INNER JOIN MysteryDiningCopy.dbo.Visit v ON ar.VisitUID = v.VisitUID
WHERE
		ar.[TimeStamp] > (SELECT [LastUpdated] 
						FROM MysteryDiningDW.dbo.ConfigInfo
						WHERE [Key] = 'FactQuestionResultLatestRecord')
	AND 
		v.VisitDate < '2020-01-01'
	AND 
		ar.[TimeStamp] IS NOT NULL
ORDER BY
	ar.AnswerRevisionUID



GO
