SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vFactSectionResult]
AS
SELECT
	A.VisitUID
,	Q.SectionUID
,	CONVERT(DECIMAL(18,2),SUM(A.Score))		AS Score
,	CONVERT(DECIMAL(18,2),SUM(A.[Weight]))	AS MaxScore
,	s.Name			AS Section
,	s.SectionNo
FROM
	MysteryDiningCopy.dbo.Answer A
	INNER JOIN MysteryDiningCopy.dbo.Question Q ON A.QuestionUID = Q.QuestionUID
    INNER JOIN MysteryDiningCopy.dbo.Visit v ON A.VisitUID = v.VisitUID
    INNER JOIN MysteryDiningCopy.dbo.Section s ON Q.SectionUID = s.SectionUID
WHERE
		A.Answer <> 'Na'
	AND VisitDate < '2020-01-01'
	AND VisitDate > (SELECT [LastUpdated] 
					FROM MysteryDiningDW.dbo.ConfigInfo
					WHERE [Key] = 'FactSectionResultEarliestRecord')
GROUP BY
	Q.SectionUID
,	A.VisitUID
,	s.Name	
,	s.SectionNo

-- Test Case
-- SELECT * FROM vFactSectionResult
GO
