SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE VIEW [dbo].[vFactVisitResultJoined]
AS
SELECT        
		V.VisitUID
,		CONVERT(DECIMAL(18, 2), ISNULL(bvs.Score, 0)) AS Score
,		CONVERT(DECIMAL(18, 2), ISNULL(bvs.MaximumScore, 0)) AS MaxScore
,		CONVERT(DECIMAL(20,4),CASE
		WHEN pf.PassFail IN ('0%','5%','10%') THEN  bvs.Score - 0.01 *(Cast(REPLACE(pf.PassFail,'%','') as int)* bvs.MaximumScore)
		ELSE NULL END) as AdjustedScore
,		ISNULL(slt.VisitScoreText, 'Error') AS ScoreText
,		CONVERT(NVARCHAR(50), [MysteryDiningCopy].dbo.fnVisitScoreMethod(V.VisitUID)) AS ScoreMethod
,		CONVERT(NVARCHAR(4000), V.AdditionalComments) AS AdditionalComments
,		ROW_NUMBER() OVER (PARTITION BY V.Period, V.BranchUID ORDER BY V.VisitDate) AS PeriodBranchVisitCount
,		ROW_NUMBER() OVER (PARTITION BY YEAR(V.VisitDate), V.BranchUID ORDER BY V.VisitDate) AS YearBranchVisitCount
,		CONVERT(NVARCHAR(10), 'Legacy') AS [Source]
,		dV.VisitKey
,		ISNULL(dA.AdminKey, 0) AS AdminKey
,		dV.VisitStatus
,		dV.PeriodKey AS PeriodKey
,		dV.BranchKey AS BranchKey
,		ISNULL(dV.AssessorKey, 0) AS AssessorKey
,		dV.VisitDateKey
,		dV.Questionnaire
,		dV.SubQuestionnaire
,	cast(IIF(dP.IncludeInAnalysis = 'Exclude' OR qc.ExcludeFromAnalysis = 1, 'Exclude','Include') as nvarchar(10)) as IncludeInAnalysis
,		dB.GeographyKey
FROM    [MysteryDiningCopy].dbo.Visit V 
			INNER JOIN		[MysteryDiningCopy].dbo.BatchedVisitScores bvs		ON V.VisitUID = bvs.VisitUID 
			LEFT JOIN		MysteryDiningDW.dbo.DimVisit dV						ON V.VisitUID = dV.VisitID AND dV.[Source] = 'Legacy' AND dV.Active = 1
			LEFT JOIN		MysteryDiningDW.dbo.DimPeriod dP					ON dV.PeriodKey = dP.PeriodKey 
			LEFT JOIN		MysteryDiningDW.dbo.DimBranch dB					ON dV.BranchKey = dB.BranchKey 
			LEFT JOIN		MysteryDiningDW.dbo.DimAdmin dA						ON dV.AdminOwner = dA.AdminName AND dA.[Source] = 'Legacy' 
			INNER JOIN		MysteryDiningDW.dbo.StagingLegacyVisitIDs slv		ON v.VisitUID = slv.VisitUID AND slv.ConfigInfoID = 7 
			LEFT JOIN		MysteryDiningDW.dbo.StagingLegacyVisitScoreText slt ON v.VisitUID = slt.VisitUID
			LEFT JOIN MysteryDiningCopy.dbo.PassFailMeta pf ON v.VisitUID = pf.VisitUID
			INNER JOIN MysteryDiningCopy.dbo.Questionnaire qn on v.QuestionnaireUID		= qn.QuestionnaireUID
			LEFT JOIN MysteryDiningCopy.dbo.QuestionnaireCategories qc on qn.CategoryUID = qc.CategoryUID
WHERE        v.VisitDate < GETDATE()







GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[24] 4[11] 2[46] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vFactVisitResultJoined', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vFactVisitResultJoined', NULL, NULL
GO
