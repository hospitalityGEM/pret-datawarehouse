SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vDimBranch]
AS
SELECT b.[BranchUID]
      ,b.[ClientUID]
      ,b.[Name]
      ,b.[Address1]
      ,b.[Postcode]
      ,b.[ClientRegionUID]
      ,b.[BranchStatusUID]
      ,b.[SSSectorUID]
      ,ISNULL(s.SegmentName, 'No Segment') AS ClientSegment
      ,ISNULL(bd.BrandName, c.Name) AS Brand
      ,ISNULL(bd.Active, 0) AS BrandStatus
      ,ISNULL(bd.BrandUID, 0) AS BrandUID
      ,c.Name AS Client
      ,c.StatusUID AS ClientStatus
FROM MysteryDiningCopy.dbo.[Branch] b
LEFT JOIN MysteryDiningCopy.dbo.Segment s ON b.SegmentUID = s.SegmentUID
LEFT JOIN MysteryDiningCopy.dbo.Brand bd ON b.BrandUID = bd.BrandUID
INNER JOIN MysteryDiningCopy.dbo.Client c ON b.ClientUID = c.ClientUID
	

GO
