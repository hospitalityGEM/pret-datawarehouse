SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vDimSocialChannelJoined]
AS
SELECT
	CONVERT(NVARCHAR(50),sac.ChannelName)	AS SocialNetwork
,	CONVERT(NVARCHAR(50),
	CASE 
		WHEN LEN(sca.AccountName) = 0
			THEN 'Not Set'
		ELSE sca.AccountName
	END)									AS AccountName
,	CONVERT(NVARCHAR(50),
	CASE 
		WHEN LEN(sca.DisplayName) = 0
			THEN 'Not Set'
		ELSE sca.DisplayName
	END)									AS DisplayName
,	CONVERT(NVARCHAR(500),
	CASE
		WHEN SUBSTRING(sca.PageURL,1,11) = 'http://www.'
			THEN SUBSTRING(sca.PageURL,12,LEN(sca.PageURL))
		WHEN SUBSTRING(sca.PageURL,1,12) = 'https://www.'
			THEN SUBSTRING(sca.PageURL,13,LEN(sca.PageURL))
		ELSE sca.PageURL
	END)									AS Link
,	CONVERT(NVARCHAR(500),sca.PicturePath)	AS PicturePath
,	CONVERT(NVARCHAR(100),
	CASE 
		WHEN LEN(sca.Category) = 0 AND sca.SocialAdvocacyChannelUID = 1
			THEN 'Not Set'
		WHEN LEN(sca.Category) = 0 AND sca.SocialAdvocacyChannelUID <> 1
			THEN 'N/A'
		ELSE sca.Category
	END)									AS Category
,	CONVERT(NVARCHAR(500),
	CASE 
		WHEN LEN(sca.About) = 0
			THEN 'Not Set'
		ELSE sca.About
	END)									AS About
,	CONVERT(NVARCHAR(500),
	CASE 
		WHEN LEN(sca.Location) = 0
			THEN 'Not Set'
		ELSE sca.Location
	END)									AS Location
,	CONVERT(NVARCHAR(100),sca.ExternalID)	AS SocialChannelID
,	CONVERT(NVARCHAR(10), 'Legacy')			AS [Source]
FROM
	MysteryDiningCopy.dbo.SocialChannelAssigned sca
	INNER JOIN MysteryDiningCopy.dbo.SocialAdvocacyChannel sac ON sca.SocialAdvocacyChannelUID = sac.SocialAdvocacyChannelUID

-- Test Case
-- SELECT * FROM vDimSocialChannelJoined ORDER BY VisitUID, DepartmentID
GO
