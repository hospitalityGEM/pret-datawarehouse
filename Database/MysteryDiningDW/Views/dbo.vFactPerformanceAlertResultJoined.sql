SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vFactPerformanceAlertResultJoined]
AS

SELECT DISTINCT
	CONVERT(DECIMAL(18,2),ISNULL(bvs.Score, 0))											AS Score
,	CONVERT(DECIMAL(18,2),ISNULL(bvs.MaximumScore, 0))									AS MaxScore
,	dv.VisitScoreText																	AS ScoreText
,	dv.VisitScoreMethod																	AS ScoreMethod
,	CONVERT(NVARCHAR(10),'Legacy')			AS [Source]
,	dV.VisitKey
,	dV.VisitStatus
,	dV.PeriodKey							AS PeriodKey
,	dV.BranchKey							AS BranchKey
,	ISNULL(dV.AssessorKey, 0)				AS AssessorKey
,	dV.VisitDateKey
,	dV.Questionnaire
,	dV.SubQuestionnaire
,	cast(IIF(dP.IncludeInAnalysis = 'Exclude' OR qc.ExcludeFromAnalysis = 1, 'Exclude','Include') as nvarchar(10)) as IncludeInAnalysis
,	dB.GeographyKey
,	ISNULL(at.Name, 'Neutral')				AS AlertType
,	a.Name									AS AlertName
,	ISNULL(ddt.DateKey, 19000101)			AS RaisedDateKey
,	ISNULL(ddc.DateKey, 19000101)			AS CompletedDateKey
,	aa.AlertUID								AS AlertID
,	V.VisitUID								AS VisitID
FROM
	MysteryDiningCopy.dbo.AlertAction aa
	INNER JOIN MysteryDiningCopy.dbo.Visit V							ON aa.VisitUID = V.VisitUID
	INNER JOIN MysteryDiningCopy.dbo.Alert a							ON aa.AlertUID = a.AlertUID
	INNER JOIN MysteryDiningCopy.dbo.BatchedVisitScores bvs			ON V.VisitUID = bvs.VisitUID
	LEFT  JOIN MysteryDiningCopy.dbo.AlertType at						ON a.AlertTypeUID = at.AlertTypeUID
	LEFT  JOIN MysteryDiningDW.dbo.DimVisit dV  ON V.VisitUID = dV.VisitID AND dV.[Source] = 'Legacy' AND dV.Active = 1
    LEFT  JOIN MysteryDiningDW.dbo.DimPeriod dP ON dV.PeriodKey = dP.PeriodKey
    LEFT  JOIN MysteryDiningDW.dbo.DimBranch dB ON dV.BranchKey = dB.BranchKey
    LEFT  JOIN MysteryDiningDW.dbo.DimAdmin dA  ON dV.AdminOwner = dA.AdminName AND dA.[Source] = 'Legacy'
    INNER JOIN MysteryDiningDW.dbo.StagingLegacyVisitIDs slv ON v.VisitUID		= slv.VisitUID AND slv.ConfigInfoID = 18
    LEFT  JOIN MysteryDiningDW.dbo.DimDate ddt ON CONVERT(DATETIME,CONVERT(DATE,aa.DateRaised)) = ddt.[Date]
	INNER JOIN MysteryDiningCopy.dbo.Questionnaire qn on v.QuestionnaireUID		= qn.QuestionnaireUID
	LEFT JOIN MysteryDiningCopy.dbo.QuestionnaireCategories qc on qn.CategoryUID = qc.CategoryUID
	INNER JOIN 
       (
			SELECT TOP 1 
				aa.DateComplete
			,	aa.VisitUID 
			FROM
				MysteryDiningCopy.dbo.AlertAction aa 
				INNER JOIN MysteryDiningDW.dbo.StagingLegacyVisitIDs slv ON aa.VisitUID = slv.VisitUID 
																		AND slv.ConfigInfoID = 18 
			ORDER BY 
				aa.DateComplete DESC
       ) AS dc ON dc.VisitUID = v.VisitUID
    LEFT  JOIN MysteryDiningDW.dbo.DimDate ddc ON CONVERT(DATETIME,CONVERT(DATE,dc.DateComplete)) = ddc.[Date]

WHERE
	v.VisitDate < GETDATE()
	



GO
