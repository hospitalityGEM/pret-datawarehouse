SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].vFactCategoryResultDW
AS
SELECT TOP 100 PERCENT
	   [CategoryResultKey]
      ,[VisitKey]
      ,[BranchKey]
      ,[GeographyKey]
      ,[PeriodKey]
      ,[VisitDateKey]
      ,[AssessorKey]
      ,[Questionnaire]
      ,[SubQuestionnaire]
      ,[Category]
      ,[Score]
      ,[MaxScore]
      ,[VisitScore]
      ,[VisitMaxScore]
      ,[VisitScoreMethod]
      ,[IncludeInAnalysis]
      ,[ValidFromDate]
      ,[ValidToDate]
      ,[Active]
      ,[Source]
      ,[VisitID]
      ,[CategoryID]
      ,[TimeStamp]
      ,[ChangeReason]
  FROM [FactCategoryResult]
  WHERE
	[Source] = 'Legacy'
AND [Active] = 1
AND	[VisitID] IN (SELECT DISTINCT VisitUID FROM MysteryDiningCopy.dbo.VisitUpdate WHERE [TimeStamp] >
					(SELECT [LastUpdated] 
					FROM MysteryDiningDW.dbo.ConfigInfo
					WHERE ConfigInfoID = 10)
								AND	[TimeStamp] > '2005-01-01'
								AND [TimeStamp] IS NOT NULL)
ORDER BY
	VisitID
,	CategoryID
GO
