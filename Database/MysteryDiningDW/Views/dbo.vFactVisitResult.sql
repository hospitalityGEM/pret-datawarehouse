SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vFactVisitResult]
AS
SELECT
	V.VisitUID
,	bvs.Score
,	bvs.MaximumScore
,	[MysteryDiningCopy].dbo.fnVisitScoreText(V.VisitUID)	AS ScoreValue
,	[MysteryDiningCopy].dbo.fnVisitScoreMethod(V.VisitUID)	AS ScoreMethod
,	V.AdditionalComments
,	ROW_NUMBER() OVER(PARTITION BY V.Period, V.BranchUID ORDER BY V.VisitDate) AS PeriodBranchVisitCount
,	ROW_NUMBER() OVER(PARTITION BY YEAR(V.VisitDate), V.BranchUID ORDER BY V.VisitDate) AS YearBranchVisitCount
FROM
	[MysteryDiningCopy].dbo.Visit V
	INNER JOIN [MysteryDiningCopy].dbo.BatchedVisitScores bvs ON V.VisitUID = bvs.VisitUID
WHERE
	v.VisitDate < '2020-01-01'
AND 
	VisitDate > (SELECT [LastUpdated] 
				FROM MysteryDiningDW.dbo.ConfigInfo
				WHERE [Key] = 'FactVisitResultEarliestRecord')

GO
