SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vFactAdvocacyTwitterDW]
AS
SELECT
	*
FROM
	FactAdvocacyTwitter
WHERE
	AdvocacyTwitterID IN
	(
		SELECT
			AdvocacyTwitterID
		FROM
			vFactAdvocacyTwitterJoined
	)
	

-- Test Case
-- SELECT * FROM vFactAdvocacyTwitterDW

GO
