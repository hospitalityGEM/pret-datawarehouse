
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vFactDepartmentResultDW]
AS
SELECT TOP 100 PERCENT
	   [DepartmentResultKey]
      ,[VisitKey]
      ,[BranchKey]
      ,[GeographyKey]
      ,[PeriodKey]
      ,[VisitDateKey]
      ,[AssessorKey]
      ,[Questionnaire]
      ,[SubQuestionnaire]
      ,[Department]
      ,[Score]
      ,[MaxScore]
      ,[VisitScore]
      ,[VisitMaxScore]
      ,[VisitScoreMethod]
      ,[IncludeInAnalysis]
      ,[ValidFromDate]
      ,[ValidToDate]
      ,[Active]
      ,[Source]
      ,[VisitID]
      ,[DepartmentID]
      ,[TimeStamp]
      ,[ChangeReason]
  FROM 
		[FactDepartmentResult]
		INNER JOIN MysteryDiningDW.dbo.StagingLegacyVisitIDs slv ON [FactDepartmentResult].VisitID		= slv.VisitUID AND slv.ConfigInfoID = 9
  WHERE
	[Source] = 'Legacy'
AND [Active] = 1

ORDER BY
	VisitID
,	DepartmentID

GO
