SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vFactAdvocacyFacebookJoined]
AS
SELECT
	dv.CurrentScore								AS VisitScore
,	dv.CurrentMaxScore							AS VisitMaxScore
,	df.CompletedDateKey							AS CompletionDateKey
,	dv.VisitDateKey								AS VisitDateKey
,	dv.BranchKey								AS BranchKey
,	dv.PeriodKey								AS PeriodKey
,	dr.RecommendationKey						AS RecommendationKey
,	CONVERT(NVARCHAR(10),
	CASE ofrf.fbExistingBrandSubscriber
		WHEN 1 THEN 'Yes'
		WHEN 0 THEN 'No'
		ELSE 'N/A'
	END)										AS LikedBefore
,	CONVERT(NVARCHAR(10),
	CASE 
		WHEN ofrf.fbConvertedBrandSubscriber = 1	THEN 'Yes'
		WHEN ofrf.fbExistingBrandSubscriber = 1		THEN 'Yes'
		ELSE 'No'
	END)										AS LikedAfter
,	CONVERT(NVARCHAR(10),
	CASE ofrf.fbConvertedBrandSubscriber
		WHEN 1 THEN 'Yes'
		WHEN 0 THEN 'No'
		ELSE 'N/A'
	END)										AS ConvertedToLiker
,	CONVERT(NVARCHAR(100),
	CASE 
		WHEN LEN(ofrf.fbUserName) = 0 THEN 'N/A'
		ELSE ofrf.fbUserName
	END)										AS UserAccount
,	CONVERT(NVARCHAR(4000),
	CASE
		WHEN LEN(ofrf.fbPost) = 0 THEN 'N/A'
		ELSE ofrf.fbPost
	END)										AS PostedMessage
,	CONVERT(NVARCHAR(500),
	CASE
		WHEN LEN(ofrf.fbMktgMessageUsed) = 0 THEN 'N/A'
		ELSE ofrf.fbMktgMessageUsed
	END)										AS MarketingMessage
,	CASE 
		WHEN ofrf.fbFriendCount < 1 THEN NULL
		ELSE ofrf.fbFriendCount 
	END											AS Friends
,	CONVERT(NVARCHAR(10),
	CASE ofrf.fbPosted
		WHEN 1 THEN 'Yes'
		WHEN 0 THEN 'No'
		ELSE 'N/A'
	END)										AS Posted
,	CONVERT(NVARCHAR(10),
	CASE ofrf.IsPrimary
		WHEN 1 THEN 'Advocate'
		WHEN 0 THEN 'Friend'
		ELSE 'Unknown'
	END)										AS Advocate
,	ofrs.AnalysisName							AS RecommendationStatus
,	ISNULL(dgm.GuestMarketingKey, 0)			AS GuestMarketingKey
,	ISNULL(dsc.SocialChannelKey,0)				AS SocialChannelKey
,	CONVERT(NVARCHAR(10), 'Legacy')				AS [Source]		
,	ofrf.OnlineFeedbackRecommendationFacebookUID	AS AdvocacyFacebookID
FROM
	MysteryDiningCopy.dbo.OnlineFeedbackRecommendationFacebook ofrf
	INNER JOIN MysteryDiningCopy.dbo.OnlineFeedbackRecommendation ofr ON ofrf.OnlineFeedbackRecommendationUID = ofr.OnlineFeedbackRecommendationUID
	INNER JOIN MysteryDiningCopy.dbo.OnlineFeedbackDetails		ofd ON ofr.OnlineFeedbackDetailsUID = ofd.OnlineFeedbackDetailsUID
	INNER JOIN MysteryDiningCopy.dbo.OnlineFeedbackRecommendationStatus ofrs ON ofr.StatusUID = ofrs.OnlineFeedbackRecommendationStatusUID
	
	LEFT JOIN MysteryDiningDW.dbo.DimSocialChannel dsc ON ofrf.fbClientId = dsc.SocialChannelID AND dsc.Active = 1 AND dsc.SocialNetwork = 'Facebook'
	LEFT JOIN MysteryDiningDW.dbo.DimRecommendation dr ON ofr.OnlineFeedbackRecommendationUID = dr.RecommendationID AND dr.Active = 1
	LEFT JOIN MysteryDiningDW.dbo.DimVisit			dv ON ofd.VisitUID = dv.VisitID AND dv.Active = 1
	LEFT JOIN MysteryDiningDW.dbo.DimFeedback		df ON ofd.VisitUID = df.FeedbackID
	LEFT JOIN MysteryDiningDW.dbo.DimGuestMarketing dgm ON ofd.VisitUID = dgm.GuestMarketingID 
														AND ofrf.OnlineFeedbackRecommendationReturnVisitorUID = dgm.GuestMarketingDetailID

	INNER JOIN MysteryDiningDW.dbo.ConfigInfo ci ON ci.ConfigInfoID = 27 AND ci.LastUpdated < ofrf.[DateTimeAdded]

-- Test Case
-- SELECT * FROM vFactAdvocacyFacebookJoined
GO
