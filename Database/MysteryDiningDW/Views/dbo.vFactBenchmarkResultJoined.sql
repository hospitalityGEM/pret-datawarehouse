
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE VIEW [dbo].[vFactBenchmarkResultJoined]
AS
SELECT
	VisitUID
,	SUM(Score)		AS Score
,	SUM(MaxScore)	AS MaxScore
,	CASE BenchmarkID	 
		WHEN 'B1' THEN 'Greeting' 
		WHEN 'B2' THEN 'Upsold Water' 
		WHEN 'B3' THEN 'Upsold Dishes' 
		WHEN 'B4' THEN 'Second Drinks' 
		WHEN 'B5' THEN 'Checkback' 
		WHEN 'B6' THEN 'Manager Presence' 
		WHEN 'B7' THEN 'Return/Recommend'
		WHEN 'B8' THEN 'Goodbye' 
		WHEN 'B9' THEN 'Warmth' 
		WHEN 'B10' THEN 'Product Knowledge'
		WHEN 'B11' THEN 'Food Quality'
		WHEN 'B12' THEN 'Bill Payment' 
		WHEN 'B13' THEN 'Pace Of service' 
		WHEN 'B14' THEN 'Telephone Enquiry' 
		WHEN 'B15' THEN 'Value For Money' 
		WHEN 'B16' THEN 'Cleanliness' 
		WHEN 'B17' THEN 'Toilet Standards'
		WHEN 'B18' THEN 'Upsold Coffee/Desserts' 
		WHEN 'B19' THEN 'Coffee Quality' 
	ELSE
		Benchmark
	END AS Benchmark
,	CONVERT(NVARCHAR(50),
	CASE 
		WHEN Benchmark = 'Customer Loyalty Score' AND CONVERT(INT,SUM(Score)) <= 6 THEN 'Demoter'  
		WHEN Benchmark = 'Customer Loyalty Score' AND CONVERT(INT,SUM(Score)) >6 AND CONVERT(INT,SUM(Score)) <9 THEN 'Neutral'
		WHEN Benchmark = 'Customer Loyalty Score' AND CONVERT(INT,SUM(Score)) >= 9 THEN 'Promoter' 
		ELSE 'N/A'
	END)			AS CustomerLoyalty
,	VisitKey
,	PeriodKey
,	BranchKey
,	AssessorKey
,	VisitDateKey
,	Questionnaire
,	SubQuestionnaire
,	VisitScore
,	VisitMaxScore
,	VisitScoreMethod
,	IncludeInAnalysis
,	GeographyKey
,	[Source]
,	BenchmarkID
FROM
	(
		SELECT
			A.VisitUID
		,	(CASE
				WHEN (SUM(A.[Weight]) <> 10 AND B.BenchmarkUID = 'GWR' AND ISNUMERIC(A.[Answer]) = 1) THEN CONVERT(DECIMAL(18,2),A.Answer)
				ELSE CONVERT(DECIMAL(18,2),SUM(A.Score))
			END)														AS Score
		,	(CASE
				WHEN (SUM(A.[Weight]) <> 10 AND B.BenchmarkUID = 'GWR' AND ISNUMERIC(A.[Answer]) = 1) THEN CONVERT(DECIMAL(18,2),10)
				ELSE CONVERT(DECIMAL(18,2),SUM(CASE WHEN a.Answer='Na' then 0 else A.[Weight] end))
			END)														AS MaxScore
		,	CONVERT(NVARCHAR(100),B.BenchmarkType)						AS Benchmark
		,	dV.VisitKey
		,	dV.PeriodKey
		,	dV.BranchKey
		,	dV.AssessorKey
		,	dV.VisitDateKey
		,	dV.Questionnaire
		,	dV.SubQuestionnaire
		,	dV.CurrentScore												AS VisitScore
		,	dV.CurrentMaxScore											AS VisitMaxScore
		,	dV.VisitScoreMethod											AS VisitScoreMethod
		,	cast(IIF(dP.IncludeInAnalysis = 'Exclude' OR qc.ExcludeFromAnalysis = 1, 'Exclude','Include') as nvarchar(10)) as IncludeInAnalysis
		,	dB.GeographyKey
		,	CONVERT(NVARCHAR(10),'Legacy')								AS [Source]
		,	B.BenchmarkUID												AS BenchmarkID
		FROM
			MysteryDiningCopy.dbo.Answer A
			INNER JOIN MysteryDiningCopy.dbo.Question Q ON A.QuestionUID				= Q.QuestionUID
			INNER JOIN MysteryDiningCopy.dbo.Visit v	ON A.VisitUID					= v.VisitUID
			INNER JOIN MysteryDiningCopy.dbo.Benchmark B ON Q.BenchmarkUID				= B.BenchmarkUID  
			INNER JOIN MysteryDiningDW.dbo.DimVisit dV	ON A.VisitUID					= dV.VisitID AND dV.[Source] = 'Legacy' AND dV.Active = 1
			INNER JOIN MysteryDiningDW.dbo.DimPeriod dP ON dV.PeriodKey					= dP.PeriodKey
			INNER JOIN MysteryDiningDW.dbo.DimBranch dB ON dV.BranchKey					= dB.BranchKey
		    INNER JOIN MysteryDiningDW.dbo.StagingLegacyVisitIDs slv ON v.VisitUID		= slv.VisitUID AND slv.ConfigInfoID = 12
			INNER JOIN MysteryDiningCopy.dbo.Questionnaire qn on v.QuestionnaireUID		= qn.QuestionnaireUID
			LEFT JOIN MysteryDiningCopy.dbo.QuestionnaireCategories qc on qn.CategoryUID = qc.CategoryUID

		WHERE
				
				 VisitDate < GETDATE()
			AND dB.ClientID <> 35418
			

		GROUP BY
			A.VisitUID
		,	dV.VisitKey
		,	dV.PeriodKey
		,	dV.BranchKey
		,	dV.AssessorKey
		,	dV.VisitDateKey
		,	dV.Questionnaire
		,	dV.SubQuestionnaire
		,	dV.VisitScoreText
		,	dV.VisitScoreMethod
		,	dB.GeographyKey
		,	B.BenchmarkType
		,	dV.CurrentMaxScore
		,	dV.CurrentScore
		,	B.BenchmarkUID
		,	A.Answer
		,	IIF(dP.IncludeInAnalysis = 'Exclude' OR qc.ExcludeFromAnalysis = 1, 'Exclude','Include')
	) PreGroup
GROUP BY
	VisitUID
,	Benchmark
,	VisitKey
,	PeriodKey
,	BranchKey
,	AssessorKey
,	VisitDateKey
,	Questionnaire
,	SubQuestionnaire
,	VisitScore
,	VisitMaxScore
,	VisitScoreMethod
,	IncludeInAnalysis
,	GeographyKey
,	[Source]
,	BenchmarkID


-- Test Case
-- SELECT * FROM vFactBenchmarkResultJoined 







GO
