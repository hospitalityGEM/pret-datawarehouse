SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vDimGeography-BranchJoined]
AS
SELECT [BranchUID]
      ,[Address1]
      ,[Address2]
      ,[Address3]
      ,[Address4]
      ,[Address5]
      ,[Postcode]
      ,[Location]
      ,[RegionUID]
      ,[GeocodeX]
      ,[GeocodeY]
	  ,CONVERT(NVARCHAR(50), 'Branch')	AS TableSource
	  ,CONVERT(NVARCHAR(50), 'Legacy')	AS [Source]
  FROM MysteryDiningCopy.dbo.[Branch]
GO
