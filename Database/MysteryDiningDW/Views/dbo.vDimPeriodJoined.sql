
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vDimPeriodJoined]
AS
SELECT TOP 100 PERCENT
	p.PeriodBriefUID
,	c.Name
,	p.PeriodName
,	ISNULL(dds.DateKey, 19000101) AS StartDateKey
,	ISNULL(dde.DateKey, 19000101) AS EndDateKey
,	ISNULL(brands.Brand, c.Name) AS Brand
,	c.ClientUID
,	ISNULL(brands.BrandUID, 0) AS StagingBrandID
,	p.ExcludeFromAnalysis
,	p.ExcludeFromOnlinePortal
,	CASE
		WHEN CONVERT(NVARCHAR(20),ISNULL(visittype.VisitType, 'Unknown')) = 'Un' THEN 'Unknown'
		ELSE CONVERT(NVARCHAR(20),ISNULL(visittype.VisitType, 'Unknown'))
	END AS VisitType
,	ISNULL(py.PeriodYearName ,CONVERT(NVARCHAR(50),YEAR(p.StartDate))) AS [Year]
,	CONVERT(NVARCHAR(50),ISNULL(pt.PeriodTypeName, 'Standard')) AS PeriodType
,	ISNULL(convert(int,convert(varchar(10),py.PeriodYearStart,112)),YEAR(p.StartDate)*10000+101) AS YearStartKey
,	ISNULL(convert(int,convert(varchar(10),py.PeriodYearEnd,112)),YEAR(p.StartDate)*10000+1231) AS YearEndKey


FROM
	MysteryDiningCopy.dbo.ClientPeriodBrief p
INNER JOIN MysteryDiningCopy.dbo.Client c ON p.ClientUID = c.ClientUID
LEFT JOIN MysteryDiningCopy.dbo.ClientPeriodYear py ON p.PeriodYearUID = py.PeriodYearUID
LEFT JOIN MysteryDiningCopy.dbo.PeriodType pt ON p.PeriodTypeUID = pt.PeriodTypeUID
LEFT JOIN MysteryDiningDW.dbo.DimDate dds ON CONVERT(DATETIME,CONVERT(DATE,p.StartDate)) = dds.[Date]
LEFT JOIN MysteryDiningDW.dbo.DimDate dde ON CONVERT(DATETIME,CONVERT(DATE,p.EndDate)) = dde.[Date]
LEFT JOIN
	(
	SELECT DISTINCT
		cpb.PeriodBriefUID	
	,	br.BrandUID
	,	b.BrandName AS Brand
	FROM
		MysteryDiningCopy.dbo.ClientPeriodBrief cpb
		INNER JOIN MysteryDiningCopy.dbo.Visit v ON cpb.PeriodBriefUID = v.Period
		INNER JOIN MysteryDiningCopy.dbo.Branch br ON v.BranchUID = br.BranchUID
		LEFT JOIN MysteryDiningCopy.dbo.Brand b ON br.BrandUID = b.BrandUID
	) brands ON p.PeriodBriefUID = brands.PeriodBriefUID
LEFT JOIN
	(
	SELECT
		v.Period
	,	CASE qu.VisitType
			WHEN 'MV' THEN 'Mystery Visit'
			WHEN 'OF' THEN 'Online Feedback'
			WHEN 'CC' THEN 'Comment Card'
			WHEN 'RC' THEN 'Real Customer'
			ELSE 'Unknown'
		END AS VisitType
	FROM
		MysteryDiningCopy.dbo.Visit v
		INNER JOIN MysteryDiningCopy.dbo.Questionnaire qu ON v.QuestionnaireUID = qu.QuestionnaireUID
	WHERE
		v.VisitUID IN (	
						SELECT
							MIN(v.VisitUID)
						FROM
							MysteryDiningCopy.dbo.Visit v
						GROUP BY
							v.Period
						)
	) visittype ON p.PeriodBriefUID = visittype.Period
ORDER BY
	p.PeriodBriefUID




GO
