SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vDimAdmin]
AS
SELECT
	UserUID
,	Name
FROM         
	MysteryDiningCopy.dbo.[User]
WHERE
	UserType = 'A'
	

GO
