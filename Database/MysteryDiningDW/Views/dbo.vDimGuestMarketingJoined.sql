
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vDimGuestMarketingJoined]
AS
SELECT
	GuestMarketingDetailID
,	GuestMarketingID
,	CONVERT(NVARCHAR(20),
	ISNULL(PivotData.Title, N'N/A'))			AS Title
,	ISNULL(PivotData.FirstName, N'N/A')			AS FirstName
,	ISNULL(PivotData.Surname, N'N/A')			AS Surname
,	ISNULL(PivotData.FullName, N'N/A')			AS FullName
,	ISNULL(PivotData.[AddressLine1], N'N/A')	AS AddressLine1
,	ISNULL(PivotData.[AddressLine2], N'N/A')	AS AddressLine2
,	ISNULL(PivotData.City, N'N/A')				AS City
,	ISNULL(PivotData.County, N'N/A')			AS County
,	CONVERT(NVARCHAR(20),
	ISNULL(PivotData.Postcode, N'N/A'))			AS Postcode
,	CONVERT(NVARCHAR(50),
	ISNULL(PivotData.Age, N'N/A'))				AS Age
,	ISNULL(PivotData.Occupation, N'N/A')		AS Occupation
,	CONVERT(NVARCHAR(50),
	ISNULL(PivotData.Mobile, N'N/A'))			AS Mobile
,	ISNULL(LOWER(PivotData.Email), N'N/A')		AS Email
,	CONVERT(NVARCHAR(30),
	ISNULL(PivotData.Birthday, N'N/A'))			AS Birthday
,	CONVERT(NVARCHAR(50),
	ISNULL(PivotData.JoinMarketing, N'N/A'))	AS JoinMarketing
,	ISNULL(PivotData.ContactEmail, N'N/A')		AS ContactEmail
,	ISNULL(PivotData.ContactPost, N'N/A')		AS ContactPost
,	ISNULL(PivotData.ContactMobile, N'N/A')		AS ContactMobile
,	N'Legacy'									AS [Source]
FROM
	(	
		SELECT
			mo.OptionName
		,	vmo.Value	
		,	vmo.VisitUID	AS GuestMarketingID
		,	vmo.OnlineFeedbackReturnVisitorUID AS GuestMarketingDetailID

		FROM
			MysteryDiningCopy.dbo.VisitMarketingOption vmo
			INNER JOIN MysteryDiningCopy.dbo.MarketingOption mo ON vmo.MarketingOptionUID = mo.MarketingOptionUID
	) gd
PIVOT
	(
		MIN(gd.Value)
	FOR
		OptionName
			IN ([Title],
				[FirstName],
				[Surname],
				[FullName],
				[AddressLine1],
				[AddressLine2],
				[City],
				[County],
				[Postcode],
				[Age],
				[Occupation],
				[Mobile],
				[Email],
				[Birthday],
				[JoinMarketing],
				[ContactEmail],
				[ContactPost],
				[ContactMobile])
	) PivotData




GO
