
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE VIEW [dbo].[vFactQuestionResultJoined]
AS
SELECT        TOP (100) PERCENT 
		a.AnswerUID
,		a.Answer
,		CONVERT(DECIMAL(18, 2), a.Score) AS AnswerScore
,		CONVERT(DECIMAL(18, 2), a.Score) AS CurrentScore
,		CASE WHEN a.Answer = 'Na' THEN 0 ELSE CONVERT(DECIMAL(18, 2), a.[Weight]) END AS MaxScore
,		CONVERT(NVARCHAR(4000), a.Comments) AS Comments	
,		a.TimeStamp
,		CONVERT(NVARCHAR(100), SUBSTRING(f.Factor, 1, 1) + LOWER(SUBSTRING(f.Factor, 2, LEN(f.Factor)))) AS Factor
,		CONVERT(NVARCHAR(200),REPLACE(REPLACE(s.Name, CHAR(10), ''), CHAR(13), '')) AS Section
,		s.SectionNo
,		ISNULL(sc.SectionCategoryName, 'No Department') AS Department
,		CONVERT(NVARCHAR(100), ISNULL(CASE b.BenchmarkUID 
										 WHEN 'GWR' THEN 'Customer Loyalty Score'
										 WHEN 'B1' THEN 'Greeting' 
										 WHEN 'B2' THEN 'Upsold Water' 
										 WHEN 'B3' THEN 'Upsold Dishes' 
										 WHEN 'B4' THEN 'Second Drinks' 
										 WHEN 'B5' THEN 'Checkback' 
										 WHEN 'B6' THEN 'Manager Presence' 
										 WHEN 'B7' THEN 'Return/Recommend'
										 WHEN 'B8' THEN 'Goodbye' 
										 WHEN 'B9' THEN 'Warmth' 
										 WHEN 'B10' THEN 'Product Knowledge'
										 WHEN 'B11' THEN 'Food Quality'
										 WHEN 'B12' THEN 'Bill Payment' 
										 WHEN 'B13' THEN 'Pace Of service' 
										 WHEN 'B14' THEN 'Telephone Enquiry' 
										 WHEN 'B15' THEN 'Value For Money' 
										 WHEN 'B16' THEN 'Cleanliness' 
										 WHEN 'B17' THEN 'Toilet Standards'
										 WHEN 'B18' THEN 'Upsold Coffee/Desserts' 
										 WHEN 'B19' THEN 'Coffee Quality' 
										ELSE NULL END, 'No Benchmark')) AS Benchmark
,		ISNULL(c.CategoryName, 'No Category') AS ManagementCategory
,		q.QuestionNo
,		qr.Description AS Questionnaire
,		CONVERT(NVARCHAR(10), CASE WHEN qr.VisitType IN ('OF', 'MF') THEN 'Feedback' ELSE 'Assessment' END) AS FeedbackType
,		CONVERT(NVARCHAR(10), 'Legacy') AS Source, 0 AS CheckedByAdmin
,		CONVERT(NVARCHAR(10), 'Answer') AS TableSource, CONVERT(NVARCHAR(50), 'Generic') AS AssessmentArea
,		0 AS QQID
,		CONVERT(NVARCHAR(100),'No SubQuestionnaire') AS SubQuestionnaire
,		dq.QuestionKey
,		dv.VisitKey
,		dv.AssessorKey
,		dv.VisitDateKey
,		dv.BranchKey
,		dv.PeriodKey
,	cast(IIF(dP.IncludeInAnalysis = 'Exclude' OR qc.ExcludeFromAnalysis = 1, 'Exclude','Include') as nvarchar(10)) as IncludeInAnalysis
,		ad.DateKey AS AnsweredDateKey
,		db.GeographyKey
,		ISNULL(dd.DishKey, 0) AS DishKey
,		a.VisitUID AS VisitID
,		0 AS AnswerValue
,		CASE	WHEN a.[Weight] = 0 THEN CASE	WHEN q.MultiAnsType = 'YN' THEN 1 
												WHEN q.MultiAnsType IN ('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'TOT', 'TEN') AND ISNUMERIC(q.MultiAnswer1Text) = 1 THEN CONVERT(INT, q.MultiAnswer1Text) 
												END 
				ELSE a.[Weight] END AS AnswerMaxValue
,		s.IncludeScoreInTotal
FROM     MysteryDiningCopy.dbo.Answer AS a 
			INNER JOIN	(SELECT  (CASE WHEN (Substring(q.Question, 1, CHARINDEX(' ', q.Question))) IN ('WELCOME', 'RECOMMEND', 'PACE', 'INTEREST', 'CONFIDENCE', 
                                  'KNOWLEDGE', 'VALUE', 'WARMTH', 'TRAINING ') THEN (Substring(q.Question, 1, CHARINDEX(' ', q.Question))) ELSE 'NONE' END) AS Factor, 
									q.QuestionUID
						 FROM   MysteryDiningCopy.dbo.Question AS q INNER JOIN	MysteryDiningCopy.dbo.Section AS s ON q.SectionUID = s.SectionUID) AS f ON a.QuestionUID = f.QuestionUID 
			INNER JOIN			MysteryDiningCopy.dbo.Visit	  AS v			ON a.VisitUID = v.VisitUID 
			INNER JOIN			[MysteryDiningCopy].dbo.BatchedVisitScores bvs		ON V.VisitUID = bvs.VisitUID 
			INNER JOIN			MysteryDiningCopy.dbo.Question AS q			ON a.QuestionUID = q.QuestionUID 
			INNER JOIN			MysteryDiningCopy.dbo.Section AS s			ON q.SectionUID = s.SectionUID 
			LEFT OUTER JOIN		MysteryDiningCopy.dbo.SectionCategory AS sc ON s.SectionCategoryUID = sc.SectionCategoryUID 
			LEFT OUTER JOIN		MysteryDiningCopy.dbo.Benchmark AS b		ON q.BenchmarkUID = b.BenchmarkUID 
			LEFT OUTER JOIN		MysteryDiningCopy.dbo.Category AS c			ON q.QuestCatagory = c.CategoryUID 
			LEFT OUTER JOIN		MysteryDiningCopy.dbo.Questionnaire AS qr	ON s.QuestionnaireUID = qr.QuestionnaireUID 
			LEFT OUTER JOIN		dbo.DimQuestion AS dq						ON a.QuestionUID = dq.QuestionID AND dq.Active = 1 AND dq.Source = 'Legacy' 
			LEFT OUTER JOIN		dbo.DimVisit AS dv							ON a.VisitUID = dv.VisitID AND dv.Active = 1 AND dv.Source = 'Legacy'
			LEFT OUTER JOIN		dbo.DimPeriod AS dp							ON dv.PeriodKey = dp.PeriodKey 
			LEFT OUTER JOIN		dbo.DimDate AS ad							ON CONVERT(DATE, a.TimeStamp) = ad.Date 
			LEFT OUTER JOIN		dbo.DimBranch AS db							ON dv.BranchKey = db.BranchKey 
			LEFT OUTER JOIN		dbo.DimDish AS dd							ON a.DishUID = dd.DishID AND dd.Active = 1 AND dd.Source = 'Legacy' 
			INNER JOIN			dbo.StagingLegacyVisitIDs AS slv			ON v.VisitUID = slv.VisitUID AND slv.ConfigInfoID = 4
			INNER JOIN MysteryDiningCopy.dbo.Questionnaire qn on v.QuestionnaireUID		= qn.QuestionnaireUID
			LEFT JOIN MysteryDiningCopy.dbo.QuestionnaireCategories qc on qn.CategoryUID = qc.CategoryUID
WHERE        (a.TimeStamp IS NOT NULL) AND (v.VisitDate < GETDATE())
ORDER BY a.AnswerUID










GO

EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[15] 4[3] 2[63] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "a"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "v"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 393
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "q"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 400
               Right = 305
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 402
               Left = 38
               Bottom = 532
               Right = 246
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sc"
            Begin Extent = 
               Top = 534
               Left = 38
               Bottom = 647
               Right = 246
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "b"
            Begin Extent = 
               Top = 402
               Left = 284
               Bottom = 498
               Right = 459
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 498
               Left = 284
               Bottom = 628
               Right = 466
            End
            DisplayFlags = 280
            TopColumn = 0
         End
', 'SCHEMA', N'dbo', 'VIEW', N'vFactQuestionResultJoined', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'         Begin Table = "qr"
            Begin Extent = 
               Top = 648
               Left = 38
               Bottom = 778
               Right = 274
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dq"
            Begin Extent = 
               Top = 780
               Left = 38
               Bottom = 910
               Right = 249
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv"
            Begin Extent = 
               Top = 912
               Left = 38
               Bottom = 1042
               Right = 301
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dp"
            Begin Extent = 
               Top = 1044
               Left = 38
               Bottom = 1174
               Right = 233
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ad"
            Begin Extent = 
               Top = 1176
               Left = 38
               Bottom = 1306
               Right = 292
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "db"
            Begin Extent = 
               Top = 630
               Left = 284
               Bottom = 760
               Right = 465
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dd"
            Begin Extent = 
               Top = 1044
               Left = 271
               Bottom = 1174
               Right = 462
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "slv"
            Begin Extent = 
               Top = 780
               Left = 287
               Bottom = 876
               Right = 457
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "f"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 102
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vFactQuestionResultJoined', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vFactQuestionResultJoined', NULL, NULL
GO
