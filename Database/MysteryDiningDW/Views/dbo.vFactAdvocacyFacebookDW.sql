SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vFactAdvocacyFacebookDW]
AS
SELECT
	*
FROM
	FactAdvocacyFacebook
WHERE
	AdvocacyFacebookID IN
	(
		SELECT
			AdvocacyFacebookID
		FROM
			vFactAdvocacyFacebookJoined
	)
	

-- Test Case
-- SELECT * FROM vFactAdvocacyFacebookDW

GO
