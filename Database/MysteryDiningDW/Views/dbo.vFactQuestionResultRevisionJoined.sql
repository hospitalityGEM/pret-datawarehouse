SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vFactQuestionResultRevisionJoined]
AS
SELECT TOP 100 PERCENT
	a.AnswerRevisionUID
,	a.Answer
,	CONVERT(DECIMAL(18,2),a.Score)					AS AnswerScore
,	CONVERT(DECIMAL(18,2),a.Score)					AS CurrentScore
,	CASE
		WHEN a.Answer = 'Na' THEN 0
		ELSE CONVERT(DECIMAL(18,2),a.[Weight])	
	END												AS MaxScore
,	CONVERT(NVARCHAR(3000),a.Comments)				AS Comments
,	a.[TimeStamp]
,	CONVERT(NVARCHAR(100),(Substring(f.Factor,1,1) + Lower(Substring(f.Factor,2,Len(f.Factor))))) AS Factor
,	s.Name											AS Section
,	s.SectionNo										AS SectionNo
,	ISNULL(sc.SectionCategoryName, 'No Department')							AS Department
,	ISNULL(CONVERT(NVARCHAR(100), b.BenchmarkType), 'No Benchmark')			AS Benchmark
,	ISNULL(c.CategoryName, 'No Category')									AS ManagementCategory
,	q.QuestionNo									AS QuestionNo
,	qr.[Description]								AS Questionnaire
,	CONVERT(NVARCHAR(10),CASE
		WHEN qr.VisitType IN ('OF','MF') THEN 'Feedback'
		ELSE 'Assessment'
	END)											AS FeedbackType
,	CONVERT(NVARCHAR(10),'Legacy')					AS [Source]
,	0												AS CheckedByAdmin
,	CONVERT(NVARCHAR(10),'Answer')					AS TableSource
,	CONVERT(NVARCHAR(50),'Generic')					AS AssessmentArea
,	0												AS QQID
,	CONVERT(NVARCHAR(100),'No SubQuestionnaire')	AS SubQuestionnaire
,	dq.QuestionKey
,	dv.VisitKey
,	dv.AssessorKey
,	dv.VisitDateKey
,	dv.BranchKey
,	dv.PeriodKey
,	dp.IncludeInAnalysis
,	ad.DateKey										AS AnsweredDateKey
,	db.GeographyKey
,	ISNULL(dd.DishKey, 0)							AS DishKey
,	a.VisitUID										AS VisitID
--,	CASE
--		WHEN a.[Weight] = 0
--			THEN
--				CASE
--					WHEN ISNUMERIC(a.Answer) = 1 AND LEN(a.Answer) < 5
--						THEN CONVERT(DECIMAL(18,2),a.Answer)
--					WHEN q.MultiAnsType = 'YN'AND a.Answer = 'Yes'
--						THEN 1
--					WHEN q.MultiAnsType = 'YN'AND a.Answer = 'No'
--						THEN 0
--					ELSE a.Score
--				END	
--		ELSE
--			a.Score
--	END												AS AnswerValue
, 0 AS AnswerValue
,	CASE
		WHEN a.[Weight] = 0
			THEN
				CASE
					WHEN q.MultiAnsType = 'YN'
						THEN 1
					WHEN q.MultiAnsType IN ('1','2','3','4','5','6','7','8','9','10','TOT','TEN') AND ISNUMERIC(q.MultiAnswer1Text) = 1
						THEN CONVERT(INT,q.MultiAnswer1Text)
				END
	ELSE
		a.[Weight]
	END												AS AnswerMaxValue

FROM         
	[MysteryDiningCopy].dbo.AnswerRevision a
	INNER JOIN
      (
      SELECT
        (CASE WHEN (Substring(q.Question,1, CHARINDEX(' ', q.Question))) IN ('WELCOME','RECOMMEND','PACE','INTEREST','CONFIDENCE','KNOWLEDGE','VALUE','WARMTH','TRAINING ')
        THEN (Substring(q.Question,1, CHARINDEX(' ', q.Question)))
        ELSE 'NONE'
        END) AS Factor
       ,	q.QuestionUID
		FROM
        MysteryDiningCopy.dbo.Question q
        INNER JOIN MysteryDiningCopy.dbo.Section s ON q.SectionUID = s.SectionUID
       ) f ON a.QuestionUID = f.QuestionUID
    INNER JOIN MysteryDiningCopy.dbo.Visit v			ON a.VisitUID			= v.VisitUID
    INNER JOIN MysteryDiningCopy.dbo.Question q			ON a.QuestionUID		= q.QuestionUID
    INNER JOIN MysteryDiningCopy.dbo.Section s			ON q.SectionUID			= s.SectionUID
    LEFT JOIN MysteryDiningCopy.dbo.SectionCategory sc	ON s.SectionCategoryUID = sc.SectionCategoryUID
    LEFT JOIN MysteryDiningCopy.dbo.Benchmark b			ON q.BenchmarkUID		= b.BenchmarkUID
    LEFT JOIN MysteryDiningCopy.dbo.Category c			ON q.QuestCatagory		= c.CategoryUID
    LEFT JOIN MysteryDiningCopy.dbo.Questionnaire qr	ON s.QuestionnaireUID	= qr.QuestionnaireUID
    LEFT JOIN MysteryDiningDW.dbo.DimQuestion dq		ON a.QuestionUID		= dq.QuestionID AND dq.Active = 1 AND dq.[Source] = 'Legacy'
    LEFT JOIN MysteryDiningDW.dbo.DimVisit dv			ON a.VisitUID			= dv.VisitID AND dv.Active = 1 AND dv.[Source] = 'Legacy'
    LEFT JOIN MysteryDiningDW.dbo.DimPeriod dp			ON dv.PeriodKey			= dp.PeriodKey
    LEFT JOIN MysteryDiningDW.dbo.DimDate ad			ON CONVERT(DATE,a.[TimeStamp])	= ad.[Date]
    LEFT JOIN MysteryDiningDW.dbo.DimBranch db			ON dv.BranchKey			= db.BranchKey
    LEFT JOIN MysteryDiningDW.dbo.DimDish dd			ON a.DishUID			= dd.DishID AND dd.Active = 1 AND dd.[Source] = 'Legacy'
    INNER JOIN MysteryDiningDW.dbo.StagingLegacyVisitIDs slv ON v.VisitUID		= slv.VisitUID AND slv.ConfigInfoID = 4

    
WHERE
	--v.VisitUID IN 
	--(SELECT DISTINCT VisitUID FROM VisitUpdate WHERE [TimeStamp] >
	--(SELECT [LastUpdated] 
	--				FROM MysteryDiningDW.dbo.ConfigInfo
	--				WHERE ConfigInfoID = 4))
	--AND 
	a.[TimeStamp] IS NOT NULL
	AND v.VisitDate < GETDATE()
ORDER BY
	a.AnswerRevisionUID


GO
