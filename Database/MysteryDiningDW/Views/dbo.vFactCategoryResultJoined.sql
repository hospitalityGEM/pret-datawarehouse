SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE VIEW [dbo].[vFactCategoryResultJoined]
AS
SELECT
	A.VisitUID
,	CONVERT(DECIMAL(18,2),SUM(A.Score))							AS Score
,	CONVERT(DECIMAL(18,2),SUM(case when A.Answer='Na' then 0 else A.[Weight] end))	AS MaxScore
,	C.CategoryName												AS Category
,	dV.VisitKey
,	dV.PeriodKey
,	dV.BranchKey
,	dV.AssessorKey
,	dV.VisitDateKey
,	dV.Questionnaire
,	dV.SubQuestionnaire
,	dV.CurrentScore												AS VisitScore
,	dV.CurrentMaxScore											AS VisitMaxScore
,	dV.VisitScoreMethod											AS VisitScoreMethod
,	cast(IIF(dP.IncludeInAnalysis = 'Exclude' OR qc.ExcludeFromAnalysis = 1, 'Exclude','Include') as nvarchar(10)) as IncludeInAnalysis
,	dB.GeographyKey
,	CONVERT(NVARCHAR(10),'Legacy')								AS [Source]
,	C.CategoryUID												AS CategoryID
FROM
	MysteryDiningCopy.dbo.Answer A
	INNER JOIN MysteryDiningCopy.dbo.Question Q ON A.QuestionUID				= Q.QuestionUID
    INNER JOIN MysteryDiningCopy.dbo.Visit v	ON A.VisitUID					= v.VisitUID
    INNER JOIN MysteryDiningCopy.dbo.Category C ON Q.QuestCatagory				= C.CategoryUID  
    LEFT  JOIN MysteryDiningDW.dbo.DimVisit dV	ON A.VisitUID					= dV.VisitID AND dV.[Source] = 'Legacy' AND dV.Active = 1
    LEFT  JOIN MysteryDiningDW.dbo.DimPeriod dP ON dV.PeriodKey					= dP.PeriodKey
    LEFT  JOIN MysteryDiningDW.dbo.DimBranch dB ON dV.BranchKey					= dB.BranchKey
    INNER JOIN MysteryDiningDW.dbo.StagingLegacyVisitIDs slv ON v.VisitUID		= slv.VisitUID AND slv.ConfigInfoID = 10
	INNER JOIN MysteryDiningCopy.dbo.Questionnaire qn on v.QuestionnaireUID		= qn.QuestionnaireUID
	LEFT JOIN MysteryDiningCopy.dbo.QuestionnaireCategories qc on qn.CategoryUID = qc.CategoryUID
WHERE
		
	 VisitDate < GETDATE()
	--AND v.VisitUID IN 
	--(SELECT DISTINCT VisitUID FROM MysteryDiningCopy.dbo.VisitUpdate WHERE [TimeStamp] >
	--(SELECT [LastUpdated] 
	--				FROM MysteryDiningDW.dbo.ConfigInfo
	--				WHERE ConfigInfoID = 10))
GROUP BY
	A.VisitUID
,	dV.VisitKey
,	dV.PeriodKey
,	dV.BranchKey
,	dV.AssessorKey
,	dV.VisitDateKey
,	dV.Questionnaire
,	dV.SubQuestionnaire
,	dV.VisitScoreText
,	dV.VisitScoreMethod
,	dB.GeographyKey
,	C.CategoryName
,	dV.CurrentMaxScore
,	dV.CurrentScore
,	C.CategoryUID
,	IIF(dP.IncludeInAnalysis = 'Exclude' OR qc.ExcludeFromAnalysis = 1, 'Exclude','Include')

-- Test Case
-- SELECT * FROM vFactCategoryResultJoined ORDER BY VisitUID, CategoryID





GO
