SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vDimGeography-Branch]
AS
SELECT [BranchUID]
      ,[Address1]
      ,[Address2]
      ,[Address3]
      ,[Address4]
      ,[Address5]
      ,[Postcode]
      ,[Location]
      ,[RegionUID]
      ,[GeocodeX]
      ,[GeocodeY]
  FROM MysteryDiningCopy.dbo.[Branch]
GO
