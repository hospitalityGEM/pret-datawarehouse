SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vDimVisit]
AS

SELECT
	v.VisitUID
,	v.DinerUID
,	v.VisitDate
,	CONVERT(DATETIME, '1900-01-01 ' + CAST(DATEPART([HOUR], v.[Time]) AS NVARCHAR(2)) + ':' + CAST(DATEPART([MINUTE], v.[Time]) AS NVARCHAR(2)) + ':00.000') AS [Time]
,	v.QuestionnaireUID
,	v.VisitStatusUID
,	v.Adults
,	v.Children
,	v.ChildrenNotEating
,	v.ReceiptNo
,	v.TradeLevelUID
,	v.SeatingLevel
,   v.WeatherUID
,	v.Comments
,	v.AdditionalComments
,	v.DateAdded
,	v.DateAvailableTo
,	v.DateAvailableFr
,	v.ReimbursementDate
,	v.[Owner]
,	v.Period
,	v.DateAllocated
,	v.BranchUID
,	CONVERT(NVARCHAR(50),ISNULL(pf.PassFail, 'N/A'))			AS PassFail
,	CONVERT(NVARCHAR(300),ISNULL(pf.PassFailMessage, 'N/A'))	AS PassFailMessage
,	ISNULL(CASE 
		WHEN (bvs.MaximumScore IS NULL) OR (bvs.MaximumScore = 0) THEN 0 
		ELSE CONVERT(DECIMAL(18,2),CONVERT(INT, ROUND(CONVERT(DECIMAL(20,4), bvs.Score) /CONVERT(DECIMAL(20,4), bvs.MaximumScore) * 100, 0))) 
	END,0) AS CurrentVisitScore
,	CONVERT(DECIMAL(20,4), bvs.Score)			AS CurrentScore
,	CONVERT(DECIMAL(20,4), bvs.MaximumScore)	AS CurrentMaxScore
,	ISNULL(slt.VisitScoreText,'Error')											AS VisitScoreText
,	CONVERT(NVARCHAR(50),MysteryDiningCopy.dbo.fnVisitScoreMethod(v.VisitUID))	AS VisitScoreMethod
FROM         
	MysteryDiningCopy.dbo.Visit v
	LEFT JOIN MysteryDiningCopy.dbo.BatchedVisitScores bvs ON v.VisitUID = bvs.VisitUID
	LEFT JOIN MysteryDiningCopy.dbo.PassFailMeta pf ON v.VisitUID = pf.VisitUID
    INNER JOIN MysteryDiningDW.dbo.StagingLegacyVisitIDs slv ON v.VisitUID		= slv.VisitUID AND slv.ConfigInfoID = 5
    LEFT  JOIN MysteryDiningDW.dbo.StagingLegacyVisitScoreText slt ON v.VisitUID	= slt.VisitUID

WHERE
	v.VisitDate IS NOT NULL
AND
	v.VisitDate < '2020-01-01'
			


GO
