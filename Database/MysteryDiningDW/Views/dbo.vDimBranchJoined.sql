
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE VIEW [dbo].[vDimBranchJoined]
AS
SELECT        TOP (100) PERCENT 
		b.BranchUID
,		b.ClientUID, rtrim(ltrim(b.Name)) as Name
,		CASE	WHEN cr.Name = 'Na' THEN 'N/A' 
				WHEN cr.Name = 'Na' THEN 'N/A' 
				ELSE rtrim(ltrim(ISNULL(cr.Name, 'Unknown'))) 
				END AS Area
,		CONVERT(NVARCHAR(50), ISNULL(bs.Status, 'Unknown')) AS BranchStatus
,		CASE	WHEN nss.NatSkillSector = 'Na' THEN 'N/A'
				WHEN nss.NatSkillSector = 'N/a' THEN 'N/A' 
				ELSE ISNULL(nss.NatSkillSector, 'Unknown') 
                END AS IndustrySector
,		CASE	WHEN nss.NatSkillSector = 'Na' THEN 'N/A' 
				WHEN nss.NatSkillSector = 'N/a' THEN 'N/A' 
				ELSE ISNULL(nss.NatSkillSector, 'Unknown') 
				END AS AssessmentType, ISNULL(s.SegmentName, 'No Segment') AS ClientSegment
,		rtrim(ltrim(ISNULL(bd.BrandName, c.Name))) AS Brand
,		CONVERT(NVARCHAR(50), CASE WHEN ISNULL(bd.Active, 0) = 0 THEN 'Inactive' ELSE 'Active' END) AS BrandStatus
,		ISNULL(bd.BrandUID, 0) AS BrandUID
,		rtrim(ltrim(c.Name)) AS Client
,		CONVERT(NVARCHAR(50), 
          CASE	WHEN c.StatusUID = 'H' THEN 'Hold' 
				WHEN c.StatusUID = 'T' THEN 'Test' 
				WHEN c.StatusUID = 'R' THEN 'Retired' 
				WHEN c.StatusUID = 'L' THEN 'Live' 
				ELSE 'Unknown' END) AS ClientStatus
,		CASE	WHEN pr.Name = 'Na' THEN 'N/A' 
				WHEN pr.Name = 'Na' THEN 'N/A' 
				ELSE rtrim(ltrim(ISNULL(pr.Name, 'Unknown'))) END AS Region
,		ISNULL(dg.GeographyKey, 0) AS GeographyKey
,		CASE	WHEN LEN(RTRIM(b.[BranchNo])) = 0 THEN 'N/A'
				 ELSE ISNULL(b.[BranchNo], 'N/A') 
                 END AS BranchCode
,		ISNULL(u.Name,'Unknown') AS AccountManager
,		c.LowThreshold
,		c.HighThreshold
,		c.PeriodPerformanceGraphTarget AS Target
,		ISNULL(b.Location, 'Unknown') as Location
,		ISNULL(b.Tel,'Unknown') as Telephone
,		CONVERT(NVARCHAR(10), 'Legacy') AS Source
,		ISNULL(r.Description,'Unknown') as OurLocation
,		r.IsInternational as IsInternational
,		LTRIM(RTRIM(ISNULL(at.Name,'Unknown'))) as AgreementType
,		LTRIM(RTRIM(ISNULL(sb.Name,'Unknown'))) as SubBrand
,		LTRIM(RTRIM(ISNULL(nb.NBDM,'Unknown'))) as NBArea
,		LTRIM(RTRIM(ISNULL(nb.NBROD,'Unknown'))) as NBRegion
,		cast(convert(varchar(8),nb.NBStartDate,112) as int) as NBStartDateKey
,		cast(convert(varchar(8),nb.NBEndDate,112) as int) as NBEndDateKey
,		cast(IIF(b.BranchStatusUID<>'L' and b.ClientUID=70589,1,0) as bit) as ClosedBranch

FROM    MysteryDiningCopy.dbo.Branch AS b 
		LEFT OUTER JOIN		MysteryDiningCopy.dbo.Segment			AS s	ON b.SegmentUID = s.SegmentUID 
		LEFT OUTER JOIN		MysteryDiningCopy.dbo.Brand				AS bd	ON b.BrandUID = bd.BrandUID 
		LEFT OUTER JOIN		MysteryDiningCopy.dbo.ClientRegion		AS cr	ON b.ClientRegionUID = cr.ClientRegionUID 
		LEFT OUTER JOIN		MysteryDiningCopy.dbo.ParentRegion		AS pr	ON cr.ParentRegionUID = pr.ParentRegionUID 
		LEFT OUTER JOIN		MysteryDiningCopy.dbo.BranchStatus		AS bs	ON b.BranchStatusUID = bs.BranchStatusUID 
		LEFT OUTER JOIN		MysteryDiningCopy.dbo.NatSkillSector	AS nss	ON b.SSSectorUID = nss.NatSkillSectorUID 
		LEFT OUTER JOIN		dbo.DimGeography						AS dg	ON b.BranchUID = dg.SourceID AND dg.Source = 'Legacy' AND dg.TableSource = 'Branch' AND dg.Active = 1
		INNER JOIN			MysteryDiningCopy.dbo.Client			AS c	ON b.ClientUID = c.ClientUID 
		LEFT OUTER JOIN		MysteryDiningCopy.dbo.[User]			AS u	ON u.UserUID = c.AccountManagerUID
		LEFT OUTER JOIN     MysteryDiningCopy.dbo.Region            AS r    ON b.RegionUID=r.RegionUID
	    LEFT OUTER JOIN     MysteryDiningCopy.dbo.AgreementType     AS at   ON b.AgreementTypeUID=at.AgreementTypeUID
		LEFT OUTER JOIN     MysteryDiningCopy.dbo.SubBrand          AS sb   ON b.SubBrandUID=sb.SubBrandUID
		LEFT OUTER JOIN     MysteryDiningCopy.dbo.NBBranchManager   AS nb   ON nb.BranchID=b.BranchUID and nb.active=1


ORDER BY b.BranchUID








GO

EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[28] 4[9] 2[44] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -166
         Left = 0
      End
      Begin Tables = 
         Begin Table = "b"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 272
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "bd"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 400
               Right = 272
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cr"
            Begin Extent = 
               Top = 138
               Left = 246
               Bottom = 268
               Right = 425
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pr"
            Begin Extent = 
               Top = 402
               Left = 38
               Bottom = 498
               Right = 217
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "bs"
            Begin Extent = 
               Top = 402
               Left = 255
               Bottom = 498
               Right = 432
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "nss"
            Begin Extent = 
               Top = 498
               Left = 38
               Bottom = 594
               Right = 219
            End
            DisplayFlags = 280
            TopColumn = 0
      ', 'SCHEMA', N'dbo', 'VIEW', N'vDimBranchJoined', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'   End
         Begin Table = "dg"
            Begin Extent = 
               Top = 498
               Left = 257
               Bottom = 628
               Right = 427
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 630
               Left = 38
               Bottom = 760
               Right = 295
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "u"
            Begin Extent = 
               Top = 15
               Left = 802
               Bottom = 145
               Right = 983
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vDimBranchJoined', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vDimBranchJoined', NULL, NULL
GO
