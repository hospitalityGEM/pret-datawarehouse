
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vFactBenchmarkResultDW]
AS
SELECT TOP 100 PERCENT
	   [BenchmarkResultKey]
      ,[VisitKey]
      ,[BranchKey]
      ,[GeographyKey]
      ,[PeriodKey]
      ,[VisitDateKey]
      ,[AssessorKey]
      ,[Questionnaire]
      ,[SubQuestionnaire]
      ,[Benchmark]
      ,[CustomerLoyalty]
      ,[Score]
      ,[MaxScore]
      ,[VisitScore]
      ,[VisitMaxScore]
      ,[VisitScoreMethod]
      ,[IncludeInAnalysis]
      ,[ValidFromDate]
      ,[ValidToDate]
      ,[Active]
      ,[Source]
      ,[VisitID]
      ,[BenchmarkID]
      ,[TimeStamp]
      ,[ChangeReason]
  FROM 
		[FactBenchmarkResult]
		INNER JOIN MysteryDiningDW.dbo.StagingLegacyVisitIDs slv ON [FactBenchmarkResult].VisitID		= slv.VisitUID AND slv.ConfigInfoID = 12
  WHERE
	[Source] = 'Legacy'
AND [Active] = 1

ORDER BY
	VisitID
,	BenchmarkID

GO
