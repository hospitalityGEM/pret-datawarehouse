CREATE TABLE [dbo].[StagingFactDepartmentResult]
(
[StagingFDRID] [int] NOT NULL IDENTITY(1, 1),
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[DepartmentID] [int] NOT NULL,
[VisitID] [int] NOT NULL,
[ValidToDate] [datetime] NOT NULL,
[Type] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[VisitScore] [decimal] (18, 2) NULL,
[VisitMaxScore] [decimal] (18, 2) NULL,
[VisitScoreMethod] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingFactDepartmentResult] ADD CONSTRAINT [PK_StagingFactDepartmentResult_1] PRIMARY KEY CLUSTERED  ([StagingFDRID]) ON [PRIMARY]
GO
