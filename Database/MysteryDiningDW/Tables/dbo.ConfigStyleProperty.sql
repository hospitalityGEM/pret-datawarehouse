CREATE TABLE [dbo].[ConfigStyleProperty]
(
[ConfigStylePropertyID] [smallint] NOT NULL IDENTITY(1, 1),
[PropertyName] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[PropertyValue] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Editable] [bit] NOT NULL CONSTRAINT [DF_ConfigStyleProperty_Editable] DEFAULT ((0)),
[Active] [bit] NOT NULL CONSTRAINT [DF_ConfigStyleProperty_Active] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConfigStyleProperty] ADD CONSTRAINT [PK_ConfigStyleProperty] PRIMARY KEY CLUSTERED  ([ConfigStylePropertyID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[ConfigStyleProperty] TO [TMDCReporting]
GRANT INSERT ON  [dbo].[ConfigStyleProperty] TO [TMDCReporting]
GRANT UPDATE ON  [dbo].[ConfigStyleProperty] TO [TMDCReporting]
GO
