CREATE TABLE [dbo].[ErrorDimFeedback]
(
[DimFeedbackErrorID] [int] NOT NULL IDENTITY(1, 1),
[FeedbackKey] [int] NOT NULL,
[CompletedDateKey] [int] NOT NULL,
[StartTime] [datetime] NOT NULL,
[EndTime] [datetime] NOT NULL,
[ElapsedTime] [int] NOT NULL,
[IPAddress] [nvarchar] (30) COLLATE Latin1_General_CI_AS NOT NULL,
[CookieGUID] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[DiscardReason] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[DiscardValue] [int] NULL,
[OFDID] [int] NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[FeedbackID] [int] NOT NULL,
[InvalidInput] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[InvalidInputErrorDescription] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[ErrorCode] [int] NULL,
[ErrorColumn] [int] NULL,
[RowChangeReason] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Resolved] [bit] NOT NULL CONSTRAINT [DF_ErrorDimFeedback_Resolved] DEFAULT ((0)),
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_ErrorDimFeedback_TimeStamp] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ErrorDimFeedback] ADD CONSTRAINT [PK_ErrorDimFeedback] PRIMARY KEY CLUSTERED  ([DimFeedbackErrorID]) ON [PRIMARY]
GO
