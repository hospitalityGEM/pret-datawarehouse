CREATE TABLE [dbo].[ConfigInfo]
(
[ConfigInfoID] [smallint] NOT NULL IDENTITY(1, 1),
[Key] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Value] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[ConfigTypeID] [tinyint] NOT NULL,
[LastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ConfigInfo_LastUpdated] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConfigInfo] ADD CONSTRAINT [PK_ConfigInfo] PRIMARY KEY CLUSTERED  ([ConfigInfoID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConfigInfo] ADD CONSTRAINT [FK_ConfigInfo_ConfigType] FOREIGN KEY ([ConfigTypeID]) REFERENCES [dbo].[ConfigType] ([ConfigTypeID])
GO
