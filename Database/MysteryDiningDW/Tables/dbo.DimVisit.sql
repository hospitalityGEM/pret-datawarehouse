CREATE TABLE [dbo].[DimVisit]
(
[VisitKey] [int] NOT NULL IDENTITY(1, 1),
[PeriodKey] [int] NOT NULL,
[Type] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[AddedDateKey] [int] NULL,
[AvailableFromDateKey] [int] NULL,
[AvailableToDateKey] [int] NULL,
[Questionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[SubQuestionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[BranchKey] [int] NOT NULL,
[AdminOwner] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[VisitDateKey] [int] NULL,
[VisitTime] [datetime] NULL,
[AdultGuests] [tinyint] NULL,
[ChildGuests] [tinyint] NULL,
[ChildGuestsNotInvolved] [tinyint] NULL,
[ReceiptNumber] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[TradeLevel] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[SeatingLevel] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Weather] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[AssessorKey] [int] NULL,
[AssessorBookDateKey] [int] NULL,
[ReimbursementDateKey] [int] NULL,
[ValidFromDate] [datetime] NULL,
[ValidToDate] [datetime] NULL,
[Active] [bit] NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[VisitID] [int] NOT NULL,
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_DimVisit_TimeStamp] DEFAULT (getdate()),
[ChangeReason] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[VisitStatus] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[CurrentVisitScore] [decimal] (18, 2) NULL,
[PassFail] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_DimVisit_PassFail] DEFAULT (N'N/A'),
[PassFailMessage] [nvarchar] (300) COLLATE Latin1_General_CI_AS NULL,
[CurrentScore] [decimal] (18, 2) NULL,
[CurrentMaxScore] [decimal] (18, 2) NULL,
[VisitScoreText] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[VisitScoreMethod] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[QuestionnaireCategory] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[AssignedAdmin] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[PeriodBranchVisitCount] [int] NULL CONSTRAINT [DF_DimVisit_PeriodBranchVisitCount] DEFAULT ((0)),
[PeriodBranchCategoryVisitCount] [int] NULL CONSTRAINT [DF_DimVisit_PeriodBranchCategoryVisitCount] DEFAULT ((0)),
[VisitEndTime] [datetime] NULL,
[StaffDescription] [nvarchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[AdjustedScore] [decimal] (18, 2) NULL,
[VisitExitDateKey] [int] NULL,
[InitialReviewDateKey] [int] NULL,
[LatestReviewDateKey] [int] NULL,
[DiscardReasonOrigin] [nvarchar] (150) COLLATE Latin1_General_CI_AS NULL,
[DiscardReason] [nvarchar] (250) COLLATE Latin1_General_CI_AS NULL,
[DiscardReasonDetail] [nvarchar] (250) COLLATE Latin1_General_CI_AS NULL,
[QuestionnaireID] [int] NULL,
[IsNBVisit] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimVisit] ADD CONSTRAINT [PK_DimVisit] PRIMARY KEY CLUSTERED  ([VisitKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ETLFactSectionTest] ON [dbo].[DimVisit] ([Active], [Source]) INCLUDE ([AssessorKey], [BranchKey], [PeriodKey], [Questionnaire], [SubQuestionnaire], [VisitDateKey], [VisitID], [VisitKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimVisit_ActiveType_BranchKey] ON [dbo].[DimVisit] ([Active], [Type]) INCLUDE ([BranchKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimVisit_BranchKeyActive_Type] ON [dbo].[DimVisit] ([BranchKey], [Active]) INCLUDE ([Type]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_DimVisit_7_1089438955__K3_K30] ON [dbo].[DimVisit] ([Type], [VisitStatus]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_DimVisit_7_1089438955__K3_K30_K1] ON [dbo].[DimVisit] ([Type], [VisitStatus], [VisitKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimVisit_VisitStatus] ON [dbo].[DimVisit] ([VisitStatus]) INCLUDE ([QuestionnaireCategory], [Type], [VisitKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_DimVisit_12_1089438955__K30_K3_K1] ON [dbo].[DimVisit] ([VisitStatus], [Type], [VisitKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_DimVisit_7_1089438955__K30_K1_K3] ON [dbo].[DimVisit] ([VisitStatus], [VisitKey], [Type]) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_1089438955_25_1_3] ON [dbo].[DimVisit] ([Active], [VisitKey], [Type])
GO
CREATE STATISTICS [_dta_stat_1089438955_38_1] ON [dbo].[DimVisit] ([QuestionnaireCategory], [VisitKey])
GO
CREATE STATISTICS [_dta_stat_1089438955_3_25] ON [dbo].[DimVisit] ([Type], [Active])
GO
CREATE STATISTICS [_dta_stat_1089438955_1_9_38] ON [dbo].[DimVisit] ([VisitKey], [BranchKey], [QuestionnaireCategory])
GO
CREATE STATISTICS [_dta_stat_1089438955_1_3] ON [dbo].[DimVisit] ([VisitKey], [Type])
GO
ALTER TABLE [dbo].[DimVisit] ADD CONSTRAINT [FK_DimVisit_DimDate2] FOREIGN KEY ([AssessorBookDateKey]) REFERENCES [dbo].[DimDate] ([DateKey])
GO
ALTER TABLE [dbo].[DimVisit] ADD CONSTRAINT [FK_DimVisit_DimDate] FOREIGN KEY ([AvailableFromDateKey]) REFERENCES [dbo].[DimDate] ([DateKey])
GO
ALTER TABLE [dbo].[DimVisit] ADD CONSTRAINT [FK_DimVisit_DimDate1] FOREIGN KEY ([AvailableToDateKey]) REFERENCES [dbo].[DimDate] ([DateKey])
GO
ALTER TABLE [dbo].[DimVisit] ADD CONSTRAINT [FK_DimVisit_DimDate5] FOREIGN KEY ([VisitDateKey]) REFERENCES [dbo].[DimDate] ([DateKey])
GO
GRANT SELECT ON  [dbo].[DimVisit] TO [TMDCReporting]
GO
