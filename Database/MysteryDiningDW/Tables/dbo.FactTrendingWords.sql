CREATE TABLE [dbo].[FactTrendingWords]
(
[ClientID] [int] NULL,
[NPSGroup] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Term] [nvarchar] (150) COLLATE Latin1_General_CI_AS NULL,
[Score] [int] NULL
) ON [PRIMARY]
GO
