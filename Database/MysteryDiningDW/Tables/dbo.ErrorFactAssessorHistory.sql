CREATE TABLE [dbo].[ErrorFactAssessorHistory]
(
[AssessorHistoryErrorID] [int] NOT NULL IDENTITY(1, 1),
[LogDateKey] [int] NULL,
[Activity] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Status] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[SystemComments] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[AssessorComments] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[AdminComments] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[VisitKey] [int] NULL,
[Points] [int] NULL,
[AdminKey] [int] NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[AssessorID] [int] NULL,
[AssessorHistoryID] [int] NULL,
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_ErrorFactAssessorHistory_TimeStamp] DEFAULT (getdate()),
[Resolved] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ErrorFactAssessorHistory] ADD CONSTRAINT [PK_ErrorFactAssessorHistory] PRIMARY KEY CLUSTERED  ([AssessorHistoryErrorID]) ON [PRIMARY]
GO
