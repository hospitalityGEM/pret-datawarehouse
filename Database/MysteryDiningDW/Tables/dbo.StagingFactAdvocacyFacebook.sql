CREATE TABLE [dbo].[StagingFactAdvocacyFacebook]
(
[StagingFAFID] [int] NOT NULL IDENTITY(1, 1),
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[AdvocacyFacebookID] [int] NOT NULL,
[ValidToDate] [datetime] NULL,
[Type] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[AdvocacyFacebookKey] [int] NOT NULL,
[LikedAfter] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[ConvertedToLiker] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[PostedMessage] [nvarchar] (4000) COLLATE Latin1_General_CI_AS NULL,
[Posted] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[SocialChannelKey] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingFactAdvocacyFacebook] ADD CONSTRAINT [PK_StagingFactAdvocacyFacebook] PRIMARY KEY CLUSTERED  ([StagingFAFID]) ON [PRIMARY]
GO
