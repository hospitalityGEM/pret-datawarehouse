CREATE TABLE [dbo].[FileCheckHistory]
(
[FileCheckHistory] [int] NOT NULL IDENTITY(1, 1),
[CheckTimestamp] [datetime] NOT NULL CONSTRAINT [DF_FileCheckHistory_CheckTimestamp] DEFAULT (getdate()),
[FileName] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[FilePath] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[FileFound] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FileCheckHistory] ADD CONSTRAINT [PK_FileCheckHistory] PRIMARY KEY CLUSTERED  ([FileCheckHistory]) ON [PRIMARY]
GO
