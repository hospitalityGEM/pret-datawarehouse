CREATE TABLE [dbo].[DimGuestMarketing]
(
[GuestMarketingKey] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (20) COLLATE Latin1_General_CI_AS NOT NULL,
[FirstName] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[Surname] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[AddressLine1] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[AddressLine2] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[City] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[County] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[Postcode] [nvarchar] (20) COLLATE Latin1_General_CI_AS NOT NULL,
[Age] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Occupation] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[Mobile] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Email] [nvarchar] (300) COLLATE Latin1_General_CI_AS NOT NULL,
[Birthday] [nvarchar] (30) COLLATE Latin1_General_CI_AS NOT NULL,
[JoinMarketing] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ContactEmail] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[ContactPost] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[ContactMobile] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[GuestMarketingID] [int] NOT NULL,
[GuestMarketingDetailID] [int] NOT NULL CONSTRAINT [DF_DimGuestMarketing_GuestMarketingDetailID] DEFAULT ((0)),
[Timestamp] [datetime] NOT NULL CONSTRAINT [DF_DimGuestMarketing_Timestamp] DEFAULT (getdate()),
[FullName] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimGuestMarketing] ADD CONSTRAINT [PK_DimGuestMarketing] PRIMARY KEY CLUSTERED  ([GuestMarketingKey]) ON [PRIMARY]
GO
