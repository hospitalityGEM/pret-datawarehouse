CREATE TABLE [dbo].[ErrorFactCategoryResult]
(
[FactCategoryResultErrorID] [int] NOT NULL IDENTITY(1, 1),
[TimeStamp] [datetime] NULL CONSTRAINT [DF_ErrorFactCategoryResult_TimeStamp] DEFAULT (getdate()),
[CategoryResultKey] [int] NULL,
[VisitKey] [int] NULL,
[BranchKey] [int] NULL,
[GeographyKey] [int] NULL,
[PeriodKey] [int] NULL,
[VisitDateKey] [int] NULL,
[AssessorKey] [int] NULL,
[Questionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[SubQuestionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Category] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Score] [decimal] (18, 2) NULL,
[MaxScore] [decimal] (18, 2) NULL,
[VisitScore] [decimal] (18, 2) NULL,
[VisitMaxScore] [decimal] (18, 2) NULL,
[VisitScoreMethod] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[IncludeInAnalysis] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[ValidFromDate] [datetime] NULL,
[ValidToDate] [datetime] NULL,
[Active] [bit] NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[VisitID] [int] NULL,
[CategoryID] [int] NULL,
[ChangeReason] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[InvalidInput] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[InvalidInputDescription] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[ErrorCode] [int] NULL,
[ErrorColumn] [int] NULL,
[Resolved] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ErrorFactCategoryResult] ADD CONSTRAINT [PK_ErrorFactCategoryResult] PRIMARY KEY CLUSTERED  ([FactCategoryResultErrorID]) ON [PRIMARY]
GO
