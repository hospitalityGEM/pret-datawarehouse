CREATE TABLE [dbo].[DimGeography]
(
[GeographyKey] [int] NOT NULL IDENTITY(1, 1),
[PostCode] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[PostalArea] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Street] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Location] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[County] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Region] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Country] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[WorldArea] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[XCoordinate] [decimal] (18, 6) NOT NULL,
[YCoordinate] [decimal] (18, 6) NOT NULL,
[Airport] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[RailStation] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Underground] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ValidFromDate] [datetime] NOT NULL,
[ValidToDate] [datetime] NULL,
[Active] [bit] NOT NULL,
[Source] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[TableSource] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[SourceID] [int] NULL,
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_DimGeography_TimeStamp] DEFAULT (getdate()),
[ChangeReason] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimGeography] ADD CONSTRAINT [PK_Geography] PRIMARY KEY CLUSTERED  ([GeographyKey]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[DimGeography] TO [TMDCReporting]
GO
