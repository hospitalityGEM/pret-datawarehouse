CREATE TABLE [dbo].[StagingQuestionFactor]
(
[QuestionUID] [int] NOT NULL,
[Factor] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingQuestionFactor] ADD CONSTRAINT [PK_StagingQuestionFactor] PRIMARY KEY CLUSTERED  ([QuestionUID]) ON [PRIMARY]
GO
