CREATE TABLE [dbo].[StagingAnswerType]
(
[StagingAnswerTypeID] [nvarchar] (4) COLLATE Latin1_General_CI_AS NOT NULL,
[Name] [nvarchar] (20) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingAnswerType] ADD CONSTRAINT [PK_StagingAnswerType] PRIMARY KEY CLUSTERED  ([StagingAnswerTypeID]) ON [PRIMARY]
GO
