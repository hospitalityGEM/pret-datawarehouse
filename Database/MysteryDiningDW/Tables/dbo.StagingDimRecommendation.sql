CREATE TABLE [dbo].[StagingDimRecommendation]
(
[SRecommendationKey] [int] NOT NULL IDENTITY(1, 1),
[RecommendationKey] [int] NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[RecommendationID] [int] NOT NULL,
[ValidToDate] [datetime] NULL,
[Active] [bit] NOT NULL,
[RowType] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[ViewCount] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingDimRecommendation] ADD CONSTRAINT [PK_StagingDimRecommendation] PRIMARY KEY CLUSTERED  ([SRecommendationKey]) ON [PRIMARY]
GO
