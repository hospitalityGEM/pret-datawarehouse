CREATE TABLE [dbo].[AuditLoadCounts]
(
[AuditLoadCountID] [int] NOT NULL IDENTITY(1, 1),
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_AuditLoadCounts_TimeStamp] DEFAULT (getdate()),
[Task] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[MachineName] [nvarchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[StartTime] [datetime] NOT NULL,
[ExistingInput] [int] NOT NULL,
[SourceSystemInput] [int] NOT NULL,
[UnchangedOutput] [int] NOT NULL,
[NewOutput] [int] NOT NULL,
[SCD2ExpiredOutput] [int] NOT NULL,
[SCD2NewOutput] [int] NOT NULL,
[SCD1UpdateOutput] [int] NOT NULL,
[InvalidInputOutput] [int] NOT NULL,
[LastOutputTime] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuditLoadCounts] ADD CONSTRAINT [PK_AuditLoadCounts] PRIMARY KEY CLUSTERED  ([AuditLoadCountID]) ON [PRIMARY]
GO
