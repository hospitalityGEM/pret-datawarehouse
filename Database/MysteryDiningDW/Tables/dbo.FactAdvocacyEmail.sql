CREATE TABLE [dbo].[FactAdvocacyEmail]
(
[AdvocacyEmailKey] [int] NOT NULL IDENTITY(1, 1),
[VisitScore] [decimal] (18, 2) NOT NULL,
[VisitMaxScore] [decimal] (18, 2) NOT NULL,
[CompletionDateKey] [int] NOT NULL,
[VisitDateKey] [int] NOT NULL,
[BranchKey] [int] NOT NULL,
[PeriodKey] [int] NOT NULL,
[RecommendationKey] [int] NOT NULL,
[GuestMarketingKey] [int] NOT NULL,
[RecipientCount] [int] NOT NULL,
[SentCount] [int] NOT NULL,
[FromAddress] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Subject] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[Message] [nvarchar] (500) COLLATE Latin1_General_CI_AS NOT NULL,
[Advocate] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[RecommendationStatus] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[AdvocacyEmailID] [int] NOT NULL,
[ValidFromDate] [datetime] NULL,
[ValidToDate] [datetime] NULL,
[Active] [bit] NULL,
[RowChangeReason] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactAdvocacyEmail] ADD CONSTRAINT [PK_FactAdvocacyEmail] PRIMARY KEY CLUSTERED  ([AdvocacyEmailKey]) ON [PRIMARY]
GO
