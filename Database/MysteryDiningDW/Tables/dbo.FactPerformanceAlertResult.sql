CREATE TABLE [dbo].[FactPerformanceAlertResult]
(
[PerformanceAlertResultKey] [int] NOT NULL IDENTITY(1, 1),
[VisitKey] [int] NOT NULL,
[PeriodKey] [int] NOT NULL,
[BranchKey] [int] NOT NULL,
[GeographyKey] [int] NOT NULL,
[VisitDateKey] [int] NOT NULL,
[AssessorKey] [int] NOT NULL,
[Questionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[SubQuestionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[AlertName] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[AlertType] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Score] [decimal] (18, 2) NOT NULL,
[MaxScore] [decimal] (18, 2) NOT NULL,
[ScoreText] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ScoreMethod] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[IncludeInAnalysis] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[VisitStatus] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[RaisedDateKey] [int] NOT NULL,
[CompletedDateKey] [int] NOT NULL,
[ValidFromDate] [datetime] NOT NULL,
[ValidToDate] [datetime] NOT NULL,
[Active] [bit] NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[AlertID] [int] NOT NULL,
[VisitID] [int] NOT NULL,
[TimeStamp] [datetime] NULL CONSTRAINT [DF_FactPerformanceAlertResult_TimeStamp] DEFAULT (getdate()),
[ChangeReason] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactPerformanceAlertResult] ADD CONSTRAINT [PK_FactPerformanceAlertResult] PRIMARY KEY CLUSTERED  ([PerformanceAlertResultKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactPerformanceAlertResult] ADD CONSTRAINT [FK_FactPerformanceAlertResult_DimAssessor] FOREIGN KEY ([AssessorKey]) REFERENCES [dbo].[DimAssessor] ([AssessorKey])
GO
ALTER TABLE [dbo].[FactPerformanceAlertResult] ADD CONSTRAINT [FK_FactPerformanceAlertResult_DimBranch] FOREIGN KEY ([BranchKey]) REFERENCES [dbo].[DimBranch] ([BranchKey])
GO
ALTER TABLE [dbo].[FactPerformanceAlertResult] ADD CONSTRAINT [FK_FactPerformanceAlertResult_DimGeography] FOREIGN KEY ([GeographyKey]) REFERENCES [dbo].[DimGeography] ([GeographyKey])
GO
ALTER TABLE [dbo].[FactPerformanceAlertResult] ADD CONSTRAINT [FK_FactPerformanceAlertResult_DimPeriod] FOREIGN KEY ([PeriodKey]) REFERENCES [dbo].[DimPeriod] ([PeriodKey])
GO
ALTER TABLE [dbo].[FactPerformanceAlertResult] ADD CONSTRAINT [FK_FactPerformanceAlertResult_DimDate] FOREIGN KEY ([VisitDateKey]) REFERENCES [dbo].[DimDate] ([DateKey])
GO
ALTER TABLE [dbo].[FactPerformanceAlertResult] ADD CONSTRAINT [FK_FactPerformanceAlertResult_DimVisit] FOREIGN KEY ([VisitKey]) REFERENCES [dbo].[DimVisit] ([VisitKey])
GO
