CREATE TABLE [dbo].[StagingDimQuestion]
(
[SQuestionKey] [int] NOT NULL IDENTITY(1, 1),
[QuestionKey] [int] NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[QuestionID] [int] NOT NULL,
[ValidToDate] [datetime] NULL,
[Type] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
[AnalysisTag] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[QuestionnaireCategory] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[MenuSection] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[IncludeInAnalysis] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[QuestionnaireID] [int] NULL,
[CurrentQuestionnaire] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingDimQuestion] ADD CONSTRAINT [PK_StagingDimQuestion] PRIMARY KEY CLUSTERED  ([SQuestionKey]) ON [PRIMARY]
GO
