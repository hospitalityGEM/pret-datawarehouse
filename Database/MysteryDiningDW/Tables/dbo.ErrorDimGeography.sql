CREATE TABLE [dbo].[ErrorDimGeography]
(
[DimGeographyErrorID] [int] NOT NULL IDENTITY(1, 1),
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_ErrorDimGeography_TimeStamp] DEFAULT (getdate()),
[GeographyKey] [int] NULL,
[PostCode] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[PostalArea] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Street] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Location] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[County] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Region] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Country] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[WorldArea] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[XCoordinate] [decimal] (18, 6) NULL,
[YCoordinate] [decimal] (18, 6) NULL,
[Airport] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[RailStation] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Underground] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ValidFromDate] [datetime] NULL,
[ValidToDate] [datetime] NULL,
[Active] [bit] NULL,
[Source] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[TableSource] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[SourceID] [int] NULL,
[InvalidInput] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[InvalidInputErrorDescription] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[ErrorCode] [int] NULL,
[ErrorColumn] [int] NULL,
[RowChangeReason] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Resolved] [bit] NOT NULL CONSTRAINT [DF_ErrorDimGeography_Resolved] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ErrorDimGeography] ADD CONSTRAINT [PK_ErrorDimGeography] PRIMARY KEY CLUSTERED  ([DimGeographyErrorID]) ON [PRIMARY]
GO
