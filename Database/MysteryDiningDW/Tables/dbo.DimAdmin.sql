CREATE TABLE [dbo].[DimAdmin]
(
[AdminKey] [int] NOT NULL IDENTITY(1, 1),
[AdminName] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Company] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL CONSTRAINT [DF_DimAdmin_Source] DEFAULT (N'Legacy'),
[AdminID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimAdmin] ADD CONSTRAINT [PK_DimAdmin] PRIMARY KEY CLUSTERED  ([AdminKey]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[DimAdmin] TO [TMDCReporting]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Admin users full name', 'SCHEMA', N'dbo', 'TABLE', N'DimAdmin', 'COLUMN', N'AdminName'
GO
