CREATE TABLE [dbo].[Report]
(
[ReportID] [int] NOT NULL,
[ReportSectionID] [smallint] NOT NULL,
[ReportTypeID] [tinyint] NOT NULL,
[ReportTitle] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[SSRSName] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Description] [nvarchar] (500) COLLATE Latin1_General_CI_AS NOT NULL,
[SampleReportPath] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Active] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Report] ADD CONSTRAINT [PK_Report] PRIMARY KEY CLUSTERED  ([ReportID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Report] ADD CONSTRAINT [FK_Report_ReportSection] FOREIGN KEY ([ReportSectionID]) REFERENCES [dbo].[ReportSection] ([ReportSectionID])
GO
ALTER TABLE [dbo].[Report] ADD CONSTRAINT [FK_Report_ReportType] FOREIGN KEY ([ReportTypeID]) REFERENCES [dbo].[ReportType] ([ReportTypeID])
GO
