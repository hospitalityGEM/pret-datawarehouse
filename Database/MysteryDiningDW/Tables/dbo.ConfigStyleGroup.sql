CREATE TABLE [dbo].[ConfigStyleGroup]
(
[ConfigStyleGroupID] [smallint] NOT NULL IDENTITY(1, 1),
[GroupName] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Editable] [bit] NOT NULL CONSTRAINT [DF_ConfigStyleGroup_Editable] DEFAULT ((0)),
[Active] [bit] NOT NULL CONSTRAINT [DF_ConfigStyleGroup_Active] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConfigStyleGroup] ADD CONSTRAINT [PK_ConfigStyleGroup] PRIMARY KEY CLUSTERED  ([ConfigStyleGroupID]) ON [PRIMARY]
GO
