CREATE TABLE [dbo].[StagingFactAdvocacyTwitter]
(
[StagingFATID] [int] NOT NULL IDENTITY(1, 1),
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[AdvocacyTwitterID] [int] NOT NULL,
[ValidToDate] [datetime] NULL,
[Type] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[AdvocacyTwitterKey] [int] NOT NULL,
[FollowingAfter] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[Tweeted] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[Tweet] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[ConvertedToFollower] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingFactAdvocacyTwitter] ADD CONSTRAINT [PK_StagingFactAdvocacyTwitter] PRIMARY KEY CLUSTERED  ([StagingFATID]) ON [PRIMARY]
GO
