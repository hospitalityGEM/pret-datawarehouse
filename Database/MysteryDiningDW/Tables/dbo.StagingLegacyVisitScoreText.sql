CREATE TABLE [dbo].[StagingLegacyVisitScoreText]
(
[VisitUID] [int] NOT NULL,
[VisitScoreText] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingLegacyVisitScoreText] ADD CONSTRAINT [PK_StagingLegacyVisitScoreMethod] PRIMARY KEY CLUSTERED  ([VisitUID]) ON [PRIMARY]
GO
