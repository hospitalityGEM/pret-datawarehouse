CREATE TABLE [dbo].[ErrorFactAdvocacyFacebook]
(
[ErrorFactAdvocacyFacebookID] [int] NOT NULL IDENTITY(1, 1),
[AdvocacyFacebookKey] [int] NULL,
[VisitScore] [decimal] (18, 2) NULL,
[VisitMaxScore] [decimal] (18, 2) NULL,
[CompletionDateKey] [int] NULL,
[VisitDateKey] [int] NULL,
[BranchKey] [int] NULL,
[PeriodKey] [int] NULL,
[RecommendationKey] [int] NULL,
[LikedBefore] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[LikedAfter] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[ConvertedToLiker] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[UserAccount] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[PostedMessage] [nvarchar] (4000) COLLATE Latin1_General_CI_AS NULL,
[MarketingMessage] [nvarchar] (500) COLLATE Latin1_General_CI_AS NULL,
[Friends] [int] NULL,
[Posted] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[Advocate] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[RecommendationStatus] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[GuestMarketingKey] [int] NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[AdvocacyFacebookID] [int] NULL,
[TimeStamp] [datetime] NULL CONSTRAINT [DF_ErrorFactAdvocacyFacebook_TimeStamp] DEFAULT (getdate()),
[InvalidInput] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[InvalidInputErrorDescription] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[ErrorCode] [int] NULL,
[ErrorColumn] [int] NULL,
[Resolved] [bit] NULL,
[RowChangeReason] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[ValidFromDate] [datetime] NULL,
[ValidToDate] [datetime] NULL,
[Active] [bit] NULL,
[SocialChannelKey] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ErrorFactAdvocacyFacebook] ADD CONSTRAINT [PK_FactAdvocacyFacebookError] PRIMARY KEY CLUSTERED  ([ErrorFactAdvocacyFacebookID]) ON [PRIMARY]
GO
