CREATE TABLE [dbo].[StagingFactRecommendationView]
(
[StagingFRVID] [int] NOT NULL IDENTITY(1, 1),
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[RecommendationViewID] [int] NOT NULL,
[ValidToDate] [datetime] NULL,
[Type] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[RecommendationViewKey] [int] NOT NULL,
[FacebookLiked] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[TwitterFollowed] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[FacebookSocialChannelKey] [int] NULL,
[TwitterSocialChannelKey] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingFactRecommendationView] ADD CONSTRAINT [PK_StagingFactRecommendationView] PRIMARY KEY CLUSTERED  ([StagingFRVID]) ON [PRIMARY]
GO
