CREATE TABLE [dbo].[ConfigStyleItemGroup]
(
[ConfigStyleItemGroupID] [smallint] NOT NULL IDENTITY(1, 1),
[GroupName] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Editable] [bit] NOT NULL CONSTRAINT [DF_ConfigStyleItemGroup_Editable] DEFAULT ((0)),
[Active] [bit] NOT NULL CONSTRAINT [DF_ConfigStyleItemGroup_Active] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConfigStyleItemGroup] ADD CONSTRAINT [PK_ConfigStyleItemGroup] PRIMARY KEY CLUSTERED  ([ConfigStyleItemGroupID]) ON [PRIMARY]
GO
