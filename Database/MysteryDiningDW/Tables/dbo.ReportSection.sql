CREATE TABLE [dbo].[ReportSection]
(
[ReportSectionID] [smallint] NOT NULL,
[SectionName] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Description] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[Visible] [bit] NOT NULL,
[Active] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReportSection] ADD CONSTRAINT [PK_ReportSection] PRIMARY KEY CLUSTERED  ([ReportSectionID]) ON [PRIMARY]
GO
