CREATE TABLE [dbo].[ConfigDimDateExceptionDays]
(
[ConfigDimDateExceptionDaysID] [int] NOT NULL IDENTITY(1, 1),
[ExceptionDate] [datetime] NOT NULL,
[ConfigExceptionTypeID] [tinyint] NOT NULL,
[DateAdded] [datetime] NOT NULL CONSTRAINT [DF_ConfigDimDateExceptionDays_DateAdded] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConfigDimDateExceptionDays] ADD CONSTRAINT [PK_ConfigDimDateExceptionDays] PRIMARY KEY CLUSTERED  ([ConfigDimDateExceptionDaysID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConfigDimDateExceptionDays] ADD CONSTRAINT [FK_ConfigDimDateExceptionDays_ConfigExceptionType] FOREIGN KEY ([ConfigExceptionTypeID]) REFERENCES [dbo].[ConfigExceptionType] ([ConfigExceptionTypeID])
GO
