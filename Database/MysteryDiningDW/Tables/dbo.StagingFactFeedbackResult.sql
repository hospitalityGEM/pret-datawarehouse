CREATE TABLE [dbo].[StagingFactFeedbackResult]
(
[StagingFFRID] [int] NOT NULL IDENTITY(1, 1),
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[FeedbackResultID] [int] NOT NULL,
[ValidToDate] [datetime] NULL,
[Type] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[FeedbackKey] [int] NOT NULL,
[TimeStarted] [datetime] NULL,
[TimeEnded] [datetime] NULL,
[IPAddress] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[CookieGUID] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[CompletedDateKey] [int] NULL,
[FeedbackResultKey] [int] NOT NULL,
[OfferedAdvocacy] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[AcceptedAdvocacy] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[PostedAdvocacy] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[SubscribeAdvocacy] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[GuestMarketingKey] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingFactFeedbackResult] ADD CONSTRAINT [PK_StagingFactFeedbackResult_1] PRIMARY KEY CLUSTERED  ([StagingFFRID]) ON [PRIMARY]
GO
