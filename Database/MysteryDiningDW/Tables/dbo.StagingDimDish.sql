CREATE TABLE [dbo].[StagingDimDish]
(
[SDishKey] [int] NOT NULL IDENTITY(1, 1),
[DishKey] [int] NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[DishID] [int] NOT NULL,
[MenuSectionID] [int] NOT NULL,
[MenuID] [int] NOT NULL,
[ValidToDate] [datetime] NULL,
[Active] [bit] NOT NULL,
[MenuStatus] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingDimDish] ADD CONSTRAINT [PK_StagingDimDish] PRIMARY KEY CLUSTERED  ([SDishKey]) ON [PRIMARY]
GO
