CREATE TABLE [dbo].[FactAdvocacyTwitter]
(
[AdvocacyTwitterKey] [int] NOT NULL IDENTITY(1, 1),
[VisitScore] [decimal] (18, 2) NOT NULL,
[VisitMaxScore] [decimal] (18, 2) NOT NULL,
[CompletionDateKey] [int] NOT NULL,
[VisitDateKey] [int] NOT NULL,
[BranchKey] [int] NOT NULL,
[PeriodKey] [int] NOT NULL,
[RecommendationKey] [int] NOT NULL,
[FollowingBefore] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[FollowingAfter] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[ConvertedToFollower] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[UserAccount] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Tweet] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[Followers] [int] NULL,
[FollowedAccount] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Tweeted] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[RecommendationStatus] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[AdvocacyTwitterID] [int] NOT NULL,
[ValidFromDate] [datetime] NULL,
[ValidToDate] [datetime] NULL,
[Active] [bit] NULL,
[RowChangeReason] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[SocialChannelKey] [int] NOT NULL,
[GuestMarketingKey] [int] NOT NULL,
[Advocate] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactAdvocacyTwitter] ADD CONSTRAINT [PK_FactAdvocacyTwitter] PRIMARY KEY CLUSTERED  ([AdvocacyTwitterKey]) ON [PRIMARY]
GO
