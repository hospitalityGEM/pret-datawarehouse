CREATE TABLE [dbo].[ReportSubscriptionHistory]
(
[ReportSubscriptionHistoryID] [int] NOT NULL,
[BrandID] [int] NOT NULL,
[ReportID] [int] NOT NULL,
[RecordAdded] [datetime] NOT NULL,
[ReportSubscriptionStatusID] [tinyint] NOT NULL,
[ManagerID] [int] NOT NULL,
[AdminID] [smallint] NOT NULL,
[Active] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReportSubscriptionHistory] ADD CONSTRAINT [PK_ReportSubscriptionHistory] PRIMARY KEY CLUSTERED  ([ReportSubscriptionHistoryID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReportSubscriptionHistory] ADD CONSTRAINT [FK_ReportSubscriptionHistory_Report] FOREIGN KEY ([ReportID]) REFERENCES [dbo].[Report] ([ReportID])
GO
ALTER TABLE [dbo].[ReportSubscriptionHistory] ADD CONSTRAINT [FK_ReportSubscriptionHistory_ReportSubscriptionStatus] FOREIGN KEY ([ReportSubscriptionStatusID]) REFERENCES [dbo].[ReportSubscriptionStatus] ([ReportSubscriptionStatusID])
GO
