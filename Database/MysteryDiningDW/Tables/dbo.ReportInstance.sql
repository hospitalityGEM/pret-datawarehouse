CREATE TABLE [dbo].[ReportInstance]
(
[ReportInstanceID] [int] NOT NULL IDENTITY(1, 1),
[ReportID] [int] NOT NULL,
[DateAdded] [datetime] NOT NULL,
[AddedByAdminID] [smallint] NOT NULL,
[BrandID] [int] NULL,
[FilePath] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Comments] [nvarchar] (300) COLLATE Latin1_General_CI_AS NULL,
[ReleaseDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReportInstance] ADD CONSTRAINT [PK_ReportInstance] PRIMARY KEY CLUSTERED  ([ReportInstanceID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReportInstance] ADD CONSTRAINT [FK_ReportInstance_Report] FOREIGN KEY ([ReportID]) REFERENCES [dbo].[Report] ([ReportID])
GO
