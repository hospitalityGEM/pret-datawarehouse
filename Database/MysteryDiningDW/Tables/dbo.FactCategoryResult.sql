CREATE TABLE [dbo].[FactCategoryResult]
(
[CategoryResultKey] [int] NOT NULL IDENTITY(1, 1),
[VisitKey] [int] NOT NULL,
[BranchKey] [int] NOT NULL,
[GeographyKey] [int] NULL,
[PeriodKey] [int] NOT NULL,
[VisitDateKey] [int] NOT NULL,
[AssessorKey] [int] NOT NULL,
[Questionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[SubQuestionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Category] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Score] [decimal] (18, 2) NOT NULL,
[MaxScore] [decimal] (18, 2) NOT NULL,
[VisitScore] [decimal] (18, 2) NOT NULL,
[VisitMaxScore] [decimal] (18, 2) NOT NULL,
[VisitScoreMethod] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[IncludeInAnalysis] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[ValidFromDate] [datetime] NOT NULL,
[ValidToDate] [datetime] NOT NULL,
[Active] [bit] NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[VisitID] [int] NOT NULL,
[CategoryID] [int] NOT NULL,
[TimeStamp] [datetime] NULL CONSTRAINT [DF_FactCategoryResult_TimeStamp] DEFAULT (getdate()),
[ChangeReason] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactCategoryResult] ADD CONSTRAINT [PK_FactCategoryResult] PRIMARY KEY CLUSTERED  ([CategoryResultKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [indexFactCategoryResult_Table] ON [dbo].[FactCategoryResult] ([Source], [Active], [VisitID], [CategoryID]) INCLUDE ([AssessorKey], [BranchKey], [Category], [CategoryResultKey], [ChangeReason], [GeographyKey], [IncludeInAnalysis], [MaxScore], [PeriodKey], [Questionnaire], [Score], [SubQuestionnaire], [TimeStamp], [ValidFromDate], [ValidToDate], [VisitDateKey], [VisitKey], [VisitMaxScore], [VisitScore], [VisitScoreMethod]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_FactCategoryResult_7_1841441634__K22_K20_K10_K2_K3_K12_K5_K11_K13_K14] ON [dbo].[FactCategoryResult] ([VisitID], [Active], [Category], [VisitKey], [BranchKey], [MaxScore], [PeriodKey], [Score], [VisitScore], [VisitMaxScore]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactCategoryResult] ADD CONSTRAINT [FK_FactCategoryResult_DimAssessor] FOREIGN KEY ([AssessorKey]) REFERENCES [dbo].[DimAssessor] ([AssessorKey])
GO
ALTER TABLE [dbo].[FactCategoryResult] ADD CONSTRAINT [FK_FactCategoryResult_DimBranch] FOREIGN KEY ([BranchKey]) REFERENCES [dbo].[DimBranch] ([BranchKey])
GO
ALTER TABLE [dbo].[FactCategoryResult] ADD CONSTRAINT [FK_FactCategoryResult_DimGeography] FOREIGN KEY ([GeographyKey]) REFERENCES [dbo].[DimGeography] ([GeographyKey])
GO
ALTER TABLE [dbo].[FactCategoryResult] ADD CONSTRAINT [FK_FactCategoryResult_DimPeriod] FOREIGN KEY ([PeriodKey]) REFERENCES [dbo].[DimPeriod] ([PeriodKey])
GO
ALTER TABLE [dbo].[FactCategoryResult] ADD CONSTRAINT [FK_FactCategoryResult_DimDate] FOREIGN KEY ([VisitDateKey]) REFERENCES [dbo].[DimDate] ([DateKey])
GO
ALTER TABLE [dbo].[FactCategoryResult] ADD CONSTRAINT [FK_FactCategoryResult_DimVisit] FOREIGN KEY ([VisitKey]) REFERENCES [dbo].[DimVisit] ([VisitKey])
GO
