CREATE TABLE [dbo].[ReportGroupManager]
(
[ReportGroupManagerID] [int] NOT NULL,
[ReportGroupID] [int] NOT NULL,
[ManagerID] [int] NOT NULL,
[Active] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReportGroupManager] ADD CONSTRAINT [PK_ReportGroupManager] PRIMARY KEY CLUSTERED  ([ReportGroupManagerID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReportGroupManager] ADD CONSTRAINT [FK_ReportGroupManager_ReportGroup] FOREIGN KEY ([ReportGroupID]) REFERENCES [dbo].[ReportGroup] ([ReportGroupID])
GO
