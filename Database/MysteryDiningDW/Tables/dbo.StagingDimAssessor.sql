CREATE TABLE [dbo].[StagingDimAssessor]
(
[SAssessorKey] [int] NOT NULL IDENTITY(1, 1),
[AssessorKey] [int] NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[AssessorID] [int] NOT NULL,
[ValidToDate] [datetime] NULL,
[Type] [nchar] (10) COLLATE Latin1_General_CI_AS NULL,
[LastLoginDateKey] [int] NULL,
[TotalWarnings] [int] NULL,
[RecentWarnings] [int] NULL,
[LatestWarningDateKey] [int] NULL,
[GeographyKey] [int] NULL,
[Age] [int] NULL,
[ApplicationDateKey] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingDimAssessor] ADD CONSTRAINT [PK_StagingDimAssessor] PRIMARY KEY CLUSTERED  ([SAssessorKey]) ON [PRIMARY]
GO
