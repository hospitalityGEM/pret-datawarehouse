CREATE TABLE [dbo].[DimRecommendation]
(
[RecommendationKey] [int] NOT NULL IDENTITY(1, 1),
[Recommendation] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NOT NULL,
[CountOfViews] [int] NOT NULL,
[PostedToFacebook] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[PostedToTwitter] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[Emailed] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[RecommendationStatus] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ValidFromDate] [datetime] NULL,
[ValidToDate] [datetime] NULL,
[Active] [bit] NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[RecommendationID] [int] NOT NULL,
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_DimRecommendation_TimeStamp] DEFAULT (getdate()),
[ChangeReason] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimRecommendation] ADD CONSTRAINT [PK_DimRecommendationView] PRIMARY KEY CLUSTERED  ([RecommendationKey]) ON [PRIMARY]
GO
