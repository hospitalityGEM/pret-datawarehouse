CREATE TABLE [dbo].[ReportGroupException]
(
[ReportGroupExceptionID] [int] NOT NULL,
[ReportID] [int] NOT NULL,
[ManagerID] [int] NOT NULL,
[Allow] [bit] NOT NULL,
[AddedByAdminID] [smallint] NOT NULL,
[DateAdded] [datetime] NOT NULL,
[DateRemoved] [datetime] NULL,
[RemovedByAdminID] [smallint] NULL,
[Active] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReportGroupException] ADD CONSTRAINT [PK_ReportGroupException] PRIMARY KEY CLUSTERED  ([ReportGroupExceptionID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReportGroupException] ADD CONSTRAINT [FK_ReportGroupException_Report] FOREIGN KEY ([ReportID]) REFERENCES [dbo].[Report] ([ReportID])
GO
