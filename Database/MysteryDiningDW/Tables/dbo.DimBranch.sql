CREATE TABLE [dbo].[DimBranch]
(
[BranchKey] [int] NOT NULL IDENTITY(1, 1),
[Branch] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Brand] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Client] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Area] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[CurrentArea] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Region] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[CurrentRegion] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[IndustrySector] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ClientSegment] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[ClientStatus] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[BrandStatus] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[BranchStatus] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ValidFromDate] [datetime] NULL,
[ValidToDate] [datetime] NULL,
[Active] [bit] NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[GeographyKey] [int] NULL,
[ClientID] [int] NOT NULL,
[BrandID] [int] NOT NULL,
[BranchID] [int] NOT NULL,
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_DimBranch_TimeStamp] DEFAULT (getdate()),
[ChangeReason] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[CurrentBranch] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[BranchCode] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[CurrentBrand] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[CurrentClient] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[AccountManager] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[LowThreshold] [int] NULL,
[HighThreshold] [int] NULL,
[Target] [int] NULL,
[Location] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Telephone] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[CurrentClientSegment] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[OurLocation] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[IsInternational] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[NBArea] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[NBRegion] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[CurrentNBArea] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[CurrentNBRegion] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[SubBrand] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[AgreementType] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[NBStartDateKey] [int] NULL,
[NBEndDateKey] [int] NULL,
[ClosedBranch] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimBranch] ADD CONSTRAINT [PK_DimBranch] PRIMARY KEY CLUSTERED  ([BranchKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_DimBranch_12_801437929__K16_K4_K13_K21_K1_K19_K7_K2] ON [dbo].[DimBranch] ([Active], [Client], [BranchStatus], [BranchID], [BranchKey], [ClientID], [Region], [Branch]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_DimBranch_12_801437929__K21_1] ON [dbo].[DimBranch] ([BranchID]) INCLUDE ([BranchKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [indexDimBranch_IDKey] ON [dbo].[DimBranch] ([BranchID], [BranchKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_index_DimBranch_B271_1] ON [dbo].[DimBranch] ([BranchID], [ClientID], [Region], [BranchStatus], [Active], [BranchKey], [Client]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_DimBranch_12_801437929__K4_K13_K16_K19_K21] ON [dbo].[DimBranch] ([Client], [BranchStatus], [Active], [ClientID], [BranchID]) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_801437929_16_1_19] ON [dbo].[DimBranch] ([Active], [BranchKey], [ClientID])
GO
CREATE STATISTICS [_dta_stat_801437929_21_16_4] ON [dbo].[DimBranch] ([Active], [Client], [BranchID])
GO
CREATE STATISTICS [_dta_stat_801437929_16_4_13_1] ON [dbo].[DimBranch] ([Active], [Client], [BranchStatus], [BranchKey])
GO
CREATE STATISTICS [_dta_stat_801437929_2_21_19_7_4_13] ON [dbo].[DimBranch] ([Branch], [BranchID], [ClientID], [Region], [Client], [BranchStatus])
GO
CREATE STATISTICS [_dta_stat_801437929_1_19_7_16_4_13_2] ON [dbo].[DimBranch] ([Branch], [BranchKey], [ClientID], [Region], [Active], [Client], [BranchStatus])
GO
CREATE STATISTICS [_dta_stat_801437929_2_19_7_4_13_16_21] ON [dbo].[DimBranch] ([Branch], [ClientID], [Region], [Client], [BranchStatus], [Active], [BranchID])
GO
CREATE STATISTICS [_dta_stat_801437929_13_1_19_21_16] ON [dbo].[DimBranch] ([BranchID], [Active], [BranchStatus], [BranchKey], [ClientID])
GO
CREATE STATISTICS [_dta_stat_801437929_21_1_4] ON [dbo].[DimBranch] ([BranchID], [BranchKey], [Client])
GO
CREATE STATISTICS [_dta_stat_801437929_21_4_13_16_7_1] ON [dbo].[DimBranch] ([BranchID], [Client], [BranchStatus], [Active], [Region], [BranchKey])
GO
CREATE STATISTICS [_dta_stat_801437929_21_19_4_13] ON [dbo].[DimBranch] ([BranchID], [ClientID], [Client], [BranchStatus])
GO
CREATE STATISTICS [_dta_stat_801437929_21_7_16_4] ON [dbo].[DimBranch] ([BranchID], [Region], [Active], [Client])
GO
CREATE STATISTICS [_dta_stat_801437929_21_7_4_13] ON [dbo].[DimBranch] ([BranchID], [Region], [Client], [BranchStatus])
GO
CREATE STATISTICS [_dta_stat_801437929_1_21_19_16_4_13_7_2] ON [dbo].[DimBranch] ([BranchKey], [BranchID], [ClientID], [Active], [Client], [BranchStatus], [Region], [Branch])
GO
CREATE STATISTICS [_dta_stat_801437929_1_13_21] ON [dbo].[DimBranch] ([BranchKey], [BranchStatus], [BranchID])
GO
CREATE STATISTICS [_dta_stat_801437929_1_4_13] ON [dbo].[DimBranch] ([BranchKey], [Client], [BranchStatus])
GO
CREATE STATISTICS [_dta_stat_801437929_1_19_21_7_16_4] ON [dbo].[DimBranch] ([BranchKey], [ClientID], [BranchID], [Region], [Active], [Client])
GO
CREATE STATISTICS [_dta_stat_801437929_1_4_13_7_2_19] ON [dbo].[DimBranch] ([BranchStatus], [Region], [Branch], [ClientID], [BranchKey], [Client])
GO
CREATE STATISTICS [_dta_stat_801437929_4_21_13_7_2_19] ON [dbo].[DimBranch] ([Client], [BranchID], [BranchStatus], [Region], [Branch], [ClientID])
GO
CREATE STATISTICS [_dta_stat_801437929_4_13_16_21_1] ON [dbo].[DimBranch] ([Client], [BranchStatus], [Active], [BranchID], [BranchKey])
GO
CREATE STATISTICS [_dta_stat_801437929_4_13_16_19_1] ON [dbo].[DimBranch] ([Client], [BranchStatus], [Active], [ClientID], [BranchKey])
GO
CREATE STATISTICS [_dta_stat_801437929_4_13_16_7] ON [dbo].[DimBranch] ([Client], [BranchStatus], [Active], [Region])
GO
CREATE STATISTICS [_dta_stat_801437929_4_13_1_21_7_2_19] ON [dbo].[DimBranch] ([Client], [BranchStatus], [BranchKey], [BranchID], [Region], [Branch], [ClientID])
GO
CREATE STATISTICS [_dta_stat_801437929_4_13_7_2_19] ON [dbo].[DimBranch] ([Client], [BranchStatus], [Region], [Branch], [ClientID])
GO
CREATE STATISTICS [_dta_stat_801437929_19_21_16_4] ON [dbo].[DimBranch] ([ClientID], [BranchID], [Active], [Client])
GO
CREATE STATISTICS [_dta_stat_801437929_7_2_19_4_21] ON [dbo].[DimBranch] ([Region], [Branch], [ClientID], [Client], [BranchID])
GO
CREATE STATISTICS [_dta_stat_801437929_7_1] ON [dbo].[DimBranch] ([Region], [BranchKey])
GO
ALTER TABLE [dbo].[DimBranch] ADD CONSTRAINT [FK_DimBranch_DimGeography] FOREIGN KEY ([GeographyKey]) REFERENCES [dbo].[DimGeography] ([GeographyKey])
GO
GRANT SELECT ON  [dbo].[DimBranch] TO [TMDCReporting]
GO
