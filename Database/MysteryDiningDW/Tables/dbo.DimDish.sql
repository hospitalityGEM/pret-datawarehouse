CREATE TABLE [dbo].[DimDish]
(
[DishKey] [int] NOT NULL IDENTITY(1, 1),
[DishName] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[DishDescription] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[MenuSection] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[MenuName] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Client] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[MenuStatus] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ValidFromDate] [datetime] NOT NULL,
[ValidtoDate] [datetime] NOT NULL,
[Active] [bit] NOT NULL,
[DishID] [int] NOT NULL,
[MenuSectionID] [int] NOT NULL,
[MenuID] [int] NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[RowChangeReason] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimDish] ADD CONSTRAINT [PK_DimDish] PRIMARY KEY CLUSTERED  ([DishKey]) ON [PRIMARY]
GO
