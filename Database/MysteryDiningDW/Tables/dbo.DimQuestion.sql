CREATE TABLE [dbo].[DimQuestion]
(
[QuestionKey] [int] NOT NULL IDENTITY(1, 1),
[MasterQuestion] [nvarchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[Question] [nvarchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[Section] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[ManagementCategory] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Benchmark] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Department] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Type] [nvarchar] (20) COLLATE Latin1_General_CI_AS NOT NULL,
[Client] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[ValidFromDate] [datetime] NULL,
[ValidToDate] [datetime] NULL,
[Active] [bit] NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[QuestionID] [int] NOT NULL,
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_DimQuestion_TimeStamp] DEFAULT (getdate()),
[ChangeReason] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[AnalysisTag] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[QuestionnaireCategory] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[MenuQuestion] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Questionnaire] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_DimQuestion_Questionnaire] DEFAULT ((1)),
[SubQuestionnaire] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_DimQuestion_SubQuestionnaire] DEFAULT ((1)),
[HiddenSection] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_DimQuestion_HiddenSection] DEFAULT (N'Visible'),
[HiddenQuestion] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[MenuSection] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[QuestionCode] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_DimQuestion_QuestionCode] DEFAULT (N'N/A'),
[QuestionSubject] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_DimQuestion_QuestionSubject] DEFAULT (N'Unknown'),
[QuestionnaireID] [int] NULL,
[IncludeInAnalysis] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[ClientID] [int] NULL,
[CurrentQuestionnaire] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimQuestion] ADD CONSTRAINT [PK_DimQuestion] PRIMARY KEY CLUSTERED  ([QuestionKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [indexDimQuestion_QuestionSection] ON [dbo].[DimQuestion] ([QuestionKey]) INCLUDE ([Question], [Section]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [indexDimQuestion_TypeMenuQuestionQuestionKey] ON [dbo].[DimQuestion] ([Type], [MenuQuestion], [QuestionKey]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[DimQuestion] TO [TMDCReporting]
GO
