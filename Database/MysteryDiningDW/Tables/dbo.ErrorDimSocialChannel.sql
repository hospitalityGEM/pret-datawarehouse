CREATE TABLE [dbo].[ErrorDimSocialChannel]
(
[ErrorSocialChannelID] [int] NOT NULL IDENTITY(1, 1),
[SocialChannelKey] [int] NULL,
[SocialNetwork] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[AccountName] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DisplayName] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Link] [nvarchar] (500) COLLATE Latin1_General_CI_AS NULL,
[PicturePath] [nvarchar] (500) COLLATE Latin1_General_CI_AS NULL,
[Category] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[About] [nvarchar] (500) COLLATE Latin1_General_CI_AS NULL,
[Location] [nvarchar] (500) COLLATE Latin1_General_CI_AS NULL,
[SocialChannelID] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[TimeStamp] [datetime] NULL CONSTRAINT [DF_ErrorDimSocialChannel_TimeStamp] DEFAULT (getdate()),
[ChangeReason] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[InvalidInput] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[InvalidInputErrorDescription] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[ErrorCode] [int] NULL,
[ErrorColumn] [int] NULL,
[Resolved] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ErrorDimSocialChannel] ADD CONSTRAINT [PK_ErrorDimSocialChannel] PRIMARY KEY CLUSTERED  ([ErrorSocialChannelID]) ON [PRIMARY]
GO
