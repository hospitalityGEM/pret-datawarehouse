CREATE TABLE [dbo].[ErrorDimRecommendation]
(
[ErrorRecommendationID] [int] NOT NULL IDENTITY(1, 1),
[RecommendationKey] [int] NULL,
[Recommendation] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[CountOfViews] [int] NULL,
[PostedToFacebook] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[PostedToTwitter] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[Emailed] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[RecommendationStatus] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ValidFromDate] [datetime] NULL,
[ValidToDate] [datetime] NULL,
[Active] [bit] NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[RecommendationID] [int] NULL,
[TimeStamp] [datetime] NULL,
[ChangeReason] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[InvalidInput] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[InvalidInputErrorDescription] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[ErrorCode] [int] NULL,
[ErrorColumn] [int] NULL,
[Resolved] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ErrorDimRecommendation] ADD CONSTRAINT [PK_DimRecommendationError] PRIMARY KEY CLUSTERED  ([ErrorRecommendationID]) ON [PRIMARY]
GO
