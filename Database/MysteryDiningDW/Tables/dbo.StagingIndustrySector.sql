CREATE TABLE [dbo].[StagingIndustrySector]
(
[StagingIndustrySectorID] [int] NOT NULL IDENTITY(1, 1),
[SectorID] [int] NOT NULL,
[GroupName] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingIndustrySector] ADD CONSTRAINT [PK_StagingIndustrySector] PRIMARY KEY CLUSTERED  ([StagingIndustrySectorID]) ON [PRIMARY]
GO
