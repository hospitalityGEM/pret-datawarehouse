CREATE TABLE [dbo].[ConfigType]
(
[ConfigTypeID] [tinyint] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConfigType] ADD CONSTRAINT [PK_ConfigType] PRIMARY KEY CLUSTERED  ([ConfigTypeID]) ON [PRIMARY]
GO
