CREATE TABLE [dbo].[StagingFactVisitResult]
(
[StagingFVRID] [int] NOT NULL IDENTITY(1, 1),
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[VisitID] [int] NOT NULL,
[ValidToDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingFactVisitResult] ADD CONSTRAINT [PK_StagingFactVisitResult] PRIMARY KEY CLUSTERED  ([StagingFVRID]) ON [PRIMARY]
GO
