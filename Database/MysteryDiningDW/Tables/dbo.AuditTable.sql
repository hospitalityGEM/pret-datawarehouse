CREATE TABLE [dbo].[AuditTable]
(
[AuditTableID] [tinyint] NOT NULL IDENTITY(1, 1),
[TableName] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Notes] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Active] [bit] NOT NULL CONSTRAINT [DF_AuditTable_Active] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuditTable] ADD CONSTRAINT [PK_AuditTable] PRIMARY KEY CLUSTERED  ([AuditTableID]) ON [PRIMARY]
GO
