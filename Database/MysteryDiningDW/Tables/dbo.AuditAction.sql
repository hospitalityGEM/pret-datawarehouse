CREATE TABLE [dbo].[AuditAction]
(
[AuditActionID] [tinyint] NOT NULL IDENTITY(1, 1),
[ActionName] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuditAction] ADD CONSTRAINT [PK_AuditAction] PRIMARY KEY CLUSTERED  ([AuditActionID]) ON [PRIMARY]
GO
