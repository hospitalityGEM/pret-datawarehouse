CREATE TABLE [dbo].[StagingFactQuestionResult]
(
[StagingFQRID] [int] NOT NULL IDENTITY(1, 1),
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[TableSource] [nvarchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[QQID] [int] NOT NULL,
[QuestionResultID] [int] NOT NULL,
[ValidToDate] [datetime] NOT NULL,
[CurrentScore] [decimal] (18, 2) NOT NULL,
[VisitID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingFactQuestionResult] ADD CONSTRAINT [PK_StagingFactQuestionResult_1] PRIMARY KEY CLUSTERED  ([StagingFQRID]) ON [PRIMARY]
GO
