CREATE TABLE [dbo].[ErrorFactSocialChannelCount]
(
[ErrorSocialChannelCountID] [int] NOT NULL IDENTITY(1, 1),
[SocialChannelCountKey] [int] NULL,
[SocialChannelKey] [int] NULL,
[CountedDateKey] [int] NULL,
[ChannelCount] [int] NULL,
[TalkingAboutCount] [int] NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[SocialChannelCountID] [int] NULL,
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_ErrorFactSocialChannelCount_TimeStamp] DEFAULT (getdate()),
[ErrorCode] [int] NULL,
[ErrorColumn] [int] NULL,
[Resolved] [bit] NULL CONSTRAINT [DF_ErrorFactSocialChannelCount_Resolved] DEFAULT ((0)),
[DateTimeCounted] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ErrorFactSocialChannelCount] ADD CONSTRAINT [PK_ErrorFactSocialChannelCount] PRIMARY KEY CLUSTERED  ([ErrorSocialChannelCountID]) ON [PRIMARY]
GO
