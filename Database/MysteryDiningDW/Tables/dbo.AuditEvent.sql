CREATE TABLE [dbo].[AuditEvent]
(
[AuditEventID] [int] NOT NULL IDENTITY(1, 1),
[Timestamp] [datetime] NOT NULL,
[AdminID] [smallint] NOT NULL,
[AuditTableID] [tinyint] NOT NULL,
[AuditActionID] [tinyint] NOT NULL,
[ID] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[Hidden] [bit] NOT NULL,
[Note] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuditEvent] ADD CONSTRAINT [PK_AuditEvent] PRIMARY KEY CLUSTERED  ([AuditEventID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuditEvent] ADD CONSTRAINT [FK_AuditEvent_AuditAction] FOREIGN KEY ([AuditActionID]) REFERENCES [dbo].[AuditAction] ([AuditActionID])
GO
ALTER TABLE [dbo].[AuditEvent] ADD CONSTRAINT [FK_AuditEvent_AuditTable] FOREIGN KEY ([AuditTableID]) REFERENCES [dbo].[AuditTable] ([AuditTableID])
GO
