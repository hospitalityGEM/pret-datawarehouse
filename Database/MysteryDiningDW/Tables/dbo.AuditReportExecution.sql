CREATE TABLE [dbo].[AuditReportExecution]
(
[AuditReportExecutionID] [int] NOT NULL,
[Timestamp] [datetime] NOT NULL,
[ManagerID] [int] NOT NULL,
[ReportID] [int] NOT NULL,
[Paramaters] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
