CREATE TABLE [dbo].[ErrorSpiritDataExtract]
(
[ErrorID] [int] NOT NULL IDENTITY(1, 1),
[VisitID] [int] NOT NULL,
[ErrorDescription] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[NewVisit] [bit] NULL,
[TimeStamp] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ErrorSpiritDataExtract] ADD CONSTRAINT [PK__ErrorSpi__358565CA21A3EE4E] PRIMARY KEY CLUSTERED  ([ErrorID]) ON [PRIMARY]
GO
