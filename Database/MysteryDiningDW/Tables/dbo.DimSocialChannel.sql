CREATE TABLE [dbo].[DimSocialChannel]
(
[SocialChannelKey] [int] NOT NULL IDENTITY(1, 1),
[SocialNetwork] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[AccountName] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[DisplayName] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Link] [nvarchar] (500) COLLATE Latin1_General_CI_AS NOT NULL,
[PicturePath] [nvarchar] (500) COLLATE Latin1_General_CI_AS NOT NULL,
[Category] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[About] [nvarchar] (500) COLLATE Latin1_General_CI_AS NOT NULL,
[Location] [nvarchar] (500) COLLATE Latin1_General_CI_AS NOT NULL,
[SocialChannelID] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[ValidFromDate] [datetime] NOT NULL,
[ValidToDate] [datetime] NOT NULL,
[Active] [bit] NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_DimSocialChannel_TimeStamp] DEFAULT (getdate()),
[ChangeReason] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimSocialChannel] ADD CONSTRAINT [PK_DimSocialChannel] PRIMARY KEY CLUSTERED  ([SocialChannelKey]) ON [PRIMARY]
GO
