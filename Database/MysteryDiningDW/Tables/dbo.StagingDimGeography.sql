CREATE TABLE [dbo].[StagingDimGeography]
(
[SGeographyKey] [int] NOT NULL IDENTITY(1, 1),
[GeographyKey] [int] NULL,
[Source] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[TableSource] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[SourceID] [int] NOT NULL,
[ValidToDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingDimGeography] ADD CONSTRAINT [PK_StagingDimGeography_1] PRIMARY KEY CLUSTERED  ([SGeographyKey]) ON [PRIMARY]
GO
