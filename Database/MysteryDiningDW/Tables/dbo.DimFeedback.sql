CREATE TABLE [dbo].[DimFeedback]
(
[FeedbackKey] [int] NOT NULL IDENTITY(1, 1),
[CompletedDateKey] [int] NOT NULL,
[StartTime] [datetime] NOT NULL,
[EndTime] [datetime] NOT NULL,
[ElapsedTime] [int] NOT NULL,
[IPAddress] [nvarchar] (30) COLLATE Latin1_General_CI_AS NOT NULL,
[CookieGUID] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[DiscardReason] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[DiscardValue] [int] NULL,
[OFDID] [int] NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[FeedbackID] [int] NOT NULL,
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_DimFeedback_TimeStamp] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimFeedback] ADD CONSTRAINT [PK_DimFeedback] PRIMARY KEY CLUSTERED  ([FeedbackKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimFeedback_FeedbackID] ON [dbo].[DimFeedback] ([FeedbackID]) INCLUDE ([CompletedDateKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimFeedback] ADD CONSTRAINT [FK_DimFeedback_DimDate] FOREIGN KEY ([CompletedDateKey]) REFERENCES [dbo].[DimDate] ([DateKey])
GO
