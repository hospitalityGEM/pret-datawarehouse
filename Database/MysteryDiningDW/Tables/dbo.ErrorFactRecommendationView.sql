CREATE TABLE [dbo].[ErrorFactRecommendationView]
(
[ErrorRecommendationViewID] [int] NOT NULL IDENTITY(1, 1),
[RecommendationViewKey] [int] NULL,
[IPAddress] [nvarchar] (30) COLLATE Latin1_General_CI_AS NULL,
[CookieGUID] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ViewDateKey] [int] NULL,
[ViewTimestamp] [datetime] NULL,
[RecommendationKey] [int] NULL,
[GuestSource] [nvarchar] (30) COLLATE Latin1_General_CI_AS NULL,
[VisitKey] [int] NULL,
[GuestMarketingKey] [int] NULL,
[FeedbackKey] [int] NULL,
[BranchKey] [int] NULL,
[PeriodKey] [int] NULL,
[FacebookLiked] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[TwitterFollowed] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[RecommendationStatus] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[RecommendationViewID] [int] NULL,
[TimeStamp] [datetime] NULL,
[ChangeReason] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[InvalidInput] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[InvalidInputErrorDescription] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[ErrorCode] [int] NULL,
[ErrorColumn] [int] NULL,
[Resolved] [bit] NULL,
[FacebookSocialChannelKey] [int] NULL,
[TwitterSocialChannelKey] [int] NULL,
[ValidFromDate] [datetime] NULL,
[ValidToDate] [datetime] NULL,
[Active] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ErrorFactRecommendationView] ADD CONSTRAINT [PK_FactRecommendationViewError] PRIMARY KEY CLUSTERED  ([ErrorRecommendationViewID]) ON [PRIMARY]
GO
