CREATE TABLE [dbo].[ErrorFactLinkTracking]
(
[ErrorLinkTrackingID] [int] NOT NULL IDENTITY(1, 1),
[LinkTrackingKey] [int] NULL,
[IPAddress] [nvarchar] (30) COLLATE Latin1_General_CI_AS NULL,
[CookieGUID] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ClickedDateKey] [int] NULL,
[ClickedTimestamp] [datetime] NULL,
[RecommendationKey] [int] NULL,
[GuestSource] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[VisitKey] [int] NULL,
[GuestMarketingKey] [int] NULL,
[FeedbackKey] [int] NULL,
[BranchKey] [int] NULL,
[PeriodKey] [int] NULL,
[Destination] [nvarchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[LinkTrackingID] [int] NULL,
[TimeStamp] [datetime] NULL,
[ChangeReason] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[InvalidInput] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[InvalidInputErrorDescription] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[ErrorCode] [int] NULL,
[ErrorColumn] [int] NULL,
[Resolved] [bit] NULL,
[DestinationSource] [nvarchar] (30) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ErrorFactLinkTracking] ADD CONSTRAINT [PK_FactLinkTrackingError] PRIMARY KEY CLUSTERED  ([ErrorLinkTrackingID]) ON [PRIMARY]
GO
