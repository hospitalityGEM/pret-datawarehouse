CREATE TABLE [dbo].[FactAdvocacyFacebook]
(
[AdvocacyFacebookKey] [int] NOT NULL IDENTITY(1, 1),
[VisitScore] [decimal] (18, 2) NOT NULL,
[VisitMaxScore] [decimal] (18, 2) NOT NULL,
[CompletionDateKey] [int] NOT NULL,
[VisitDateKey] [int] NOT NULL,
[BranchKey] [int] NOT NULL,
[PeriodKey] [int] NOT NULL,
[RecommendationKey] [int] NOT NULL,
[LikedBefore] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[LikedAfter] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[ConvertedToLiker] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[UserAccount] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[PostedMessage] [nvarchar] (4000) COLLATE Latin1_General_CI_AS NULL,
[MarketingMessage] [nvarchar] (500) COLLATE Latin1_General_CI_AS NOT NULL,
[Friends] [int] NULL,
[Posted] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[Advocate] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[RecommendationStatus] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[GuestMarketingKey] [int] NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[AdvocacyFacebookID] [int] NOT NULL,
[ValidFromDate] [datetime] NULL,
[ValidToDate] [datetime] NULL,
[Active] [bit] NULL,
[RowChangeReason] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[SocialChannelKey] [int] NOT NULL CONSTRAINT [DF_FactAdvocacyFacebook_SocialChannelKey] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactAdvocacyFacebook] ADD CONSTRAINT [PK_FactAdvocacyFacebook] PRIMARY KEY CLUSTERED  ([AdvocacyFacebookKey]) ON [PRIMARY]
GO
