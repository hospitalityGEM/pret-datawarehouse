CREATE TABLE [dbo].[ErrorFactAdvocacyTwitter]
(
[ErrorFactAdvocacyTwitterID] [int] NOT NULL IDENTITY(1, 1),
[AdvocacyTwitterKey] [int] NULL,
[VisitScore] [decimal] (18, 2) NULL,
[VisitMaxScore] [decimal] (18, 2) NULL,
[CompletionDateKey] [int] NULL,
[VisitDateKey] [int] NULL,
[BranchKey] [int] NULL,
[PeriodKey] [int] NULL,
[RecommendationKey] [int] NULL,
[FollowingBefore] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[FollowingAfter] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[ConvertedToFollower] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[UserAccount] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Tweet] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Followers] [int] NULL,
[FollowedAccount] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Tweeted] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[RecommendationStatus] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[AdvocacyTwitterID] [int] NULL,
[TimeStamp] [datetime] NULL CONSTRAINT [DF_ErrorFactAdvocacyTwitter_TimeStamp] DEFAULT (getdate()),
[InvalidInput] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[InvalidInputErrorDescription] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[ErrorCode] [int] NULL,
[ErrorColumn] [int] NULL,
[Resolved] [bit] NULL,
[Advocate] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[GuestMarketingKey] [int] NULL,
[RowChangeReason] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[ValidFromDate] [datetime] NULL,
[ValidToDate] [datetime] NULL,
[Active] [bit] NULL,
[SocialChannelKey] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ErrorFactAdvocacyTwitter] ADD CONSTRAINT [PK_ErrorFactAdvocacyTwitter] PRIMARY KEY CLUSTERED  ([ErrorFactAdvocacyTwitterID]) ON [PRIMARY]
GO
