CREATE TABLE [dbo].[ConfigInfoDetail]
(
[ConfigInfoDetailID] [int] NOT NULL,
[ConfigInfoID] [smallint] NOT NULL,
[Key] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Value] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[LastUpdated] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConfigInfoDetail] ADD CONSTRAINT [PK_ConfigInfoDetail] PRIMARY KEY CLUSTERED  ([ConfigInfoDetailID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConfigInfoDetail] ADD CONSTRAINT [FK_ConfigInfoDetail_ConfigInfo] FOREIGN KEY ([ConfigInfoID]) REFERENCES [dbo].[ConfigInfo] ([ConfigInfoID])
GO
