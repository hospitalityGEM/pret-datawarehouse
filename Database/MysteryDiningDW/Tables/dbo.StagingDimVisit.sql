CREATE TABLE [dbo].[StagingDimVisit]
(
[SVisitKey] [int] NOT NULL IDENTITY(1, 1),
[VisitKey] [int] NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[VisitID] [int] NOT NULL,
[ValidToDate] [datetime] NULL,
[Active] [bit] NULL,
[Type] [nchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[CurrentVisitScore] [decimal] (18, 2) NULL,
[PassFail] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[PassFailMessage] [nvarchar] (300) COLLATE Latin1_General_CI_AS NULL,
[CurrentScore] [decimal] (18, 2) NULL,
[CurrentMaxScore] [decimal] (18, 2) NULL,
[VisitScoreText] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[VisitScoreMethod] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[QuestionnaireCategory] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[AdjustedScore] [decimal] (18, 2) NULL,
[LatestReviewDateKey] [int] NULL,
[InitialReviewDateKey] [int] NULL,
[DiscardReasonOrigin] [nvarchar] (150) COLLATE Latin1_General_CI_AS NULL,
[DiscardReason] [nvarchar] (250) COLLATE Latin1_General_CI_AS NULL,
[DiscardReasonDetail] [nvarchar] (250) COLLATE Latin1_General_CI_AS NULL,
[IsNBVisit] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingDimVisit] ADD CONSTRAINT [PK_StagingDimVisit] PRIMARY KEY CLUSTERED  ([SVisitKey]) ON [PRIMARY]
GO
