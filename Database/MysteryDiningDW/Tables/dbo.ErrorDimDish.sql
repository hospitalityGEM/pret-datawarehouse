CREATE TABLE [dbo].[ErrorDimDish]
(
[ErrorDishKey] [int] NOT NULL IDENTITY(1, 1),
[DishKey] [int] NOT NULL,
[DishName] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[DishDescription] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[MenuSection] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[MenuName] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Client] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[MenuStatus] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ValidFromDate] [datetime] NOT NULL,
[ValidtoDate] [datetime] NOT NULL,
[Active] [bit] NOT NULL,
[DishID] [int] NOT NULL,
[MenuSectionID] [int] NOT NULL,
[MenuID] [int] NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[InvalidInput] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[InvalidInputErrorDescription] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[ErrorCode] [int] NULL,
[ErrorColumn] [int] NULL,
[RowChangeReason] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Resolved] [bit] NULL,
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_ErrorDimDish_TimeStamp] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ErrorDimDish] ADD CONSTRAINT [PK_ErrorDimDish] PRIMARY KEY CLUSTERED  ([ErrorDishKey]) ON [PRIMARY]
GO
