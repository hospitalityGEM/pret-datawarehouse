CREATE TABLE [dbo].[ErrorDimAdmin]
(
[DimAdminErrorID] [int] NOT NULL IDENTITY(1, 1),
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_ErrorDimAdmin_TimeStamp] DEFAULT (getdate()),
[AdminKey] [int] NULL,
[AdminName] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Company] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[AdminID] [int] NULL,
[InvalidInput] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[InvalidInputErrorDescription] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[ErrorCode] [int] NULL,
[ErrorColumn] [int] NULL,
[RowChangeReason] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Resolved] [bit] NOT NULL CONSTRAINT [DF_ErrorDimAdmin_Resolved] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ErrorDimAdmin] ADD CONSTRAINT [PK_ErrorDimAdmin] PRIMARY KEY CLUSTERED  ([DimAdminErrorID]) ON [PRIMARY]
GO
