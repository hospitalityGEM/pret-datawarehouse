CREATE TABLE [dbo].[ReportType]
(
[ReportTypeID] [tinyint] NOT NULL IDENTITY(1, 1),
[TypeName] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Active] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReportType] ADD CONSTRAINT [PK_ReportType] PRIMARY KEY CLUSTERED  ([ReportTypeID]) ON [PRIMARY]
GO
