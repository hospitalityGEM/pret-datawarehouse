CREATE TABLE [dbo].[FactLinkTracking]
(
[LinkTrackingKey] [int] NOT NULL IDENTITY(1, 1),
[IPAddress] [nvarchar] (30) COLLATE Latin1_General_CI_AS NOT NULL,
[CookieGUID] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ClickedDateKey] [int] NOT NULL,
[ClickedTimestamp] [datetime] NOT NULL,
[RecommendationKey] [int] NOT NULL,
[GuestSource] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[VisitKey] [int] NOT NULL,
[GuestMarketingKey] [int] NOT NULL,
[FeedbackKey] [int] NOT NULL,
[BranchKey] [int] NOT NULL,
[PeriodKey] [int] NOT NULL,
[Destination] [nvarchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[LinkTrackingID] [int] NOT NULL,
[DestinationSource] [nvarchar] (30) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactLinkTracking] ADD CONSTRAINT [PK_FactLinkTracking] PRIMARY KEY CLUSTERED  ([LinkTrackingKey]) ON [PRIMARY]
GO
