CREATE TABLE [dbo].[ConfigExceptionType]
(
[ConfigExceptionTypeID] [tinyint] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConfigExceptionType] ADD CONSTRAINT [PK_ConfigExceptionType] PRIMARY KEY CLUSTERED  ([ConfigExceptionTypeID]) ON [PRIMARY]
GO
