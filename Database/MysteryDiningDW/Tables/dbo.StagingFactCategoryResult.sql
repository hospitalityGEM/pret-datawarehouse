CREATE TABLE [dbo].[StagingFactCategoryResult]
(
[StagingFCRID] [int] NOT NULL IDENTITY(1, 1),
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[CategoryID] [int] NOT NULL,
[VisitID] [int] NOT NULL,
[ValidToDate] [datetime] NOT NULL,
[Type] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[VisitScore] [decimal] (18, 2) NULL,
[VisitMaxScore] [decimal] (18, 2) NULL,
[VisitScoreText] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[VisitScoreMethod] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingFactCategoryResult] ADD CONSTRAINT [PK_StagingFactCategoryResult_1] PRIMARY KEY CLUSTERED  ([StagingFCRID]) ON [PRIMARY]
GO
