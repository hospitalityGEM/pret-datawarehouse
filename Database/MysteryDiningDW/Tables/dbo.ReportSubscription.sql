CREATE TABLE [dbo].[ReportSubscription]
(
[ReportSubscriptionID] [int] NOT NULL,
[ReportID] [int] NOT NULL,
[BrandID] [int] NOT NULL,
[AddedByAdminID] [smallint] NOT NULL,
[RemovedByAdminID] [smallint] NULL,
[DateAdded] [datetime] NOT NULL,
[DateRemoved] [datetime] NULL,
[ValidFromDate] [datetime] NOT NULL,
[ValidToDate] [datetime] NULL,
[Active] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReportSubscription] ADD CONSTRAINT [PK_ReportSubscription] PRIMARY KEY CLUSTERED  ([ReportSubscriptionID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReportSubscription] ADD CONSTRAINT [FK_ReportSubscription_Report] FOREIGN KEY ([ReportID]) REFERENCES [dbo].[Report] ([ReportID])
GO
