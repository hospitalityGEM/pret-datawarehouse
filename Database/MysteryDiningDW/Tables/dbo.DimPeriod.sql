CREATE TABLE [dbo].[DimPeriod]
(
[PeriodKey] [int] NOT NULL IDENTITY(1, 1),
[PeriodName] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Client] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Brand] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[ClientPeriodsAgo] [int] NULL,
[BrandPeriodsAgo] [int] NULL,
[ClientPeriodOfYear] [int] NULL,
[BrandPeriodOfYear] [int] NULL,
[PeriodOfBrand] [int] NULL,
[PeriodOfClient] [int] NULL,
[PeriodType] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[VisitType] [nvarchar] (20) COLLATE Latin1_General_CI_AS NOT NULL,
[StartDateKey] [int] NOT NULL,
[EndDateKey] [int] NOT NULL,
[CurrentPeriod] [bit] NULL,
[IncludeInAnalysis] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[ValidFromDate] [datetime] NOT NULL,
[ValidToDate] [datetime] NOT NULL,
[Active] [bit] NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[ClientID] [int] NOT NULL,
[BrandID] [int] NOT NULL,
[PeriodID] [int] NOT NULL,
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_DimPeriod_TimeStamp] DEFAULT (getdate()),
[ChangeReason] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Year] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[CurrentPeriodName] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[IncludeInPortal] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF__DimPeriod__Inclu__7BB1235D] DEFAULT ('Include'),
[YearStartKey] [int] NULL,
[YearEndKey] [int] NULL
) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_DimPeriod_Active_ClientID_ClientPeriodsAgo] ON [dbo].[DimPeriod] ([Active], [ClientID], [ClientPeriodsAgo]) INCLUDE ([Brand], [BrandID], [BrandPeriodOfYear], [BrandPeriodsAgo], [ChangeReason], [Client], [ClientPeriodOfYear], [CurrentPeriod], [CurrentPeriodName], [EndDateKey], [IncludeInAnalysis], [IncludeInPortal], [PeriodID], [PeriodKey], [PeriodName], [PeriodOfBrand], [PeriodOfClient], [PeriodType], [Source], [StartDateKey], [TimeStamp], [ValidFromDate], [ValidToDate], [VisitType], [Year]) ON [PRIMARY]

GO
ALTER TABLE [dbo].[DimPeriod] ADD CONSTRAINT [PK_DimPeriod] PRIMARY KEY CLUSTERED  ([PeriodKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_DimPeriod_12_669961463__K16_K19_K23] ON [dbo].[DimPeriod] ([IncludeInAnalysis], [Active], [PeriodID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_DimPeriod_12_669961463__K16_K5_K1_K23] ON [dbo].[DimPeriod] ([IncludeInAnalysis], [ClientPeriodsAgo], [PeriodKey], [PeriodID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_DimPeriod_7_669961463__K16_K23] ON [dbo].[DimPeriod] ([IncludeInAnalysis], [PeriodID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_DimPeriod_12_669961463__K16_K1] ON [dbo].[DimPeriod] ([IncludeInAnalysis], [PeriodKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_DimPeriod_12_669961463__K23_K16_K19_K2_K5_K1] ON [dbo].[DimPeriod] ([PeriodID], [IncludeInAnalysis], [Active], [PeriodName], [ClientPeriodsAgo], [PeriodKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [indexDimPeriod_IDKey] ON [dbo].[DimPeriod] ([PeriodKey], [PeriodID]) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_669961463_19_1_21_23] ON [dbo].[DimPeriod] ([Active], [PeriodKey], [ClientID], [PeriodID])
GO
CREATE STATISTICS [_dta_stat_669961463_19_1_23_16_2_5] ON [dbo].[DimPeriod] ([Active], [PeriodKey], [PeriodID], [IncludeInAnalysis], [PeriodName], [ClientPeriodsAgo])
GO
CREATE STATISTICS [_dta_stat_669961463_21_5_16] ON [dbo].[DimPeriod] ([ClientID], [ClientPeriodsAgo], [IncludeInAnalysis])
GO
CREATE STATISTICS [_dta_stat_669961463_21_23_1] ON [dbo].[DimPeriod] ([ClientID], [PeriodID], [PeriodKey])
GO
CREATE STATISTICS [_dta_stat_669961463_13_23_27_5_16_21] ON [dbo].[DimPeriod] ([ClientID], [StartDateKey], [PeriodID], [CurrentPeriodName], [ClientPeriodsAgo], [IncludeInAnalysis])
GO
CREATE STATISTICS [_dta_stat_669961463_5_23_16] ON [dbo].[DimPeriod] ([ClientPeriodsAgo], [PeriodID], [IncludeInAnalysis])
GO
CREATE STATISTICS [_dta_stat_669961463_16_19_23_2] ON [dbo].[DimPeriod] ([IncludeInAnalysis], [Active], [PeriodID], [PeriodName])
GO
CREATE STATISTICS [_dta_stat_669961463_16_19_1] ON [dbo].[DimPeriod] ([IncludeInAnalysis], [Active], [PeriodKey])
GO
CREATE STATISTICS [_dta_stat_669961463_16_1] ON [dbo].[DimPeriod] ([IncludeInAnalysis], [PeriodKey])
GO
CREATE STATISTICS [_dta_stat_669961463_16_1_23] ON [dbo].[DimPeriod] ([IncludeInAnalysis], [PeriodKey], [PeriodID])
GO
CREATE STATISTICS [_dta_stat_669961463_23_1_16_5] ON [dbo].[DimPeriod] ([PeriodID], [PeriodKey], [IncludeInAnalysis], [ClientPeriodsAgo])
GO
CREATE STATISTICS [_dta_stat_669961463_23_2_5_16_19] ON [dbo].[DimPeriod] ([PeriodID], [PeriodName], [ClientPeriodsAgo], [IncludeInAnalysis], [Active])
GO
CREATE STATISTICS [_dta_stat_669961463_1_21_5_16_13_23_27] ON [dbo].[DimPeriod] ([PeriodKey], [ClientID], [ClientPeriodsAgo], [IncludeInAnalysis], [StartDateKey], [PeriodID], [CurrentPeriodName])
GO
ALTER TABLE [dbo].[DimPeriod] ADD CONSTRAINT [FK__DimPeriod__EndDa__6DAD1CC9] FOREIGN KEY ([EndDateKey]) REFERENCES [dbo].[DimDate] ([DateKey])
GO
ALTER TABLE [dbo].[DimPeriod] ADD CONSTRAINT [FK__DimPeriod__Start__6CB8F890] FOREIGN KEY ([StartDateKey]) REFERENCES [dbo].[DimDate] ([DateKey])
GO
GRANT SELECT ON  [dbo].[DimPeriod] TO [TMDCReporting]
GO
