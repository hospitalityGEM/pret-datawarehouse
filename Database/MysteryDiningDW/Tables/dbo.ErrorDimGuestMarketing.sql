CREATE TABLE [dbo].[ErrorDimGuestMarketing]
(
[ErrorGuestMarketingID] [int] NOT NULL IDENTITY(1, 1),
[GuestMarketingKey] [int] NULL,
[Title] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
[FirstName] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Surname] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[AddressLine1] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[AddressLine2] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[City] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[County] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Postcode] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
[Age] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Occupation] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Mobile] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Email] [nvarchar] (300) COLLATE Latin1_General_CI_AS NULL,
[Birthday] [nvarchar] (30) COLLATE Latin1_General_CI_AS NULL,
[JoinMarketing] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ContactEmail] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[ContactPost] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[ContactMobile] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[GuestMarketingID] [int] NULL,
[TimeStamp] [datetime] NULL,
[ChangeReason] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[InvalidInput] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[InvalidInputErrorDescription] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[ErrorCode] [int] NULL,
[ErrorColumn] [int] NULL,
[Resolved] [bit] NULL,
[GuestMarketingDetailID] [int] NULL,
[FullName] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ErrorDimGuestMarketing] ADD CONSTRAINT [PK_DimGuestMarketingError] PRIMARY KEY CLUSTERED  ([ErrorGuestMarketingID]) ON [PRIMARY]
GO
