CREATE TABLE [dbo].[StagingFactSectionResult]
(
[StagingFSRID] [int] NOT NULL IDENTITY(1, 1),
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[SectionID] [int] NOT NULL,
[VisitID] [int] NOT NULL,
[ValidToDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingFactSectionResult] ADD CONSTRAINT [PK_StagingFactSectionResult_1] PRIMARY KEY CLUSTERED  ([StagingFSRID]) ON [PRIMARY]
GO
