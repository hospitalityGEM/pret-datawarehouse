CREATE TABLE [dbo].[StagingDimPeriod]
(
[SPeriodKey] [int] NOT NULL IDENTITY(1, 1),
[PeriodKey] [int] NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[PeriodID] [int] NULL,
[ValidToDate] [datetime] NULL,
[Type] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[Year] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[CurrentPeriodName] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[PeriodType] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[IncludeInAnalysis] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[IncludeInPortal] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[YearStartKey] [int] NULL,
[YearEndKey] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingDimPeriod] ADD CONSTRAINT [PK_StagingDimPeriod] PRIMARY KEY CLUSTERED  ([SPeriodKey]) ON [PRIMARY]
GO
