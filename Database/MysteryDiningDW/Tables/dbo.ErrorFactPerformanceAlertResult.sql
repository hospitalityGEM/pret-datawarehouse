CREATE TABLE [dbo].[ErrorFactPerformanceAlertResult]
(
[FactPerformanceAlertResultErrorID] [int] NOT NULL IDENTITY(1, 1),
[TimeStamp] [datetime] NULL CONSTRAINT [DF_ErrorFactPerformanceAlertResult_TimeStamp] DEFAULT (getdate()),
[PerformanceAlertResultKey] [int] NULL,
[VisitKey] [int] NULL,
[PeriodKey] [int] NULL,
[BranchKey] [int] NULL,
[GeographyKey] [int] NULL,
[VisitDateKey] [int] NULL,
[AssessorKey] [int] NULL,
[Questionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[SubQuestionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[AlertName] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[AlertType] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Score] [decimal] (18, 2) NULL,
[MaxScore] [decimal] (18, 2) NULL,
[ScoreText] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ScoreMethod] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[IncludeInAnalysis] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[VisitStatus] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[RaisedDateKey] [int] NULL,
[CompletedDateKey] [int] NULL,
[ValidFromDate] [datetime] NULL,
[ValidToDate] [datetime] NULL,
[Active] [bit] NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[AlertID] [int] NULL,
[VisitID] [int] NULL,
[InvalidInput] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[InvalidInputErrorDescription] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[ErrorCode] [int] NULL,
[ErrorColumn] [int] NULL,
[RowChangeReason] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Resolved] [bit] NOT NULL CONSTRAINT [DF_ErrorFactPerformanceAlertResult_Resolved] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ErrorFactPerformanceAlertResult] ADD CONSTRAINT [PK_FactPerformanceAlertResultErrorID] PRIMARY KEY CLUSTERED  ([FactPerformanceAlertResultErrorID]) ON [PRIMARY]
GO
