CREATE TABLE [dbo].[DimAssessor]
(
[AssessorKey] [int] NOT NULL IDENTITY(1, 1),
[Forename] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Surname] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[BirthDateKey] [int] NOT NULL,
[Address1] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Address2] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Address3] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Address4] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Address5] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Postcode] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[GeographyKey] [int] NULL,
[HomeTelephone] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[WorkTelephone] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Mobile] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Fax] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[BankName] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Occupation] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Gender] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[Status] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL CONSTRAINT [DF_DimAssessor_Source] DEFAULT (N'Legacy'),
[ValidFromDate] [datetime] NULL,
[ValidToDate] [datetime] NULL,
[Active] [bit] NULL,
[AssessorID] [int] NOT NULL,
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_DimAssessor_TimeStamp] DEFAULT (getdate()),
[ChangeReason] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[LastLoginDateKey] [int] NULL,
[TotalWarnings] [int] NOT NULL CONSTRAINT [DF_DimAssessor_TotalWarnings] DEFAULT ((0)),
[RecentWarnings] [int] NOT NULL CONSTRAINT [DF_DimAssessor_RecentWarnings] DEFAULT ((0)),
[LatestWarningDateKey] [int] NOT NULL CONSTRAINT [DF_DimAssessor_LatestWarningDateKey] DEFAULT ((19000101)),
[Email] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[FullName] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[ApplicationDateKey] [int] NULL,
[Age] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimAssessor] ADD CONSTRAINT [PK_DimAssessor] PRIMARY KEY CLUSTERED  ([AssessorKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimAssessor] ADD CONSTRAINT [FK_DimAssessor_DimDate] FOREIGN KEY ([BirthDateKey]) REFERENCES [dbo].[DimDate] ([DateKey])
GO
ALTER TABLE [dbo].[DimAssessor] ADD CONSTRAINT [FK_DimAssessor_Geography] FOREIGN KEY ([GeographyKey]) REFERENCES [dbo].[DimGeography] ([GeographyKey])
GO
ALTER TABLE [dbo].[DimAssessor] ADD CONSTRAINT [FK_DimAssessor_DimDateLastLogin] FOREIGN KEY ([LastLoginDateKey]) REFERENCES [dbo].[DimDate] ([DateKey])
GO
ALTER TABLE [dbo].[DimAssessor] ADD CONSTRAINT [FK_DimAssessor_DimDateLatestWarning] FOREIGN KEY ([LatestWarningDateKey]) REFERENCES [dbo].[DimDate] ([DateKey])
GO
GRANT SELECT ON  [dbo].[DimAssessor] TO [TMDCReporting]
GO
