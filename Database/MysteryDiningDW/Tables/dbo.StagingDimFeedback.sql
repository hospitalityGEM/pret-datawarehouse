CREATE TABLE [dbo].[StagingDimFeedback]
(
[StagingFeedbackID] [int] NOT NULL IDENTITY(1, 1),
[FeedbackKey] [int] NULL,
[CompletedDateKey] [int] NULL,
[StartTime] [datetime] NULL,
[EndTime] [datetime] NULL,
[ElapsedTime] [int] NULL,
[IPAddress] [nvarchar] (30) COLLATE Latin1_General_CI_AS NULL,
[CookieGUID] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DiscardReason] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DiscardValue] [int] NULL,
[OFDID] [int] NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[FeedbackID] [int] NULL,
[SCDType] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingDimFeedback] ADD CONSTRAINT [PK_StagingDimFeedback] PRIMARY KEY CLUSTERED  ([StagingFeedbackID]) ON [PRIMARY]
GO
