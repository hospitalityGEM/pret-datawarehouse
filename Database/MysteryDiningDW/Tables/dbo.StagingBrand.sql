CREATE TABLE [dbo].[StagingBrand]
(
[StagingBrandID] [int] NOT NULL,
[Name] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[StagingBrandStatusID] [tinyint] NOT NULL,
[ClientUID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingBrand] ADD CONSTRAINT [PK_StagingBrand] PRIMARY KEY CLUSTERED  ([StagingBrandID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingBrand] ADD CONSTRAINT [FK_StagingBrand_StagingBrandStatus] FOREIGN KEY ([StagingBrandStatusID]) REFERENCES [dbo].[StagingBrandStatus] ([StagingBrandStatusID])
GO
