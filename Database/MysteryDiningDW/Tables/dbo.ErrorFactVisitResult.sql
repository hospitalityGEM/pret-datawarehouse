CREATE TABLE [dbo].[ErrorFactVisitResult]
(
[FactVisitResultErrorID] [int] NOT NULL IDENTITY(1, 1),
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_ErrorFactVisitResult_TimeStamp] DEFAULT (getdate()),
[VisitResultKey] [int] NULL,
[VisitKey] [int] NULL,
[BranchKey] [int] NULL,
[GeographyKey] [int] NULL,
[PeriodKey] [int] NULL,
[VisitDateKey] [int] NULL,
[AssessorKey] [int] NULL,
[Questionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[SubQuestionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Status] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[AdminKey] [int] NULL,
[Score] [decimal] (18, 2) NULL,
[MaxScore] [decimal] (18, 2) NULL,
[ScoreText] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ScoreMethod] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Comment] [nvarchar] (4000) COLLATE Latin1_General_CI_AS NULL,
[IncludeInAnalysis] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[ValidFromDate] [datetime] NULL,
[ValidToDate] [datetime] NULL,
[Active] [bit] NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[VisitID] [int] NULL,
[InvalidInput] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[InvalidInputErrorDescription] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[ErrorCode] [int] NULL,
[ErrorColumn] [int] NULL,
[RowChangeReason] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Resolved] [bit] NOT NULL CONSTRAINT [DF_ErrorFactVisitResult_Resolved] DEFAULT ((0)),
[QuestionnaireCategory] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[AdjustedScore] [decimal] (18, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ErrorFactVisitResult] ADD CONSTRAINT [PK_ErrorFactVisitResult] PRIMARY KEY CLUSTERED  ([FactVisitResultErrorID]) ON [PRIMARY]
GO
