CREATE TABLE [dbo].[StagingFactAdvocacyEmail]
(
[StagingFAEID] [int] NOT NULL IDENTITY(1, 1),
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[AdvocacyEmailID] [int] NOT NULL,
[ValidToDate] [datetime] NULL,
[Type] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[AdvocacyEmailKey] [int] NOT NULL,
[RecipientCount] [int] NULL,
[SendCount] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingFactAdvocacyEmail] ADD CONSTRAINT [PK_StagingFactAdvocacyEmail] PRIMARY KEY CLUSTERED  ([StagingFAEID]) ON [PRIMARY]
GO
