CREATE TABLE [dbo].[StagingBrandBranch]
(
[StagingBrandBranchID] [int] NOT NULL,
[BranchUID] [int] NOT NULL,
[StagingBrandID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingBrandBranch] ADD CONSTRAINT [PK_StagingBrandBranch] PRIMARY KEY CLUSTERED  ([StagingBrandBranchID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingBrandBranch] ADD CONSTRAINT [FK_StagingBrandBranch_StagingBrand] FOREIGN KEY ([StagingBrandID]) REFERENCES [dbo].[StagingBrand] ([StagingBrandID])
GO
