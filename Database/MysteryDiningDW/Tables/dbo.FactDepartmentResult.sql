CREATE TABLE [dbo].[FactDepartmentResult]
(
[DepartmentResultKey] [int] NOT NULL IDENTITY(1, 1),
[VisitKey] [int] NOT NULL,
[BranchKey] [int] NOT NULL,
[GeographyKey] [int] NULL,
[PeriodKey] [int] NOT NULL,
[VisitDateKey] [int] NOT NULL,
[AssessorKey] [int] NOT NULL,
[Questionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[SubQuestionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Department] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Score] [decimal] (18, 2) NOT NULL,
[MaxScore] [decimal] (18, 2) NOT NULL,
[VisitScore] [decimal] (18, 2) NOT NULL,
[VisitMaxScore] [decimal] (18, 2) NOT NULL,
[VisitScoreMethod] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[IncludeInAnalysis] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[ValidFromDate] [datetime] NOT NULL,
[ValidToDate] [datetime] NOT NULL,
[Active] [bit] NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[VisitID] [int] NOT NULL,
[DepartmentID] [int] NOT NULL,
[TimeStamp] [datetime] NULL CONSTRAINT [DF_FactDepartmentResult_TimeStamp] DEFAULT (getdate()),
[ChangeReason] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactDepartmentResult] ADD CONSTRAINT [PK_FactDepartmentResult] PRIMARY KEY CLUSTERED  ([DepartmentResultKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactDepartmentResult] ADD CONSTRAINT [FK_FactDepartmentResult_DimAssessor] FOREIGN KEY ([AssessorKey]) REFERENCES [dbo].[DimAssessor] ([AssessorKey])
GO
ALTER TABLE [dbo].[FactDepartmentResult] ADD CONSTRAINT [FK_FactDepartmentResult_DimBranch] FOREIGN KEY ([BranchKey]) REFERENCES [dbo].[DimBranch] ([BranchKey])
GO
ALTER TABLE [dbo].[FactDepartmentResult] ADD CONSTRAINT [FK_FactDepartmentResult_DimGeography] FOREIGN KEY ([GeographyKey]) REFERENCES [dbo].[DimGeography] ([GeographyKey])
GO
ALTER TABLE [dbo].[FactDepartmentResult] ADD CONSTRAINT [FK_FactDepartmentResult_DimPeriod] FOREIGN KEY ([PeriodKey]) REFERENCES [dbo].[DimPeriod] ([PeriodKey])
GO
ALTER TABLE [dbo].[FactDepartmentResult] ADD CONSTRAINT [FK_FactDepartmentResult_DimDate] FOREIGN KEY ([VisitDateKey]) REFERENCES [dbo].[DimDate] ([DateKey])
GO
ALTER TABLE [dbo].[FactDepartmentResult] ADD CONSTRAINT [FK_FactDepartmentResult_DimVisit] FOREIGN KEY ([VisitKey]) REFERENCES [dbo].[DimVisit] ([VisitKey])
GO
