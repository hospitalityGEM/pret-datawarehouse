CREATE TABLE [dbo].[ConfigStyleConfig]
(
[ConfigStyleConfigID] [int] NOT NULL IDENTITY(1, 1),
[ConfigStylePropertyItemID] [smallint] NOT NULL,
[ConfigStyleGroupID] [smallint] NOT NULL,
[StyleValue] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Active] [bit] NOT NULL CONSTRAINT [DF_ConfigStyleConfig_Active] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConfigStyleConfig] ADD CONSTRAINT [PK_ConfigStyleConfig] PRIMARY KEY CLUSTERED  ([ConfigStyleConfigID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConfigStyleConfig] ADD CONSTRAINT [FK_ConfigStyleConfig_ConfigStyleGroup] FOREIGN KEY ([ConfigStyleGroupID]) REFERENCES [dbo].[ConfigStyleGroup] ([ConfigStyleGroupID])
GO
ALTER TABLE [dbo].[ConfigStyleConfig] ADD CONSTRAINT [FK_ConfigStyleConfig_ConfigStylePropertyItem] FOREIGN KEY ([ConfigStylePropertyItemID]) REFERENCES [dbo].[ConfigStylePropertyItem] ([ConfigStylePropertyItemID])
GO
