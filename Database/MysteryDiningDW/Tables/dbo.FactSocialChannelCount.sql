CREATE TABLE [dbo].[FactSocialChannelCount]
(
[SocialChannelCountKey] [int] NOT NULL IDENTITY(1, 1),
[SocialChannelKey] [int] NOT NULL,
[CountedDateKey] [int] NOT NULL,
[ChannelCount] [int] NOT NULL,
[TalkingAboutCount] [int] NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[SocialChannelCountID] [int] NOT NULL,
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_FactSocialChannelCount_TimeStamp] DEFAULT (getdate()),
[DateTimeCounted] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactSocialChannelCount] ADD CONSTRAINT [PK_FactSocialChannelCount] PRIMARY KEY CLUSTERED  ([SocialChannelCountKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactSocialChannelCount] ADD CONSTRAINT [FK_FactSocialChannelCount_DimDate] FOREIGN KEY ([CountedDateKey]) REFERENCES [dbo].[DimDate] ([DateKey])
GO
ALTER TABLE [dbo].[FactSocialChannelCount] ADD CONSTRAINT [FK_FactSocialChannelCount_DimSocialChannel] FOREIGN KEY ([SocialChannelKey]) REFERENCES [dbo].[DimSocialChannel] ([SocialChannelKey])
GO
