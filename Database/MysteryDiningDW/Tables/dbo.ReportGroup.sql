CREATE TABLE [dbo].[ReportGroup]
(
[ReportGroupID] [int] NOT NULL,
[GroupName] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Description] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[AddedByAdminID] [smallint] NOT NULL,
[Visible] [bit] NOT NULL,
[Active] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReportGroup] ADD CONSTRAINT [PK_ReportGroup] PRIMARY KEY CLUSTERED  ([ReportGroupID]) ON [PRIMARY]
GO
