CREATE TABLE [dbo].[FactBenchmarkResult]
(
[BenchmarkResultKey] [int] NOT NULL IDENTITY(1, 1),
[VisitKey] [int] NOT NULL,
[BranchKey] [int] NOT NULL,
[GeographyKey] [int] NULL,
[PeriodKey] [int] NOT NULL,
[VisitDateKey] [int] NOT NULL,
[AssessorKey] [int] NOT NULL,
[Questionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[SubQuestionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Benchmark] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[CustomerLoyalty] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Score] [decimal] (18, 2) NOT NULL,
[MaxScore] [decimal] (18, 2) NOT NULL,
[VisitScore] [decimal] (18, 2) NOT NULL,
[VisitMaxScore] [decimal] (18, 2) NOT NULL,
[VisitScoreMethod] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[IncludeInAnalysis] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[ValidFromDate] [datetime] NOT NULL,
[ValidToDate] [datetime] NOT NULL,
[Active] [bit] NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[VisitID] [int] NOT NULL,
[BenchmarkID] [nvarchar] (5) COLLATE Latin1_General_CI_AS NOT NULL,
[TimeStamp] [datetime] NULL CONSTRAINT [DF_FactBenchmarkResult_TimeStamp] DEFAULT (getdate()),
[ChangeReason] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactBenchmarkResult] ADD CONSTRAINT [PK_FactBenchmarkResult] PRIMARY KEY CLUSTERED  ([BenchmarkResultKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [index_FactBenchmarkResult_12_03_2013_2] ON [dbo].[FactBenchmarkResult] ([Benchmark], [IncludeInAnalysis], [Active], [MaxScore]) INCLUDE ([BranchKey], [CustomerLoyalty], [PeriodKey], [VisitKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [index_FactBenchmarkResult_12_03_2013_1] ON [dbo].[FactBenchmarkResult] ([BranchKey], [Benchmark], [IncludeInAnalysis], [Active], [MaxScore]) INCLUDE ([CustomerLoyalty], [PeriodKey], [VisitKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [indexFactBenchmarkResult_Table] ON [dbo].[FactBenchmarkResult] ([Source], [Active], [VisitID], [BenchmarkID]) INCLUDE ([AssessorKey], [Benchmark], [BenchmarkResultKey], [BranchKey], [ChangeReason], [CustomerLoyalty], [GeographyKey], [IncludeInAnalysis], [MaxScore], [PeriodKey], [Questionnaire], [Score], [SubQuestionnaire], [TimeStamp], [ValidFromDate], [ValidToDate], [VisitDateKey], [VisitKey], [VisitMaxScore], [VisitScore], [VisitScoreMethod]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_index_FactBenchmarkResult_B271_2] ON [dbo].[FactBenchmarkResult] ([VisitDateKey], [MaxScore], [Active], [Benchmark], [BranchKey], [PeriodKey], [VisitKey], [VisitID]) INCLUDE ([Score]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactBenchmarkResult] ADD CONSTRAINT [FK_FactBenchmarkResult_DimAssessor] FOREIGN KEY ([AssessorKey]) REFERENCES [dbo].[DimAssessor] ([AssessorKey])
GO
ALTER TABLE [dbo].[FactBenchmarkResult] ADD CONSTRAINT [FK_FactBenchmarkResult_DimBranch] FOREIGN KEY ([BranchKey]) REFERENCES [dbo].[DimBranch] ([BranchKey])
GO
ALTER TABLE [dbo].[FactBenchmarkResult] ADD CONSTRAINT [FK_FactBenchmarkResult_DimGeography] FOREIGN KEY ([GeographyKey]) REFERENCES [dbo].[DimGeography] ([GeographyKey])
GO
ALTER TABLE [dbo].[FactBenchmarkResult] ADD CONSTRAINT [FK_FactBenchmarkResult_DimPeriod] FOREIGN KEY ([PeriodKey]) REFERENCES [dbo].[DimPeriod] ([PeriodKey])
GO
ALTER TABLE [dbo].[FactBenchmarkResult] ADD CONSTRAINT [FK_FactBenchmarkResult_DimDate] FOREIGN KEY ([VisitDateKey]) REFERENCES [dbo].[DimDate] ([DateKey])
GO
ALTER TABLE [dbo].[FactBenchmarkResult] ADD CONSTRAINT [FK_FactBenchmarkResult_DimVisit] FOREIGN KEY ([VisitKey]) REFERENCES [dbo].[DimVisit] ([VisitKey])
GO
