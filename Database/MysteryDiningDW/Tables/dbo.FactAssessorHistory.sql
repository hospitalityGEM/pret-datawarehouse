CREATE TABLE [dbo].[FactAssessorHistory]
(
[AssessorHistoryKey] [int] NOT NULL IDENTITY(1, 1),
[LogDateKey] [int] NULL,
[Activity] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Status] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[SystemComments] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[AssessorComments] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[AdminComments] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[VisitKey] [int] NULL,
[Points] [int] NULL,
[AdminKey] [int] NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[AssessorID] [int] NULL,
[AssessorHistoryID] [int] NULL,
[TimeStamp] [datetime] NULL CONSTRAINT [DF_FactAssessorHistory_TimeStamp] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactAssessorHistory] ADD CONSTRAINT [PK_FactAssessorHistory] PRIMARY KEY CLUSTERED  ([AssessorHistoryKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactAssessorHistory] ADD CONSTRAINT [FK_FactAssessorHistory_DimAdmin] FOREIGN KEY ([AdminKey]) REFERENCES [dbo].[DimAdmin] ([AdminKey])
GO
ALTER TABLE [dbo].[FactAssessorHistory] ADD CONSTRAINT [FK_FactAssessorHistory_DimDate] FOREIGN KEY ([LogDateKey]) REFERENCES [dbo].[DimDate] ([DateKey])
GO
