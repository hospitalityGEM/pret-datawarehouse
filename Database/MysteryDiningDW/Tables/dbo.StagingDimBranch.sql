CREATE TABLE [dbo].[StagingDimBranch]
(
[SBranchKey] [int] NOT NULL IDENTITY(1, 1),
[BranchKey] [int] NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[BranchID] [int] NOT NULL,
[ValidToDate] [datetime] NOT NULL,
[CurrentArea] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[CurrentRegion] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Type] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[CurrentBranch] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[GeographyKey] [int] NULL,
[BranchCode] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[CurrentBrand] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[CurrentClient] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[AccountManager] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[LowThreshold] [int] NULL,
[HighThreshold] [int] NULL,
[Target] [int] NULL,
[Location] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Telephone] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[CurrentClientSegment] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[OurLocation] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[IsInternational] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[CurrentNBArea] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[CurrentNBRegion] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[SubBrand] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[AgreementType] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ClosedBranch] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingDimBranch] ADD CONSTRAINT [PK_StagingDimBranch] PRIMARY KEY CLUSTERED  ([SBranchKey]) ON [PRIMARY]
GO
