CREATE TABLE [dbo].[StagingBrandGroup]
(
[BrandID] [int] NOT NULL,
[BrandGroup] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[ManagedOrTenanted] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingBrandGroup] ADD CONSTRAINT [PK__StagingB__DAD4F3BE291CBF14] PRIMARY KEY CLUSTERED  ([BrandID]) ON [PRIMARY]
GO
