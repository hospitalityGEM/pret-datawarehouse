CREATE TABLE [dbo].[FactSectionResult]
(
[SectionResultKey] [int] NOT NULL IDENTITY(1, 1),
[VisitKey] [int] NOT NULL,
[BranchKey] [int] NOT NULL,
[GeographyKey] [int] NULL,
[PeriodKey] [int] NOT NULL,
[VisitDateKey] [int] NOT NULL,
[AssessorKey] [int] NOT NULL,
[Questionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[SubQuestionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[SectionTitle] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[SectionNumber] [int] NOT NULL,
[Score] [decimal] (18, 2) NOT NULL,
[MaxScore] [decimal] (18, 2) NOT NULL,
[IncludeInAnalysis] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[ValidFromDate] [datetime] NOT NULL,
[ValidToDate] [datetime] NOT NULL,
[Active] [bit] NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[SectionID] [int] NOT NULL,
[VisitID] [int] NOT NULL,
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_FactSectionResult_TimeStamp] DEFAULT (getdate()),
[ChangeReason] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[SectionDepartment] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[MenuSection] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[HiddenSection] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_FactSectionResult_HiddenSection] DEFAULT (N'Visible'),
[IncludeScoreInTotal] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactSectionResult] ADD CONSTRAINT [PK_FactSectionResult] PRIMARY KEY CLUSTERED  ([SectionResultKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_FactSectionResult_7_1860917701__K3_K14_K18_K10_K21_K5_K6_K2_K11_12] ON [dbo].[FactSectionResult] ([BranchKey], [MaxScore], [Active], [SectionTitle], [VisitID], [PeriodKey], [VisitDateKey], [VisitKey], [SectionNumber]) INCLUDE ([Score]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactSectionResult] ADD CONSTRAINT [FK_FactSectionResult_DimAssessor] FOREIGN KEY ([AssessorKey]) REFERENCES [dbo].[DimAssessor] ([AssessorKey])
GO
ALTER TABLE [dbo].[FactSectionResult] ADD CONSTRAINT [FK_FactSectionResult_DimBranch] FOREIGN KEY ([BranchKey]) REFERENCES [dbo].[DimBranch] ([BranchKey])
GO
ALTER TABLE [dbo].[FactSectionResult] ADD CONSTRAINT [FK_FactSectionResult_DimGeography] FOREIGN KEY ([GeographyKey]) REFERENCES [dbo].[DimGeography] ([GeographyKey])
GO
ALTER TABLE [dbo].[FactSectionResult] ADD CONSTRAINT [FK_FactSectionResult_DimPeriod] FOREIGN KEY ([PeriodKey]) REFERENCES [dbo].[DimPeriod] ([PeriodKey])
GO
ALTER TABLE [dbo].[FactSectionResult] ADD CONSTRAINT [FK_FactSectionResult_DimDate] FOREIGN KEY ([VisitDateKey]) REFERENCES [dbo].[DimDate] ([DateKey])
GO
ALTER TABLE [dbo].[FactSectionResult] ADD CONSTRAINT [FK_FactSectionResult_DimVisit] FOREIGN KEY ([VisitKey]) REFERENCES [dbo].[DimVisit] ([VisitKey])
GO
GRANT SELECT ON  [dbo].[FactSectionResult] TO [TMDCReporting]
GO
