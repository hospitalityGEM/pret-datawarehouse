CREATE TABLE [dbo].[ConfigStylePropertyItem]
(
[ConfigStylePropertyItemID] [smallint] NOT NULL IDENTITY(1, 1),
[ConfigStylePropertyID] [smallint] NOT NULL,
[ConfigStyleItemID] [smallint] NOT NULL,
[DefaultValue] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Editable] [bit] NOT NULL CONSTRAINT [DF_ConfigStylePropertyItem_Editable] DEFAULT ((0)),
[Active] [bit] NOT NULL CONSTRAINT [DF_ConfigStylePropertyItem_Active] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConfigStylePropertyItem] ADD CONSTRAINT [PK_ConfigStylePropertyItem] PRIMARY KEY CLUSTERED  ([ConfigStylePropertyItemID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConfigStylePropertyItem] ADD CONSTRAINT [FK_ConfigStylePropertyItem_ConfigStyleItem] FOREIGN KEY ([ConfigStyleItemID]) REFERENCES [dbo].[ConfigStyleItem] ([ConfigStyleItemID])
GO
ALTER TABLE [dbo].[ConfigStylePropertyItem] ADD CONSTRAINT [FK_ConfigStylePropertyItem_ConfigStyleProperty] FOREIGN KEY ([ConfigStylePropertyID]) REFERENCES [dbo].[ConfigStyleProperty] ([ConfigStylePropertyID])
GO
GRANT SELECT ON  [dbo].[ConfigStylePropertyItem] TO [TMDCReporting]
GRANT INSERT ON  [dbo].[ConfigStylePropertyItem] TO [TMDCReporting]
GRANT UPDATE ON  [dbo].[ConfigStylePropertyItem] TO [TMDCReporting]
GO
