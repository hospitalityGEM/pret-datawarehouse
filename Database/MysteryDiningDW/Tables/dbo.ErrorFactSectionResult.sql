CREATE TABLE [dbo].[ErrorFactSectionResult]
(
[FactSectionResultErrorID] [int] NOT NULL IDENTITY(1, 1),
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_ErrorFactSectionResult_TimeStamp] DEFAULT (getdate()),
[SectionResultKey] [int] NULL,
[VisitKey] [int] NULL,
[BranchKey] [int] NULL,
[GeographyKey] [int] NULL,
[PeriodKey] [int] NULL,
[VisitDateKey] [int] NULL,
[AssessorKey] [int] NULL,
[Questionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[SubQuestionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[SectionTitle] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[SectionNumber] [int] NULL,
[Score] [decimal] (18, 2) NULL,
[MaxScore] [decimal] (18, 2) NULL,
[IncludeInAnalysis] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[ValidFromDate] [datetime] NULL,
[ValidToDate] [datetime] NULL,
[Active] [bit] NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[SectionID] [int] NULL,
[VisitID] [int] NULL,
[InvalidInput] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[InvalidInputErrorDescription] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[ErrorCode] [int] NULL,
[ErrorColumn] [int] NULL,
[RowChangeReason] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Resolved] [bit] NOT NULL CONSTRAINT [DF_ErrorFactSectionResult_Resolved] DEFAULT ((0)),
[SectionDepartment] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[MenuSection] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[HiddenSection] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[IncludeScoreInTotal] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ErrorFactSectionResult] ADD CONSTRAINT [PK_ErrorFactSectionResult] PRIMARY KEY CLUSTERED  ([FactSectionResultErrorID]) ON [PRIMARY]
GO
