CREATE TABLE [dbo].[StagingFactPerformanceAlertResult]
(
[StagingFPARID] [int] NOT NULL IDENTITY(1, 1),
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[VisitID] [int] NOT NULL,
[AlertID] [int] NOT NULL,
[ValidToDate] [datetime] NULL,
[Type] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[Questionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[SubQuestionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[IncludeInAnalysis] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[AlertName] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[AlertType] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[VisitStatus] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[CompletedDateKey] [int] NULL,
[PerformanceAlertResultKey] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingFactPerformanceAlertResult] ADD CONSTRAINT [PK_StagingFactPerformanceAlertResult] PRIMARY KEY CLUSTERED  ([StagingFPARID]) ON [PRIMARY]
GO
