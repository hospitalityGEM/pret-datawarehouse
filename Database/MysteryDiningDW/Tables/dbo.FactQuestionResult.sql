CREATE TABLE [dbo].[FactQuestionResult]
(
[QuestionResultKey] [int] NOT NULL IDENTITY(1, 1),
[QuestionKey] [int] NOT NULL,
[AssessorKey] [int] NOT NULL,
[Questionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[VisitKey] [int] NOT NULL,
[AssessmentArea] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[BranchKey] [int] NOT NULL,
[GeographyKey] [int] NULL,
[PeriodKey] [int] NULL,
[VisitDateKey] [int] NOT NULL,
[QuestionSection] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[SectionNumber] [int] NOT NULL,
[ManagementCategory] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Benchmark] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[QuestionDepartment] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[QuestionNumber] [int] NOT NULL,
[AnswerText] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Score] [decimal] (18, 2) NOT NULL,
[MaxScore] [decimal] (18, 2) NOT NULL,
[AnsweredDateKey] [int] NOT NULL,
[CheckedByAdminKey] [int] NOT NULL,
[Comments] [nvarchar] (4000) COLLATE Latin1_General_CI_AS NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[TableSource] [nvarchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[FeedbackType] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[IncludeInAnalysis] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[ValidFromDate] [datetime] NOT NULL,
[ValidToDate] [datetime] NOT NULL,
[Active] [bit] NOT NULL,
[QQID] [int] NULL,
[QuestionResultID] [int] NOT NULL,
[SubQuestionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Factor] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_FactQuestionResult_TimeStamp] DEFAULT (getdate()),
[ChangeReason] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[DishKey] [int] NOT NULL,
[VisitID] [int] NULL,
[AnswerValue] [int] NULL,
[AnswerMaxValue] [int] NULL,
[IncludeScoreInTotal] [bit] NULL
) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_FactQuestionResult_IncludeInAnalysis_Active_IncludeScoreInTotal_MaxScore] ON [dbo].[FactQuestionResult] ([IncludeInAnalysis], [Active], [IncludeScoreInTotal], [MaxScore]) INCLUDE ([BranchKey], [PeriodKey], [QuestionKey], [QuestionResultKey], [Score], [VisitKey]) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_FactQuestionResult_Active_VisitID_Source_QuestionResultID_(ManyColumns)] ON [dbo].[FactQuestionResult] ([Active], [VisitID], [Source], [QuestionResultID]) INCLUDE ([AnsweredDateKey], [AnswerMaxValue], [AnswerText], [AnswerValue], [AssessmentArea], [AssessorKey], [Benchmark], [BranchKey], [CheckedByAdminKey], [Comments], [DishKey], [Factor], [FeedbackType], [GeographyKey], [IncludeInAnalysis], [ManagementCategory], [MaxScore], [PeriodKey], [QQID], [QuestionDepartment], [QuestionKey], [Questionnaire], [QuestionNumber], [QuestionResultKey], [QuestionSection], [Score], [SectionNumber], [SubQuestionnaire], [TableSource], [TimeStamp], [ValidFromDate], [ValidToDate], [VisitDateKey], [VisitKey]) ON [PRIMARY]

GO
ALTER TABLE [dbo].[FactQuestionResult] ADD CONSTRAINT [PK_FactQuestionResult] PRIMARY KEY CLUSTERED  ([QuestionResultKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [indexFactQuestionResult_BQrPVdQVKeys] ON [dbo].[FactQuestionResult] ([Active], [BranchKey], [MaxScore], [QuestionResultKey], [IncludeInAnalysis], [PeriodKey], [VisitDateKey], [QuestionKey], [VisitKey]) INCLUDE ([Score]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [index_ScoreMaxScore] ON [dbo].[FactQuestionResult] ([Active], [QuestionKey], [BranchKey], [QuestionResultKey], [PeriodKey], [VisitKey], [VisitDateKey], [VisitID], [ManagementCategory], [Benchmark]) INCLUDE ([MaxScore], [Score]) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_513436903_30_20_7_1_9] ON [dbo].[FactQuestionResult] ([Active], [MaxScore], [BranchKey], [QuestionResultKey], [PeriodKey])
GO
CREATE STATISTICS [_dta_stat_513436903_30_20_7_1_10_9_5] ON [dbo].[FactQuestionResult] ([Active], [MaxScore], [BranchKey], [QuestionResultKey], [VisitDateKey], [PeriodKey], [VisitKey])
GO
CREATE STATISTICS [_dta_stat_513436903_30_20_13] ON [dbo].[FactQuestionResult] ([Active], [MaxScore], [ManagementCategory])
GO
CREATE STATISTICS [_dta_stat_513436903_30_4_9] ON [dbo].[FactQuestionResult] ([Active], [Questionnaire], [PeriodKey])
GO
CREATE STATISTICS [_dta_stat_513436903_30_4_5] ON [dbo].[FactQuestionResult] ([Active], [Questionnaire], [VisitKey])
GO
CREATE STATISTICS [_dta_stat_513436903_30_1_9_13_7_20] ON [dbo].[FactQuestionResult] ([Active], [QuestionResultKey], [PeriodKey], [ManagementCategory], [BranchKey], [MaxScore])
GO
CREATE STATISTICS [_dta_stat_513436903_30_1_10_13_7] ON [dbo].[FactQuestionResult] ([Active], [QuestionResultKey], [VisitDateKey], [ManagementCategory], [BranchKey])
GO
CREATE STATISTICS [_dta_stat_513436903_7_9_5_2_30_20_1_13] ON [dbo].[FactQuestionResult] ([BranchKey], [PeriodKey], [VisitKey], [QuestionKey], [Active], [MaxScore], [QuestionResultKey], [ManagementCategory])
GO
CREATE STATISTICS [_dta_stat_513436903_7_1_9_5_30_20_13] ON [dbo].[FactQuestionResult] ([BranchKey], [QuestionResultKey], [PeriodKey], [VisitKey], [Active], [MaxScore], [ManagementCategory])
GO
CREATE STATISTICS [_dta_stat_513436903_7_1_10_9_5_2] ON [dbo].[FactQuestionResult] ([BranchKey], [QuestionResultKey], [VisitDateKey], [PeriodKey], [VisitKey], [QuestionKey])
GO
CREATE STATISTICS [_dta_stat_513436903_7_10_9_5_2_30] ON [dbo].[FactQuestionResult] ([BranchKey], [VisitDateKey], [PeriodKey], [VisitKey], [QuestionKey], [Active])
GO
CREATE STATISTICS [_dta_stat_513436903_13_7_1_10_9_5_2_30] ON [dbo].[FactQuestionResult] ([ManagementCategory], [BranchKey], [QuestionResultKey], [VisitDateKey], [PeriodKey], [VisitKey], [QuestionKey], [Active])
GO
CREATE STATISTICS [_dta_stat_513436903_20_30_7_13] ON [dbo].[FactQuestionResult] ([MaxScore], [Active], [BranchKey], [ManagementCategory])
GO
CREATE STATISTICS [_dta_stat_513436903_20_30_9_13_7] ON [dbo].[FactQuestionResult] ([MaxScore], [Active], [PeriodKey], [ManagementCategory], [BranchKey])
GO
CREATE STATISTICS [_dta_stat_513436903_20_30_2_13_7_1_10_9] ON [dbo].[FactQuestionResult] ([MaxScore], [Active], [QuestionKey], [ManagementCategory], [BranchKey], [QuestionResultKey], [VisitDateKey], [PeriodKey])
GO
CREATE STATISTICS [_dta_stat_513436903_20_30_1_10_13] ON [dbo].[FactQuestionResult] ([MaxScore], [Active], [QuestionResultKey], [VisitDateKey], [ManagementCategory])
GO
CREATE STATISTICS [_dta_stat_513436903_20_30_1_5_13] ON [dbo].[FactQuestionResult] ([MaxScore], [Active], [QuestionResultKey], [VisitKey], [ManagementCategory])
GO
CREATE STATISTICS [_dta_stat_513436903_20_30_10_13_7] ON [dbo].[FactQuestionResult] ([MaxScore], [Active], [VisitDateKey], [ManagementCategory], [BranchKey])
GO
CREATE STATISTICS [_dta_stat_513436903_20_30_5_13_7_1_10] ON [dbo].[FactQuestionResult] ([MaxScore], [Active], [VisitKey], [ManagementCategory], [BranchKey], [QuestionResultKey], [VisitDateKey])
GO
CREATE STATISTICS [_dta_stat_513436903_9_30] ON [dbo].[FactQuestionResult] ([PeriodKey], [Active])
GO
CREATE STATISTICS [_dta_stat_513436903_9_30_5_1_4] ON [dbo].[FactQuestionResult] ([PeriodKey], [Active], [VisitKey], [QuestionResultKey], [Questionnaire])
GO
CREATE STATISTICS [_dta_stat_513436903_9_20_30_1_13] ON [dbo].[FactQuestionResult] ([PeriodKey], [MaxScore], [Active], [QuestionResultKey], [ManagementCategory])
GO
CREATE STATISTICS [_dta_stat_513436903_9_5_30_4] ON [dbo].[FactQuestionResult] ([PeriodKey], [VisitKey], [Active], [Questionnaire])
GO
CREATE STATISTICS [_dta_stat_513436903_9_5_1] ON [dbo].[FactQuestionResult] ([PeriodKey], [VisitKey], [QuestionResultKey])
GO
CREATE STATISTICS [_dta_stat_513436903_2_30_1_13_7] ON [dbo].[FactQuestionResult] ([QuestionKey], [Active], [QuestionResultKey], [ManagementCategory], [BranchKey])
GO
CREATE STATISTICS [_dta_stat_513436903_2_20] ON [dbo].[FactQuestionResult] ([QuestionKey], [MaxScore])
GO
CREATE STATISTICS [_dta_stat_513436903_1_30_13_7] ON [dbo].[FactQuestionResult] ([QuestionResultKey], [Active], [ManagementCategory], [BranchKey])
GO
CREATE STATISTICS [_dta_stat_513436903_1_13_20] ON [dbo].[FactQuestionResult] ([QuestionResultKey], [ManagementCategory], [MaxScore])
GO
CREATE STATISTICS [_dta_stat_513436903_1_20_30_13_7_10_9_5_2] ON [dbo].[FactQuestionResult] ([QuestionResultKey], [MaxScore], [Active], [ManagementCategory], [BranchKey], [VisitDateKey], [PeriodKey], [VisitKey], [QuestionKey])
GO
CREATE STATISTICS [_dta_stat_513436903_1_4] ON [dbo].[FactQuestionResult] ([QuestionResultKey], [Questionnaire])
GO
CREATE STATISTICS [_dta_stat_513436903_10_30] ON [dbo].[FactQuestionResult] ([VisitDateKey], [Active])
GO
CREATE STATISTICS [_dta_stat_513436903_10_20_30] ON [dbo].[FactQuestionResult] ([VisitDateKey], [MaxScore], [Active])
GO
CREATE STATISTICS [_dta_stat_513436903_5_30_1_13_7] ON [dbo].[FactQuestionResult] ([VisitKey], [Active], [QuestionResultKey], [ManagementCategory], [BranchKey])
GO
CREATE STATISTICS [_dta_stat_513436903_5_20] ON [dbo].[FactQuestionResult] ([VisitKey], [MaxScore])
GO
ALTER TABLE [dbo].[FactQuestionResult] ADD CONSTRAINT [FK_FactQuestionResult_DimDate1] FOREIGN KEY ([AnsweredDateKey]) REFERENCES [dbo].[DimDate] ([DateKey])
GO
ALTER TABLE [dbo].[FactQuestionResult] ADD CONSTRAINT [FK_FactQuestionResult_DimAssessor] FOREIGN KEY ([AssessorKey]) REFERENCES [dbo].[DimAssessor] ([AssessorKey])
GO
ALTER TABLE [dbo].[FactQuestionResult] ADD CONSTRAINT [FK_FactQuestionResult_DimBranch] FOREIGN KEY ([BranchKey]) REFERENCES [dbo].[DimBranch] ([BranchKey])
GO
ALTER TABLE [dbo].[FactQuestionResult] ADD CONSTRAINT [FK_FactQuestionResult_DimAdmin] FOREIGN KEY ([CheckedByAdminKey]) REFERENCES [dbo].[DimAdmin] ([AdminKey])
GO
ALTER TABLE [dbo].[FactQuestionResult] ADD CONSTRAINT [FK_FactQuestionResult_DimDish] FOREIGN KEY ([DishKey]) REFERENCES [dbo].[DimDish] ([DishKey])
GO
ALTER TABLE [dbo].[FactQuestionResult] ADD CONSTRAINT [FK_FactQuestionResult_DimGeography] FOREIGN KEY ([GeographyKey]) REFERENCES [dbo].[DimGeography] ([GeographyKey])
GO
ALTER TABLE [dbo].[FactQuestionResult] ADD CONSTRAINT [FK_FactQuestionResult_DimPeriod] FOREIGN KEY ([PeriodKey]) REFERENCES [dbo].[DimPeriod] ([PeriodKey])
GO
ALTER TABLE [dbo].[FactQuestionResult] ADD CONSTRAINT [FK_FactQuestionResult_DimQuestion] FOREIGN KEY ([QuestionKey]) REFERENCES [dbo].[DimQuestion] ([QuestionKey])
GO
ALTER TABLE [dbo].[FactQuestionResult] ADD CONSTRAINT [FK_FactQuestionResult_DimDate] FOREIGN KEY ([VisitDateKey]) REFERENCES [dbo].[DimDate] ([DateKey])
GO
ALTER TABLE [dbo].[FactQuestionResult] ADD CONSTRAINT [FK_FactQuestionResult_DimVisit] FOREIGN KEY ([VisitKey]) REFERENCES [dbo].[DimVisit] ([VisitKey])
GO
GRANT SELECT ON  [dbo].[FactQuestionResult] TO [TMDCReporting]
GO
