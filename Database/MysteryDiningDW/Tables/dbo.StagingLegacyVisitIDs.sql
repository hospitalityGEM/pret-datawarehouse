CREATE TABLE [dbo].[StagingLegacyVisitIDs]
(
[VisitUID] [int] NOT NULL,
[ConfigInfoID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingLegacyVisitIDs] ADD CONSTRAINT [PK_StagingLegacyVisitIDs] PRIMARY KEY CLUSTERED  ([VisitUID], [ConfigInfoID]) ON [PRIMARY]
GO
