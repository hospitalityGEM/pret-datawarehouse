CREATE TABLE [dbo].[ErrorFactAdvocacyEmail]
(
[ErrorAdvocacyEmailID] [int] NOT NULL IDENTITY(1, 1),
[AdvocacyEmailKey] [int] NULL,
[VisitScore] [decimal] (18, 2) NULL,
[VisitMaxScore] [decimal] (18, 2) NULL,
[CompletionDateKey] [int] NULL,
[VisitDateKey] [int] NULL,
[BranchKey] [int] NULL,
[PeriodKey] [int] NULL,
[RecommendationKey] [int] NULL,
[GuestMarketingKey] [int] NULL,
[RecipientCount] [int] NULL,
[SentCount] [int] NULL,
[FromAddress] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Subject] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Message] [nvarchar] (500) COLLATE Latin1_General_CI_AS NULL,
[Advocate] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[RecommendationStatus] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
[AdvocacyEmailID] [int] NULL,
[TimeStamp] [datetime] NULL CONSTRAINT [DF_ErrorFactAdvocacyEmail_TimeStamp] DEFAULT (getdate()),
[InvalidInput] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[InvalidInputErrorDescription] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[ErrorCode] [int] NULL,
[ErrorColumn] [int] NULL,
[Resolved] [bit] NULL,
[RowChangeReason] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[ValidFromDate] [datetime] NULL,
[ValidToDate] [datetime] NULL,
[Active] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ErrorFactAdvocacyEmail] ADD CONSTRAINT [PK_FactAdvocacyEmailError] PRIMARY KEY CLUSTERED  ([ErrorAdvocacyEmailID]) ON [PRIMARY]
GO
