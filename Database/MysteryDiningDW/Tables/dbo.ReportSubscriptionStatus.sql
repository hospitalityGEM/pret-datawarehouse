CREATE TABLE [dbo].[ReportSubscriptionStatus]
(
[ReportSubscriptionStatusID] [tinyint] NOT NULL,
[StatusName] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Active] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReportSubscriptionStatus] ADD CONSTRAINT [PK_ReportSubscriptionStatus] PRIMARY KEY CLUSTERED  ([ReportSubscriptionStatusID]) ON [PRIMARY]
GO
