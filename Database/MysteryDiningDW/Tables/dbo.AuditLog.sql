CREATE TABLE [dbo].[AuditLog]
(
[AuditLogID] [int] NOT NULL IDENTITY(1, 1),
[AuditTask] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_AuditLog_TimeStamp] DEFAULT (getdate()),
[ErrorMessage] [nvarchar] (500) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuditLog] ADD CONSTRAINT [PK_AuditLog] PRIMARY KEY CLUSTERED  ([AuditLogID]) ON [PRIMARY]
GO
