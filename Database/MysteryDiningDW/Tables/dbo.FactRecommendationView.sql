CREATE TABLE [dbo].[FactRecommendationView]
(
[RecommendationViewKey] [int] NOT NULL IDENTITY(1, 1),
[IPAddress] [nvarchar] (30) COLLATE Latin1_General_CI_AS NOT NULL,
[CookieGUID] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ViewDateKey] [int] NOT NULL,
[ViewTimestamp] [datetime] NOT NULL,
[RecommendationKey] [int] NOT NULL,
[GuestSource] [nvarchar] (30) COLLATE Latin1_General_CI_AS NOT NULL,
[VisitKey] [int] NOT NULL,
[GuestMarketingKey] [int] NOT NULL,
[FeedbackKey] [int] NOT NULL,
[BranchKey] [int] NOT NULL,
[PeriodKey] [int] NOT NULL,
[FacebookLiked] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[TwitterFollowed] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[RecommendationStatus] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[RecommendationViewID] [int] NOT NULL,
[FacebookSocialChannelKey] [int] NOT NULL,
[TwitterSocialChannelKey] [int] NOT NULL,
[ValidFromDate] [datetime] NOT NULL,
[ValidToDate] [datetime] NOT NULL,
[Active] [bit] NOT NULL,
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_FactRecommendationView_TimeStamp] DEFAULT (getdate()),
[ChangeReason] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactRecommendationView] ADD CONSTRAINT [PK_FactRecommendationView] PRIMARY KEY CLUSTERED  ([RecommendationViewKey]) ON [PRIMARY]
GO
