CREATE TABLE [dbo].[ConfigStyleItem]
(
[ConfigStyleItemID] [smallint] NOT NULL IDENTITY(1, 1),
[ItemName] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ItemValue] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ConfigStyleItemGroupID] [smallint] NOT NULL,
[Editable] [bit] NOT NULL CONSTRAINT [DF_ConfigStyleItem_Editable] DEFAULT ((1)),
[Active] [bit] NOT NULL CONSTRAINT [DF_ConfigStyleItem_Active] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConfigStyleItem] ADD CONSTRAINT [PK_ConfigStyleItem] PRIMARY KEY CLUSTERED  ([ConfigStyleItemID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConfigStyleItem] ADD CONSTRAINT [FK_ConfigStyleItem_ConfigStyleItemGroup] FOREIGN KEY ([ConfigStyleItemGroupID]) REFERENCES [dbo].[ConfigStyleItemGroup] ([ConfigStyleItemGroupID])
GO
GRANT SELECT ON  [dbo].[ConfigStyleItem] TO [TMDCReporting]
GRANT INSERT ON  [dbo].[ConfigStyleItem] TO [TMDCReporting]
GRANT UPDATE ON  [dbo].[ConfigStyleItem] TO [TMDCReporting]
GO
