CREATE TABLE [dbo].[FactFeedbackResult]
(
[FeedbackResultKey] [int] NOT NULL IDENTITY(1, 1),
[VisitKey] [int] NOT NULL,
[BranchKey] [int] NOT NULL,
[GeographyKey] [int] NOT NULL,
[PeriodKey] [int] NOT NULL,
[VisitDateKey] [int] NOT NULL,
[FeedbackKey] [int] NOT NULL,
[Questionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[SubQuestionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Score] [decimal] (18, 2) NOT NULL,
[MaxScore] [decimal] (18, 2) NOT NULL,
[ManagerComments] [nvarchar] (3000) COLLATE Latin1_General_CI_AS NOT NULL,
[IncludeInAnalysis] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[TimeStarted] [datetime] NOT NULL,
[TimeEnded] [datetime] NOT NULL,
[IPAddress] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[CookieGUID] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[CompletedDateKey] [int] NOT NULL,
[DiscardReason] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[DiscardValue] [int] NULL,
[ValidFromDate] [datetime] NOT NULL,
[ValidToDate] [datetime] NOT NULL,
[Active] [bit] NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[FeedbackResultID] [int] NOT NULL,
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_FactFeedbackResult_TimeStamp] DEFAULT (getdate()),
[ChangeReason] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[FeedbackType] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_FactFeedbackResult_FeedbackType] DEFAULT ((1)),
[OfferedAdvocacy] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_FactFeedbackResult_OfferedAdvocacy] DEFAULT (N'N/A'),
[AcceptedAdvocacy] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_FactFeedbackResult_AcceptedAdvocacy] DEFAULT (N'N/A'),
[PostedAdvocacy] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_FactFeedbackResult_PostedAdvocacy] DEFAULT (N'N/A'),
[SubscribeAdvocacy] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_FactFeedbackResult_SubscribeAdvocacy] DEFAULT (N'N/A'),
[GuestMarketingKey] [int] NOT NULL CONSTRAINT [DF_FactFeedbackResult_GuestMarketingKey] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactFeedbackResult] ADD CONSTRAINT [PK_FactFeedbackResult] PRIMARY KEY CLUSTERED  ([FeedbackResultKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactFeedbackResult] ADD CONSTRAINT [FK_FactFeedbackResult_DimBranch] FOREIGN KEY ([BranchKey]) REFERENCES [dbo].[DimBranch] ([BranchKey])
GO
ALTER TABLE [dbo].[FactFeedbackResult] ADD CONSTRAINT [FK_FactFeedbackResult_DimBranch1] FOREIGN KEY ([BranchKey]) REFERENCES [dbo].[DimBranch] ([BranchKey])
GO
ALTER TABLE [dbo].[FactFeedbackResult] ADD CONSTRAINT [FK_FactFeedbackResult_DimFeedback] FOREIGN KEY ([FeedbackKey]) REFERENCES [dbo].[DimFeedback] ([FeedbackKey])
GO
ALTER TABLE [dbo].[FactFeedbackResult] ADD CONSTRAINT [FK_FactFeedbackResult_DimGeography] FOREIGN KEY ([GeographyKey]) REFERENCES [dbo].[DimGeography] ([GeographyKey])
GO
ALTER TABLE [dbo].[FactFeedbackResult] ADD CONSTRAINT [FK_FactFeedbackResult_DimPeriod] FOREIGN KEY ([PeriodKey]) REFERENCES [dbo].[DimPeriod] ([PeriodKey])
GO
ALTER TABLE [dbo].[FactFeedbackResult] ADD CONSTRAINT [FK_FactFeedbackResult_DimVisit] FOREIGN KEY ([VisitKey]) REFERENCES [dbo].[DimVisit] ([VisitKey])
GO
ALTER TABLE [dbo].[FactFeedbackResult] ADD CONSTRAINT [FK_FactFeedbackResult_DimVisit1] FOREIGN KEY ([VisitKey]) REFERENCES [dbo].[DimVisit] ([VisitKey])
GO
