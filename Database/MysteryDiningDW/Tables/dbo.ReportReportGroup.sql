CREATE TABLE [dbo].[ReportReportGroup]
(
[ReportReportGroupID] [int] NOT NULL,
[ReportGroupID] [int] NOT NULL,
[ReportID] [int] NOT NULL,
[DateAdded] [datetime] NOT NULL,
[DateRemoved] [datetime] NULL,
[AddedByAdminID] [smallint] NOT NULL,
[RemovedByAdminID] [smallint] NULL,
[Active] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReportReportGroup] ADD CONSTRAINT [PK_ReportReportGroup] PRIMARY KEY CLUSTERED  ([ReportReportGroupID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReportReportGroup] ADD CONSTRAINT [FK_ReportReportGroup_ReportGroup] FOREIGN KEY ([ReportGroupID]) REFERENCES [dbo].[ReportGroup] ([ReportGroupID])
GO
ALTER TABLE [dbo].[ReportReportGroup] ADD CONSTRAINT [FK_ReportReportGroup_Report] FOREIGN KEY ([ReportID]) REFERENCES [dbo].[Report] ([ReportID])
GO
