CREATE TABLE [dbo].[StagingBrandStatus]
(
[StagingBrandStatusID] [tinyint] NOT NULL,
[Name] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StagingBrandStatus] ADD CONSTRAINT [PK_StagingBrandStatus] PRIMARY KEY CLUSTERED  ([StagingBrandStatusID]) ON [PRIMARY]
GO
