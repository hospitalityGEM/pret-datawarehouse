CREATE TABLE [dbo].[FactVisitResult]
(
[VisitResultKey] [int] NOT NULL IDENTITY(1, 1),
[VisitKey] [int] NOT NULL,
[BranchKey] [int] NOT NULL,
[GeographyKey] [int] NULL,
[PeriodKey] [int] NOT NULL,
[VisitDateKey] [int] NOT NULL,
[AssessorKey] [int] NOT NULL,
[Questionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[SubQuestionnaire] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Status] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[AdminKey] [int] NOT NULL,
[Score] [decimal] (18, 2) NOT NULL,
[MaxScore] [decimal] (18, 2) NOT NULL,
[ScoreText] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ScoreMethod] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Comment] [nvarchar] (4000) COLLATE Latin1_General_CI_AS NULL,
[IncludeInAnalysis] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[ValidFromDate] [datetime] NOT NULL,
[ValidToDate] [datetime] NOT NULL,
[Active] [bit] NOT NULL,
[Source] [nvarchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[VisitID] [int] NOT NULL,
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_FactVisitResult_TimeStamp] DEFAULT (getdate()),
[ChangeReason] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[AdjustedScore] [decimal] (18, 2) NULL
) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_FactVisitResult_Active] ON [dbo].[FactVisitResult] ([Active]) INCLUDE ([PeriodKey]) ON [PRIMARY]

GO
ALTER TABLE [dbo].[FactVisitResult] ADD CONSTRAINT [PK_FactVisitResult] PRIMARY KEY CLUSTERED  ([VisitResultKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_FactVisitResult_7_1716917188__K3_K14_K22_K2_K5_K6_12] ON [dbo].[FactVisitResult] ([BranchKey], [MaxScore], [Active], [VisitKey], [PeriodKey], [VisitDateKey]) INCLUDE ([Score]) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_1716917188_5_3_2_6_22] ON [dbo].[FactVisitResult] ([PeriodKey], [BranchKey], [VisitKey], [VisitDateKey], [Active])
GO
CREATE STATISTICS [_dta_stat_1716917188_2_14_22_5] ON [dbo].[FactVisitResult] ([VisitKey], [MaxScore], [Active], [PeriodKey])
GO
ALTER TABLE [dbo].[FactVisitResult] ADD CONSTRAINT [FK_FactVisitResult_DimAdmin] FOREIGN KEY ([AdminKey]) REFERENCES [dbo].[DimAdmin] ([AdminKey])
GO
ALTER TABLE [dbo].[FactVisitResult] ADD CONSTRAINT [FK_FactVisitResult_DimAssessor] FOREIGN KEY ([AssessorKey]) REFERENCES [dbo].[DimAssessor] ([AssessorKey])
GO
ALTER TABLE [dbo].[FactVisitResult] ADD CONSTRAINT [FK_FactVisitResult_DimBranch] FOREIGN KEY ([BranchKey]) REFERENCES [dbo].[DimBranch] ([BranchKey])
GO
ALTER TABLE [dbo].[FactVisitResult] ADD CONSTRAINT [FK_FactVisitResult_DimGeography] FOREIGN KEY ([GeographyKey]) REFERENCES [dbo].[DimGeography] ([GeographyKey])
GO
ALTER TABLE [dbo].[FactVisitResult] ADD CONSTRAINT [FK_FactVisitResult_DimPeriod] FOREIGN KEY ([PeriodKey]) REFERENCES [dbo].[DimPeriod] ([PeriodKey])
GO
ALTER TABLE [dbo].[FactVisitResult] ADD CONSTRAINT [FK_FactVisitResult_DimDate] FOREIGN KEY ([VisitDateKey]) REFERENCES [dbo].[DimDate] ([DateKey])
GO
ALTER TABLE [dbo].[FactVisitResult] ADD CONSTRAINT [FK_FactVisitResult_DimVisit] FOREIGN KEY ([VisitKey]) REFERENCES [dbo].[DimVisit] ([VisitKey])
GO
GRANT SELECT ON  [dbo].[FactVisitResult] TO [TMDCReporting]
GO
