SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_Minerva_GetManagersBranches]
	@ManagerID int
AS

BEGIN
SELECT
		1 AS BranchID
	,	'Branch Name 1' AS BranchName
UNION
SELECT
		2 AS BranchID
	,	'Branch Name 2' AS BranchName
UNION
SELECT
		3 AS BranchID
	,	'Branch Name 3' AS BranchName
UNION
SELECT
		4 AS BranchID
	,	'Branch Name 4' AS BranchName

END
GO
GRANT EXECUTE ON  [dbo].[usp_Minerva_GetManagersBranches] TO [TMDCReporting]
GO
