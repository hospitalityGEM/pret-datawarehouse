SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_ETL_EmailDWRunSummary]
	@EmailRecipient NVARCHAR(200) = 'phil.collins@hospitalitygem.com'
AS
BEGIN

DECLARE 
	@DWStart		DATETIME
,	@DWEnd			DATETIME
,	@MaintenanceEnd DATETIME
,	@LatestDate		DATETIME
,	@Body			NVARCHAR(MAX)
,	@ErrorCount		INT
,	@NewRowCount	INT
,	@AuditTableHTML	NVARCHAR(MAX)

SELECT @DWStart = 
CASE
	WHEN @DWStart IS NULL 
		THEN (SELECT LastUpdated FROM ConfigInfo WHERE ConfigInfoID = 15)
	ELSE @DWStart
END

SELECT @DWEnd = LastUpdated FROM ConfigInfo WHERE ConfigInfoID = 3
SELECT @MaintenanceEnd = LastUpdated FROM ConfigInfo WHERE ConfigInfoID = 24
SELECT @LatestDate = MAX(TimeStamp) FROM MysteryDiningCopy.dbo.VisitUpdate

SELECT
	@ErrorCount =  SUM(et.ErrorCount)
FROM 
		(
			SELECT
				'DimAdmin'				AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorDimAdmin
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				'DimAssessor'			AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorDimAssessor
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				'DimBranch'				AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorDimBranch
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				'DimDish'				AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorDimDish
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				'DimFeedback'			AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorDimFeedback
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				'DimGeography'			AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorDimGeography
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				'DimGuestMarketing'	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorDimGuestMarketing
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				'DimPeriod'				AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorDimPeriod
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				'DimQuestion'			AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorDimQuestion
			WHERE
				[TimeStamp] >  @DWStart
			
			UNION

			SELECT
				'DimRecommendation'	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorDimRecommendation
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				'DimSocialChannel'	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorDimSocialChannel
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				'DimVisit'				AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorDimVisit
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				'FactAdvocacyFacebook'		AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactAdvocacyFacebook
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				'FactAdvocacyTwitter'		AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactAdvocacyTwitter
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				'FactAdvocacyEmail'	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactAdvocacyEmail
			WHERE
				[TimeStamp] >  @DWStart
			
			UNION

			SELECT
				'FactLinkTracking'	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactLinkTracking
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				'FactRecommendationView'	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactRecommendationView
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				'FactSocialChannelCount'	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactSocialChannelCount
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				'FactVisitResult'		AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactVisitResult
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				'FactAssessorHistory'	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactAssessorHistory
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				'FactBenchmarkResult'	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactBenchmarkResult
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				'FactCategoryResult'	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactCategoryResult
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				'FactDepartmentResult'	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactDepartmentResult
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				'FactFeedbackResult'	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactFeedbackResult
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				'FactPerformanceAlertResult'	AS Task
			,	COUNT(*)						AS ErrorCount
			FROM
				ErrorFactPerformanceAlertResult
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				'FactQuestionResult'	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactQuestionResult
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				'FactSectionResult'		AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactSectionResult
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				'FactVisitResult'		AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactVisitResult
			WHERE
				[TimeStamp] >  @DWStart
		) et 

SELECT
	@NewRowCount = (SUM([NewOutput]) + SUM([SCD2NewOutput]))
FROM 
	[AuditLoadCounts] alc
WHERE
	[TimeStamp] >  @DWStart

SELECT
	@AuditTableHTML = 
	'
	<table border = 2><tr>
	<th>TimeStamp</th>
	<th>Task</th>
	<th>Error</th>
	</tr>
	' 
	+ 		
	(CAST(
	(SELECT
		CONVERT(NVARCHAR(20),al.[TimeStamp], 108)		AS 'td',''
	,	al.[AuditTask]									AS 'td',''
	,	ISNULL(al.ErrorMessage, 'No Errors Detected')	AS 'td',''
	
	FROM 
	[AuditLog] al
	WHERE
	[TimeStamp] >  @DWStart

	GROUP BY
		CONVERT(NVARCHAR(20),al.[TimeStamp], 108)		
	,	al.ErrorMessage				
	,	al.[AuditTask] FOR XML PATH('tr'), ELEMENTS) AS NVARCHAR(MAX)))
	+
	'</table>'

SELECT
@Body =
	'
	<html><H1>DW ETL Summary</H1><body'
	+
	CASE 
		WHEN @ErrorCount > 0	THEN ' bgcolor="red"' 
		WHEN @NewRowCount < 1	THEN ' bgcolor="orange"' 
		ELSE '' 
	END
	+
	'>'
	+
	'<p>Latest Date in Source Database: ' + CONVERT(NVARCHAR(20),@LatestDate) + '</p>'
	+
	'<p>DW Started: ' + CONVERT(NVARCHAR(20),@DWStart) + '</p>'
	+
	'<p>ETL Ended: '   + CONVERT(NVARCHAR(20),@DWEnd) + ' - Elapsed Time (Minutes): '   + CONVERT(NVARCHAR(10),DATEDIFF(MINUTE, CONVERT(NVARCHAR(20),@DWStart), CONVERT(NVARCHAR(20),@DWEnd))) + '</p>'
	+
	'<p>Maintenance Ended: '   + CONVERT(NVARCHAR(20),@MaintenanceEnd) + ' - Elapsed Time (Minutes): '   + CONVERT(NVARCHAR(10),DATEDIFF(MINUTE, CONVERT(NVARCHAR(20),@DWStart), CONVERT(NVARCHAR(20),@MaintenanceEnd))) + '</p>'
	+
	'<p>Errors Detected: ' + CONVERT(NVARCHAR(20),@ErrorCount) + '</p>'
	+
	'
	<table border = 2><tr>
	<th>TimeStamp</th>
	<th>Table</th>
	<th>Start</th>
	<th>Existing</th>
	<th>Source</th>
	<th>Unchanged</th>
	<th>New</th>
	<th>SCD2 Expired</th>
	<th>SCD2 New</th>
	<th>SCD1 Update</th>
	<th>Invalid</th>
	<th>Errors</th>
	<th>Last Output</th>
	</tr>
	' 
	+ dr.DataRows + 
	'
	</table>
	</br>
	<h2>Audit Log</h2>
	</br>
	'
	+ @AuditTableHTML 
	+
	'</body></html>'
FROM 
(
SELECT
		
(CAST((
SELECT
	CONVERT(NVARCHAR(10),[TimeStamp], 108)		AS 'td',''
,	alc.[Task]									AS 'td',''
,	CONVERT(NVARCHAR(10),[StartTime], 108)		AS 'td',''
,	[ExistingInput]								AS 'td',''
,	[SourceSystemInput]							AS 'td',''
,	[UnchangedOutput]							AS 'td',''
,	[NewOutput]									AS 'td',''
,	[SCD2ExpiredOutput]							AS 'td',''
,	[SCD2NewOutput]								AS 'td',''
,	[SCD1UpdateOutput]							AS 'td',''
,	[InvalidInputOutput]						AS 'td',''
,	et.ErrorCount								AS 'td',''
,	CONVERT(NVARCHAR(10),[LastOutputTime], 108)	AS 'td',''
		
FROM 
[AuditLoadCounts] alc
INNER JOIN 
(
	SELECT
		'DimAdmin'				AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorDimAdmin
	WHERE
		[TimeStamp] >  @DWStart

	UNION

	SELECT
		'DimAssessor'			AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorDimAssessor
	WHERE
		[TimeStamp] >  @DWStart

	UNION

	SELECT
		'DimBranch'				AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorDimBranch
	WHERE
		[TimeStamp] >  @DWStart

	UNION

	SELECT
		'DimDish'				AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorDimDish
	WHERE
		[TimeStamp] >  @DWStart

	UNION

	SELECT
		'DimFeedback'			AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorDimFeedback
	WHERE
		[TimeStamp] >  @DWStart

	UNION

	SELECT
		'DimGeography'			AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorDimGeography
	WHERE
		[TimeStamp] >  @DWStart

	UNION

	SELECT
		'DimGuestMarketing'	AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorDimGuestMarketing
	WHERE
		[TimeStamp] >  @DWStart

	UNION

	SELECT
		'DimPeriod'				AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorDimPeriod
	WHERE
		[TimeStamp] >  @DWStart

	UNION

	SELECT
		'DimQuestion'			AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorDimQuestion
	WHERE
		[TimeStamp] >  @DWStart
			
	UNION

	SELECT
		'DimRecommendation'	AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorDimRecommendation
	WHERE
		[TimeStamp] >  @DWStart

	UNION

	SELECT
		'DimSocialChannel'	AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorDimSocialChannel
	WHERE
		[TimeStamp] >  @DWStart

	UNION

	SELECT
		'DimVisit'				AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorDimVisit
	WHERE
		[TimeStamp] >  @DWStart

	UNION

	SELECT
		'FactAdvocacyFacebook'		AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorFactAdvocacyFacebook
	WHERE
		[TimeStamp] >  @DWStart

	UNION

	SELECT
		'FactAdvocacyTwitter'		AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorFactAdvocacyTwitter
	WHERE
		[TimeStamp] >  @DWStart

	UNION

	SELECT
		'FactAdvocacyEmail'	AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorFactAdvocacyEmail
	WHERE
		[TimeStamp] >  @DWStart
			
	UNION

	SELECT
		'FactLinkTracking'	AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorFactLinkTracking
	WHERE
		[TimeStamp] >  @DWStart

	UNION

	SELECT
		'FactRecommendationView'	AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorFactRecommendationView
	WHERE
		[TimeStamp] >  @DWStart

	UNION

	SELECT
		'FactSocialChannelCount'	AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorFactSocialChannelCount
	WHERE
		[TimeStamp] >  @DWStart

	UNION

	SELECT
		'FactVisitResult'		AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorFactVisitResult
	WHERE
		[TimeStamp] >  @DWStart

	UNION

	SELECT
		'FactAssessorHistory'	AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorFactAssessorHistory
	WHERE
		[TimeStamp] >  @DWStart

	UNION

	SELECT
		'FactBenchmarkResult'	AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorFactBenchmarkResult
	WHERE
		[TimeStamp] >  @DWStart

	UNION

	SELECT
		'FactCategoryResult'	AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorFactCategoryResult
	WHERE
		[TimeStamp] >  @DWStart

	UNION

	SELECT
		'FactDepartmentResult'	AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorFactDepartmentResult
	WHERE
		[TimeStamp] >  @DWStart

	UNION

	SELECT
		'FactFeedbackResult'	AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorFactFeedbackResult
	WHERE
		[TimeStamp] >  @DWStart

	UNION

	SELECT
		'FactPerformanceAlertResult'	AS Task
	,	COUNT(*)						AS ErrorCount
	FROM
		ErrorFactPerformanceAlertResult
	WHERE
		[TimeStamp] >  @DWStart

	UNION

	SELECT
		'FactQuestionResult'	AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorFactQuestionResult
	WHERE
		[TimeStamp] >  @DWStart

	UNION

	SELECT
		'FactSectionResult'		AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorFactSectionResult
	WHERE
		[TimeStamp] >  @DWStart

	UNION

	SELECT
		'FactVisitResult'		AS Task
	,	COUNT(*)				AS ErrorCount
	FROM
		ErrorFactVisitResult
	WHERE
		[TimeStamp] >  @DWStart
) et ON alc.Task = et.Task
WHERE
[TimeStamp] >  @DWStart

GROUP BY
	CONVERT(NVARCHAR(10),[TimeStamp], 108)	
,	CONVERT(NVARCHAR(10),[StartTime], 108)	
,	[ExistingInput]				
,	[SourceSystemInput]			
,	[UnchangedOutput]			
,	[NewOutput]					
,	[SCD2ExpiredOutput]			
,	[SCD2NewOutput]				
,	[SCD1UpdateOutput]			
,	[InvalidInputOutput]		
,	et.ErrorCount
,	CONVERT(NVARCHAR(10),[LastOutputTime], 108)
,	alc.Task FOR XML PATH('tr'), ELEMENTS) AS NVARCHAR(MAX)
)) AS DataRows
) dr

EXEC msdb.dbo.sp_send_dbmail
    @profile_name = 'BT SmartHost',
    @recipients = @EmailRecipient,
    @body = @Body,
    @subject = 'DW ETL Summary',
	@body_format ='HTML',
    @attach_query_result_as_file = 0 ;

-- Test Case
-- EXEC usp_ETL_EmailDWRunSummary 
END
GO
