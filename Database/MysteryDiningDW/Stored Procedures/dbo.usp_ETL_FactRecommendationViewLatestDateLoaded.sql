SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_FactRecommendationViewLatestDateLoaded] 
AS

UPDATE 
	ConfigInfo
SET 
	Value = ld.Value
,	LastUpdated = ld.LatestDate

FROM
	ConfigInfo ci

INNER JOIN
	(
	SELECT
		MAX(DateTimeAdded)	AS LatestDate
	,	MAX(OnlineFeedbackRecommendationReturnVisitorUID) AS Value
	,	28					AS ConfigInfoID
	FROM
		MysteryDiningCopy.dbo.OnlineFeedbackRecommendationReturnVisitor
	) ld ON ci.ConfigInfoID = ld.ConfigInfoID
WHERE
	ld.LatestDate IS NOT NULL
	
-- EXEC usp_ETL_FactRecommendationViewLatestDateLoaded
GO
