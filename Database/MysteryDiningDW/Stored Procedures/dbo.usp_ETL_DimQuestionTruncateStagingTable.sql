SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_DimQuestionTruncateStagingTable] 
AS
	-- DimQuestion
	TRUNCATE TABLE StagingDimQuestion

-- Test Case
-- EXEC usp_ETL_DimQuestionTruncateStagingTable
GO
