SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_UpdateDWDates] 
AS

BEGIN

	EXEC usp_ETL_vDimVisitEarliestDate
	
	EXEC usp_ETL_UpdateFactsEarliestDate

	EXEC usp_ETL_UpdateFactTimeStamps
	
	EXEC usp_ETL_UpdateLastSuccessfulRun

END
	
-- EXEC usp_ETL_UpdateDWDates
GO
