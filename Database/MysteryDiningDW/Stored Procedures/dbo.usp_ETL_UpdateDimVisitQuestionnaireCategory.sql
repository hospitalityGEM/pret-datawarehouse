SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[usp_ETL_UpdateDimVisitQuestionnaireCategory] 
AS

UPDATE DimVisit
SET QuestionnaireCategory=sdq.QuestionnaireCategory
FROM FactQuestionResult fqr 
	INNER JOIN DimQuestion dq on dq.QuestionKey=fqr.QuestionKey
	INNER JOIN StagingDimQuestion sdq on sdq.QuestionID=dq.QuestionID
	INNER JOIN DimVisit dv ON dv.VisitID=fqr.VisitID
WHERE
	fqr.Active=1
AND
	dv.QuestionnaireCategory<>sdq.QuestionnaireCategory



GO
