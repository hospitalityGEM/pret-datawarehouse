SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_ETL_AddAuditTransaction] 
	@Task				NVARCHAR(200)
,	@Error				NVARCHAR(500)= NULL
AS
BEGIN
	SET NOCOUNT ON
		INSERT INTO AuditLog 
		(
			AuditTask
		,	ErrorMessage
		)
		VALUES
		(
			@Task
		,	@Error
		)
END
-- Test
-- EXEC usp_ETL_AddAuditTransaction
	

GO
