SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_UpdateLastSuccessfulRun] 
AS
					
UPDATE 
	ConfigInfo
SET 
	Value = GETDATE()
,	LastUpdated = GETDATE()
WHERE
	[Key] = 'LastSuccessfulRun'
	
-- EXEC usp_ETL_UpdateLastSuccessfulRun
GO
