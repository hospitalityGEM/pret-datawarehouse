SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_FactVisitResultTruncateStagingTable] 
AS
	-- FactVisitResult
	TRUNCATE TABLE StagingFactVisitResult

-- Test Case
-- EXEC usp_ETL_FactVisitResultTruncateStagingTable
GO
