SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_UpdateDimPeriodCounts] 
AS

-- RESET EXISTING VALUES
UPDATE DimPeriod SET ClientPeriodOfYear = NULL, BrandPeriodOfYear = NULL, PeriodOfBrand = NULL, PeriodOfClient = NULL

--Set relevant periods as current period
UPDATE DimPeriod
SET
      [CurrentPeriod] = 
      (
            CASE
                  WHEN St.[Date] < DATEADD(DAY,-1, GETDATE()) AND En.[Date] >= DATEADD(DAY,-1, GETDATE()) THEN
                        1
                  ELSE
                        0     
            END
      )
FROM
      DimPeriod P
INNER JOIN DimDate St ON P.StartDateKey = St.DateKey
INNER JOIN DimDate En ON P.EndDateKey     = En.DateKey

-- Client Period of Year
UPDATE  DimPeriod
SET 
      ClientPeriodOfYear = Results.ClientPeriodOfYear
FROM
(
      SELECT
            PeriodKey
      ,     ROW_NUMBER() OVER(PARTITION BY p.ClientID, YEAR(d.[Date]) ORDER BY d.[Date], p.PeriodID) AS ClientPeriodOfYear
      FROM DimPeriod p
            LEFT JOIN DimDate d on p.StartDateKey = d.DateKey
      WHERE
            Active = 1
		AND       
			IncludeInAnalysis = 'Include'
      Group BY 
            p.PeriodID
      ,		p.ClientID
      ,		p.BrandID
      ,		d.DateKey
      ,		d.Date
      ,		PeriodKey
) AS Results
WHERE DimPeriod.PeriodKey = Results.PeriodKey

UPDATE DimPeriod
SET
      ClientPeriodOfYear = Result.ClientPeriodOfYear
FROM
(
      SELECT 
                  ClientPeriodOfYear
            ,     ClientID
            ,     PeriodID
      FROM 
            DimPeriod p 
      WHERE 
            ClientPeriodOfYear IS NOT NULL 
) AS Result
WHERE DimPeriod.ClientPeriodOfYear IS NULL
      AND DimPeriod.PeriodID = Result.PeriodID

-- Brand Period of Year
UPDATE DimPeriod
SET
	BrandPeriodOfYear = ResultSet.[Counter]
FROM
(
      SELECT 
            PeriodKey
            ,     ROW_NUMBER() OVER(PARTITION BY  ISNULL((CASE WHEN p.BrandID = 0 THEN NULL ELSE p.BrandID END), p.ClientID), YEAR(d.[Date]) ORDER BY d.[Date]) AS [Counter]
      FROM 
            DimPeriod p
      LEFT JOIN DimDate d on p.StartDateKey = d.DateKey
      WHERE 
			Active = 1
		AND       
			IncludeInAnalysis = 'Include'
      GROUP BY 
			p.PeriodID
		,	p.ClientID
		,	p.BrandID
		,	d.DateKey
		,	d.Date
		,	PeriodKey
) AS ResultSet
WHERE DimPeriod.PeriodKey = ResultSet.PeriodKey


UPDATE DimPeriod
SET
	BrandPeriodOfYear = results.BrandPeriodOfYear
FROM
(     
      SELECT 
            BrandPeriodOfYear
      ,     ClientID
      ,     BrandID     
      ,     PeriodID
      FROM 
            DimPeriod p 
      WHERE 
            BrandPeriodOfYear IS NOT NULL 
)  AS results
WHERE DimPeriod.BrandPeriodOfYear IS NULL
      AND DimPeriod.PeriodID = results.PeriodID

-- Period of Client
UPDATE DimPeriod
SET
      PeriodOfClient = Result.PeriodOfClient 
	  FROM (
            SELECT 
                        p.ClientID
                  ,     p.PeriodID
                  ,     ROW_NUMBER() OVER(PARTITION BY ISNULL(p.ClientID, (CASE WHEN p.BrandID = 0 THEN NULL ELSE p.BrandID END)) ORDER BY d.[Date], p.PeriodID) AS PeriodOfClient
            FROM DimPeriod p
				LEFT JOIN DimDate d on p.StartDateKey = d.DateKey
			WHERE       
				IncludeInAnalysis = 'Include'
			AND
				Active = 1
            GROUP BY 
				p.ClientID
			,	d.DateKey
			,	d.Date
			,	PeriodKey
			,	p.BrandID
			,	p.PeriodID
			,	PeriodOfClient
      ) AS Result
WHERE DimPeriod.PeriodID = Result.PeriodID

-- Period of Brand --
UPDATE DimPeriod
SET
      PeriodOfBrand = Result.PeriodOfBrand FROM (
            SELECT 
                        p.ClientID
                  ,     p.PeriodID
                  ,     ROW_NUMBER() OVER(PARTITION BY ISNULL((CASE WHEN p.BrandID = 0 THEN NULL ELSE p.BrandID END), p.ClientID) ORDER BY d.[Date], p.PeriodID) AS PeriodOfBrand
            FROM DimPeriod p
				LEFT JOIN DimDate d on p.StartDateKey = d.DateKey
			WHERE       
				IncludeInAnalysis = 'Include'
			AND
				Active = 1
			GROUP BY 
				p.BrandID
			,	p.ClientID
			,	d.DateKey
			,	d.Date
			,	PeriodKey
			,	p.PeriodID
			,	PeriodOfClient
      ) AS Result
WHERE DimPeriod.PeriodID = Result.PeriodID


-- Brand Periods Ago --
-- Reset values and set current periods to 0 periods ago
UPDATE DimPeriod SET ClientPeriodsAgo = NULL, BrandPeriodsAgo = NULL

UPDATE DimPeriod SET 
	BrandPeriodsAgo = 0
,	ClientPeriodsAgo = 0 
WHERE 
	PeriodID IN
	(
		SELECT
			PeriodID
		FROM
			DimPeriod
		WHERE
			IncludeInAnalysis = 'Include' 
		AND 
			CurrentPeriod = 1
	)

-- Past periods without brands --
--UPDATE DimPeriod
--SET 
--      BrandPeriodsAgo = ago.RowCounter
--      FROM
--      (
--            SELECT
--				p.ClientID
--            ,   p.PeriodID
--            ,   ROW_NUMBER() OVER(Partition by p.ClientID, p.IncludeInAnalysis, p.PeriodType ORDER BY p.StartDateKey DESC, p.PeriodID DESC) as RowCounter
--            FROM
--                DimPeriod p
--            INNER JOIN DimDate d on p.StartDateKey = d.DateKey
--            INNER JOIN DimDate e on p.EndDateKey = e.DateKey
--            WHERE
--                e.[Date] < DATEADD(DAY,-1, GETDATE())
--            AND p.BrandID = 0
--            AND p.IncludeInAnalysis = 'Include'
--            AND p.CurrentPeriod = 0
--			AND	p.Active = 1
--            Group BY 
--                  p.ClientID
--            ,     p.IncludeInAnalysis
--			,	  p.PeriodType
--            ,     p.StartDateKey
--            ,     p.PeriodID
--      ) as ago
--      where DimPeriod.PeriodID = ago.PeriodID
--	  AND   DimPeriod.BrandID = 0
---- Future periods without brands --
--UPDATE DimPeriod
--SET
--      BrandPeriodsAgo = future.RowCounter*-1
--      FROM
--      (
--            SELECT
--                        p.ClientID
--                  ,     p.PeriodID
--                  ,     ROW_NUMBER() OVER(PARTITION BY p.ClientID, p.IncludeInAnalysis, p.PeriodType ORDER BY p.StartDateKey, p.PeriodID DESC) AS RowCounter
--            FROM
--                  DimPeriod p
--            INNER JOIN DimDate d ON p.StartDateKey = d.DateKey
--            INNER JOIN DimDate e ON p.EndDateKey = e.DateKey
--            WHERE
--						p.ClientPeriodsAgo is null AND d.[Date] > DATEADD(DAY,-1, GETDATE())
--                  AND	p.BrandID = 0
--                  AND	p.IncludeInAnalysis = 'Include'
--                  AND   p.CurrentPeriod = 0
--				  AND	p.Active = 1
--            GROUP BY 
--                         p.ClientID
--            ,            p.IncludeInAnalysis 
--			,			 p.PeriodType
--            ,            p.StartDateKey
--            ,            p.PeriodID
      
--      ) AS future
--      WHERE DimPeriod.PeriodID = future.PeriodID
--      AND	DimPeriod.BrandID = 0
-- Past periods with brands --      
UPDATE DimPeriod
SET
            BrandPeriodsAgo = ago.RowCounter
FROM
(
      SELECT
				  p.ClientID
            ,     p.BrandID
            ,     p.PeriodID
            ,     ROW_NUMBER() OVER(PARTITION BY p.ClientId,p.BrandID, p.IncludeInAnalysis, p.PeriodType  ORDER BY min(p.StartDateKey) DESC, p.PeriodID DESC) AS RowCounter
      FROM 
            DimPeriod p
      INNER JOIN DimDate d ON p.StartDateKey = d.DateKey
      INNER JOIN DimDate e ON p.EndDateKey = e.DateKey
      WHERE 
                        e.Date < DATEADD(DAY,-1, GETDATE())
            AND         BrandID <> 0
            AND         p.IncludeInAnalysis = 'Include'
            AND         p.CurrentPeriod = 0
			AND			p.Active = 1
      GROUP BY 
                  p.BrandID
            ,     p.IncludeInAnalysis 
			,	  p.PeriodType
            ,     p.PeriodID    
			,	  p.ClientId
) AS ago
WHERE                      DimPeriod.PeriodID = ago.PeriodID 
AND                           DimPeriod.BrandID = ago.BrandID
AND							 DimPeriod.ClientID = ago.ClientID
-- Future periods with brands --
UPDATE DimPeriod
SET
      BrandPeriodsAgo = future.RowCounter*-1
FROM
(
      SELECT
                  p.BrandID
			,	  p.ClientId  
            ,     p.PeriodID
            ,     ROW_NUMBER() OVER(PARTITION BY p.ClientId,p.BrandID, p.IncludeInAnalysis, p.PeriodType  ORDER BY min(p.StartDateKey) DESC, p.PeriodID DESC) as RowCounter
      FROM 
            DimPeriod p
      INNER JOIN DimDate d ON p.StartDateKey = d.DateKey
      INNER JOIN DimDate e ON p.EndDateKey = e.DateKey
      WHERE 
                                                                        d.Date > DATEADD(DAY,-1, GETDATE())
            AND         BrandID <> 0
            AND         p.IncludeInAnalysis = 'Include'
            AND         p.CurrentPeriod = 0
			AND			p.Active = 1
      GROUP BY 
			  p.BrandID
		,     p.IncludeInAnalysis 
		,	  p.PeriodType
		,     p.PeriodID  
		,	  p.ClientId  
) AS future
WHERE 
            DimPeriod.PeriodID = future.PeriodID 
AND                           DimPeriod.BrandID = future.BrandID
AND							 DimPeriod.ClientID = future.ClientID
AND         DimPeriod.BrandPeriodsAgo is NULL

-- Client Period Ago
-- Past periods --
UPDATE DimPeriod
SET 
      ClientPeriodsAgo = ago.RowCounter
      FROM
      (
            SELECT
                        p.ClientID
                  ,     p.PeriodID
                  ,     ROW_NUMBER() OVER(Partition by p.ClientID, p.IncludeInAnalysis, p.PeriodType ORDER BY min(p.StartDateKey) DESC, p.PeriodID DESC) as RowCounter
            FROM
                  DimPeriod p
            INNER JOIN DimDate d on p.StartDateKey = d.DateKey
            INNER JOIN DimDate e on p.EndDateKey = e.DateKey
            WHERE
                        e.[Date] < DATEADD(DAY,-1, GETDATE())
                  AND   p.IncludeInAnalysis = 'Include'
                  AND   p.CurrentPeriod = 0
				  AND	p.Active = 1
            Group BY 
                p.ClientID
            ,   p.IncludeInAnalysis 
			,	p.PeriodType
            ,   p.PeriodID
      ) as ago
      where DimPeriod.PeriodID = ago.PeriodID

-- Future periods --
UPDATE DimPeriod
SET
      ClientPeriodsAgo = future.RowCounter*-1
      FROM
      (
            SELECT
                        p.ClientID
                  ,     p.PeriodID
                  ,     ROW_NUMBER() OVER(PARTITION BY p.ClientID, p.IncludeInAnalysis, p.PeriodType ORDER BY min(p.StartDateKey), p.PeriodID DESC) AS RowCounter
            FROM
                  DimPeriod p
            INNER JOIN DimDate d ON p.StartDateKey = d.DateKey
            INNER JOIN DimDate e ON p.EndDateKey = e.DateKey
            WHERE
                            p.ClientPeriodsAgo is null AND d.[Date] > DATEADD(DAY,-1, GETDATE())
                  AND       p.IncludeInAnalysis = 'Include'
                  AND       p.CurrentPeriod = 0
				  AND		p.Active = 1
            GROUP BY 
                                                 p.ClientID
            ,     p.IncludeInAnalysis 
			,	  p.PeriodType

            ,     p.PeriodID
      
      ) AS future
      WHERE DimPeriod.PeriodID = future.PeriodID

-- Update BrandPeriodsAgo where no Brand Set
UPDATE DimPeriod
SET
    BrandPeriodsAgo = ClientPeriodsAgo
FROM
	DimPeriod
WHERE
	BrandID = 0


-- Test
-- EXEC usp_ETL_UpdateDimPeriodCounts
GO
