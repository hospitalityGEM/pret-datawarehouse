SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_FactCategoryResultTruncateStagingTable] 
AS
	-- FactCategoryResult
	TRUNCATE TABLE StagingFactCategoryResult

-- Test Case
-- EXEC usp_ETL_FactCategoryResultTruncateStagingTable
GO
