SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[usp_Reports_Perceptions]
AS
Select
'Welcome' as Perception

Union

Select 
'Behaviours' as Perception

Union

Select 
'Conversations' as Perception

Union

Select 
'Organisation' as Perception

Union

Select 
'Pace' as Perception

Union

Select 
'Knowledge' as Perception

Union

Select 
'Selling' as Perception

Union

Select 
'Product' as Perception
Union

Select
'Housekeeping' as Perception

Union

Select 
'Payment' as Perception

Union

Select 
'Recommend' as Perception



GO
