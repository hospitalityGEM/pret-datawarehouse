SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_LegacyExpireFactSectionResultRecords] 
AS

--Expired Records

UPDATE FactSectionResult
SET
	[Active] = 0
,	[ValidToDate] = SfR.ValidToDate

FROM
	FactSectionResult fR
INNER JOIN
	StagingFactSectionResult SfR	ON	fR.[Source]		= SfR.[Source]
									AND	fR.SectionID	= SfR.SectionID
									AND fR.VisitID		= SfR.VisitID
	
WHERE
	fR.[Active] = 1
AND	fR.[ValidFromDate] <= SfR.ValidToDate
AND fR.[Source] = 'Legacy'


-- Update Current Score values on old records

--UPDATE FactSectionResult
--SET
--	[CurrentScore] = SfR.CurrentScore

--FROM
--	FactSectionResult fR
--INNER JOIN
--	StagingFactSectionResult SfR	ON	fR.[Source]		= SfR.[Source]
--									AND	fR.SectionID	= SfR.SectionID
--									AND fR.VisitID		= SfR.VisitID
--WHERE
--	fR.[Source] = 'Legacy'

-- Set Active flag Appropriately

--UPDATE
--    FactSectionResult
--SET
--    [Active] = 
--				CASE 
--					WHEN active.realmax IS NULL THEN 0
--					ELSE 1
--				END
--FROM
--	FactSectionResult [source]
--LEFT JOIN
--    (    
--          SELECT
--                MAX(fsr.SectionResultKey) realmax
--          FROM
--                (
--                  SELECT
--						VisitID,
--                        SectionID,
--                        [Source],
--                        MAX(ValidFromDate) maxdate
--                  FROM
--                        FactSectionResult
--                  WHERE
--						[Source] = 'Legacy'
--                  GROUP BY
--                        VisitID,
--                        SectionID,
--                        [Source]
--                ) x
--                INNER JOIN FactSectionResult fsr ON x.SectionID = fsr.SectionID
--												AND x.VisitID = fsr.VisitID
--												AND x.maxdate = fsr.ValidFromDate
--          GROUP BY
--                fsr.SectionID
--			,	fsr.VisitID
--			,	fsr.[Source]
--	) active ON [source].SectionResultKey = active.realmax

-- Test
-- EXEC usp_ETL_LegacyExpireFactSectionResultRecords
	
GO
