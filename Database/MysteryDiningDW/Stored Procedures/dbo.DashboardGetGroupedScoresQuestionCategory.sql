
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[DashboardGetGroupedScoresQuestionCategory]
	-- Add the parameters for the stored procedure here
	@DateFrom datetime,
	@DateTo datetime,
	@ClientID int = 0, 
	@BranchIDs nvarchar(max),
	@Count int = 5,
	@IsTop bit = 1,
	@VisitTypes nvarchar(max),
	@HideCurrentPeriod bit,
	@Benchmark nvarchar(max) = '',
	@QuestionCategory nvarchar(max) = '',
	@QuestionnaireCategory nvarchar(max) = '',
	@Department nvarchar(max) = '',
	@Brand nvarchar(max) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE	@DynamicSQL		NVARCHAR(MAX)
		DECLARE @ReturnTable TABLE 
	(
		 		[Group]		NVARCHAR(200)
						,	[Score]	DECIMAL(18,2)
	)
	SET @DynamicSQL =
	'
	DECLARE
		@DateKeyFrom	INT
	,	@DateKeyTo		INT
	,	@MaxScore		DECIMAL(18,2)
	,	@TopCount		INT
	,	@DateKeyCurrent	INT
	,	@HideCurrentPeriod BIT = ' + CONVERT(NVARCHAR,@HideCurrentPeriod) +
	'		
	DECLARE @Results	TABLE
		(
			QuestionCategory		NVARCHAR(200)
		,	QuestionCategoryScore	DECIMAL(18,2)
		)

	SELECT @DateKeyFrom	= DateKey FROM DimDate WHERE [Date] = ''' + CONVERT(NVARCHAR, @DateFrom, 112) + '''
	SELECT @DateKeyTo	= DateKey FROM DimDate WHERE [Date] = ''' + CONVERT(NVARCHAR, @DateTo, 112) + '''
	SELECT @DateKeyCurrent	= DateKey FROM DimDate WHERE [Date] = CONVERT(DATE, GETDATE())

	INSERT INTO @Results (QuestionCategory, QuestionCategoryScore)
	SELECT
		fqr.ManagementCategory
	,	ROUND((SUM(fqr.Score) / SUM(fqr.MaxScore))*100,0)
	FROM
		FactQuestionResult fqr
		INNER JOIN DimBranch db ON fqr.BranchKey = db.BranchKey
		INNER JOIN DimVisit dv	ON fqr.VisitKey	 = dv.VisitKey
		INNER JOIN DimPeriod dp ON fqr.PeriodKey = dp.PeriodKey
		INNER JOIN DimQuestion dq ON fqr.QuestionKey = dq.QuestionKey
	WHERE
			db.BranchID IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(''' + @BranchIDs + ''', '',''))
		AND
			dv.VisitStatus IN (''Reviewed'',''Complete'')
		AND
			dv.Type IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(''' + @VisitTypes + ''', '',''))
		AND
			dp.StartDateKey >= @DateKeyFrom
		AND
			dp.StartDateKey <= @DateKeyTo
		AND
			fqr.MaxScore > 0
		AND
			fqr.ManagementCategory <> ''No Category''
		AND
			dq.HiddenSection = ''Visible''
		AND
			dq.MenuQuestion = ''N/A''
		AND
			dq.HiddenQuestion = ''Visible''
		AND
			fqr.Active = 1
		AND
			fqr.IncludeInAnalysis = ''Include''
		AND
			dp.IncludeInPortal = ''Include''
		AND
			(CASE
				WHEN @HideCurrentPeriod = 1 AND dp.EndDateKey < @DateKeyCurrent THEN 1
				WHEN @HideCurrentPeriod = 0 THEN 1
				ELSE 0
			END) = 1
		AND
			(CASE
				WHEN LEN(''' + @Benchmark + ''') = 0 THEN 1
				WHEN dq.Benchmark IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(''' + @Benchmark + ''','','')) THEN 1
				ELSE 0
			END) = 1
		AND
			(CASE
				WHEN LEN(''' + @QuestionCategory+ ''') = 0 THEN 1
				WHEN dq.ManagementCategory IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(''' + @QuestionCategory+ ''','','')) THEN 1
				ELSE 0
			END) = 1
		AND
			(CASE
				WHEN LEN(''' + @QuestionnaireCategory+ ''') = 0 THEN 1
				WHEN dq.QuestionnaireCategory IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(''' + @QuestionnaireCategory + ''','','')) THEN 1
				ELSE 0
			END) = 1
		AND
			(CASE
				WHEN LEN(''' + @Department+ ''') = 0 THEN 1
				WHEN dq.Department IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(''' + @Department + ''','','')) THEN 1
				ELSE 0
			END) = 1
		AND
			(CASE
				WHEN LEN(''' + @Brand+ ''') = 0 THEN 1
				WHEN db.Brand IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(''' + @Brand + ''','','')) THEN 1
				ELSE 0
			END) = 1
	GROUP BY
		fqr.ManagementCategory
	
	SELECT
		[Group]
	,	[Score]
	FROM
	(
		SELECT
			QuestionCategory	  AS [Group]
		,	QuestionCategoryScore AS [Score]
		,	RANK()OVER(ORDER BY QuestionCategoryScore ' + IIF(@IsTop = 1, 'DESC', 'ASC') + ') AS Ranker
		FROM
			@Results		 
	) Scores
	WHERE
		Ranker BETWEEN 1 AND ' + CONVERT(NVARCHAR,@Count)

	INSERT INTO @ReturnTable EXEC(@DynamicSQL)
	SELECT * FROM @ReturnTable

END

-- Test Case
-- EXEC [dbo].[DashboardGetGroupedScoresQuestionCategory]	@DateFrom = '01/07/2012',	@DateTo = '07/09/2012',	@ClientID = 0, 	@BranchIDs = '4518, 4519, 4520',	@Count = 5,	@IsTop = 1, @VisitTypes = 'Mystery Visit', @HideCurrentPeriod = 0

GO
