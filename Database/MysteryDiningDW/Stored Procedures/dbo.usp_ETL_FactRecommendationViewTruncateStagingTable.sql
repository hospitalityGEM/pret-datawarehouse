SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_FactRecommendationViewTruncateStagingTable] 
AS
	-- FactRecommendationView
	TRUNCATE TABLE StagingFactRecommendationView

-- Test Case
-- EXEC usp_ETL_FactRecommendationViewTruncateStagingTable
GO
