
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE [dbo].[usp_ETL_LegacyExpireDimBranchRecords] 
AS

UPDATE DimBranch
SET
	[Active] = 0
,	[ValidToDate] = SdB.ValidToDate

FROM
	DimBranch dB
INNER JOIN
	StagingDimBranch SdB	ON	dB.[BranchKey]	= SdB.[BranchKey]
	
WHERE
	SdB.[Type] = 'Expired'
AND SdB.[Source] = 'Legacy'

-- Update Missing Current Values --
UPDATE dB
SET
	CurrentArea		= CdB.Area
FROM
	DimBranch dB
	INNER JOIN DimBranch CdB ON dB.BranchID = CdB.BranchID AND CdB.Active = 1
WHERE
	dB.CurrentArea IS NULL

UPDATE dB
SET
	CurrentRegion	= CdB.Region
FROM
	DimBranch dB
	INNER JOIN DimBranch CdB ON dB.BranchID = CdB.BranchID AND CdB.Active = 1
WHERE
	dB.CurrentRegion IS NULL

UPDATE dB
SET
	CurrentBranch	= CdB.Branch
FROM
	DimBranch dB
	INNER JOIN DimBranch CdB ON dB.BranchID = CdB.BranchID AND CdB.Active = 1
WHERE
	dB.CurrentBranch IS NULL

UPDATE dB
SET
	CurrentBrand	= CdB.Brand
FROM
	DimBranch dB
	INNER JOIN DimBranch CdB ON dB.BranchID = CdB.BranchID AND CdB.Active = 1
WHERE
	dB.CurrentBrand IS NULL

UPDATE dB
SET
	CurrentClientSegment	= CdB.ClientSegment
FROM
	DimBranch dB
	INNER JOIN DimBranch CdB ON dB.BranchID = CdB.BranchID AND CdB.Active = 1
WHERE
	dB.CurrentClientSegment IS NULL

UPDATE dB
SET
	BranchCode	= CdB.BranchCode
FROM
	DimBranch dB
	INNER JOIN DimBranch CdB ON dB.BranchID = CdB.BranchID AND CdB.Active = 1
WHERE
	dB.BranchCode IS NULL

UPDATE dB
SET
	CurrentClient	= CdB.CurrentClient
FROM
	DimBranch dB
	INNER JOIN DimBranch CdB ON dB.BranchID = CdB.BranchID AND CdB.Active = 1
WHERE
	dB.CurrentClient IS NULL

-- Update SCD1 Changes --
UPDATE DimBranch
SET
	CurrentArea		  = SdB.CurrentArea
,	CurrentRegion	  = SdB.CurrentRegion
,	CurrentBranch	  = SdB.CurrentBranch
,	CurrentBrand	  = SdB.CurrentBrand
,	CurrentClientSegment = Sdb.CurrentClientSegment
,	GeographyKey	  = SdB.GeographyKey
,	BranchCode		  = Sdb.BranchCode
,	CurrentClient     = Sdb.CurrentClient
,	AccountManager    = Sdb.AccountManager
,	LowThreshold      = Sdb.LowThreshold
,	HighThreshold     = Sdb.HighThreshold
,	[Target]		  = Sdb.[Target]
,	[Location]		  = Sdb.[Location]
,	[Telephone]       = Sdb.[Telephone]
,	[OurLocation]     = Sdb.[OurLocation]
,	[IsInternational] = Sdb.[IsInternational]
,	[CurrentNBArea]   = Sdb.CurrentNBArea
,	[CurrentNBRegion] = Sdb.CurrentNBRegion
,	[AgreementType]   = Sdb.AgreementType
,	[SubBrand]        = Sdb.SubBrand
,	[ClosedBranch]    = Sdb.ClosedBranch

FROM
	DimBranch dB
INNER JOIN
	StagingDimBranch SdB	ON	dB.[Source]		= SdB.[Source]
							AND dB.[BranchID]	= SdB.[BranchID]
WHERE
--	SdB.[Type] = 'SCD1'
--AND 
	SdB.[Source] = 'Legacy'

-- Set Active flag Appropriately

--UPDATE
--    DimBranch
--SET
--    [Active] = 
--				CASE 
--					WHEN active.realmax IS NULL THEN 0
--					ELSE 1
--				END
--FROM
--	DimBranch [source]
--LEFT JOIN
--    (    
--          SELECT
--                MAX(db.BranchKey) realmax
--          FROM
--                (
--                  SELECT
--						BranchID,
--                        [Source],
--                        MAX(ValidFromDate) maxdate
--                  FROM
--                        DimBranch
--                  WHERE
--						[Source] = 'Legacy'
--                  GROUP BY
--                        BranchID,
--                        [Source]
--                ) x
--                INNER JOIN DimBranch db			ON  x.BranchID = db.BranchID
--												AND x.maxdate = db.ValidFromDate
--          GROUP BY
--				db.BranchID
--			,	db.[Source]
--	) active ON [source].BranchKey = active.realmax



-- Test
-- EXEC usp_ETL_LegacyExpireDimBranchRecords
	







GO
