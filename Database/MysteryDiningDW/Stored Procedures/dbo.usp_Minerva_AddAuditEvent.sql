SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_Minerva_AddAuditEvent] 
	@AdminID		SMALLINT
,	@AuditTableID	TINYINT
,	@AuditActionID	TINYINT
,	@ID				NVARCHAR(10)
,	@Note			NVARCHAR(200) = NULL
,	@Hidden			BIT = 0
AS

BEGIN

INSERT INTO AuditEvent ([Timestamp], AdminID, AuditTableID, AuditActionID, ID, Hidden, Note)
VALUES (GETDATE(), @AdminID, @AuditTableID, @AuditActionID, @ID, @Hidden, @Note)

END

-- Test Case
-- EXEC usp_Minerva_AddAuditEvent 0, 1, 1, '1', NULL, 0
GO
