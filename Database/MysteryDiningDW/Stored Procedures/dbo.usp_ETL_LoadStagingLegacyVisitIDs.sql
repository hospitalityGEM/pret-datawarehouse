
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_ETL_LoadStagingLegacyVisitIDs] 
AS

TRUNCATE TABLE StagingLegacyVisitIDs

INSERT INTO StagingLegacyVisitIDs (VisitUID, ConfigInfoID)
SELECT DISTINCT 	
	VisitUID 
,	4
FROM 
	MysteryDiningCopy.dbo.VisitUpdate vu
	INNER JOIN MysteryDiningDW.dbo.ConfigInfo ci ON ci.ConfigInfoID = 4 AND vu.[TimeStamp] > ci.LastUpdated

UNION

SELECT DISTINCT 	
	VisitUID 
,	5
FROM 
	MysteryDiningCopy.dbo.VisitUpdate vu
	INNER JOIN MysteryDiningDW.dbo.ConfigInfo ci ON ci.ConfigInfoID = 5 AND vu.[TimeStamp] > ci.LastUpdated
 
UNION

SELECT DISTINCT 	
	VisitUID 
,	6
FROM 
	MysteryDiningCopy.dbo.VisitUpdate vu
	INNER JOIN MysteryDiningDW.dbo.ConfigInfo ci ON ci.ConfigInfoID = 6 AND vu.[TimeStamp] > ci.LastUpdated
 
UNION

SELECT DISTINCT 	
	VisitUID 
,	7
FROM 
	MysteryDiningCopy.dbo.VisitUpdate vu
	INNER JOIN MysteryDiningDW.dbo.ConfigInfo ci ON ci.ConfigInfoID = 7 AND vu.[TimeStamp] > ci.LastUpdated
		
UNION

SELECT DISTINCT 	
	VisitUID 
,	8
FROM 
	MysteryDiningCopy.dbo.VisitUpdate vu
	INNER JOIN MysteryDiningDW.dbo.ConfigInfo ci ON ci.ConfigInfoID = 8 AND vu.[TimeStamp] > ci.LastUpdated
		
UNION

SELECT DISTINCT 	
	VisitUID 
,	9
FROM 
	MysteryDiningCopy.dbo.VisitUpdate vu
	INNER JOIN MysteryDiningDW.dbo.ConfigInfo ci ON ci.ConfigInfoID = 9 AND vu.[TimeStamp] > ci.LastUpdated
		
UNION

SELECT DISTINCT 	
	VisitUID 
,	10
FROM 
	MysteryDiningCopy.dbo.VisitUpdate vu
	INNER JOIN MysteryDiningDW.dbo.ConfigInfo ci ON ci.ConfigInfoID = 10 AND vu.[TimeStamp] > ci.LastUpdated
		
UNION

SELECT DISTINCT 	
	VisitUID 
,	11
FROM 
	MysteryDiningCopy.dbo.VisitUpdate vu
	INNER JOIN MysteryDiningDW.dbo.ConfigInfo ci ON ci.ConfigInfoID = 11 AND vu.[TimeStamp] > ci.LastUpdated
		
UNION

SELECT DISTINCT 	
	VisitUID 
,	12
FROM 
	MysteryDiningCopy.dbo.VisitUpdate vu
	INNER JOIN MysteryDiningDW.dbo.ConfigInfo ci ON ci.ConfigInfoID = 12 AND vu.[TimeStamp] > ci.LastUpdated

UNION

SELECT DISTINCT 	
	VisitUID 
,	18
FROM 
	MysteryDiningCopy.dbo.AlertAction 
WHERE
	( 
	[DateRaised] > (
	SELECT 
		[LastUpdated] 
	FROM 
		MysteryDiningDW.dbo.ConfigInfo
	WHERE 
		ConfigInfoID = 18)
	OR
	[DateComplete] > (
	SELECT 
		[LastUpdated] 
	FROM 
		MysteryDiningDW.dbo.ConfigInfo
	WHERE 
		ConfigInfoID = 18)
	)
		
 -- Test Case
 -- EXEC usp_ETL_LoadStagingLegacyVisitIDs

GO
