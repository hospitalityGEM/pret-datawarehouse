SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_DimPeriodTruncateStagingTable] 
AS
	-- DimGeography
	TRUNCATE TABLE StagingDimPeriod

-- Test Case
-- EXEC usp_ETL_DimPeriodTruncateStagingTable
GO
