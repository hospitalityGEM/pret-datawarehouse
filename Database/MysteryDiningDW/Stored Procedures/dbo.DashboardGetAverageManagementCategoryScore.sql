
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[DashboardGetAverageManagementCategoryScore]
	-- Add the parameters for the stored procedure here
	@DateFrom datetime, 
	@DateTo datetime, 
	@ClientID int = 0, 
	@BranchIDs nvarchar(max),
	@VisitTypes nvarchar(max),
	@HideCurrentPeriod bit,
	@Benchmark nvarchar(max) = '',
	@QuestionCategory nvarchar(max) = '',
	@QuestionnaireCategory nvarchar(max) = '',
	@Department nvarchar(max) = '',
	@Brand nvarchar(max) = ''


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

	DECLARE
		@DateKeyFrom	INT = cast(CONVERT(VARCHAR(19),@DateFrom,112) as int)
	,	@DateKeyTo		INT = cast(CONVERT(VARCHAR(19),@DateTo,112) as int)
    ,   @BranchIDInts AS IdListInt
    ,   @VisitTypesStrings AS IdListString
    ,   @BrandStrings as IdListString
    ,   @QuestionnaireCategoryStrings as IdListString
	,	@QuestionCategoriesString as IdListString

INSERT INTO @BranchIDInts
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@BranchIDs,',')

INSERT INTO @VisitTypesStrings
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@VisitTypes,',')


INSERT INTO @BrandStrings
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@Brand,',')


INSERT INTO @QuestionnaireCategoryStrings
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@QuestionnaireCategory,',')

INSERT INTO @QuestionCategoriesString
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@QuestionCategory,',')
		

	SELECT
		
			  CONVERT(DECIMAL(5,2),(SUM(fcr.Score)/SUM(fcr.MaxScore))*100)  

	FROM
		FactCategoryResult fcr
		INNER JOIN DimBranch db ON fcr.BranchKey = db.BranchKey
		INNER JOIN DimVisit  dv	ON fcr.VisitKey	 = dv.VisitKey
		INNER JOIN DimPeriod dp ON fcr.PeriodKey = dp.PeriodKey
	WHERE
	
			db.BranchID = ANY (SELECT Id FROM @BranchIDInts)
		AND
			(case when LEN(@Brand)=0  then 1 when db.CurrentBrand = ANY (SELECT Id FROM @BrandStrings) then 1 else 0 end)=1
		AND
			(case when Len(@QuestionnaireCategory)=0  then 1 when dv.QuestionnaireCategory = ANY (SELECT Id FROM @QuestionnaireCategoryStrings) then 1 else 0 end)=1
		AND
			(case when Len(@QuestionCategory)=0  then 1 when fcr.Category = ANY (SELECT Id FROM @QuestionCategoriesString) then 1 else 0 end)=1
		AND
			dv.VisitStatus IN ('Reviewed','Complete')
		AND
			dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)
		AND
			dp.StartDateKey >= @DateKeyFrom
		AND
			dp.StartDateKey <= @DateKeyTo
		AND
			fcr.MaxScore > 0
		AND
			fcr.Active = 1
		AND
			dp.ClientPeriodsAgo>= @HideCurrentPeriod
		AND
			fcr.IncludeInAnalysis = 'Include'
		AND
			dp.IncludeInPortal='Include'
		AND
			(db.ClientID<>28647 OR dv.Type<>'Online Feedback')

		GROUP BY db.ClientID		
		
UNION

	
	SELECT
		
			  CONVERT(DECIMAL(5,2),(SUM(fcr.Score)/SUM(fcr.MaxScore))*100)  

	FROM
		FactCategoryResult fcr
		INNER JOIN DimBranch db ON fcr.BranchKey = db.BranchKey
		INNER JOIN DimVisit  dv	ON fcr.VisitKey	 = dv.VisitKey
		INNER JOIN FactFeedbackResult ffr ON ffr.FeedbackResultID=fcr.VisitID and ffr.active=1

	WHERE
	
			db.BranchID = ANY (SELECT Id FROM @BranchIDInts)
		AND
			(case when LEN(@Brand)=0  then 1 when db.CurrentBrand = ANY (SELECT Id FROM @BrandStrings) then 1 else 0 end)=1
		AND
			(case when Len(@QuestionnaireCategory)=0  then 1 when dv.QuestionnaireCategory = ANY (SELECT Id FROM @QuestionnaireCategoryStrings) then 1 else 0 end)=1
		AND
			(case when Len(@QuestionCategory)=0  then 1 when fcr.Category = ANY (SELECT Id FROM @QuestionCategoriesString) then 1 else 0 end)=1	
		AND
			dv.VisitStatus IN ('Reviewed','Complete')
		AND
			dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)
		AND
			ffr.CompletedDateKey  >= @DateKeyFrom
		AND
			ffr.CompletedDateKey  <= @DateKeyTo
		AND
			fcr.MaxScore > 0
		AND
			fcr.Active = 1
		AND
			fcr.IncludeInAnalysis = 'Include'
		AND
			db.ClientID=28647 
		AND
		    dv.Type='Online Feedback'
		
		
GROUP BY db.ClientID				
END

-- Test Case
-- EXEC [dbo].[DashboardGetAverageAssessmentScore] @DateFrom = '10-01-2012', @DateTo = '12-31-2012', @ClientID = 18867,  @BranchIDs = '5589,3776,3775,3778', @VisitTypes = 'Mystery Visit', @HideCurrentPeriod = 0







GO
