SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_FactDepartmentResultTruncateStagingTable] 
AS
	-- FactDepartmentResult
	TRUNCATE TABLE StagingFactDepartmentResult

-- Test Case
-- EXEC usp_ETL_FactDepartmentResultTruncateStagingTable
GO
