SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_LoadStagingLegacyVisitScoreText] 
AS

INSERT INTO StagingLegacyVisitScoreText (VisitUID)
SELECT DISTINCT 	
	v.VisitUID 
FROM 
	MysteryDiningCopy.dbo.Visit v
	LEFT JOIN StagingLegacyVisitScoreText svt ON v.VisitUID = svt.VisitUID
WHERE
	svt.VisitUID IS NULL

UPDATE StagingLegacyVisitScoreText
SET VisitScoreText = 	CONVERT(NVARCHAR(50),MysteryDiningCopy.dbo.fnVisitScoreText(ViDs.VisitUID))
FROM
	StagingLegacyVisitScoreText vst
	INNER JOIN
	(
		SELECT DISTINCT
			VisitUID
		FROM
			StagingLegacyVisitIDs
		WHERE
			ConfigInfoID IN (4,7)
	) ViDs ON vst.VisitUID = ViDs.VisitUID

UPDATE StagingLegacyVisitScoreText
SET VisitScoreText = 	CONVERT(NVARCHAR(50),MysteryDiningCopy.dbo.fnVisitScoreText(vst.VisitUID))
FROM
	StagingLegacyVisitScoreText vst
WHERE
	VisitScoreText IS NULL

 -- Test Case
 -- EXEC usp_ETL_LoadStagingLegacyVisitScoreText
GO
