SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_FactAdvocacyFacebookTruncateStagingTable] 
AS
	-- FactAdvocacyFacebook
	TRUNCATE TABLE StagingFactAdvocacyFacebook

-- Test Case
-- EXEC usp_ETL_FactAdvocacyFacebookTruncateStagingTable
GO
