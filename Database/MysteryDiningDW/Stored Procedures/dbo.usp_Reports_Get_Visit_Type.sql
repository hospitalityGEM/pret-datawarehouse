SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Reports_Get_Visit_Type]
	@Client			INT


AS

SELECT DISTINCT
	dv.Type 
FROM
	DimVisit dv
	inner join DimBranch db on db.BranchKey=dv.BranchKey
WHERE
	dv.Active = 1	
AND
	dv.Type <> 'Unknown' 
AND 
	db.ClientID=@Client




GO
