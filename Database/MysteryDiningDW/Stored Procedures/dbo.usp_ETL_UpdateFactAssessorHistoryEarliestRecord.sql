SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_UpdateFactAssessorHistoryEarliestRecord] 
AS

DECLARE @MaxLogID INT

SELECT @MaxLogID = MAX(AssessorHistoryID) FROM MysteryDiningDW.dbo.FactAssessorHistory

UPDATE 
	ConfigInfo
SET 
	Value = @MaxLogID
,	LastUpdated = GETDATE()
WHERE
	ConfigInfoID = 21
	
-- EXEC usp_ETL_UpdateFactAssessorHistoryEarliestRecord
GO
