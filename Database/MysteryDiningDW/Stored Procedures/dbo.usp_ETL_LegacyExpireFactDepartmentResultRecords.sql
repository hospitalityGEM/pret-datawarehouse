SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_LegacyExpireFactDepartmentResultRecords] 
AS

--Expired Records

UPDATE FactDepartmentResult
SET
	[Active] = 0
,	[ValidToDate] = DfR.ValidToDate

FROM
	FactDepartmentResult fR
INNER JOIN
	StagingFactDepartmentResult DfR	ON	fR.[Source]		= DfR.[Source]
									AND	fR.DepartmentID	= DfR.DepartmentID
									AND fR.VisitID		= DfR.VisitID	
WHERE
	fR.[Active] = 1
AND	fR.[ValidFromDate] <= DfR.ValidToDate
AND fR.[Source] = 'Legacy'


-- Update Current Score values on old records

--UPDATE FactSectionResult
--SET
--	[CurrentScore] = SfR.CurrentScore

--FROM
--	FactSectionResult fR
--INNER JOIN
--	StagingFactSectionResult SfR	ON	fR.[Source]		= SfR.[Source]
--									AND	fR.SectionID	= SfR.SectionID
--									AND fR.VisitID		= SfR.VisitID
--WHERE
--	fR.[Source] = 'Legacy'

-- Set Active flag Appropriately

--UPDATE
--    FactDepartmentResult
--SET
--    [Active] = 
--				CASE 
--					WHEN active.realmax IS NULL THEN 0
--					ELSE 1
--				END
--FROM
--	FactDepartmentResult [source]
--LEFT JOIN
--    (    
--          SELECT
--                MAX(fdr.DepartmentResultKey) realmax
--          FROM
--                (
--                  SELECT
--						VisitID,
--                        DepartmentID,
--                        [Source],
--                        MAX(ValidFromDate) maxdate
--                  FROM
--                        FactDepartmentResult
--                  WHERE
--						[Source] = 'Legacy'
--                  GROUP BY
--                        VisitID,
--                        DepartmentID,
--                        [Source]
--                ) x
--                INNER JOIN FactDepartmentResult fdr ON x.DepartmentID = fdr.DepartmentID
--												AND x.VisitID = fdr.VisitID
--												AND x.maxdate = fdr.ValidFromDate
--          GROUP BY
--                fdr.DepartmentID
--			,	fdr.VisitID
--			,	fdr.[Source]
--	) active ON [source].DepartmentResultKey = active.realmax

-- Test
-- EXEC usp_ETL_LegacyExpireFactDepartmentResultRecords
	
GO
