SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_UpdateLastDWStart] 
AS
					
UPDATE 
	ConfigInfo
SET 
	Value = GETDATE()
,	LastUpdated = GETDATE()
WHERE
	[ConfigInfoID] = 15
	
-- EXEC usp_ETL_UpdateLastDWStart
GO
