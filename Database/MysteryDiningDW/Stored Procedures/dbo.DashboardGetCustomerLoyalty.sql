SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[DashboardGetCustomerLoyalty]
	-- Add the parameters for the stored procedure here
	@DateFrom datetime, 
	@DateTo datetime, 
	@ClientID int = 0, 
	@BranchIDs nvarchar(max),
	@VisitTypes nvarchar(max),
	@HideCurrentPeriod bit,
	@Benchmark nvarchar(max) = '',
	@QuestionCategory nvarchar(max) = '',
	@QuestionnaireCategory nvarchar(max) = '',
	@Department nvarchar(max) = '',
	@Brand nvarchar(max) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE
		@DateKeyFrom	INT
	,	@DateKeyTo		INT
	,	@DateKeyCurrent	INT
		
	SELECT @DateKeyFrom	= DateKey FROM DimDate WHERE [Date] = @DateFrom
	SELECT @DateKeyTo	= DateKey FROM DimDate WHERE [Date] = @DateTo
	SELECT @DateKeyCurrent	= DateKey FROM DimDate WHERE [Date] = CONVERT(DATE, GETDATE())

	SELECT	
		ROUND((
			CONVERT(DECIMAL(5,2),((CONVERT(DECIMAL(5,2),CLS.PromoterCount))/(CONVERT(DECIMAL(5,2),CLS.VisitCount))))*100
			-
			CONVERT(DECIMAL(5,2),((CONVERT(DECIMAL(5,2),CLS.DemoterCount))/(CONVERT(DECIMAL(5,2),CLS.VisitCount))))*100
		),0) AS Score
	FROM
		(
		SELECT
			COUNT(*) AS VisitCount
		,	SUM(CASE 
					WHEN fbr.CustomerLoyalty = 'Promoter' THEN 1
					ELSE 0
				END)	AS PromoterCount
		,	SUM(CASE 
					WHEN fbr.CustomerLoyalty = 'Demoter' THEN 1
					ELSE 0
				END)	AS DemoterCount
		FROM
			FactBenchmarkResult fbr
			INNER JOIN DimBranch db ON fbr.BranchKey = db.BranchKey
			INNER JOIN DimVisit  dv	ON fbr.VisitKey	 = dv.VisitKey
			INNER JOIN DimPeriod dp ON fbr.PeriodKey = dp.PeriodKey
		WHERE
				fbr.Benchmark = 'Customer Loyalty Score'
			AND
				db.BranchID IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@BranchIDs, ','))
			AND
				(CASE
					WHEN LEN(@QuestionnaireCategory) = 0 THEN 1
					WHEN dv.QuestionnaireCategory IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@QuestionnaireCategory,',')) THEN 1
					ELSE 0
				END) = 1
			AND
				(CASE
					WHEN LEN(@Brand) = 0 THEN 1
					WHEN db.Brand IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@Brand,',')) THEN 1
					ELSE 0
				END) = 1
			AND
				dv.VisitStatus IN ('Reviewed','Complete')
			AND
				dv.[Type] IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@VisitTypes, ','))
			AND
				dp.StartDateKey >= @DateKeyFrom
			AND
				dp.StartDateKey <= @DateKeyTo
			AND
				fbr.MaxScore > 0
			AND
				(CASE
					WHEN @HideCurrentPeriod = 1 AND dp.EndDateKey < @DateKeyCurrent THEN 1
					WHEN @HideCurrentPeriod = 0 THEN 1
					ELSE 0
				END) = 1
			AND
				fbr.Active = 1
			AND
				fbr.IncludeInAnalysis = 'Include'
			AND
				dp.IncludeInPortal='Include'
		) CLS	
END

-- Test Case
-- EXEC [dbo].[DashboardGetCustomerLoyalty] @DateFrom = '10-01-2012', @DateTo = '12-31-2012', @ClientID = 18867,  @BranchIDs = '5589,3776,3775,3778', @VisitTypes = 'Mystery Visit', @HideCurrentPeriod = 0


GO
