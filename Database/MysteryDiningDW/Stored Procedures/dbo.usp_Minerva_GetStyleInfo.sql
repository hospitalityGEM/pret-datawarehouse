SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_Minerva_GetStyleInfo] 
	@BrandID		INT
AS
BEGIN
	
	DECLARE @GroupID	INT
	SELECT  @GroupID = dbo.udf_Minerva_GetStyleGroupForBrand(@BrandID)
	
	DECLARE @MultiRow	TABLE
	(
		[Property]		NVARCHAR(50)
	,	[Value]			NVARCHAR(50)	
	)
	
	INSERT INTO @MultiRow
	(Property, Value)	
	SELECT
		I.ItemValue + '-' + P.PropertyValue	AS Property
	,	ISNULL(sq.Value, PrI.DefaultValue)	AS Value
	FROM
		ConfigStylePropertyItem PrI
		INNER JOIN	ConfigStyleProperty P	ON P.ConfigStylePropertyID		= PrI.ConfigStylePropertyID
		INNER JOIN	ConfigStyleItem		I	ON I.ConfigStyleItemID			= PrI.ConfigStyleItemID
		LEFT JOIN
		(
		SELECT
			PrI.ConfigStylePropertyItemID
		,	I.ItemValue + '-' + P.PropertyValue		AS Property
		,	ISNULL(C.StyleValue, PrI.DefaultValue)	AS Value
		FROM
			ConfigStylePropertyItem PrI
			LEFT JOIN	ConfigStyleConfig	C	ON C.ConfigStylePropertyItemID	= PrI.ConfigStylePropertyItemID
			INNER JOIN	ConfigStyleProperty P	ON P.ConfigStylePropertyID		= PrI.ConfigStylePropertyID
			INNER JOIN	ConfigStyleItem		I	ON I.ConfigStyleItemID			= PrI.ConfigStyleItemID
		WHERE
			C.ConfigStyleGroupID = @GroupID
		) sq								ON PrI.ConfigStylePropertyItemID = sq.ConfigStylePropertyItemID
		
	SELECT
		(SELECT [Value] FROM @MultiRow WHERE [Property] = 'SmallTextbox-BackgroundColor')	AS SmallTextboxBackgroundColor
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'SmallTextbox-Color')				AS SmallTextboxColor	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'SmallTextbox-FontSize')			AS SmallTextboxFontSize	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'SmallTextbox-FontStyle')			AS SmallTextboxFontStyle	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'SmallTextbox-FontWeight')		AS SmallTextboxFontWeight	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'SmallTextbox-TextDecoration')	AS SmallTextboxTextDecoration	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'SmallTextbox-TextAlign')			AS SmallTextboxTextAlign	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'MediumTextbox-BackgroundColor')	AS MediumTextboxBackgroundColor	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'MediumTextbox-Color')			AS MediumTextboxColor	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'MediumTextbox-FontSize')			AS MediumTextboxFontSize	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'MediumTextbox-FontStyle')		AS MediumTextboxFontStyle	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'MediumTextbox-FontWeight')		AS MediumTextboxFontWeight
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'MediumTextbox-TextDecoration')	AS MediumTextboxTextDecoration	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'MediumTextbox-TextAlign')		AS MediumTextboxTextAlign
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'LargeTextbox-BackgroundColor')	AS LargeTextboxBackgroundColor	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'LargeTextbox-Color')				AS LargeTextboxColor
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'LargeTextbox-FontSize')			AS LargeTextboxFontSize	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'LargeTextbox-FontStyle')			AS LargeTextboxFontStyle	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'LargeTextbox-FontWeight')		AS LargeTextboxFontWeight	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'LargeTextbox-TextDecoration')	AS LargeTextboxTextDecoration	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'LargeTextbox-TextAlign')			AS LargeTextboxTextAlign	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'ThinLine-Color')					AS ThinLineColor	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'ThinLine-LineStyle')				AS ThinLineLineStyle	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'ThinLine-LineWidth')				AS ThinLineLineWidth	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'MediumLine-Color')				AS MediumLineColor	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'MediumLine-LineStyle')			AS MediumLineLineStyle	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'MediumLine-LineWidth')			AS MediumLineLineWidth	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'ThickLine-Color')				AS ThickLineColor	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'ThickLine-LineStyle')			AS ThickLineLineStyle	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'ThickLine-LineWidth')			AS ThickLineLineWidth	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'Logo-ImagePath')					AS LogoImagePath	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'Tablix-BackgroundColor')			AS TablixBackgroundColor	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'Tablix-Color')					AS TablixColor	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'Tablix-FontSize')				AS TablixFontSize	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'Tablix-FontStyle')				AS TablixFontStyle	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'Tablix-FontWeight')				AS TablixFontWeight	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'Tablix-TextDecoration')			AS TablixTextDecoration	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'Tablix-TextAlign')				AS TablixTextAlign	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'Tablix-BorderColor')				AS TablixBorderColor	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'Tablix-BorderStyle')				AS TablixBorderStyle	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'Tablix-BorderWidth')				AS TablixBorderWidth	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'TablixHeader-BackgroundColor')	AS TablixHeaderBackgroundColor	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'TablixHeader-Color')				AS TablixHeaderColor	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'TablixHeader-FontSize')			AS TablixHeaderFontSize	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'TablixHeader-FontStyle')			AS TablixHeaderFontStyle	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'TablixHeader-FontWeight')		AS TablixHeaderFontWeight	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'TablixHeader-TextDecoration')	AS TablixHeaderTextDecoration	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'TablixHeader-TextAlign')			AS TablixHeaderTextAlign	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'TablixHeader-BorderColor')		AS TablixHeaderBorderColor	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'TablixHeader-BorderStyle')		AS TablixHeaderBorderStyle	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'TablixHeader-BorderWidth')		AS TablixHeaderBorderWidth	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'TablixData-BackgroundColor')		AS TablixDataBackgroundColor	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'TablixData-Color')				AS TablixDataColor	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'TablixData-FontSize')			AS TablixDataFontSize	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'TablixData-FontStyle')			AS TablixDataFontStyle	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'TablixData-FontWeight')			AS TablixDataFontWeight	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'TablixData-TextDecoration')		AS TablixDataTextDecoration	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'TablixData-TextAlign')			AS TablixDataTextAlign	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'TablixData-BorderColor')			AS TablixDataBorderColor	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'TablixData-BorderStyle')			AS TablixDataBorderStyle	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'TablixData-BorderWidth')			AS TablixDataBorderWidth	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'Chart-Palette')					AS ChartPalette	
	,	(SELECT [Value] FROM @MultiRow WHERE [Property] = 'FontFamily-FontFamily')			AS FontFamilyFontFamily	
 
END

-- EXEC usp_Minerva_GetStyleInfo 123
GO
GRANT EXECUTE ON  [dbo].[usp_Minerva_GetStyleInfo] TO [TMDCReporting]
GO
