
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[DashboardGetChartDataScores]
-- Add the parameters for the stored procedure here
	@DateFrom datetime,
	@DateTo datetime,
	@ClientID int = 0, 
	@PeriodsAgo int = -1, -- -1 if using year to date 
	@BranchIDs nvarchar(max),
	@VisitTypes nvarchar(max),
	@HideCurrentPeriod bit,
	@Benchmark nvarchar(max) = '',
	@QuestionCategory nvarchar(max) = '',
	@QuestionnaireCategory nvarchar(max) = '',
	@Department nvarchar(max) = '',
	@Brand nvarchar(max) = '',
	@ShowCompanyScore bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE
		@DateKeyFrom	INT
	,	@DateKeyTo		INT
	,	@DateKeyCurrent	INT
	
		
	SELECT @DateKeyFrom	= DateKey FROM DimDate WHERE [Date] = CONVERT(DATE, @DateFrom)
	SELECT @DateKeyTo	= DateKey FROM DimDate WHERE [Date] = CONVERT(DATE, @DateTo)
	SELECT @DateKeyCurrent	= DateKey FROM DimDate WHERE [Date] = CONVERT(DATE, GETDATE()) 
	

	DECLARE @BranchIDInts AS IdListInt
	DECLARE @BenchmarkStrings AS IdListString
	DECLARE @QuestionCategoryStrings AS IdListString
	DECLARE @QuestionnaireCategoryStrings AS IdListString
	DECLARE @DepartmentStrings AS IdListString
	DECLARE @BrandStrings AS IdListString
	DECLARE @VisitTypesStrings AS IdListString
	

	INSERT INTO @BranchIDInts
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@BranchIDs,',')

	INSERT INTO @BenchmarkStrings
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@Benchmark,',')

	INSERT INTO @QuestionCategoryStrings
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@QuestionCategory,',')

	INSERT INTO @QuestionnaireCategoryStrings
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@QuestionnaireCategory,',')

	INSERT INTO @DepartmentStrings
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@Department,',')

	INSERT INTO @BrandStrings
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@Brand,',')

	INSERT INTO @VisitTypesStrings
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@VisitTypes,',')

	DECLARE @OFPeriodType nvarchar(50)

	SELECT @OFPeriodType= PeriodType
		FROM DimVisit dv 
		INNER JOIN DimPeriod dp ON dv.PeriodKey=dp.PeriodKey
		INNER JOIN DimBranch db  ON dv.BranchKey = db.BranchKey

		WHERE

			db.ClientID = @ClientID	
		AND
			dv.VisitStatus IN ('Reviewed','Complete')
		AND
			dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)
		AND
			dp.StartDateKey >= @DateKeyFrom
		AND
			dp.StartDateKey <= @DateKeyTo
		AND
			dp.IncludeInAnalysis='Include'
		AND
			dp.IncludeInPortal='Include'
		AND
			dv.type='Online Feedback'
		AND
			dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)

	DECLARE @Results TABLE
	(
		BranchID	INT
	,	Label		NVARCHAR(200)
	,	StartDateKey INT
	,	Score		DECIMAL(18,2)
	,	MaxScore	DECIMAL(18,2)	
	)

	INSERT INTO @Results
	SELECT
		db.BranchID			AS BranchID
	,	dp.PeriodName		AS [Label]
	,	dp.StartDateKey
	,	SUM(fqr.Score)		AS [Score]
	,	SUM(fqr.MaxScore)	AS [MaxScore]
	FROM
		FactQuestionResult fqr WITH (NOLOCK)
		INNER JOIN DimBranch db WITH (NOLOCK) ON fqr.BranchKey = db.BranchKey
		INNER JOIN DimBranch cdb WITH (NOLOCK) ON db.BranchID = cdb.BranchID AND cdb.Active = 1
		INNER JOIN DimVisit dv WITH (NOLOCK) ON fqr.VisitKey	 = dv.VisitKey
		INNER JOIN DimPeriod dp WITH (NOLOCK) ON fqr.PeriodKey = dp.PeriodKey
		INNER JOIN DimQuestion dq WITH (NOLOCK) ON fqr.QuestionKey = dq.QuestionKey
	WHERE
			db.ClientID = @ClientID
	
		AND
			dv.VisitStatus IN ('Reviewed','Complete')
		AND
			dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)
		AND
			(CASE
				WHEN @PeriodsAgo = -1 THEN
					CASE
						WHEN
								dp.StartDateKey >= @DateKeyFrom
							AND
								dp.StartDateKey <= @DateKeyTo
						THEN 1
						ELSE 0
					END
				WHEN @PeriodsAgo = 0 THEN
					CASE
						WHEN dp.ClientPeriodsAgo = @PeriodsAgo THEN 1
						ELSE 0
					END
				ELSE
					CASE
						WHEN 
							dp.ClientPeriodsAgo <= @PeriodsAgo 
						AND
							dp.ClientPeriodsAgo	>= 0
						THEN 1
						ELSE 0
					END
			END) = 1
		AND
			fqr.MaxScore > 0
		AND
			(CASE
				WHEN @HideCurrentPeriod = 1 AND dp.EndDateKey < @DateKeyCurrent THEN 1
				WHEN @HideCurrentPeriod = 0 THEN 1
				ELSE 0
			END) = 1
		AND
			fqr.Active = 1
		AND
			fqr.IncludeInAnalysis = 'Include'
		AND
			dp.IncludeInPortal='Include'
		AND
			fqr.IncludeScoreInTotal = 1
		AND
			dq.HiddenSection = 'Visible'
		AND
			dq.HiddenQuestion = 'Visible'
		AND
			cdb.BranchStatus = 'Live'
		AND
			(CASE
				WHEN LEN(@Benchmark) = 0 THEN 1
				WHEN dq.Benchmark = ANY (SELECT Id FROM @BenchmarkStrings) THEN 1
				ELSE 0
			END) = 1
		AND
			(CASE
				WHEN LEN(@QuestionCategory) = 0 THEN 1
				WHEN dq.ManagementCategory = ANY (SELECT Id FROM @QuestionCategoryStrings) THEN 1
				ELSE 0
			END) = 1
		AND
			(CASE
				WHEN LEN(@QuestionnaireCategory) = 0 THEN 1
				WHEN dq.QuestionnaireCategory = ANY (SELECT Id FROM @QuestionnaireCategoryStrings) THEN 1
				ELSE 0
			END) = 1
		AND
			(CASE
				WHEN LEN(@Department) = 0 THEN 1
				WHEN dq.Department = ANY (SELECT Id FROM @DepartmentStrings) THEN 1
				ELSE 0
			END) = 1
		AND
			(CASE
				WHEN LEN(@Brand) = 0 THEN 1
				WHEN db.Brand = ANY (SELECT Id FROM @BrandStrings) THEN 1
				ELSE 0
			END) = 1
	AND
			(db.ClientID<>28647 OR dv.Type<>'Online Feedback')
	GROUP BY
		db.BranchID
	,	dp.PeriodName
	,	dp.StartDateKey

	UNION

	SELECT
		db.BranchID			AS BranchID
	,	Period.PeriodName		AS [Label]
	,	Period.StartDateKey
	,	SUM(fqr.Score)		AS [Score]
	,	SUM(fqr.MaxScore)	AS [MaxScore]
	FROM
		FactQuestionResult fqr WITH (NOLOCK)
		INNER JOIN DimBranch db WITH (NOLOCK) ON fqr.BranchKey = db.BranchKey
		INNER JOIN DimBranch cdb WITH (NOLOCK) ON db.BranchID = cdb.BranchID AND cdb.Active = 1
		INNER JOIN DimVisit dv WITH (NOLOCK) ON fqr.VisitKey	 = dv.VisitKey
		INNER JOIN FactFeedbackResult ffr ON ffr.FeedbackResultID=fqr.VisitID and ffr.active=1
		INNER JOIN DimQuestion dq WITH (NOLOCK) ON fqr.QuestionKey = dq.QuestionKey
		INNER JOIN (select distinct PeriodName, StartDateKey,EndDateKey 
					FROM DimPeriod dp 
					WHERE
						 dp.active=1
					AND	
						dp.ClientID= 28647
					AND
						dp.PeriodType=@OFPeriodType
					AND
						dp.IncludeInPortal='Include'
					AND
						dp.IncludeInAnalysis='Include'
					AND
						(CASE
							WHEN @PeriodsAgo = -1 THEN
								CASE
									WHEN
											dp.StartDateKey >= @DateKeyFrom
										AND
											dp.StartDateKey <= @DateKeyTo
									THEN 1
									ELSE 0
								END
							WHEN @PeriodsAgo = 0 THEN
								CASE
									WHEN dp.ClientPeriodsAgo = @PeriodsAgo THEN 1
									ELSE 0
								END
							ELSE
								CASE
									WHEN 
										dp.ClientPeriodsAgo <= @PeriodsAgo 
									AND
										dp.ClientPeriodsAgo	>= 0
									THEN 1
									ELSE 0
								END
						END) = 1
					AND (
						CASE
						WHEN @HideCurrentPeriod = 1 AND dp.EndDateKey < @DateKeyCurrent THEN 1
						WHEN @HideCurrentPeriod = 0 THEN 1
						ELSE 0
						END) = 1
					)Period ON ffr.CompletedDateKey between Period.StartDateKey and Period.EndDateKey 
	WHERE
			db.ClientID = @ClientID
		AND
			db.ClientID = 28647
		
		AND
			dv.VisitStatus IN ('Reviewed','Complete')
		AND
			dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)
	
		AND
			dv.Type='Online Feedback'
		
		AND
			fqr.MaxScore > 0
		
		AND
			fqr.Active = 1
		AND
			fqr.IncludeInAnalysis = 'Include'
		AND
			fqr.IncludeScoreInTotal = 1
		AND
			dq.HiddenSection = 'Visible'
		AND
			dq.HiddenQuestion = 'Visible'
		AND
			cdb.BranchStatus = 'Live'
		AND
			(CASE
				WHEN LEN(@Benchmark) = 0 THEN 1
				WHEN dq.Benchmark = ANY (SELECT Id FROM @BenchmarkStrings) THEN 1
				ELSE 0
			END) = 1
		AND
			(CASE
				WHEN LEN(@QuestionCategory) = 0 THEN 1
				WHEN dq.ManagementCategory = ANY (SELECT Id FROM @QuestionCategoryStrings) THEN 1
				ELSE 0
			END) = 1
		AND
			(CASE
				WHEN LEN(@QuestionnaireCategory) = 0 THEN 1
				WHEN dq.QuestionnaireCategory = ANY (SELECT Id FROM @QuestionnaireCategoryStrings) THEN 1
				ELSE 0
			END) = 1
		AND
			(CASE
				WHEN LEN(@Department) = 0 THEN 1
				WHEN dq.Department = ANY (SELECT Id FROM @DepartmentStrings) THEN 1
				ELSE 0
			END) = 1
		AND
			(CASE
				WHEN LEN(@Brand) = 0 THEN 1
				WHEN db.Brand = ANY (SELECT Id FROM @BrandStrings) THEN 1
				ELSE 0
			END) = 1
	GROUP BY
		db.BranchID
	,	Period.PeriodName
	,	Period.StartDateKey


	SELECT
		r.Label
	,	ROUND((SUM(Score) / SUM(MaxScore))*100,0) AS Value
	,	CompanyValue
	FROM
		@Results r
		INNER JOIN 
		(
			SELECT
				Label
			,	CASE
					WHEN @ShowCompanyScore = 0 THEN 1
					ELSE ROUND((SUM(Score) / SUM(MaxScore))*100,0)			
				END	AS [CompanyValue]
			FROM
				@Results				
			GROUP BY
				Label
		) cv ON r.Label = cv.Label
	WHERE
		BranchID = ANY (SELECT Id FROM @BranchIdInts)
	GROUP BY
		r.Label
	,	r.StartDateKey
	,	cv.CompanyValue
	ORDER BY
		StartDateKey

OPTION(OPTIMIZE FOR UNKNOWN)
END


GO
