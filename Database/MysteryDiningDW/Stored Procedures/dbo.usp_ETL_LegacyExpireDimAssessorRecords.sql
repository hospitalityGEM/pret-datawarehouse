SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_ETL_LegacyExpireDimAssessorRecords] 
AS

-- Expired Records --
UPDATE DimAssessor
SET
	[Active] = 0
,	[ValidToDate] = SdA.ValidToDate

FROM
	DimAssessor dA
INNER JOIN
	StagingDimAssessor SdA	ON	dA.AssessorKey	= SdA.AssessorKey
	
WHERE
	dA.[Source] = 'Legacy'
AND
	SdA.[Type] = 'Expired' 

-- SCD1 Changes --
UPDATE DimAssessor
SET
	[LastLoginDateKey]	= SdA.LastLoginDateKey
,	[TotalWarnings]		= SdA.TotalWarnings -- Set as SCD2 for the time being
,	[RecentWarnings]	= SdA.RecentWarnings
,	[LatestWarningDateKey] = SdA.LatestWarningDateKey
,	[GeographyKey] = SdA.GeographyKey
,	[ApplicationDateKey]=SdA.ApplicationDateKey
,	[Age]=SdA.Age

FROM
	DimAssessor dA
INNER JOIN
	StagingDimAssessor SdA	ON	dA.AssessorKey	= SdA.AssessorKey
	
WHERE
	dA.[Source] = 'Legacy'
--AND
--	SdA.[Type] = 'SCD1' 

-- Set current records (in case any errors occured during SCD control)

--UPDATE
--    DimAssessor
--SET
--    [Active] = 1
--WHERE
--    AssessorKey IN
--    (    
--          SELECT
--                MAX(ahf.AssessorKey) realmax
--          FROM
--                (
--                  SELECT
--                        AssessorID,
--                        Max(ValidFromDate) maxdate
--                  FROM
--                        DimAssessor
--                  GROUP BY
--                        AssessorID
--                ) x
--                INNER JOIN DimAssessor ahf
--                ON x.AssessorID = ahf.AssessorID
--                AND x.maxdate = ahf.ValidFromDate
--          GROUP BY
--                ahf.AssessorID
--	)

-- Test
-- EXEC usp_ETL_LegacyExpireDimAssessorRecords
	

GO
