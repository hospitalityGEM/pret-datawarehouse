SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[usp_ETL_LegacyExpireDimQuestionRecords] 
AS

-- Update Expired Records
UPDATE DimQuestion
SET
	[Active] = 0
,	[ValidToDate] = SdQ.ValidToDate

FROM
	DimQuestion dQ
INNER JOIN
	StagingDimQuestion SdQ	ON	dQ.[QuestionKey]	= SdQ.[QuestionKey]
	
WHERE
	dQ.[Source] = 'Legacy'
AND
	SdQ.[Type] = 'Expired'
	
-- Update SCD1 changes by question
UPDATE DimQuestion
SET
	AnalysisTag = SdQ.AnalysisTag

FROM
	DimQuestion dQ
INNER JOIN
	StagingDimQuestion SdQ	ON	dQ.[QuestionID]	= SdQ.[QuestionID]
	
WHERE
	dQ.[Source] = 'Legacy'	
--AND
--	SdQ.[Type] = 'SCD1'	


-- Update SCD1 changes by Questionnaire
UPDATE DimQuestion
SET
	[CurrentQuestionnaire]=sdQ.CurrentQuestionnaire
,	[QuestionnaireCategory] = SdQ.QuestionnaireCategory	
,	[IncludeInAnalysis] = SdQ.IncludeInAnalysis
FROM
	DimQuestion dQ
INNER JOIN
	StagingDimQuestion SdQ	ON	dQ.[QuestionnaireID]= SdQ.[QuestionnaireID]
	
WHERE
	dQ.[Source] = 'Legacy'	
	
-- Set Active Flags --

--UPDATE
--    DimQuestion
--SET
--    [Active] = 
--				CASE 
--					WHEN active.realmax IS NULL THEN 0
--					ELSE 1
--				END
--FROM
--	DimQuestion [source]
--LEFT JOIN
--    (    
--          SELECT
--                MAX(dq.QuestionKey) realmax
--          FROM
--                (
--                  SELECT
--						QuestionID,
--                        [Source],
--                        MAX(ValidFromDate) maxdate
--                  FROM
--                        DimQuestion
--                  WHERE
--						[Source] = 'Legacy'
--                  GROUP BY
--                        QuestionID,
--                        [Source]
--                ) x
--                INNER JOIN DimQuestion dq ON x.QuestionID = dq.QuestionID
--										  AND x.maxdate   = dq.ValidFromDate
--										  AND x.[Source]  = dq.[Source]
--          GROUP BY
--                dq.QuestionID
--			,	dq.[Source]
--	) active ON [source].QuestionKey = active.realmax

-- Test
-- EXEC usp_ETL_LegacyExpireDimQuestionRecords
	


GO
