SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_LegacyExpireFactVisitResultRecords] 
AS

--Expired Records

UPDATE FactVisitResult
SET
	[Active] = 0
,	[ValidToDate] = SfR.ValidToDate

FROM
	FactVisitResult fR
INNER JOIN
	StagingFactVisitResult SfR		ON	fR.[Source]		= SfR.[Source]
									AND fR.VisitID		= SfR.VisitID
	
WHERE
	fR.[Active] = 1
AND	fR.[ValidFromDate] <= SfR.ValidToDate
AND fR.[Source] = 'Legacy'


-- Update Current Score values on old records

--UPDATE FactVisitResult
--SET
--	[CurrentScore] = SfR.CurrentScore

--FROM
--	FactVisitResult fR
--INNER JOIN
--	StagingFactVisitResult SfR	ON	fR.[Source]		= SfR.[Source]
--									AND fR.VisitID		= SfR.VisitID
--WHERE
--	fR.[Source] = 'Legacy'


--TRUNCATE TABLE StagingFactVisitResult

-- Set Active flag Appropriately

--UPDATE
--    FactVisitResult
--SET
--    [Active] = 
--				CASE 
--					WHEN active.realmax IS NULL THEN 0
--					ELSE 1
--				END
--FROM
--	FactVisitResult source
--LEFT JOIN
--    (    
--          SELECT
--                MAX(fvr.VisitResultKey) realmax
--          FROM
--                (
--                  SELECT
--						VisitID,
--                        MAX(ValidFromDate) maxdate
--                  FROM
--                        FactVisitResult
--                  WHERE
--						[Source] = 'Legacy'
--                  GROUP BY
--                        VisitID
--                ) x
--                INNER JOIN FactVisitResult fvr ON	x.VisitID = fvr.VisitID
--												AND x.maxdate = fvr.ValidFromDate
--          GROUP BY
--                fvr.VisitResultKey
--	) active ON source.VisitResultKey = active.realmax

-- Test
-- EXEC usp_ETL_LegacyExpireFactVisitResultRecords
	
GO
