SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_DimDishTruncateStagingTable] 
AS
	-- DimDish
	TRUNCATE TABLE StagingDimDish

-- Test Case
-- EXEC usp_ETL_DimDishTruncateStagingTable
GO
