SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
Stored procedure that polls a specific folder for a given file
once a minute a given amount of time. If/when the file is found
the filename is prefixed with the current date/time and is left 
in the same folder. If the file isn't found after the predefined 
number of minutes the procedure throws an error. Executed like this:

EXEC database.dbo.WaitForFile 
@FilePath = 'c:	emp', 
@FileName = 'file.txt',
@WaitMinutes = 60 --> Poll every minute for 60 minutes

This procedure depends on access to the system stored procedures
xp_fileExist and xp_cmdshell and proper permissions on the disks 
where the files reside.

Procedure written by:
Henning Frettem (aka Lumbago, sqlteam.com) 2007-10-03
*/

CREATE PROCEDURE [dbo].[WaitForFile] (
@FilePath VARCHAR(200),
@FileName VARCHAR(200),
@WaitMinutes INT,
@WaitForDelay NVARCHAR(8) = '00:00:30'
)
AS

EXECUTE SP_CONFIGURE 'show advanced options', 1
RECONFIGURE WITH OVERRIDE
 
EXECUTE SP_CONFIGURE 'xp_cmdshell', '1'
RECONFIGURE WITH OVERRIDE

DECLARE 
@FilePathFull VARCHAR(400),
@FileExists INT,
@Counter INT,
@ErrMsg VARCHAR(400),
@cmd VARCHAR(600)

SET @FilePathFull = @FilePath + @FileName
SET @FileExists = 0
SET @Counter = 0

EXEC master.dbo.xp_fileExist @FilePathFull, @FileExists OUTPUT

--Add Check Log to DB
EXEC dbo.usp_TEC_LogFileCheck @FileName, @FilePath, @FileExists

WHILE @FileExists = 0 AND @Counter < @WaitMinutes
BEGIN 
SET @Counter = @Counter + 1
WAITFOR delay @WaitForDelay --'00:01:00'

EXEC master.dbo.xp_fileExist @FilePathFull, @FileExists OUTPUT

--Add Check Log to DB
EXEC dbo.usp_TEC_LogFileCheck @FileName, @FilePath, @FileExists

END 

IF @FileExists = 0
BEGIN 
SET @ErrMsg = 'FileWait for ''' + @FilePathFull + 
''' timed out after waiting for ' + CAST(@Counter AS VARCHAR(20)) + 
' minutes'
RAISERROR (@ErrMsg, 16, 1)
RETURN
END
ELSE IF @FileExists = 1
BEGIN
SET @cmd = 'rename ' + @FilePathFull + ' ' + 
CONVERT(VARCHAR(20), GETDATE(), 112) + '_' + 
REPLACE(CONVERT(VARCHAR(20), GETDATE(), 108), ':', '') + '_' + 
@FileName
EXEC master.dbo.xp_cmdshell @cmd, no_output
END

EXECUTE SP_CONFIGURE 'xp_cmdshell', '0'
RECONFIGURE WITH OVERRIDE
 
EXECUTE SP_CONFIGURE 'show advanced options', 0
RECONFIGURE WITH OVERRIDE

--Test Case
--EXEC TestEnvironmentConfig.dbo.WaitForFile 
--@FilePath = 'E:\Microsoft SQL Server\Backup\', 
--@FileName = 'MysteryDining.bak',
--@WaitMinutes = 90, --> Poll for X minutes
--@WaitForDelay = '00:10:00' --> Delay time between checks
GO
