SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_FactSocialChannelCountLatestDateLoaded] 
AS

UPDATE 
	ConfigInfo
SET 
	Value = ld.LatestDate
,	LastUpdated = ld.LatestDate

FROM
	ConfigInfo ci

INNER JOIN
	(
		SELECT
		MAX(DateTimeCaptured)	AS LatestDate
	,	29						AS ConfigInfoID
	FROM
		MysteryDiningCopy.dbo.SocialChannelAssignedUserCount
	) ld ON ci.ConfigInfoID = ld.ConfigInfoID
WHERE
	ld.LatestDate IS NOT NULL
	
-- EXEC usp_ETL_FactSocialChannelCountLatestDateLoaded
GO
