SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_FactAdvocacyTwitterTruncateStagingTable] 
AS
	-- FactAdvocacyTwitter
	TRUNCATE TABLE StagingFactAdvocacyTwitter

-- Test Case
-- EXEC usp_ETL_FactAdvocacyTwitterTruncateStagingTable
GO
