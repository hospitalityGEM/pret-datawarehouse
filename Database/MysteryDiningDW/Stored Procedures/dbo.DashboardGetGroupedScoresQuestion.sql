SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[DashboardGetGroupedScoresQuestion]
	@DateFrom datetime,
	@DateTo datetime,
	@ClientID int = 0, 
	@BranchIDs nvarchar(max),
	@Count int,
	@IsTop bit,
	@VisitTypes nvarchar(max),
	@HideCurrentPeriod bit,
	@Benchmark nvarchar(max) = '',
	@QuestionCategory nvarchar(max) = '',
	@QuestionnaireCategory nvarchar(max) = '',
	@Department nvarchar(max) = '',
	@Brand nvarchar(max) = '',
	@IsNB bit=0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	DECLARE
		@DateKeyFrom	INT= cast(CONVERT(VARCHAR(19),@DateFrom,112) as int)
	,	@DateKeyTo		INT = cast(CONVERT(VARCHAR(19),@DateTo,112) as int)
	,	@MaxScore		DECIMAL(18,2)
	,	@TopCount		INT
	,   @BranchIDInts AS IdListInt
    ,   @VisitTypesStrings AS IdListString
    ,   @BrandStrings as IdListString
    ,   @QuestionnaireCategoryStrings as IdListString
	,   @QuestionCategoryStrings as IdListString
	,   @DepartmentStrings as IdListString
    ,   @BenchmarkStrings as IdListString


INSERT INTO @BranchIDInts
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@BranchIDs,',')

INSERT INTO @VisitTypesStrings
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@VisitTypes,',')


INSERT INTO @BrandStrings
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@Brand,',')

INSERT INTO @QuestionCategoryStrings
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@QuestionCategory,',')

INSERT INTO @DepartmentStrings
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@Department,',')

INSERT INTO @BenchmarkStrings
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@Benchmark,',')

INSERT INTO @QuestionnaireCategoryStrings
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@QuestionnaireCategory,',')
	

	
SELECT Question as [Group], QuestionScore AS [Score] 
FROM

(
	
	SELECT
		dq.Question 
	,	ROUND((SUM(fqr.Score) / SUM(fqr.MaxScore))*100,0) AS QuestionScore
	,	RANK () OVER (order by case when @IsTop=1 then ROUND((-1*SUM(fqr.Score) / SUM(fqr.MaxScore))*100,0) else ROUND((SUM(fqr.Score) / SUM(fqr.MaxScore))*100,0) end ) as QRank


	FROM
		FactQuestionResult fqr
		INNER JOIN DimBranch db ON fqr.BranchKey = db.BranchKey
		INNER JOIN DimVisit dv	ON fqr.VisitKey	 = dv.VisitKey
		INNER JOIN DimPeriod dp ON fqr.PeriodKey = dp.PeriodKey
		INNER JOIN DimQuestion dq ON fqr.QuestionKey = dq.QuestionKey
	WHERE
			db.BranchID  = ANY (SELECT Id FROM @BranchIDInts)
		AND
			dv.VisitStatus IN ('Reviewed','Complete')
		AND
			dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)
		AND
			dp.StartDateKey >= @DateKeyFrom
		AND
			dp.StartDateKey <= @DateKeyTo
		AND
			dp.ClientPeriodsAgo>= @HideCurrentPeriod
		AND
			fqr.MaxScore > 0
		AND
			dq.HiddenSection = 'Visible'
		AND
			dq.MenuQuestion = 'N/A'
		AND
			dq.HiddenQuestion = 'Visible'
		AND
			fqr.Active = 1
		AND
			fqr.IncludeInAnalysis = 'Include'
		AND
			dp.IncludeInPortal = 'Include'
		AND
			(dq.Benchmark =  ANY (SELECT Id FROM @BenchmarkStrings)  OR Len(@Benchmark)=0)
		AND
			(dq.ManagementCategory =  ANY (SELECT Id FROM @QuestionCategoryStrings) OR Len(@QuestionCategory)=0)
		AND
			(dq.QuestionnaireCategory = ANY (SELECT Id FROM @QuestionnaireCategoryStrings) OR Len(@QuestionnaireCategory)=0)
		AND
			(dq.Department = ANY (SELECT Id FROM @DepartmentStrings) OR Len(@Department)=0)
		AND
			(db.Brand = ANY (SELECT Id FROM @BrandStrings) OR Len(@Brand)=0)
		AND
			(dv.IsNBVisit=1 OR @IsNB=0)
	GROUP BY
		dq.Question
		

)A where QRank<=@Count 

order by QuestionScore desc

OPTION(OPTIMIZE FOR UNKNOWN)
 

 END

-- Test Case
-- EXEC [dbo].[DashboardGetGroupedScoresQuestion] 	@DateFrom = '2012-11-25',	@DateTo = '2012-12-30',	@ClientID = 209, 	@BranchIDs = '3316,259,735,308,4931,1337,963,975,738,161,312,5135,50,3319,739,136,129,311,1286,1087,5192,121,435,138,623,734,4932,619,126,5538,130,123,5191,1049,125,1017,1237,1050,4359,122,133,51,137,737,5456,624,120,5673,1238,1284,134,427,143,5088,446,736,4734,559,139,3750,128,889,2415,251,258,205,418,1086,1089,3749,1048,135,609,5676,1285,127,131,124,239,453,1029,4966,1090,118,405,740,5087',	@Count = 5,	@IsTop = 0, @VisitTypes = 'Mystery Visit', @HideCurrentPeriod = 0,@IsNB=0









GO
