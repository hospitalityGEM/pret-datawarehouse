SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_Minerva_UpdateDimDate] 
AS
BEGIN

	-- ==================================================================================
	-- Worked
	-- ==================================================================================
	-- Worked (Worked/Not Worked)
	UPDATE
		DimDate
	SET
		Worked = 'Worked'
	FROM
		DimDate dw
		INNER JOIN
		(
			SELECT 
				cid.[Key]
			FROM 
				ConfigInfo ci
				INNER JOIN ConfigInfoDetail cid ON ci.ConfigInfoID = cid.[ConfigInfoID] AND ci.[Key] = 'WeekDayWorking' AND cid.[Value] = 'Working'
		) data
		ON dw.[DayName] = data.[Key]	AND
		dw.[Date] < GETDATE()			AND
		dw.[Date] > (SELECT CONVERT(DATETIME, Value) FROM ConfigInfo WHERE [Key] = 'LastSuccessfulRun')

	-- =============================================================
	-- Remember to add AND > LAST ETL RUN !!!!!!!!!!!!!!!!!!!!!!!
	-- =============================================================

	-- ==================================================================================
	-- Work Value
	-- ==================================================================================
	-- WorkValue (Custom value for each day, e.g. Monday = 1, Saturday = 0.5, etc)
	UPDATE
		DimDate
	SET
		WorkValue = data.[Value]
	FROM
		DimDate dw
		INNER JOIN
		(
			SELECT
				cid.[Value],
				cid.[Key]
			FROM
				ConfigInfo ci
				INNER JOIN ConfigInfoDetail cid ON ci.ConfigInfoID = cid.ConfigInfoID AND ci.[Key] = 'WeekDayValue'
		) data
		ON dw.[DayName] = data.[Key]	AND
		dw.[Date] > GETDATE()			

	-- ==================================================================================
	-- Working Day
	-- ==================================================================================
	UPDATE
		DimDate
	SET
		WorkingDay = data.[Value]
	FROM
		DimDate dw
		INNER JOIN
		(
			SELECT
				cid.[Value],
				cid.[Key]
			FROM
				ConfigInfo ci
				INNER JOIN ConfigInfoDetail cid ON ci.ConfigInfoID = cid.ConfigInfoID AND ci.[Key] = 'WeekDayWorking'	
		) data
		ON dw.[DayName] = data.[Key]	AND
		dw.[Date] > (SELECT CONVERT(DATETIME, Value) FROM ConfigInfo WHERE [Key] = 'LastSuccessfulRun')

	-- =============================================================
	-- Remember to add AND > LAST ETL RUN !!!!!!!!!!!!!!!!!!!!!!!
	-- =============================================================

	-- ==================================================================================
	-- Today
	-- (Yesterday/Today/Tomorrow/NULL)
	-- ==================================================================================
	UPDATE
		DimDate
	SET
		Today = 
		(
			CASE 
				WHEN DATEDIFF(dd, dw.[Date], GETDATE()) = -1	THEN 'Tomorrow'
				WHEN DATEDIFF(dd, dw.[Date], GETDATE()) =  0	THEN 'Today'
				WHEN DATEDIFF(dd, dw.[Date], GETDATE()) =  1	THEN 'Yesterday'
				ELSE NULL
			END
		)
	FROM
		DimDate dw

	-- ==================================================================================
	-- DaysAgo (0 - Today, 1 - Yesterday etc, -1 - Tomorrow etc)
	-- ==================================================================================
	UPDATE
		DimDate
	SET
		DaysAgo = DATEDIFF(dd, dw.Date, GETDATE())
	FROM
		DimDate dw
	
	-- ==================================================================================
	-- DaysAhead (0 - Today, -1 - Yesterday etc, 1 - Tomorrow etc)
	-- ==================================================================================
	UPDATE
		DimDate
	SET
		DaysAhead = DATEDIFF(dd, dw.Date, GETDATE())*-1
	FROM
		DimDate dw

	-- ==================================================================================
	-- ThisWeek (Last Week/This Week/Next Week/NULL)
	-- ==================================================================================
	UPDATE
		DimDate
	SET
		ThisWeek = 
		(
			CASE 
				WHEN DATEDIFF(wk, dw.Date, GETDATE()) = -1	THEN 'Next Week'
				WHEN DATEDIFF(wk, dw.Date, GETDATE()) =  0	THEN 'This Week'
				WHEN DATEDIFF(wk, dw.Date, GETDATE()) =  1	THEN 'Last Week'
				ELSE NULL
			END
		)
	FROM
		DimDate dw

	-- ==================================================================================
	-- WeeksAgo (0 - This Week, 1 - Last Week etc, -1 - Next Week etc)
	-- ==================================================================================
	UPDATE
		DimDate
	SET
		WeeksAgo = DATEDIFF(wk, dw.Date, GETDATE())
	FROM
		DimDate dw

	-- ==================================================================================
	-- WeeksAhead (0 - This Week, -1 - Last Week etc, 1 - Next Week etc)
	-- ==================================================================================
	UPDATE
		DimDate
	SET
		WeeksAhead = DATEDIFF(wk, dw.Date, GETDATE())*-1
	FROM
		DimDate dw

	-- ==================================================================================
	-- ThisMonth (Last Month/This Month/Next Month/NULL)
	-- ==================================================================================
	UPDATE
		DimDate
	SET
		ThisMonth = 
		(
			CASE 
				WHEN DATEDIFF(mm, dw.Date, GETDATE()) = -1	THEN 'Next Month'
				WHEN DATEDIFF(mm, dw.Date, GETDATE()) =  0	THEN 'This Month'
				WHEN DATEDIFF(mm, dw.Date, GETDATE()) =  1	THEN 'Last Month'
				ELSE NULL
			END
		)
	FROM
		DimDate dw

	-- ==================================================================================
	-- MonthsAgo (0 - This Month, 1 - Last Month etc, -1 - Next Month etc)
	-- ==================================================================================
	UPDATE
		DimDate
	SET
		MonthsAgo = DATEDIFF(mm, dw.Date, GETDATE())
	FROM
		DimDate dw

	-- ==================================================================================
	-- MonthsAhead (0 - This Month, -1 - Last Month etc, 1 - Next Month etc)
	-- ==================================================================================
	UPDATE
		DimDate
	SET
		MonthsAhead = DATEDIFF(mm, dw.Date, GETDATE())*-1
	FROM
		DimDate dw

	-- ==================================================================================
	-- ThisQuarter (Last Quarter/This Quarter/Next Quarter/NULL)
	-- ==================================================================================
	UPDATE
		DimDate
	SET
		ThisQuarter = 
		(
			CASE 
				WHEN DATEDIFF(qq, dw.Date, GETDATE()) = -1	THEN 'Next Quarter'
				WHEN DATEDIFF(qq, dw.Date, GETDATE()) =  0	THEN 'This Quarter'
				WHEN DATEDIFF(qq, dw.Date, GETDATE()) =  1	THEN 'Last Quarter'
				ELSE NULL
			END
		)
	FROM
		DimDate dw

	-- ==================================================================================
	-- QuartersAgo (0 - This Quarter, 1 - Last Quarter etc. -1 - Next Quarter etc)
	-- ==================================================================================
	UPDATE
		DimDate
	SET
		MonthsAhead = DATEDIFF(qq, dw.Date, GETDATE())
	FROM
		DimDate dw

	-- ==================================================================================
	-- QuartersAhead (0 - This Quarter, -1 - Last Quarter etc. 1 - Next Quarter etc)
	-- ==================================================================================
	UPDATE
		DimDate
	SET
		MonthsAhead = DATEDIFF(qq, dw.Date, GETDATE())*-1
	FROM
		DimDate dw

	-- ==================================================================================
	-- ThisYear (Last Year/This Year/Next Year)
	-- ==================================================================================
	UPDATE
		DimDate
	SET
		ThisYear = 
		(
			CASE 
				WHEN DATEDIFF(yy, dw.Date, GETDATE()) = -1	THEN 'Next Year'
				WHEN DATEDIFF(yy, dw.Date, GETDATE()) =  0	THEN 'This Year'
				WHEN DATEDIFF(yy, dw.Date, GETDATE()) =  1	THEN 'Last Year'
				ELSE NULL
			END
		)
	FROM
		DimDate dw

	-- ==================================================================================
	-- YearsAgo (0 - This Year, 1 - Last Year etc, -1 - Next Year etc)
	-- ==================================================================================
	UPDATE
		DimDate
	SET
		YearsAgo = DATEDIFF(yy, dw.Date, GETDATE())
	FROM
		DimDate dw

	-- ==================================================================================
	-- YearsAhead (0 - This Year, -1 - Last Year etc, 1 - Next Year etc)
	-- ==================================================================================
	UPDATE
		DimDate
	SET
		YearsAhead = DATEDIFF(yy, dw.Date, GETDATE())*-1
	FROM
		DimDate dw

	-- ==================================================================================
	-- ThisFiscalQuarter (Last Quarter/This Quarter/Next Quarter/NULL)
	-- ==================================================================================
	UPDATE
		DimDate
	SET
		ThisFiscalQuarter = 
		(
			CASE 
				WHEN DATEDIFF(qq, dw.Date, GETDATE()) = -1	THEN 'Next Fiscal Quarter'
				WHEN DATEDIFF(qq, dw.Date, GETDATE()) =  0	THEN 'This Fiscal Quarter'
				WHEN DATEDIFF(qq, dw.Date, GETDATE()) =  1	THEN 'Last Fiscal Quarter'
				ELSE NULL
			END
		)
	FROM
		DimDate dw

	-- ==================================================================================
	-- FiscalQuartersAgo (0 - This Quarter, 1 - Last Quarter etc. -1 - Next Quarter etc)
	-- ==================================================================================
	UPDATE
		DimDate
	SET
		FiscalQuartersAgo = DATEDIFF(qq, dw.Date, GETDATE())
	FROM
		DimDate dw
	
	-- ==================================================================================
	-- FiscalQuartersAhead (0 - This Quarter, -1 - Last Quarter etc. 1 - Next Quarter etc)
	-- ==================================================================================
	UPDATE
		DimDate
	SET
		FiscalQuartersAhead = DATEDIFF(qq, dw.Date, GETDATE())*-1
	FROM
		DimDate dw

	-- ==================================================================================
	-- ThisFiscalYear (Last Year/This Year/Next Year)
	-- ==================================================================================
	UPDATE
		DimDate
	SET
		ThisFiscalYear = 
		(
			CASE 
				WHEN DATEDIFF(yy, dw.Date, GETDATE()) = -1	THEN 'Next Fiscal Year'
				WHEN DATEDIFF(yy, dw.Date, GETDATE()) =  0	THEN 'This Fiscal Year'
				WHEN DATEDIFF(yy, dw.Date, GETDATE()) =  1	THEN 'Last Fiscal Year'
				ELSE NULL
			END
		)
	FROM
		DimDate dw

	-- ==================================================================================
	-- FiscalYearsAgo (0 - This Year, 1 - Last Year etc, -1 - Next Year etc)
	-- ==================================================================================
	UPDATE
		DimDate
	SET
		FiscalYearsAgo = DATEDIFF(yy, dw.Date, GETDATE())
	FROM
		DimDate dw

	-- ==================================================================================
	-- FiscalYearsAhead (0 - This Year, -1 - Last Year etc, 1 - Next Year etc)
	-- ==================================================================================
	UPDATE
		DimDate
	SET
		FiscalYearsAhead = DATEDIFF(yy, dw.Date, GETDATE())*-1
	FROM
		DimDate dw

	-- ==================================================================================
	-- Exception Days - Set Bank Holidays
	-- ==================================================================================
	UPDATE
		DimDate
	SET
		BankHoliday = 'Bank Holiday'
	FROM
		DimDate dw
		INNER JOIN 
		(
			SELECT
				sdwed.ExceptionDate
			FROM
				ConfigDimDateExceptionDays sdwed
				INNER JOIN ConfigExceptionType edt ON sdwed.ConfigExceptionTypeID = edt.ConfigExceptionTypeID AND edt.[Name] = 'Bank Holiday'
		) data
		ON dw.date = data.ExceptionDate

	-- ==================================================================================
	-- Exception Days - Set Non-Working Days
	-- ==================================================================================
	UPDATE
		DimDate
	SET
		Worked = 'Not Worked'
	FROM
		DimDate dw
		INNER JOIN 
		(
			SELECT
				sdwed.ExceptionDate
			FROM
				ConfigDimDateExceptionDays sdwed
				INNER JOIN ConfigExceptionType edt ON sdwed.ConfigExceptionTypeID = edt.ConfigExceptionTypeID AND edt.[Name] = 'Non Working Days'
		) data
		ON dw.date = data.ExceptionDate

	-- ==================================================================================
	-- Exception Days - Set Working Days
	-- ==================================================================================
	UPDATE
		DimDate
	SET
		Worked = 'Worked'
	FROM
		DimDate dw
		INNER JOIN 
		(
			SELECT
				sdwed.ExceptionDate
			FROM
				ConfigDimDateExceptionDays sdwed
				INNER JOIN ConfigExceptionType edt ON sdwed.ConfigExceptionTypeID = edt.ConfigExceptionTypeID AND edt.[Name] = 'Working Days'
		) data
		ON dw.date = data.ExceptionDate

	
END

-- ================================
-- Test Case
-- EXEC usp_Minerva_UpdateDimDate
-- ================================
GO
