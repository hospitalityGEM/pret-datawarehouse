SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[usp_Reports_PeriodList(withPeriodEndDelay)]
	@Client			INT
,	@PeriodEndDelay BIT = 0
,	@ShowExcludeFromAnalysis BIT = 0

AS

SELECT DISTINCT 
      dp.PeriodID  
,     dp.CurrentPeriodName AS PeriodName
,     dp.StartDateKey 
,	  dp.ClientPeriodsAgo

FROM
FactVisitResult fvr
INNER JOIN DimPeriod dp ON fvr.PeriodKey = dp.PeriodKey

WHERE
      dp.ClientID = @Client
AND
      fvr.Active = 1
AND
	CASE
		WHEN @ShowExcludeFromAnalysis = 0 AND dp.IncludeInAnalysis = 'Include' THEN 1
		WHEN @ShowExcludeFromAnalysis = 0 AND dp.IncludeInAnalysis <> 'Include' THEN 0
		ELSE 1
	END = 1
AND
	CASE 
		WHEN @PeriodEndDelay = 1 AND dp.ClientPeriodsAgo > 0 THEN 1
		WHEN @PeriodEndDelay = 0 THEN 1
		ELSE 0
	END = 1

ORDER BY
     dp.StartDateKey DESC


GO
