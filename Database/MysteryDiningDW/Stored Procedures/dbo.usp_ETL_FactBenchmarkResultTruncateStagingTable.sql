SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_FactBenchmarkResultTruncateStagingTable] 
AS
	-- FactBenchmarkResult
	TRUNCATE TABLE StagingFactBenchmarkResult

-- Test Case
-- EXEC usp_ETL_FactBenchmarkResultTruncateStagingTable
GO
