SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[DashboardGetChartDataCategory]
-- Add the parameters for the stored procedure here
	@DateFrom datetime,
	@DateTo datetime,
	@ClientID int = 0, 
	@BranchIDs nvarchar(max),
	@VisitTypes nvarchar(max),
	@HideCurrentPeriod bit,
	@Benchmark nvarchar(max) = '',
	@QuestionCategory nvarchar(max) = '',
	@QuestionnaireCategory nvarchar(max) = '',
	@Department nvarchar(max) = '',
	@Brand nvarchar(max) = '',
	@ShowCompanyScore bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE
		@DateKeyFrom	INT
	,	@DateKeyTo		INT
	,	@DateKeyCurrent	INT

	SELECT @DateKeyFrom	= DateKey FROM DimDate WHERE [Date] = CONVERT(DATE, @DateFrom)
	SELECT @DateKeyTo	= DateKey FROM DimDate WHERE [Date] = CONVERT(DATE, @DateTo)
	SELECT @DateKeyCurrent	= DateKey FROM DimDate WHERE [Date] = CONVERT(DATE, GETDATE()) 

	DECLARE @Results TABLE
	(
		BranchID	INT
	,	Label		NVARCHAR(200)
	,	Score		DECIMAL(18,2)
	,	MaxScore	DECIMAL(18,2)	
	)

	INSERT INTO @Results
	SELECT
		db.BranchID		AS BranchID
	,	dq.ManagementCategory		AS [Label]
	,	fqr.Score		AS [Score]
	,	fqr.MaxScore	AS [MaxScore]
	FROM
		FactQuestionResult fqr
		INNER JOIN DimBranch db ON fqr.BranchKey = db.BranchKey
		INNER JOIN DimBranch cdb ON db.BranchID = cdb.BranchID AND cdb.Active = 1
		INNER JOIN DimVisit dv	ON fqr.VisitKey	 = dv.VisitKey
		INNER JOIN DimPeriod dp ON fqr.PeriodKey = dp.PeriodKey
		INNER JOIN DimQuestion dq ON fqr.QuestionKey = dq.QuestionKey
	WHERE
			db.ClientID = @ClientID
		AND
			dv.VisitStatus IN ('Reviewed','Complete')
		AND
			dv.Type IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable( @VisitTypes, ','))
		AND
			dp.StartDateKey >= @DateKeyFrom
		AND	 
			dp.StartDateKey <= @DateKeyTo
		AND	 
			fqr.MaxScore > 0
		AND	 
			dq.HiddenSection = 'Visible'
		AND
			dq.ManagementCategory <> 'Not Set'
		AND
			cdb.BranchStatus = 'Live'
		--AND	 
		--	dq.MenuSection = 'N/A'
		AND
			dp.IncludeInAnalysis = 'Include'
		
		AND
			dp.IncludeInPortal='Include'
		AND
			(CASE
				WHEN @HideCurrentPeriod = 1 AND dp.EndDateKey < @DateKeyCurrent THEN 1
				WHEN @HideCurrentPeriod = 0 THEN 1
				ELSE 0
			END) = 1
		AND
			fqr.Active = 1
		
	SELECT
		r.Label										AS Label
	,	ROUND((SUM(Score) / SUM(MaxScore))*100,0)	AS Value
	,	ROUND(CompanyValue,0)						AS CompanyValue
	FROM
		@Results r
		INNER JOIN 
		(
			SELECT
				Label
			,	CONVERT(DECIMAL(18,2),
				CASE
					WHEN @ShowCompanyScore = 0 THEN 0
					ELSE ROUND((SUM(Score) / SUM(MaxScore))*100,0)			
				END)	AS [CompanyValue]
			FROM
				@Results				
			GROUP BY
				Label
		) cv ON r.Label = cv.Label
	WHERE
		BranchID IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@BranchIDs,','))
	GROUP BY
		r.Label
	,	cv.CompanyValue
	ORDER BY
		ROUND((SUM(Score) / SUM(MaxScore))*100,0)
END

-- Test Case
-- EXEC [dbo].[DashboardGetChartDataCategory] 	@DateFrom = '2012-09-01',	@DateTo = '2013-09-30',		@ClientID = 209, 	@BranchIDs = '3316,259,735,308,4931,1337,963,975,738,161,312,5135,50,3319,739,136,129,311,1286,1087,5192,121,435,138,623,734,4932,619,126,5538,130,123,5191,1049,125,1017,1237,1050,4359,122,133,51,137,737,5456,624,120,5673,1238,1284,134,427,143,5088,446,736,4734,559,139,3750,128,889,2415,251,258,205,418,1086,1089,3749,1048,135,609,5676,1285,127,131,124,239,453,1029,4966,1090,118,405,740,5087', @VisitTypes = 'Mystery Visit', @HideCurrentPeriod = 1, @ShowCompanyScore=1



GO
