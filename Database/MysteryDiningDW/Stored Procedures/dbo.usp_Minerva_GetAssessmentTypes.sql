SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_Minerva_GetAssessmentTypes]
AS

BEGIN
SELECT
		1 AS AssessmentTypeID
	,	'Mystery Visit (Example)' AS AssessmentType
UNION
SELECT
		2 AS AssessmentTypeID
	,	'Phone Call (Example)' AS AssessmentType
UNION
SELECT
		3 AS AssessmentTypeID
	,	'Real Customer (Example)' AS AssessmentType
UNION
SELECT
		4 AS AssessmentTypeID
	,	'blah (Example)' AS AssessmentType

END
GO
GRANT EXECUTE ON  [dbo].[usp_Minerva_GetAssessmentTypes] TO [TMDCReporting]
GO
