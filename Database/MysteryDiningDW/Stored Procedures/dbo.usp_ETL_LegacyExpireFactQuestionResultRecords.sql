SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_LegacyExpireFactQuestionResultRecords] 
AS

--Expired Records

UPDATE FactQuestionResult
SET
	[Active] = 0
,	[ValidToDate] = SfQ.ValidToDate

FROM
	FactQuestionResult fQ
INNER JOIN
	StagingFactQuestionResult SfQ	ON	fQ.[Source]	= SfQ.[Source]
									AND	fQ.QuestionResultID	= SfQ.QuestionResultID
	
WHERE
	fQ.[Active] = 1
AND	fQ.[ValidFromDate] <= SfQ.ValidToDate
AND fQ.[Source] = 'Legacy'


-- Update Current Score values on old records

--UPDATE FactQuestionResult
--SET
--	[CurrentScore] = SfQ.CurrentScore

--FROM
--	FactQuestionResult fQ
--INNER JOIN
--	StagingFactQuestionResult SfQ	ON	fQ.[Source]	= SfQ.[Source]
--									AND	fQ.QuestionResultID	= SfQ.QuestionResultID
--WHERE
--	fQ.[Source] = 'Legacy'

-- Set current records (in case any errors occured during SCD control)

--SELECT MAX(fqr.QuestionResultKey) AS RealMaxKey INTO #MaxKeys
--          FROM
--                (
--                  SELECT
--                        QuestionResultID,
--                        MAX(ValidFromDate) maxdate
--                  FROM
--                        FactQuestionResult
--                  WHERE
--						[Source] = 'Legacy'
--                  GROUP BY
--                        QuestionResultID
--                ) x
--                INNER JOIN FactQuestionResult fqr ON x.QuestionResultID = fqr.QuestionResultID
--                AND x.maxdate = fqr.ValidFromDate
--          GROUP BY
--                fqr.QuestionResultID

--UPDATE
--    FactQuestionResult
--SET
--    [Active] = 
--				CASE 
--					WHEN #MaxKeys.RealMaxKey IS NULL THEN 0
--					ELSE 1
--				END
--FROM
--	FactQuestionResult [source]
--LEFT JOIN
--		#MaxKeys ON [source].QuestionResultKey = #MaxKeys.RealMaxKey

 --   (    
 --         SELECT
 --               MAX(fqr.QuestionResultKey) realmax
 --         FROM
 --               (
 --                 SELECT
 --                       QuestionResultID,
 --                       MAX(ValidFromDate) maxdate
 --                 FROM
 --                       FactQuestionResult
 --                 WHERE
	--					[Source] = 'Legacy'
 --                 GROUP BY
 --                       QuestionResultID
 --               ) x
 --               INNER JOIN FactQuestionResult fqr ON x.QuestionResultID = fqr.QuestionResultID
 --               AND x.maxdate = fqr.ValidFromDate
 --         GROUP BY
 --               fqr.QuestionResultID
	--) active ON source.QuestionResultKey = active.realmax

-- Test
-- EXEC usp_ETL_LegacyExpireFactQuestionResultRecords
	
GO
