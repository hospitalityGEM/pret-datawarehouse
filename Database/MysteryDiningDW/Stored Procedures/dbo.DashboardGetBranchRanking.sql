SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[DashboardGetBranchRanking]
-- Add the parameters for the stored procedure here
	@DateFrom datetime,
	@DateTo datetime,
	@ClientID int = 0, 
	@Count int = 5,
	@IsTop bit = 1,
	@BranchIDs nvarchar(max),
	@VisitTypes nvarchar(max),
	@HideCurrentPeriod bit,
	@ShowAllBranchesForClient bit,
	@QuestionnaireCategory nvarchar(max) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE	@DynamicSQL						NVARCHAR(MAX)
	DECLARE @DynamicQuestionnaireCatClause	NVARCHAR(200)
	DECLARE @ReturnTable					TABLE 
					(
							[BranchName]	NVARCHAR(200)
						,	[Score]			DECIMAL(18,2)
					)

	SELECT
		@DynamicQuestionnaireCatClause =
		CASE
			WHEN LEN(@QuestionnaireCategory) > 0 THEN
			'
				dv.QuestionnaireCategory IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(''' + @QuestionnaireCategory + ''','',''))
			AND'
		ELSE
			''
		END

	SET @DynamicSQL =
	'
	DECLARE
		@DateKeyFrom	INT
	,	@DateKeyTo		INT
	,	@MaxScore		DECIMAL(18,2)
	,	@TopCount		INT
	,	@DateKeyCurrent	INT
	,	@HideCurrentPeriod BIT = ' + CONVERT(NVARCHAR,@HideCurrentPeriod) +
	'		
	DECLARE @Results	TABLE
		(
			Branch		NVARCHAR(200)
		,	BranchScore	DECIMAL(18,2)
		)

	SELECT @DateKeyFrom	= DateKey FROM DimDate WHERE [Date] = ''' + CONVERT(NVARCHAR, @DateFrom) + '''
	SELECT @DateKeyTo	= DateKey FROM DimDate WHERE [Date] = ''' + CONVERT(NVARCHAR, @DateTo) + '''
	SELECT @DateKeyCurrent	= DateKey FROM DimDate WHERE [Date] = CONVERT(DATE, GETDATE())

	INSERT INTO @Results (Branch, BranchScore)
	SELECT
		db.CurrentBranch
	,	ROUND((SUM(fvr.Score) / SUM(fvr.MaxScore))*100,0)
	FROM
		FactVisitResult fvr
		INNER JOIN DimBranch db ON fvr.BranchKey = db.BranchKey
		INNER JOIN DimBranch cdb ON db.BranchID = cdb.BranchID AND cdb.Active = 1
		INNER JOIN DimVisit dv	ON fvr.VisitKey	 = dv.VisitKey
		INNER JOIN DimPeriod dp ON fvr.PeriodKey = dp.PeriodKey
	WHERE' +
	CASE WHEN @ShowAllBranchesForClient = 0 THEN
			'	db.BranchID IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(''' + @BranchIDs + ''', '',''))'
		ELSE
			'	db.ClientID = ' + CONVERT(NVARCHAR(10),@ClientID)
	END +
	'	AND
			dv.VisitStatus IN (''Reviewed'',''Complete'')
		AND
			dv.Type IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(''' + @VisitTypes + ''', '',''))
		AND
			cdb.BranchStatus IN (''Live'')
		AND
			dp.StartDateKey >= @DateKeyFrom
		AND
			dp.StartDateKey <= @DateKeyTo
		AND
			fvr.MaxScore > 0
		AND
			(CASE
				WHEN @HideCurrentPeriod = 1 AND dp.EndDateKey < @DateKeyCurrent THEN 1
				WHEN @HideCurrentPeriod = 0 THEN 1
				ELSE 0
			END) = 1
		AND
			fvr.IncludeInAnalysis = ''Include''
		AND
			dp.IncludeInPortal=''Include''
		AND
	' +
		@DynamicQuestionnaireCatClause		
	+ '
			fvr.Active = 1
	GROUP BY
		db.CurrentBranch
	
	SELECT
		[BranchName]
	,	[Score]
	FROM
	(
		SELECT
			Branch		AS [BranchName]
		,	BranchScore AS [Score]
		,	RANK()OVER(ORDER BY BranchScore ' + IIF(@IsTop = 1, 'DESC', 'ASC') + ') AS Ranker
		FROM
			@Results
		 
	) Scores
	WHERE
		Ranker BETWEEN 1 AND ' + CONVERT(NVARCHAR,@Count)

	INSERT INTO @ReturnTable EXEC(@DynamicSQL)
	SELECT * FROM @ReturnTable

END

-- Test Case
-- EXEC [dbo].[DashboardGetBranchRanking] 	@DateFrom = '2012-10-01',	@DateTo = '2012-11-30',	@ClientID = 12, @Count=5,	@BranchIDs = '5539,5473,1474,3395,1469,1450,1459,1456,2256,1445,3394,936,1448,2257,2503,3392,4969,1460,3398,5592,2421,2910,2909,1487,1461,1458,1476,2248,1462,1449,5033,1463,1457,1464,5505,3393,1478,1423,3781,4329,1471,5173,5401,1451,5172,3078,3540,4336,3396,5400,1466,3743,1443,5284,1477,5512,1452,1446,1472,1453,1454,1467,1455,1468,1479,5460,1447,1473,1470,937,5499,1465,5586,2416,1444,5647', @VisitTypes = 'Mystery Visit', @HideCurrentPeriod = 0, @ShowAllBranchesForClient = 1


GO
