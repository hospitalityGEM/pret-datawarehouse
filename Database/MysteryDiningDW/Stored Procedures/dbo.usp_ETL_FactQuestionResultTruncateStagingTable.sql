SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_FactQuestionResultTruncateStagingTable] 
AS
	-- FactQuestionResult
	TRUNCATE TABLE StagingFactQuestionResult

-- Test Case
-- EXEC usp_ETL_FactQuestionResultTruncateStagingTable
GO
