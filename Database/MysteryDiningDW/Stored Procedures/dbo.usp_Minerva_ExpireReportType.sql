SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_Minerva_ExpireReportType]
	@ReportTypeID	TINYINT
,	@AdminID		SMALLINT
AS

BEGIN
	UPDATE ReportType
	SET Active = 0
	WHERE ReportTypeID = @ReportTypeID
END

EXEC usp_Minerva_AddAuditEvent @AdminID, 27, 5, @ReportTypeID

-- Test Case
-- EXEC usp_Minerva_ExpireReportType 1, 1
GO
