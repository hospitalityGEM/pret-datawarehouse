SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_LegacyExpireFactRecommendationViewRecords] 
AS

--Expired Records

UPDATE FactRecommendationView
SET
	[Active] = 0
,	[ValidToDate] = SfRv.ValidToDate

FROM
	FactRecommendationView fRv
INNER JOIN
	StagingFactRecommendationView SfRv ON	fRv.RecommendationViewKey = SfRv.RecommendationViewKey
WHERE
	SfRv.[Source] = 'Legacy'
AND SfRv.[Type] = 'Expired'


-- Update SCD 1 records

UPDATE FactRecommendationView
SET
	[FacebookLiked]				= SfRv.FacebookLiked
,	[TwitterFollowed]			= SfRv.TwitterFollowed
,	[FacebookSocialChannelKey]	= SfRv.FacebookSocialChannelKey
,	[TwitterSocialChannelKey]	= SfRv.TwitterSocialChannelKey
FROM
	FactRecommendationView fRv
INNER JOIN
	StagingFactRecommendationView SfRv ON	fRv.RecommendationViewID = SfRv.RecommendationViewID
WHERE
	SfRv.[Source] = 'Legacy'
--AND 
--	SfRv.[Type]	= 'SCD1'

-- Set Active flag Appropriately

--UPDATE
--    FactRecommendationView
--SET
--    [Active] = 
--				CASE 
--					WHEN active.realmax IS NULL THEN 0
--					ELSE 1
--				END
--FROM
--	FactRecommendationView [source]
--LEFT JOIN
--    (    
--          SELECT
--                MAX(fdr.AdvocacyTwitterKey) realmax
--          FROM
--                (
--                  SELECT
--						VisitID,
--                        CategoryID,
--                        [Source],
--                        MAX(ValidFromDate) maxdate
--                  FROM
--                        FactRecommendationView
--                  WHERE
--						[Source] = 'Legacy'
--                  GROUP BY
--                        VisitID,
--                        CategoryID,
--                        [Source]
--                ) x
--                INNER JOIN FactRecommendationView fdr ON x.CategoryID = fdr.CategoryID
--												AND x.VisitID = fdr.VisitID
--												AND x.maxdate = fdr.ValidFromDate
--          GROUP BY
--                fdr.CategoryID
--			,	fdr.VisitID
--			,	fdr.[Source]
--	) active ON [source].AdvocacyTwitterKey = active.realmax

-- Test
-- EXEC usp_ETL_LegacyExpireFactRecommendationViewRecords

	
GO
