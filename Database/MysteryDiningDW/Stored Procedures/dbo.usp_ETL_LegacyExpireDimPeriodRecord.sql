
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[usp_ETL_LegacyExpireDimPeriodRecord] 
AS

-- Expire old records
UPDATE DimPeriod
SET
      [Active] = 0
,     [ValidToDate] = SdP.ValidToDate

FROM
      DimPeriod dP
INNER JOIN
      StagingDimPeriod SdP    ON    dP.[PeriodKey]          = SdP.[PeriodKey]
      
WHERE
      dP.[Source] = 'Legacy'
AND
	  SdP.[Type] = 'Expired'

-- Update SCD1 Changes
UPDATE DimPeriod
SET
      [Year] = SdP.[Year]
,     [CurrentPeriodName] = SdP.CurrentPeriodName
,	  [IncludeInAnalysis] = SdP.IncludeInAnalysis
,	  [IncludeInPortal] = SdP.IncludeInPortal
,		[PeriodType]=Sdp.PeriodType
,		[YearStartKey]=Sdp.YearStartKey
,		[YearEndKey]=Sdp.YearEndKey


FROM
      DimPeriod dP
INNER JOIN
      StagingDimPeriod SdP    ON    dP.[PeriodID]          = SdP.[PeriodID]
      
WHERE
      dP.[Source] = 'Legacy'
--AND
--	  SdP.[Type] = 'SCD1'

-- Test
-- EXEC usp_ETL_LegacyExpireDimPeriodRecord


GO
