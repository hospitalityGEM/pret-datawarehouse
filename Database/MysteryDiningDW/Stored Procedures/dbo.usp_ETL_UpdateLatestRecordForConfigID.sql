SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_UpdateLatestRecordForConfigID] 
	@ConfigInfoID INT
AS
	
DECLARE @EarliestDate DATETIME

SELECT @EarliestDate =		MAX([TimeStamp])
						FROM
							MysteryDiningCopy.dbo.VisitUpdate
						
UPDATE 
	ConfigInfo
SET 
	Value = @EarliestDate
,	LastUpdated = @EarliestDate
WHERE
	ConfigInfoID = @ConfigInfoID


-- Test Case
-- EXEC usp_ETL_UpdateLatestRecordForConfigID
GO
