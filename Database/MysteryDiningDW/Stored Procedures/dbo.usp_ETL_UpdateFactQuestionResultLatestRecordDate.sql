
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_UpdateFactQuestionResultLatestRecordDate] 
AS

DECLARE @LastestDate DATETIME

SELECT @LastestDate =	DATEADD(d,-5,MAX([TimeStamp]))
						FROM
							MysteryDiningCopy.dbo.Answer
						WHERE
							[TimeStamp] <= GETDATE()

UPDATE 
	ConfigInfo
SET 
	Value = @LastestDate
,	LastUpdated = @LastestDate
WHERE
	ConfigInfoID = 4
	
-- EXEC usp_ETL_UpdateFactQuestionResultLatestRecordDate
GO
