SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_DimRecommendationTruncateStagingTable] 
AS
	-- DimRecommendation
	TRUNCATE TABLE StagingDimRecommendation

-- Test Case
-- EXEC usp_ETL_DimRecommendationTruncateStagingTable
GO
