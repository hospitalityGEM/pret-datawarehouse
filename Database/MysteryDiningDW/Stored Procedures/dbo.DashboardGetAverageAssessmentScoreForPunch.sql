
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[DashboardGetAverageAssessmentScoreForPunch]
       @DateFrom datetime, 
       @DateTo datetime, 
       @ClientID int = 0, 
       @BranchIDs nvarchar(max),
       @Type nvarchar(max),
       @VisitTypes nvarchar(max),
       @ClientYear bit =0,
       @NB bit =0,
       @HideCurrentPeriod bit,
       @Benchmark nvarchar(max) = '',    
       @QuestionCategory nvarchar(max) = '',  
       @QuestionnaireCategory nvarchar(max) = '',  
       @Department nvarchar(max) = '',   
       @Brand nvarchar(max) = ''

AS BEGIN

SET NOCOUNT ON;

DECLARE 
	@LatestPeriodAgo int,
	@PeriodYear	nvarchar(50),
	@YearDateFromKey int, 
    @YearDateToKey int 

SELECT @LatestPeriodAgo=Min(ClientPEriodsAgo) FROM DimPeriod dp WHERE dp.ClientID=@ClientID and dp.Active=1 AND dp.IncludeInAnalysis='Include' and dp.IncludeInPortal='Include' AND dp.ClientPeriodsAgo>=@HideCurrentPeriod AND @ClientYear=1
		
--SELECT @PeriodYear= [Year] FROM DimPeriod dp WHERE dp.ClientID=@ClientID and dp.Active=1 AND dp.IncludeInAnalysis='Include' AND dp.ClientPeriodsAgo=@LatestPeriodAgo AND @ClientYear=1

--SELECT @YearDateFromKey = Min(YearStartKey) FROM DimPeriod dp WHERE dp.ClientID=@ClientID and dp.Active=1 AND dp.IncludeInAnalysis='Include' AND dp.ClientPeriodsAgo=@LatestPeriodAgo AND @ClientYear=1

--SELECT @YearDateToKey = Max(YearEndKey) FROM DimPeriod dp WHERE dp.ClientID=@ClientID and dp.Active=1 AND dp.IncludeInAnalysis='Include' AND dp.ClientPeriodsAgo=@LatestPeriodAgo AND @ClientYear=1

SELECT  @PeriodYear= [Year]
		,@YearDateFromKey = Min(YearStartKey) 
		,@YearDateToKey = Max(YearEndKey)
		FROM DimPeriod dp WHERE dp.ClientID=@ClientID and dp.Active=1 AND dp.IncludeInAnalysis='Include' and dp.IncludeInPortal='Include' AND dp.ClientPeriodsAgo=@LatestPeriodAgo AND @ClientYear=1 Group by [Year]

DECLARE
		@DateKeyFrom	INT = cast(CONVERT(VARCHAR(19),@DateFrom,112) as int)
	,	@DateKeyTo		INT = cast(CONVERT(VARCHAR(19),@DateTo,112) as int)
    ,   @BranchIDInts AS IdListInt
    ,   @VisitTypesStrings AS IdListString
    ,   @BrandStrings as IdListString
    ,   @QuestionnaireCategoryStrings as IdListString

INSERT INTO @BranchIDInts
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@BranchIDs,',')

INSERT INTO @VisitTypesStrings
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@VisitTypes,',')


INSERT INTO @BrandStrings
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@Brand,',')


INSERT INTO @QuestionnaireCategoryStrings
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@QuestionnaireCategory,',')

IF @Type='Average Assessment Score' 
SELECT
		CASE 
			WHEN db.ClientID=35418 THEN CONVERT(DECIMAL(5,2),(SUM(fvr.AdjustedScore)/SUM(fvr.MaxScore))*100)
			ELSE  CONVERT(DECIMAL(5,2),(SUM(fvr.Score)/SUM(fvr.MaxScore))*100) END 

	FROM
		FactVisitResult fvr
		INNER JOIN DimBranch db ON fvr.BranchKey = db.BranchKey
		INNER JOIN DimVisit  dv	ON fvr.VisitKey	 = dv.VisitKey
		INNER JOIN DimPeriod dp ON fvr.PeriodKey = dp.PeriodKey
	WHERE
			db.ClientID = @ClientID
		AND
			dv.VisitStatus IN ('Reviewed','Complete')
		AND
			dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)
		AND
			db.BranchID = ANY (SELECT Id FROM @BranchIDInts)
		AND
			(case when LEN(@Brand)=0 then 1 when db.CurrentBrand = ANY (SELECT Id FROM @BrandStrings) then 1 else 0 end)=1
		AND
			(case when Len(@QuestionnaireCategory)=0 then 1 when dv.QuestionnaireCategory = ANY (SELECT Id FROM @QuestionnaireCategoryStrings) then 1 else 0 end)=1
		AND
			(
				(dp.StartDateKey >= @DateKeyFrom	AND dp.StartDateKey <= @DateKeyTo AND @ClientYear=0)
			OR
				(dp.[Year]= @PeriodYear and @ClientYear=1)
			)
		AND
			fvr.MaxScore > 0
		AND
			fvr.Active = 1
		AND
			fvr.IncludeInAnalysis='Include'	
		AND
			dp.ClientPeriodsAgo>= @HideCurrentPeriod
		AND
			dp.IncludeInPortal='Include'
		AND
			(dv.IsNBVisit=1 OR @NB=0)
		AND
			(db.ClientID<>28647 OR dv.Type<>'Online Feedback')
		GROUP BY db.ClientID

UNION

	SELECT
		
			CONVERT(DECIMAL(5,2),(SUM(fvr.Score)/SUM(fvr.MaxScore))*100)  

	FROM
		FactVisitResult fvr
		INNER JOIN DimBranch db ON fvr.BranchKey = db.BranchKey
		INNER JOIN DimVisit  dv	ON fvr.VisitKey	 = dv.VisitKey
		INNER JOIN FactFeedbackResult ffr ON ffr.FeedbackResultID=fvr.VisitID and ffr.active=1
	WHERE
			db.ClientID = @ClientID
		AND
			dv.VisitStatus IN ('Reviewed','Complete')
		AND
			dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)
		AND
			db.BranchID = ANY (SELECT Id FROM @BranchIDInts)
		AND
			(case when LEN(@Brand)=0 then 1 when db.CurrentBrand = ANY (SELECT Id FROM @BrandStrings) then 1 else 0 end)=1
		AND
			(case when Len(@QuestionnaireCategory)=0 then 1 when dv.QuestionnaireCategory = ANY (SELECT Id FROM @QuestionnaireCategoryStrings) then 1 else 0 end)=1
		AND
			(
				(ffr.CompletedDateKey >= @DateKeyFrom	AND ffr.CompletedDateKey <= @DateKeyTo AND @ClientYear=0)
			OR
				(ffr.CompletedDateKey >= @YearDateFromKey	AND ffr.CompletedDateKey <= @DateKeyTo AND @ClientYear=1)
			)
		AND
			fvr.MaxScore > 0
		AND
			fvr.Active = 1
		AND
			fvr.IncludeInAnalysis='Include'	
		AND
			(dv.IsNBVisit=1 OR @NB=0)
		AND
			db.ClientID=28647
		AND
			 dv.Type='Online Feedback'
		GROUP BY db.ClientID
ELSE
 
	IF @Type='Customer Loyalty Score' 
	
	SELECT 	
		IIF(count(dv.visitid)=0,null,CONVERT(decimal(5,0),(cast(SUM(case when CustomerLoyalty='Promoter' then 1 else 0 end) as decimal)/count(dv.visitid))*100) - CONVERT(decimal(5,0),(cast(SUM(case when CustomerLoyalty='Demoter' then 1 else 0 end) as decimal)/count(dv.visitid))*100))
		
	FROM
		FactBenchmarkResult fbr 
		INNER JOIN DimBranch db ON fbr.BranchKey = db.BranchKey
		INNER JOIN DimVisit  dv	ON fbr.VisitKey	 = dv.VisitKey
		INNER JOIN DimPeriod dp ON fbr.PeriodKey = dp.PeriodKey
	WHERE
			db.ClientID = @ClientID
		AND
			fbr.BenchmarkID='GWR'

		AND
			dv.VisitStatus IN ('Reviewed','Complete')
		AND
			dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)
		AND
			db.BranchID = ANY (SELECT Id FROM @BranchIDInts)
		AND
			(case when LEN(@Brand)=0 then 1 when db.CurrentBrand = ANY (SELECT Id FROM @BrandStrings) then 1 else 0 end)=1
		AND
			(case when Len(@QuestionnaireCategory)=0 then 1 when dv.QuestionnaireCategory = ANY (SELECT Id FROM @QuestionnaireCategoryStrings) then 1 else 0 end)=1
		AND
			(
				(dp.StartDateKey >= @DateKeyFrom AND dp.StartDateKey <= @DateKeyTo AND @ClientYear=0)
			OR
				(dp.[Year]= @PeriodYear and @ClientYear=1)
			)
		AND
			fbr.MaxScore > 0
		AND
			fbr.Active = 1
		AND
			fbr.IncludeInAnalysis='Include'	
		AND
			dp.ClientPeriodsAgo>= @HideCurrentPeriod
		AND
			dp.IncludeInPortal='Include'
		AND
			(dv.IsNBVisit=1 OR @NB=0)
		AND
			(db.ClientID<>28647 OR dv.Type<>'Online Feedback')
		GROUP BY db.ClientID

	UNION

		SELECT 	
		IIF(count(dv.visitid)=0,null,CONVERT(decimal(5,0),(cast(SUM(case when CustomerLoyalty='Promoter' then 1 else 0 end) as decimal)/count(dv.visitid))*100) - CONVERT(decimal(5,0),(cast(SUM(case when CustomerLoyalty='Demoter' then 1 else 0 end) as decimal)/count(dv.visitid))*100))
		
	FROM
		FactBenchmarkResult fbr 
		INNER JOIN DimBranch db ON fbr.BranchKey = db.BranchKey
		INNER JOIN DimVisit  dv	ON fbr.VisitKey	 = dv.VisitKey
		INNER JOIN FactFeedbackResult ffr ON ffr.FeedbackResultID=fbr.VisitID and ffr.active=1

	WHERE
			db.ClientID = @ClientID
		AND
			fbr.BenchmarkID='GWR'

		AND
			dv.VisitStatus IN ('Reviewed','Complete')
		AND
			dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)
		AND
			db.BranchID = ANY (SELECT Id FROM @BranchIDInts)
		AND
			(case when LEN(@Brand)=0 then 1 when db.CurrentBrand = ANY (SELECT Id FROM @BrandStrings) then 1 else 0 end)=1
		AND
			(case when Len(@QuestionnaireCategory)=0 then 1 when dv.QuestionnaireCategory = ANY (SELECT Id FROM @QuestionnaireCategoryStrings) then 1 else 0 end)=1
		AND
			(
				(ffr.CompletedDateKey >= @DateKeyFrom	AND ffr.CompletedDateKey <= @DateKeyTo AND @ClientYear=0)
		   OR
				(ffr.CompletedDateKey >= @YearDateFromKey	AND ffr.CompletedDateKey <=  @DateKeyTo AND @ClientYear=1)
			)
		AND
			fbr.MaxScore > 0
		AND
			fbr.Active = 1
		AND
			fbr.IncludeInAnalysis='Include'	
		AND
			(dv.IsNBVisit=1 OR @NB=0)
		AND
			db.ClientID=28647 
		AND
			 dv.Type='Online Feedback'
		GROUP BY db.ClientID

OPTION(OPTIMIZE FOR UNKNOWN)
		
END

-- Test Case
-- EXEC [dbo].[DashboardGetAverageAssessmentScoreForPunch] @DateFrom = '10-01-2012', @DateTo = '12-31-2012', @ClientID = 18867, @BranchIDs = '5589,3776,3775,3778',@Type='Average Assessment Score' ,@VisitTypes = 'Mystery Visit', @HideCurrentPeriod = 1







GO
