SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_Minerva_PopulateDimDate] 
	@StartDate	DATETIME
,	@EndDate	DATETIME

AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- Set Date format to the correct format
SET DATEFORMAT dmy;

-- Store current date first
DECLARE @DateFirstNumber INT
SET @DateFirstNumber = @@DATEFIRST

-- Change date first to Monday
SET DATEFIRST 1

--Use a WHILE LOOP to increment from start date to end date
DECLARE @LoopDate	DATETIME
SET @LoopDate	= @StartDate

WHILE @LoopDate <= @EndDate
BEGIN

-- Add a record to DimWhen for @LoopDate
INSERT INTO DimDate VALUES (

-- Static Field (fields that will not be updated)
	CONVERT(INTEGER,(	dbo.udf_Minerva_PadNumeric(YEAR(@LoopDate),4,'0') + 
						dbo.udf_Minerva_PadNumeric(MONTH(@LoopDate),2,'0') + 
						dbo.udf_Minerva_PadNumeric(DAY(@LoopDate),2,'0')))

---- Date (Format: 15/01/2010)
,	@LoopDate

-- DateName (Format: 15 January 2010)
,	CAST(DAY(@LoopDate) AS VARCHAR(2)) + ' ' + DATENAME(MM, @LoopDate) + ' ' + CAST(YEAR(@LoopDate) AS VARCHAR(4))

-- YearStart (Format: 01/01/2010)
,	CONVERT(DATETIME,'01/01/' + CONVERT(VARCHAR(4),YEAR(@LoopDate)))

-- YearName (Format: Year 2010)
,	'Year ' + CONVERT(VARCHAR(10),YEAR(@LoopDate))

---- QuarterStart (Format: 01/01/2010)
,	DATEADD(qq, DATEDIFF(qq,  0, @LoopDate),  0)

---- QuarterEnd (Format: 31/03/2010)
,	DATEADD(qq, DATEDIFF(qq, -1, @LoopDate), -1)

---- MonthStart (Format: 01/01/2010)
,	CONVERT(DATETIME,'01/' + 
	CONVERT(VARCHAR(2),dbo.udf_Minerva_PadNumeric(DATEPART(mm,@LoopDate), 2, '0')) + '/' +
	CONVERT(VARCHAR(4),YEAR(@LoopDate)))

---- MonthEnd (Format: 31/01/2010)
,	CONVERT(DATETIME,
	CONVERT(VARCHAR(4),YEAR(@LoopDate))	+
	CONVERT(VARCHAR(2),dbo.udf_Minerva_PadNumeric(DATEPART(mm,@LoopDate), 2, '0')) +
	CONVERT(VARCHAR(2),dbo.udf_Minerva_DaysInMonth(@LoopDate))
	)

---- MonthName (Format: January)
,	DATENAME(mm, @LoopDate)

---- WeekStart (Format: 11/01/2010)
,	(CASE 
		WHEN DATENAME(dw, @LoopDate) = 'Monday'		THEN (DATEADD(dd,  0, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Tuesday'	THEN (DATEADD(dd, -1, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Wednesday'  THEN (DATEADD(dd, -2, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Thursday'	THEN (DATEADD(dd, -3, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Friday'		THEN (DATEADD(dd, -4, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Saturday'	THEN (DATEADD(dd, -5, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Sunday'		THEN (DATEADD(dd, -6, @LoopDate))
	END)

---- WeekName (Format: Week 3)
,	'Week ' + DATENAME(wk, @LoopDate)

---- WeekSundayStart (Format: 10/01/2010)
,	(CASE 
		WHEN DATENAME(dw, @LoopDate) = 'Monday'		THEN (DATEADD(dd, -1, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Tuesday'	THEN (DATEADD(dd, -2, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Wednesday'  THEN (DATEADD(dd, -3, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Thursday'	THEN (DATEADD(dd, -4, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Friday'		THEN (DATEADD(dd, -5, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Saturday'	THEN (DATEADD(dd, -6, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Sunday'		THEN (DATEADD(dd,  0, @LoopDate))
	END)

---- WeekSundayName (Format: Week 3)
,	'Week ' + DATENAME(wk, DATEADD(dd, -1, @LoopDate))

---- WeekEnd (Format: 17/01/2010)
,	(CASE 
		WHEN DATENAME(dw, @LoopDate) = 'Monday'		THEN (DATEADD(dd, 6, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Tuesday'	THEN (DATEADD(dd, 5, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Wednesday'  THEN (DATEADD(dd, 4, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Thursday'	THEN (DATEADD(dd, 3, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Friday'		THEN (DATEADD(dd, 2, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Saturday'	THEN (DATEADD(dd, 1, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Sunday'		THEN (DATEADD(dd, 0, @LoopDate))
	END)

---- WeekSundayEnd (Format: 18/01/2010) 
,	(CASE 
		WHEN DATENAME(dw, @LoopDate) = 'Monday'		THEN (DATEADD(dd, 5, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Tuesday'	THEN (DATEADD(dd, 4, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Wednesday'  THEN (DATEADD(dd, 3, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Thursday'	THEN (DATEADD(dd, 2, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Friday'		THEN (DATEADD(dd, 1, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Saturday'	THEN (DATEADD(dd, 0, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Sunday'		THEN (DATEADD(dd, 6, @LoopDate))
	END)

---- DayName (Format: Friday)
,	DATENAME(dw, @LoopDate)

---- DayOfYear (Format: 15)
,	CONVERT(SMALLINT,DATEPART(dy, @LoopDate))

---- DayOfYearName (Format: Day 15)
,	'Day ' +  DATENAME(dy, @LoopDate)

---- DayOfQuarter (Format: 15)
,	CONVERT(TINYINT, DATEDIFF(dd, DATEADD(qq, DATEDIFF(qq, 0, @LoopDate), 0), @LoopDate) + 1)

---- DayOfQuarterName (Format: Day 15)
,	'Day ' + CONVERT(VARCHAR(7),DATEDIFF(dd, DATEADD(qq, DATEDIFF(qq, 0, @LoopDate), 0), @LoopDate) + 1)

---- DayOfMonth (Format: 15)
,	CONVERT(TINYINT, DAY(@LoopDate))

---- DayOfMonthName (Format: Day 15)
,	'Day ' + CONVERT(VARCHAR(2),DAY(@LoopDate))

-- DayOfWeek (Format: 5)
,	CONVERT(TINYINT,
		(CASE 
			WHEN DATENAME(dw, @LoopDate) = 'Monday'		THEN 1
			WHEN DATENAME(dw, @LoopDate) = 'Tuesday'	THEN 2
			WHEN DATENAME(dw, @LoopDate) = 'Wednesday'  THEN 3
			WHEN DATENAME(dw, @LoopDate) = 'Thursday'	THEN 4
			WHEN DATENAME(dw, @LoopDate) = 'Friday'		THEN 5
			WHEN DATENAME(dw, @LoopDate) = 'Saturday'	THEN 6
			WHEN DATENAME(dw, @LoopDate) = 'Sunday'		THEN 7
		END))

---- DayOfWeekName (Format: Day 5)
,	'Day ' + CONVERT(VARCHAR(6),
		(CASE 
			WHEN DATENAME(dw, @LoopDate) = 'Monday'		THEN 1
			WHEN DATENAME(dw, @LoopDate) = 'Tuesday'	THEN 2
			WHEN DATENAME(dw, @LoopDate) = 'Wednesday'  THEN 3
			WHEN DATENAME(dw, @LoopDate) = 'Thursday'	THEN 4
			WHEN DATENAME(dw, @LoopDate) = 'Friday'		THEN 5
			WHEN DATENAME(dw, @LoopDate) = 'Saturday'	THEN 6
			WHEN DATENAME(dw, @LoopDate) = 'Sunday'		THEN 7
		END))

---- DayOfWeekSunday (Format: 6)
,	CONVERT(TINYINT,
		(CASE 
			WHEN DATENAME(dw, @LoopDate) = 'Monday'		THEN 2
			WHEN DATENAME(dw, @LoopDate) = 'Tuesday'	THEN 3
			WHEN DATENAME(dw, @LoopDate) = 'Wednesday'  THEN 4
			WHEN DATENAME(dw, @LoopDate) = 'Thursday'	THEN 5
			WHEN DATENAME(dw, @LoopDate) = 'Friday'		THEN 6
			WHEN DATENAME(dw, @LoopDate) = 'Saturday'	THEN 7
			WHEN DATENAME(dw, @LoopDate) = 'Sunday'		THEN 1
		END))

---- DayOfWeekSundayName (Format: Day 6)
,	'Day ' + CONVERT(VARCHAR(6),
		(CASE 
			WHEN DATENAME(dw, @LoopDate) = 'Monday'		THEN 2
			WHEN DATENAME(dw, @LoopDate) = 'Tuesday'	THEN 3
			WHEN DATENAME(dw, @LoopDate) = 'Wednesday'  THEN 4
			WHEN DATENAME(dw, @LoopDate) = 'Thursday'	THEN 5
			WHEN DATENAME(dw, @LoopDate) = 'Friday'		THEN 6
			WHEN DATENAME(dw, @LoopDate) = 'Saturday'	THEN 7
			WHEN DATENAME(dw, @LoopDate) = 'Sunday'		THEN 1
		END))

---- WeekOfYear (Format: 3)
,	CONVERT(TINYINT, DATEPART(wk, @LoopDate))

---- WeekOfYearName (Format: Week 3)
,	'Week ' + DATENAME(wk, @LoopDate)

---- WeekOfYearSunday (Format: 3)
,	CONVERT(TINYINT, DATEPART(wk, DATEADD(dd, -1, @LoopDate)))

---- WeekOfYearSundayName (Format: Week  3)
,	'Week ' + DATENAME(wk, DATEADD(dd, -1, @LoopDate))

---- MonthOfYear (Format: 1)
,	CONVERT(TINYINT, DATEPART(mm, @LoopDate))

---- MonthOfYearName (Format: Month 1)
,	'Month ' + CONVERT(VARCHAR(10),MONTH(@LoopDate))

---- MonthOfQuarter (Format: 1)
,	CONVERT(TINYINT, 
	(CASE 
		WHEN Month(@LoopDate) IN (1, 4, 7, 10)	THEN 1
		WHEN Month(@LoopDate) IN (2, 5, 8, 11)	THEN 2
		WHEN Month(@LoopDate) IN (3, 6, 9, 12)	THEN 3
	END))

---- MonthOfQuarterName (Format: Month 1)
,	('Month ' +(CONVERT(VARCHAR(10),
	CASE 
		WHEN Month(@LoopDate) IN (1, 4, 7, 10)	THEN 1
		WHEN Month(@LoopDate) IN (2, 5, 8, 11)	THEN 2
		WHEN Month(@LoopDate) IN (3, 6, 9, 12)	THEN 3
	END)))

---- QuarterOfYear (Format: 1)
,	CONVERT(TINYINT,
	(CASE 
		WHEN Month(@LoopDate) IN (1, 2, 3)		THEN 1
		WHEN Month(@LoopDate) IN (4, 5, 6)		THEN 2
		WHEN Month(@LoopDate) IN (7, 8, 9)		THEN 3
		WHEN Month(@LoopDate) IN (10, 11, 12)	THEN 4
	END)) 

---- QuarterOfYearName (Format: Quarter 1)
,	('Quarter ' +(CONVERT(VARCHAR(10),
	CASE 
		WHEN Month(@LoopDate) IN (1, 2, 3)		THEN 1
		WHEN Month(@LoopDate) IN (4, 5, 6)		THEN 2
		WHEN Month(@LoopDate) IN (7, 8, 9)		THEN 3
		WHEN Month(@LoopDate) IN (10, 11, 12)	THEN 4
	END)))

---- FiscalYear (Format: 2009)
,	CONVERT(SMALLINT,
	(CASE
		WHEN Month(@LoopDate) IN (1, 2, 3)		THEN (CONVERT(INTEGER,YEAR(@LoopDate)) - 1)
		ELSE (YEAR(@LoopDate))
	END))

---- FiscalYearName (Format: Fiscal Year 2009)
,	('Fiscal Year ' +(CONVERT(VARCHAR(15),
	CASE
		WHEN Month(@LoopDate) IN (1, 2, 3)		THEN (CONVERT(INTEGER,YEAR(@LoopDate)) - 1)
		ELSE (YEAR(@LoopDate))
	END)))

---- FiscalQuarter (Format: 4)
,	CONVERT(TINYINT,
	(CASE 
		WHEN Month(@LoopDate) IN (1, 2, 3)		THEN 4
		WHEN Month(@LoopDate) IN (4, 5, 6)		THEN 1
		WHEN Month(@LoopDate) IN (7, 8, 9)		THEN 2
		WHEN Month(@LoopDate) IN (10, 11, 12)	THEN 3
	END)) 

---- FiscalQuarterName (Format: Fiscal Quarter 4)
,	('Fiscal Quarter ' +(CONVERT(VARCHAR(20),
	CASE 
		WHEN Month(@LoopDate) IN (1, 2, 3)		THEN 4
		WHEN Month(@LoopDate) IN (4, 5, 6)		THEN 1
		WHEN Month(@LoopDate) IN (7, 8, 9)		THEN 2
		WHEN Month(@LoopDate) IN (10, 11, 12)	THEN 3
	END)))

---- FiscalMonth (Format: 01/01/2010)
,	CONVERT(DATETIME,'01/' + 
	CONVERT(VARCHAR(3),dbo.udf_Minerva_PadNumeric(DATEPART(mm,@LoopDate), 2, '0')) + '/' +
	CONVERT(VARCHAR(4),YEAR(@LoopDate)))

---- FiscalMonthName (Format: Month 1)
,	('Fiscal Month ' + CONVERT(VARCHAR(9),DATENAME(mm, @LoopDate)))

---- FiscalWeek (Format: 11/01/2009)
,	(CASE 
		WHEN DATENAME(dw, @LoopDate) = 'Monday'		THEN (DATEADD(dd,  0, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Tuesday'	THEN (DATEADD(dd, -1, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Wednesday'  THEN (DATEADD(dd, -2, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Thursday'	THEN (DATEADD(dd, -3, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Friday'		THEN (DATEADD(dd, -4, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Saturday'	THEN (DATEADD(dd, -5, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Sunday'		THEN (DATEADD(dd, -6, @LoopDate))
	END)

---- FiscalWeekName (Format: Fiscal Week X)
,	'Fiscal Week ' + CONVERT(VARCHAR(2),
	CONVERT(TINYINT,
	(CASE
		WHEN DATEPART(mm, @LoopDate) IN (1, 2, 3) THEN DATEDIFF(wk, CONVERT(DATETIME, '01/04/' + DATENAME(yy, DATEADD(yy, -1, @LoopDate))), @LoopDate)
		ELSE DATEDIFF(wk, CONVERT(DATETIME, '01/04/' + DATENAME(yy, DATEADD(yy, 0, @LoopDate))), @LoopDate)
	END) + 1))

---- FiscalWeekSunday (Format: 10/01/2009)
,	(CASE 
		WHEN DATENAME(dw, @LoopDate) = 'Monday'		THEN (DATEADD(dd, -1, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Tuesday'	THEN (DATEADD(dd, -2, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Wednesday'  THEN (DATEADD(dd, -3, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Thursday'	THEN (DATEADD(dd, -4, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Friday'		THEN (DATEADD(dd, -5, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Saturday'	THEN (DATEADD(dd, -6, @LoopDate))
		WHEN DATENAME(dw, @LoopDate) = 'Sunday'		THEN (DATEADD(dd,  0, @LoopDate))
	END)

---- FiscalWeekSundayName (Format: Week X)
,	'Fiscal Week ' + CONVERT(VARCHAR(14),
	(CASE
		WHEN DATEPART(mm, @LoopDate) IN (1, 2, 3) THEN DATEDIFF(wk, CONVERT(DATETIME, '01/04/' + DATENAME(yy, DATEADD(yy, -1, DATEADD(dy, -1, @LoopDate)))), DATEADD(dy, -1, @LoopDate))
		ELSE DATEDIFF(wk, CONVERT(DATETIME, '01/04/' + DATENAME(yy, DATEADD(yy, 0, DATEADD(dy, -1, @LoopDate)))), DATEADD(dy, -1, @LoopDate))
	END) + 1)

---- FiscalDay (Format: 15/01/2010)
,	@LoopDate

---- FiscalDayName (Format: 15th January 2010)
,	CAST(DAY(@LoopDate) AS VARCHAR(2)) + ' ' + DATENAME(MM, @LoopDate) + ' ' + CAST(YEAR(@LoopDate) AS VARCHAR(4))

---- FiscalDayOfYear (Format: 290)
,	CONVERT(SMALLINT,
	(CASE
		WHEN DATEPART(mm, @LoopDate) IN (1, 2, 3) THEN DATEDIFF(dd,CONVERT(DATETIME, '01/04/' + DATENAME(yy, DATEADD(yy, -1, @LoopDate))),@LoopDate) + 1
		ELSE DATEDIFF(dd,CONVERT(DATETIME, '01/04/' + DATENAME(yy, DATEADD(yy, 0, @LoopDate))),@LoopDate) + 1
	END))

---- FiscalDayOfYearName (Format: Day 290)
,	'Day ' + CONVERT(VARCHAR(7),
	(CASE
		WHEN DATEPART(mm, @LoopDate) IN (1, 2, 3) THEN DATEDIFF(dd,CONVERT(DATETIME, '01/04/' + DATENAME(yy, DATEADD(yy, -1, @LoopDate))),@LoopDate) + 1
		ELSE DATEDIFF(dd,CONVERT(DATETIME, '01/04/' + DATENAME(yy, DATEADD(yy, 0, @LoopDate))),@LoopDate) + 1
	END))

---- FiscalDayOfQuarter (Format: 15)
,	CONVERT(TINYINT, DATEDIFF(dd, DATEADD(qq, DATEDIFF(qq, 0, @LoopDate), 0), @LoopDate) + 1)

---- FiscalDayOfQuarterName (Format: Day 15) 
,	'Day ' + CONVERT(VARCHAR(7),DATEDIFF(dd, DATEADD(qq, DATEDIFF(qq, 0, @LoopDate), 0), @LoopDate) + 1)

---- FiscalDayOfMonth (Format: 15)
,	CONVERT(TINYINT, DAY(@LoopDate))

---- FiscalDayOfMonthName (Format: Day 15) 
,	'Day ' + CONVERT(VARCHAR(2), DATEPART(dd, @LoopDate))

---- FiscalDayOfWeek (Format: 5)
,	CONVERT(TINYINT,
	(CASE 
		WHEN DATENAME(dw, @LoopDate) = 'Monday'		THEN 2
		WHEN DATENAME(dw, @LoopDate) = 'Tuesday'	THEN 3
		WHEN DATENAME(dw, @LoopDate) = 'Wednesday'  THEN 4
		WHEN DATENAME(dw, @LoopDate) = 'Thursday'	THEN 5
		WHEN DATENAME(dw, @LoopDate) = 'Friday'		THEN 6
		WHEN DATENAME(dw, @LoopDate) = 'Saturday'	THEN 7
		WHEN DATENAME(dw, @LoopDate) = 'Sunday'		THEN 1
	END))

---- FiscalDayOfWeekName (Format: Day 5)
,	'Day ' + CONVERT(VARCHAR(6),
		(CASE 
			WHEN DATENAME(dw, @LoopDate) = 'Monday'		THEN 2
			WHEN DATENAME(dw, @LoopDate) = 'Tuesday'	THEN 3
			WHEN DATENAME(dw, @LoopDate) = 'Wednesday'  THEN 4
			WHEN DATENAME(dw, @LoopDate) = 'Thursday'	THEN 5
			WHEN DATENAME(dw, @LoopDate) = 'Friday'		THEN 6
			WHEN DATENAME(dw, @LoopDate) = 'Saturday'	THEN 7
			WHEN DATENAME(dw, @LoopDate) = 'Sunday'		THEN 1
		END))

---- FiscalDayOfWeekSunday (Format: 6)
,	CONVERT(TINYINT,
	(CASE 
			WHEN DATENAME(dw, @LoopDate) = 'Monday'		THEN 2
			WHEN DATENAME(dw, @LoopDate) = 'Tuesday'	THEN 3
			WHEN DATENAME(dw, @LoopDate) = 'Wednesday'  THEN 4
			WHEN DATENAME(dw, @LoopDate) = 'Thursday'	THEN 5
			WHEN DATENAME(dw, @LoopDate) = 'Friday'		THEN 6
			WHEN DATENAME(dw, @LoopDate) = 'Saturday'	THEN 7
			WHEN DATENAME(dw, @LoopDate) = 'Sunday'		THEN 1
	END))

---- FiscalDayOfWeekSundayName (Format: Day 6)
,	'Day ' + CONVERT(VARCHAR(6),
		(CASE 
			WHEN DATENAME(dw, @LoopDate) = 'Monday'		THEN 2
			WHEN DATENAME(dw, @LoopDate) = 'Tuesday'	THEN 3
			WHEN DATENAME(dw, @LoopDate) = 'Wednesday'  THEN 4
			WHEN DATENAME(dw, @LoopDate) = 'Thursday'	THEN 5
			WHEN DATENAME(dw, @LoopDate) = 'Friday'		THEN 6
			WHEN DATENAME(dw, @LoopDate) = 'Saturday'	THEN 7
			WHEN DATENAME(dw, @LoopDate) = 'Sunday'		THEN 1
		END))

---- FiscalWeekOfYear (Format: 42)
,	CONVERT(TINYINT,
	(CASE
		WHEN DATEPART(mm, @LoopDate) IN (1, 2, 3) THEN DATEDIFF(wk, CONVERT(DATETIME, '01/04/' + DATENAME(yy, DATEADD(yy, -1, @LoopDate))), @LoopDate)
		ELSE DATEDIFF(wk, CONVERT(DATETIME, '01/04/' + DATENAME(yy, DATEADD(yy, 0, @LoopDate))), @LoopDate)
	END) + 1)

---- FiscalWeekOfYearName (Format: Week 42)
,	'Fiscal Week ' + CONVERT(VARCHAR(2),
	CONVERT(TINYINT,
	(CASE
		WHEN DATEPART(mm, @LoopDate) IN (1, 2, 3) THEN DATEDIFF(wk, CONVERT(DATETIME, '01/04/' + DATENAME(yy, DATEADD(yy, -1, @LoopDate))), @LoopDate)
		ELSE DATEDIFF(wk, CONVERT(DATETIME, '01/04/' + DATENAME(yy, DATEADD(yy, 0, @LoopDate))), @LoopDate)
	END) + 1))

-- FiscalWeekOfYearSunday (Format: 42)
,	CONVERT(TINYINT,
	(CASE
		WHEN DATEPART(mm, @LoopDate) IN (1, 2, 3) THEN DATEDIFF(wk, CONVERT(DATETIME, '01/04/' + DATENAME(yy, DATEADD(yy, -1, DATEADD(dy, -1, @LoopDate)))), DATEADD(dy, -1, @LoopDate))
		ELSE DATEDIFF(wk, CONVERT(DATETIME, '01/04/' + DATENAME(yy, DATEADD(yy, 0, DATEADD(dy, -1, @LoopDate)))), DATEADD(dy, -1, @LoopDate))
	END) + 1)

---- FiscalWeekOfYearSundayName (Format: Week 42) 
,	'Fiscal Week ' + CONVERT(VARCHAR(14),
	(CASE
		WHEN DATEPART(mm, @LoopDate) IN (1, 2, 3) THEN DATEDIFF(wk, CONVERT(DATETIME, '01/04/' + DATENAME(yy, DATEADD(yy, -1, DATEADD(dy, -1, @LoopDate)))), DATEADD(dy, -1, @LoopDate))
		ELSE DATEDIFF(wk, CONVERT(DATETIME, '01/04/' + DATENAME(yy, DATEADD(yy, 0, DATEADD(dy, -1, @LoopDate)))), DATEADD(dy, -1, @LoopDate))
	END) + 1)

---- FiscalMonthOfYear (Format: 10)
,	CONVERT(TINYINT,
	(CASE
		WHEN DATEPART(mm, @LoopDate) IN (1, 2, 3) THEN DATEDIFF(mm, CONVERT(DATETIME, '01/04/' + DATENAME(yy, DATEADD(yy, -1, @LoopDate))), @LoopDate)
		ELSE DATEDIFF(mm, CONVERT(DATETIME, '01/04/' + DATENAME(yy, DATEADD(yy, 0, @LoopDate))), @LoopDate)
	END) + 1)

---- FiscalMonthOfYearName (Format: Month 10) 
,	'Fiscal Month ' + CONVERT(VARCHAR(14),
	(CASE
		WHEN DATEPART(mm, @LoopDate) IN (1, 2, 3) THEN DATEDIFF(mm, CONVERT(DATETIME, '01/04/' + DATENAME(yy, DATEADD(yy, -1, @LoopDate))), @LoopDate)
		ELSE DATEDIFF(mm, CONVERT(DATETIME, '01/04/' + DATENAME(yy, DATEADD(yy, 0, @LoopDate))), @LoopDate)
	END) + 1)

---- FiscalMonthOfQuarter (Format: 1)
,	CONVERT(TINYINT,
	(CASE 
		WHEN Month(@LoopDate) IN (1, 4, 7, 10)	THEN 1
		WHEN Month(@LoopDate) IN (2, 5, 8, 11)	THEN 2
		WHEN Month(@LoopDate) IN (3, 6, 9, 12)	THEN 3
	END))

---- FiscalMonthOfQuarterName (Format: Month 1)
,	('Fiscal Month ' +(CONVERT(VARCHAR(14),
	CASE 
		WHEN Month(@LoopDate) IN (1, 4, 7, 10)	THEN 1
		WHEN Month(@LoopDate) IN (2, 5, 8, 11)	THEN 2
		WHEN Month(@LoopDate) IN (3, 6, 9, 12)	THEN 3
	END)))

---- FiscalQuarterOfYear (Format: 4)
,	CONVERT(TINYINT,
	(CASE 
		WHEN Month(@LoopDate) IN (4, 5, 6)		THEN 1
		WHEN Month(@LoopDate) IN (7, 8, 9)		THEN 2
		WHEN Month(@LoopDate) IN (10, 11, 12)	THEN 3
		WHEN Month(@LoopDate) IN (1, 2, 3)		THEN 4
	END)) 

---- FiscalQuarterOfYearName (Format: Quarter 4)
,	('Fiscal Quarter ' + CONVERT(VARCHAR(16),(
	CASE 
		WHEN Month(@LoopDate) IN (4, 5, 6)		THEN 1
		WHEN Month(@LoopDate) IN (7, 8, 9)		THEN 2
		WHEN Month(@LoopDate) IN (10, 11, 12)	THEN 3
		WHEN Month(@LoopDate) IN (1, 2, 3)		THEN 4
	END)))

---- Season (Format: Spring/Summer/Autumn/Winter)
,	(CASE 
		WHEN Month(@LoopDate) IN (12, 1, 2)		THEN 'Winter'
		WHEN Month(@LoopDate) IN (3, 4, 5)		THEN 'Spring'
		WHEN Month(@LoopDate) IN (6, 7, 8)		THEN 'Summer'
		WHEN Month(@LoopDate) IN (9, 10, 11)	THEN 'Autumn'
	END)

---- Weekday (Weekday/Weekend)
,	(CASE 
		WHEN DATENAME(dw, @LoopDate) = 'Monday'		THEN 'Weekday'
		WHEN DATENAME(dw, @LoopDate) = 'Tuesday'	THEN 'Weekday'
		WHEN DATENAME(dw, @LoopDate) = 'Wednesday'  THEN 'Weekday'
		WHEN DATENAME(dw, @LoopDate) = 'Thursday'	THEN 'Weekday'
		WHEN DATENAME(dw, @LoopDate) = 'Friday'		THEN 'Weekday'
		WHEN DATENAME(dw, @LoopDate) = 'Saturday'	THEN 'Weekend'
		WHEN DATENAME(dw, @LoopDate) = 'Sunday'		THEN 'Weekend'
	END)

---- WorkingDay (Working/Non Working)
,	(CASE 
		WHEN DATENAME(dw, @LoopDate) = 'Monday'		THEN 'Working'
		WHEN DATENAME(dw, @LoopDate) = 'Tuesday'	THEN 'Working'
		WHEN DATENAME(dw, @LoopDate) = 'Wednesday'  THEN 'Working'
		WHEN DATENAME(dw, @LoopDate) = 'Thursday'	THEN 'Working'
		WHEN DATENAME(dw, @LoopDate) = 'Friday'		THEN 'Working'
		WHEN DATENAME(dw, @LoopDate) = 'Saturday'	THEN 'Non Working'
		WHEN DATENAME(dw, @LoopDate) = 'Sunday'		THEN 'Non Working'
	END)

---- Holiday (Bank Holiday/Normal)
,	'Normal'

---- Worked (Worked/Not Worked)
,	'Not Worked'

---- WorkValue (Custom value for each day, e.g. Monday = 1, Saturday = 0.5, etc)
,	CONVERT(DECIMAL(3, 1),
	(CASE 
		WHEN DATENAME(dw, @LoopDate) = 'Monday'		THEN 1
		WHEN DATENAME(dw, @LoopDate) = 'Tuesday'	THEN 1
		WHEN DATENAME(dw, @LoopDate) = 'Wednesday'  THEN 1
		WHEN DATENAME(dw, @LoopDate) = 'Thursday'	THEN 1
		WHEN DATENAME(dw, @LoopDate) = 'Friday'		THEN 1
		WHEN DATENAME(dw, @LoopDate) = 'Saturday'	THEN 0
		WHEN DATENAME(dw, @LoopDate) = 'Sunday'		THEN 0
	END))

---- Today (Yesterday/Today/Tomorrow/NULL)
,	(CASE 
		WHEN DATEDIFF(dd, @LoopDate, GETDATE()) = -1	THEN 'Tomorrow'
		WHEN DATEDIFF(dd, @LoopDate, GETDATE()) =  0	THEN 'Today'
		WHEN DATEDIFF(dd, @LoopDate, GETDATE()) =  1	THEN 'Yesterday'
		ELSE NULL
	END)

---- DaysAgo (0 - Today, 1 - Yesterday etc, -1 - Tomorrow etc)
,	DATEDIFF(dd, @LoopDate, GETDATE())

---- DaysAhead (0 - Today, -1 - Yesterday etc, 1 - Tomorrow etc)
,	DATEDIFF(dd, @LoopDate, GETDATE())*-1

---- ThisWeek (Last Week/This Week/Next Week/NULL)
,	(CASE 
		WHEN DATEDIFF(wk, @LoopDate, GETDATE()) = -1	THEN 'Next Week'
		WHEN DATEDIFF(wk, @LoopDate, GETDATE()) =  0	THEN 'This Week'
		WHEN DATEDIFF(wk, @LoopDate, GETDATE()) =  1	THEN 'Last Week'
		ELSE NULL
	END)

---- WeeksAgo (0 - This Week, 1 - Last Week etc, -1 - Next Week etc)
,	DATEDIFF(wk, @LoopDate, GETDATE())

---- WeeksAhead (0 - This Week, -1 - Last Week etc, 1 - Next Week etc)
,	DATEDIFF(wk, @LoopDate, GETDATE())*-1

---- ThisMonth (Last Month/This Month/Next Month/NULL)
,	(CASE 
		WHEN DATEDIFF(mm, @LoopDate, GETDATE()) = -1	THEN 'Next Month'
		WHEN DATEDIFF(mm, @LoopDate, GETDATE()) =  0	THEN 'This Month'
		WHEN DATEDIFF(mm, @LoopDate, GETDATE()) =  1	THEN 'Last Month'
		ELSE NULL
	END)

---- MonthsAgo (0 - This Month, 1 - Last Month etc, -1 - Next Month etc)
,	DATEDIFF(mm, @LoopDate, GETDATE())

---- MonthsAhead (0 - This Month, -1 - Last Month etc, 1 - Next Month etc)
,	DATEDIFF(mm, @LoopDate, GETDATE())*-1

---- ThisQuarter (Last Quarter/This Quarter/Next Quarter/NULL)
,	(CASE 
		WHEN DATEDIFF(qq, @LoopDate, GETDATE()) = -1	THEN 'Next Quarter'
		WHEN DATEDIFF(qq, @LoopDate, GETDATE()) =  0	THEN 'This Quarter'
		WHEN DATEDIFF(qq, @LoopDate, GETDATE()) =  1	THEN 'Last Quarter'
		ELSE NULL
	END)

---- QuartersAgo (0 - This Quarter, 1 - Last Quarter etc. -1 - Next Quarter etc)
,	DATEDIFF(qq, @LoopDate, GETDATE())

--- QuartersAhead (0 - This Quarter, -1 - Last Quarter etc. 1 - Next Quarter etc)
,	DATEDIFF(qq, @LoopDate, GETDATE())*-1

---- ThisYear (Last Year/This Year/Next Year)
,	(CASE 
		WHEN DATEDIFF(yy, @LoopDate, GETDATE()) = -1	THEN 'Next Year'
		WHEN DATEDIFF(yy, @LoopDate, GETDATE()) =  0	THEN 'This Year'
		WHEN DATEDIFF(yy, @LoopDate, GETDATE()) =  1	THEN 'Last Year'
		ELSE NULL
	END)

---- YearsAgo (0 - This Year, 1 - Last Year etc, -1 - Next Year etc)
,	DATEDIFF(yy, @LoopDate, GETDATE())

---- YearsAhead (0 - This Year, -1 - Last Year etc, 1 - Next Year etc)
,	DATEDIFF(yy, @LoopDate, GETDATE())*-1

---- ThisFiscalQuarter (Last Quarter/This Quarter/Next Quarter/NULL)
,	(CASE 
		WHEN DATEDIFF(qq, @LoopDate, GETDATE()) = -1	THEN 'Next Fiscal Quarter'
		WHEN DATEDIFF(qq, @LoopDate, GETDATE()) =  0	THEN 'This Fiscal Quarter'
		WHEN DATEDIFF(qq, @LoopDate, GETDATE()) =  1	THEN 'Last Fiscal Quarter'
		ELSE NULL
	END)

---- FiscalQuartersAgo (0 - This Quarter, 1 - Last Quarter etc. -1 - Next Quarter etc)
,	DATEDIFF(qq, @LoopDate, GETDATE())

---- FiscalQuartersAhead (0 - This Quarter, -1 - Last Quarter etc. 1 - Next Quarter etc)
,	DATEDIFF(qq, @LoopDate, GETDATE())*-1

---- ThisFiscalYear (Last Year/This Year/Next Year)
,	(CASE 
		WHEN DATEDIFF(yy, @LoopDate, GETDATE()) = -1	THEN 'Next Fiscal Year'
		WHEN DATEDIFF(yy, @LoopDate, GETDATE()) =  0	THEN 'This Fiscal Year'
		WHEN DATEDIFF(yy, @LoopDate, GETDATE()) =  1	THEN 'Last Fiscal Year'
		ELSE NULL
	END)

---- FiscalYearsAgo (0 - This Year, 1 - Last Year etc, -1 - Next Year etc)
,	DATEDIFF(yy, @LoopDate, GETDATE())

---- FiscalYearsAhead (0 - This Year, -1 - Last Year etc, 1 - Next Year etc)
,	DATEDIFF(yy, @LoopDate, GETDATE())*-1

)

-- Increment the LoopDate by 1 day before the loop starts again
SET @LoopDate = DATEADD(d, 1, @LoopDate)
END

SET DATEFIRST @DateFirstNumber

END

-- EXEC usp_Minerva_PopulateDimDate '19000101', '20491201'
GO
