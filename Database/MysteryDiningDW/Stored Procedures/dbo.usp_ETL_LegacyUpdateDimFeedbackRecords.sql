SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_LegacyUpdateDimFeedbackRecords] 
AS

UPDATE DimFeedback SET
	[CompletedDateKey]	= sdf.CompletedDateKey
,	[StartTime]			= sdf.StartTime
,	[EndTime]			= sdf.EndTime
,	[ElapsedTime]		= sdf.ElapsedTime
,	[IPAddress]			= sdf.IPAddress
,	[CookieGUID]		= sdf.CookieGUID
,	[DiscardReason]		= sdf.DiscardReason
,	[DiscardValue]		= sdf.DiscardValue

FROM
	StagingDimFeedback sdf
WHERE
	DimFeedback.FeedbackKey =  sdf.FeedbackKey
AND
	sdf.SCDType = 'Update'
GO
