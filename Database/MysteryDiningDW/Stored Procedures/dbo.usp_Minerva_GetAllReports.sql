SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_Minerva_GetAllReports] 
	@IncludeInactive	BIT
,	@ReportType			TINYINT
AS
BEGIN

DECLARE	@ActiveCondition	NVARCHAR(1)
DECLARE @TypeCondition		NVARCHAR(5)

IF (@IncludeInactive = 1)
		SET @ActiveCondition = '%'
ELSE
		SET @ActiveCondition = 1
	
IF @ReportType = 0
		SET @TypeCondition = '%'
ELSE
		SET @TypeCondition = @ReportType

SELECT
	R.ReportID		AS ID
,	RS.SectionName	AS Section
,	RT.TypeName		AS [Type]
,	R.ReportTitle	AS Title
,	R.SSRSName		AS SSRSName
,	R.Active		AS Active
FROM
	Report R
	INNER JOIN ReportSection	RS ON R.ReportSectionID = RS.ReportSectionID
	INNER JOIN ReportType		RT ON R.ReportTypeID	= RT.ReportTypeID
WHERE
	R.Active			LIKE @ActiveCondition
AND	RT.ReportTypeID		LIKE @TypeCondition			

END
GO
