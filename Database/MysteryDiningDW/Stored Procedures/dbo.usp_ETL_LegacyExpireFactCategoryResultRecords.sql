SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_LegacyExpireFactCategoryResultRecords] 
AS

--Expired Records
UPDATE FactCategoryResult
SET
	[Active] = 0
,	[ValidToDate] = SfCr.ValidToDate

FROM
	FactCategoryResult fR
INNER JOIN
	StagingFactCategoryResult SfCr	ON	fR.[Source]		= SfCr.[Source]
									AND	fR.CategoryID	= SfCr.CategoryID
									AND fR.VisitID		= SfCr.VisitID
									AND SfCr.Type = 'Expired'	
WHERE
	fR.[Active] = 1
AND	fR.[ValidFromDate] <= SfCr.ValidToDate
AND fR.[Source] = 'Legacy'

--SCD1 Changes
UPDATE FactCategoryResult
SET
	[VisitScore] = SfCr.VisitScore
,	[VisitMaxScore] = SfCr.VisitMaxScore
,	[VisitScoreMethod] = SfCr.VisitScoreMethod

FROM
	StagingFactCategoryResult SfCr
INNER JOIN
	FactCategoryResult fR	ON	fR.[Source]		= SfCr.[Source]
							AND	fR.CategoryID	= SfCr.CategoryID
							AND fR.VisitID		= SfCr.VisitID
							--AND SfCr.Type = 'SCD1'	
WHERE
	fR.[Source] = 'Legacy'

-- Update Current Score values on old records

--UPDATE FactSectionResult
--SET
--	[CurrentScore] = SfR.CurrentScore

--FROM
--	FactSectionResult fR
--INNER JOIN
--	StagingFactSectionResult SfR	ON	fR.[Source]		= SfR.[Source]
--									AND	fR.SectionID	= SfR.SectionID
--									AND fR.VisitID		= SfR.VisitID
--WHERE
--	fR.[Source] = 'Legacy'

-- Set Active flag Appropriately

--UPDATE
--    FactCategoryResult
--SET
--    [Active] = 
--				CASE 
--					WHEN active.realmax IS NULL THEN 0
--					ELSE 1
--				END
--FROM
--	FactCategoryResult [source]
--LEFT JOIN
--    (    
--          SELECT
--                MAX(fdr.CategoryResultKey) realmax
--          FROM
--                (
--                  SELECT
--						VisitID,
--                        CategoryID,
--                        [Source],
--                        MAX(ValidFromDate) maxdate
--                  FROM
--                        FactCategoryResult
--                  WHERE
--						[Source] = 'Legacy'
--                  GROUP BY
--                        VisitID,
--                        CategoryID,
--                        [Source]
--                ) x
--                INNER JOIN FactCategoryResult fdr ON x.CategoryID = fdr.CategoryID
--												AND x.VisitID = fdr.VisitID
--												AND x.maxdate = fdr.ValidFromDate
--          GROUP BY
--                fdr.CategoryID
--			,	fdr.VisitID
--			,	fdr.[Source]
--	) active ON [source].CategoryResultKey = active.realmax

-- Test
-- EXEC usp_ETL_LegacyExpireFactCategoryResultRecords

	
GO
