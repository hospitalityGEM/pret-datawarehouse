SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_LegacyExpireDimDishRecords] 
AS

UPDATE DimDish
SET
	[Active] = 0
,	[ValidToDate] = SdD.ValidToDate

FROM
	DimDish dD
INNER JOIN
	StagingDimDish SdD ON	dD.[DishKey] = SdD.[DishKey]
	
WHERE
	dD.[Source] = 'Legacy'

-- Set Active records --
--UPDATE
--    DimDish
--SET
--    [Active] = 
--				CASE 
--					WHEN active.realmax IS NULL THEN 0
--					ELSE 1
--				END
--FROM
--	DimDish [source]
--LEFT JOIN
--    (    
--          SELECT
--                MAX(dd.DishKey) realmax
--          FROM
--                (
--                  SELECT
--						DishID,
--                        [Source],
--                        MAX(ValidFromDate) maxdate
--                  FROM
--                        DimDish
--                  WHERE
--						[Source] = 'Legacy'
--                  GROUP BY
--                        DishID,
--                        [Source]
--                ) x
--                INNER JOIN DimDish dd			ON  x.DishID = dd.DishID
--												AND x.maxdate = dd.ValidFromDate
--          GROUP BY
--				dd.DishID
--			,	dd.[Source]
--	) active ON [source].DishKey = active.realmax

-- Test
-- EXEC usp_ETL_LegacyExpireDimDishRecords
GO
