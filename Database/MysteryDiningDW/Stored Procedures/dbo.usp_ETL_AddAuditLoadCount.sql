SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_ETL_AddAuditLoadCount] 
	@Task				NVARCHAR(50)
,	@MachineName		NVARCHAR(225)
,	@StartTime			DATETIME	= NULL
,	@ExistingInput		INT			= 0
,	@SourceSystemInput	INT			= 0
,	@UnchangedOutput	INT			= 0
,	@NewOutput			INT			= 0
,	@SCD2ExpiredOutput	INT			= 0
,	@SCD2NewOutput		INT			= 0
,	@SCD1UpdateOutput	INT			= 0
,	@InvalidInputOutput	INT			= 0
,	@LastOutputTime		DATETIME	= NULL
AS
BEGIN
	SET NOCOUNT ON
		INSERT INTO AuditLoadCounts 
		(
			Task
		,	MachineName
		,	StartTime
		,	ExistingInput
		,	SourceSystemInput
		,	UnchangedOutput
		,	NewOutput
		,	SCD2ExpiredOutput
		,	SCD2NewOutput
		,	SCD1UpdateOutput
		,	InvalidInputOutput
		,	LastOutputTime
		)
		VALUES
		(
			@Task
		,	@MachineName
		,	ISNULL(@StartTime, GETDATE())
		,	@ExistingInput
		,	@SourceSystemInput
		,	@UnchangedOutput
		,	@NewOutput
		,	@SCD2ExpiredOutput
		,	@SCD2NewOutput
		,	@SCD1UpdateOutput
		,	@InvalidInputOutput
		,	ISNULL(@LastOutputTime, GETDATE())
		)
END
-- Test
-- EXEC usp_ETL_AddAuditLoadCount
	

GO
