SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_DimVisitTruncateStagingTable] 
AS
	-- DimVisit
	TRUNCATE TABLE StagingDimVisit

-- Test Case
-- EXEC usp_ETL_DimVisitTruncateStagingTable
GO
