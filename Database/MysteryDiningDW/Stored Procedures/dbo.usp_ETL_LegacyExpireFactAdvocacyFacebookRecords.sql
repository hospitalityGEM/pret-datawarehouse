SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_LegacyExpireFactAdvocacyFacebookRecords] 
AS

--Expired Records

UPDATE FactAdvocacyFacebook
SET
	[Active] = 0
,	[ValidToDate] = SfAf.ValidToDate

FROM
	FactAdvocacyFacebook fAf
INNER JOIN
	StagingFactAdvocacyFacebook SfAf ON	fAf.AdvocacyFacebookKey = SfAf.AdvocacyFacebookKey
WHERE
	SfAf.[Source] = 'Legacy'
AND SfAf.[Type] = 'Expired'


-- Update SCD 1 records

UPDATE FactAdvocacyFacebook
SET
	[LikedAfter]		= SfAf.LikedAfter
,	[ConvertedToLiker]	= SfAf.ConvertedToLiker
,	[PostedMessage]		= SfAf.PostedMessage
,	[Posted]			= SfAf.Posted

FROM
	FactAdvocacyFacebook fAf
INNER JOIN
	StagingFactAdvocacyFacebook SfAf ON	fAf.AdvocacyFacebookID = SfAf.AdvocacyFacebookID
WHERE
	SfAf.[Source] = 'Legacy'
--AND 
--	SfAf.[Type]	= 'SCD1'

-- Set Active flag Appropriately

--UPDATE
--    FactAdvocacyFacebook
--SET
--    [Active] = 
--				CASE 
--					WHEN active.realmax IS NULL THEN 0
--					ELSE 1
--				END
--FROM
--	FactAdvocacyFacebook [source]
--LEFT JOIN
--    (    
--          SELECT
--                MAX(fdr.AdvocacyFacebookKey) realmax
--          FROM
--                (
--                  SELECT
--						VisitID,
--                        CategoryID,
--                        [Source],
--                        MAX(ValidFromDate) maxdate
--                  FROM
--                        FactAdvocacyFacebook
--                  WHERE
--						[Source] = 'Legacy'
--                  GROUP BY
--                        VisitID,
--                        CategoryID,
--                        [Source]
--                ) x
--                INNER JOIN FactAdvocacyFacebook fdr ON x.CategoryID = fdr.CategoryID
--												AND x.VisitID = fdr.VisitID
--												AND x.maxdate = fdr.ValidFromDate
--          GROUP BY
--                fdr.CategoryID
--			,	fdr.VisitID
--			,	fdr.[Source]
--	) active ON [source].AdvocacyFacebookKey = active.realmax

-- Test
-- EXEC usp_ETL_LegacyExpireFactAdvocacyFacebookRecords

	
GO
