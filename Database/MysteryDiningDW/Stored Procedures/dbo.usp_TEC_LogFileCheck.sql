SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_TEC_LogFileCheck] 
	@FileName			VARCHAR(200)
,	@FilePath			VARCHAR(200)
,	@FileFound			INT
AS

INSERT INTO FileCheckHistory ([FileName], [FilePath], [FileFound])
VALUES
	(@FileName, @FilePath, @FileFound)

-- Test Case
-- EXEC dbo.usp_TEC_LogFileCheck 'Test', 'Test', 10
GO
