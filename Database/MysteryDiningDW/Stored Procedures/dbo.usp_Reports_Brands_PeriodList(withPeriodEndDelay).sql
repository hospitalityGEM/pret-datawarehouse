SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[usp_Reports_Brands_PeriodList(withPeriodEndDelay)]
	@Client			INT
,	@Brand			nvarchar(100)
,	@PeriodEndDelay BIT = 0
,	@ShowExcludeFromAnalysis BIT = 0

AS

SELECT DISTINCT 
      dp.PeriodID  
,     dp.CurrentPeriodName AS PeriodName
,     dp.StartDateKey 
,	  dp.BrandPeriodsAgo


FROM
FactVisitResult fvr
INNER JOIN DimPeriod dp ON fvr.PeriodKey = dp.PeriodKey
INNER JOIN DimBranch db ON fvr.BranchKey=db.BranchKey

WHERE
      dp.ClientID = @Client
AND
	db.CurrentBrand IN (@Brand)			
AND
      fvr.Active = 1
AND
	CASE
		WHEN @ShowExcludeFromAnalysis = 0 AND dp.IncludeInAnalysis = 'Include' THEN 1
		WHEN @ShowExcludeFromAnalysis = 0 AND dp.IncludeInAnalysis <> 'Include' THEN 0
		ELSE 1
	END = 1
AND
	CASE 
		WHEN @PeriodEndDelay = 1 AND dp.ClientPeriodsAgo > 0 THEN 1
		WHEN @PeriodEndDelay = 0 THEN 1
		ELSE 0
	END = 1

ORDER BY
     dp.StartDateKey DESC

GO
