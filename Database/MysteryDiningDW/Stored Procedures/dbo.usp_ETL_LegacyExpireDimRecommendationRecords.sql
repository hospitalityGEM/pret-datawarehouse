SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_LegacyExpireDimRecommendationRecords] 
AS

-- Expire Records --
UPDATE DimRecommendation
SET
	[Active] = 0
,	[ValidToDate] = SdR.ValidToDate

FROM
	DimRecommendation dR
INNER JOIN
	StagingDimRecommendation SdR	ON	dR.[RecommendationKey]	= SdR.[RecommendationKey]
	
WHERE
	SdR.[RowType] = 'Expired'
AND SdR.[Source] = 'Legacy'

-- Update SCD1 Changes --
UPDATE DimRecommendation
SET
	CountOfViews = SdR.ViewCount
FROM
	DimRecommendation dR
INNER JOIN
	StagingDimRecommendation SdR	ON	dR.[Source]	 = SdR.[Source]
								AND dR.[RecommendationID] = SdR.[RecommendationID]
WHERE
--	SdR.[RowType] = 'SCD1'
--AND 
	SdR.[Source] = 'Legacy'

-- Set Active records --
--UPDATE
--    DimRecommendation
--SET
--    [Active] = 
--				CASE 
--					WHEN active.realmax IS NULL THEN 0
--					ELSE 1
--				END
--FROM
--	DimRecommendation [source]
--LEFT JOIN
--    (    
--          SELECT
--                MAX(dr.RecommendationKey) realmax
--          FROM
--                (
--                  SELECT
--						RecommendationID,
--                        MAX(ValidFromDate) maxdate
--                  FROM
--                        DimRecommendation
--                  WHERE
--						[Source] = 'Legacy'
--                  GROUP BY
--                        RecommendationID
--                ) x
--                INNER JOIN DimRecommendation dr ON	x.RecommendationID = dr.RecommendationID
--										AND x.maxdate = dr.ValidFromDate
--          GROUP BY
--                dr.RecommendationKey
--	) active ON [source].RecommendationKey = active.realmax

-- Test
-- EXEC usp_ETL_LegacyExpireDimRecommendationRecords
	
GO
