SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_UpdateFactTimeStamps] 
AS


UPDATE 
	ConfigInfo
SET 
	Value = ld.LatestDate
,	LastUpdated = ld.LatestDate

FROM
	ConfigInfo ci

INNER JOIN
	(
	SELECT
		MAX(DateTimeAdded)	AS LatestDate
	,	27					AS ConfigInfoID
	FROM
		MysteryDiningCopy.dbo.OnlineFeedbackRecommendationFacebook
	UNION
	SELECT
		MAX(DateTimeAdded)	AS LatestDate
	,	26					AS ConfigInfoID
	FROM
		MysteryDiningCopy.dbo.OnlineFeedbackRecommendationTwitter
	UNION
	SELECT
		MAX(DateTimeAdded)	AS LatestDate
	,	25					AS ConfigInfoID
	FROM
		MysteryDiningCopy.dbo.OnlineFeedbackRecommendationEmail
	UNION
	SELECT
		MAX(DateTimeAdded)	AS LatestDate
	,	28					AS ConfigInfoID
	FROM
		MysteryDiningCopy.dbo.OnlineFeedbackRecommendationReturnVisitor
	UNION
	SELECT
		MAX(DateTimeAdded)	AS LatestDate
	,	20					AS ConfigInfoID
	FROM
		MysteryDiningCopy.dbo.OnlineFeedbackLinkTracking
	UNION
	SELECT
		MAX(DateTimeCaptured)	AS LatestDate
	,	29						AS ConfigInfoID
	FROM
		MysteryDiningCopy.dbo.SocialChannelAssignedUserCount
	) ld ON ci.ConfigInfoID = ld.ConfigInfoID
WHERE
	ld.LatestDate IS NOT NULL		  	  
	
-- EXEC usp_ETL_UpdateFactTimeStamps
GO
