SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[DashboardGetReturnRecommendScore]
	-- Add the parameters for the stored procedure here
	@DateFrom datetime, 
	@DateTo datetime, 
	@ClientID int = 0, 
	@BranchIDs nvarchar(max),
	@VisitTypes nvarchar(max),
	@HideCurrentPeriod bit,
	@Benchmark nvarchar(max) = '',
	@QuestionCategory nvarchar(max) = '',
	@QuestionnaireCategory nvarchar(max) = '',
	@Department nvarchar(max) = '',
	@Brand nvarchar(max) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE
		@DateKeyFrom	INT
	,	@DateKeyTo		INT
	,	@DateKeyCurrent	INT
		
	SELECT @DateKeyFrom	= DateKey FROM DimDate WHERE [Date] = CONVERT(NVARCHAR, @DateFrom, 103)
	SELECT @DateKeyTo	= DateKey FROM DimDate WHERE [Date] = CONVERT(NVARCHAR, @DateTo, 103)
	SELECT @DateKeyCurrent	= DateKey FROM DimDate WHERE [Date] = CONVERT(DATE, GETDATE())


	SELECT
		ROUND((SUM(fbr.Score) / SUM(fbr.MaxScore)*100),0) AS [Score]
	FROM
		FactBenchmarkResult fbr
		INNER JOIN DimBranch db ON fbr.BranchKey = db.BranchKey
		INNER JOIN DimVisit  dv	ON fbr.VisitKey	 = dv.VisitKey
		INNER JOIN DimPeriod dp ON fbr.PeriodKey = dp.PeriodKey
	WHERE
			fbr.Benchmark = 'Return/Recommend'
		AND
			(CASE
				WHEN LEN(@QuestionnaireCategory) = 0 THEN 1
				WHEN dv.QuestionnaireCategory IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@QuestionnaireCategory,',')) THEN 1
				ELSE 0
			END) = 1
		AND
			(CASE
				WHEN LEN(@Brand) = 0 THEN 1
				WHEN db.Brand IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@Brand,',')) THEN 1
				ELSE 0
			END) = 1
		AND
			db.BranchID IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@BranchIDs, ','))
		AND
			dv.[Type] IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@VisitTypes, ','))
		AND
			dv.VisitStatus IN ('Reviewed','Complete')
		AND
			dp.StartDateKey >= @DateKeyFrom
		AND
			dp.StartDateKey <= @DateKeyTo	
		AND
			fbr.MaxScore > 0
		AND
			fbr.Active = 1
		AND
			(CASE
				WHEN @HideCurrentPeriod = 1 AND dp.EndDateKey < @DateKeyCurrent THEN 1
				WHEN @HideCurrentPeriod = 0 THEN 1
				ELSE 0
			END) = 1
		AND
			fbr.IncludeInAnalysis = 'Include'
		AND
			dp.IncludeInPortal = 'Include'
END

-- Test Case
-- EXEC [dbo].[DashboardGetReturnRecommendScore] @DateFrom = '09/08/2012', @DateTo = '10/09/2012', @ClientID = 0,  @BranchIDs = '25,28,29,30', @VisitTypes = 'Mystery Visit', @HideCurrentPeriod = 0




GO
