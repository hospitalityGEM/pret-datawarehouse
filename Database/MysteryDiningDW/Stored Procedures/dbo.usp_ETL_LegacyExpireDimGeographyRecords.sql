SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_LegacyExpireDimGeographyRecords] 
AS

UPDATE DimGeography
SET
	[Active] = 0
,	[ValidToDate] = SdG.ValidToDate

FROM
	DimGeography dG
INNER JOIN
	StagingDimGeography SdG ON	dG.[GeographyKey] = SdG.[GeographyKey]
	
WHERE
	dG.[Source] = 'Legacy'

-- Test
-- EXEC usp_ETL_LegacyExpireDimGeographyRecords
GO
