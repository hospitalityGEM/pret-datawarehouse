
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[usp_ETL_LegacyExpireDimVisitRecords] 
AS

-- Expire Records --
UPDATE DimVisit
SET
	[Active] = 0
,	[ValidToDate] = SdV.ValidToDate

FROM
	DimVisit dV
INNER JOIN
	StagingDimVisit SdV	ON	dV.[VisitKey]	= SdV.[VisitKey]
	
WHERE
	SdV.[Type] = 'Expired'
AND SdV.[Source] = 'Legacy'

-- Update SCD1 Changes --
UPDATE DimVisit
SET
	CurrentVisitScore				= SdV.CurrentVisitScore
,	CurrentScore					= SdV.CurrentScore
,	CurrentMaxScore					= SdV.CurrentMaxScore	
,	[PassFail]						= SdV.PassFail
,	[PassFailMessage]				= SdV.PassFailMessage
,	[VisitScoreText]				= SdV.VisitScoreText
,	[VisitScoreMethod]				= SdV.VisitScoreMethod
,	[QuestionnaireCategory]			= SdV.QuestionnaireCategory
,	[AdjustedScore]                 = SdV.AdjustedScore
,	[InitialReviewDateKey]			= SdV.InitialReviewDateKey
,	[LatestReviewDateKey]			= SdV.LatestReviewDateKey
,	[DiscardReason]					= SdV.DiscardReason
,	[DiscardReasonDetail]			= SdV.DiscardReasonDetail
,	[DiscardReasonOrigin]			=SdV.DiscardReasonOrigin
,	[IsNBVisit]                     =SdV.IsNBVisit

FROM
	DimVisit dV
INNER JOIN
	StagingDimVisit SdV	ON	dV.[Source]	 = SdV.[Source]
						AND dV.[VisitID] = SdV.[VisitID]
	
WHERE
--	SdV.[Type] = 'SCD1'
--AND 
	SdV.[Source] = 'Legacy'

-- Update missing scores info
UPDATE
	DimVisit
SET
	CurrentScore = ISNULL(MysteryDiningCopy.dbo.GetNewScore(v.VisitID), 0)
	--CASE
	--	WHEN v.VisitStatus IN ('Reviewed', 'Completed') THEN MysteryDiningCopy.dbo.GetNewScore(v.VisitID)
	--	ELSE 0
	--END
FROM
	DimVisit v
WHERE
	v.CurrentScore IS NULL
AND
	v.[Source] = 'Legacy'

UPDATE
	DimVisit
SET
	CurrentMaxScore = ISNULL(MysteryDiningCopy.dbo.GetNewMaxScore(v.VisitID),0)
	--CASE
	--	WHEN v.VisitStatus IN ('Reviewed', 'Completed') THEN MysteryDiningCopy.dbo.GetNewMaxScore(v.VisitID)
	--	ELSE 0
	--END
FROM
	DimVisit v
WHERE
	v.CurrentMaxScore IS NULL
AND
	v.[Source] = 'Legacy'

-- Set Active records --
--UPDATE
--    DimVisit
--SET
--    [Active] = 
--				CASE 
--					WHEN active.realmax IS NULL THEN 0
--					ELSE 1
--				END
--FROM
--	DimVisit [source]
--LEFT JOIN
--    (    
--          SELECT
--                MAX(dv.VisitKey) realmax
--          FROM
--                (
--                  SELECT
--						VisitID,
--                        MAX(ValidFromDate) maxdate
--                  FROM
--                        DimVisit
--                  WHERE
--						[Source] = 'Legacy'
--                  GROUP BY
--                        VisitID
--                ) x
--                INNER JOIN DimVisit dv ON	x.VisitID = dv.VisitID
--										AND x.maxdate = dv.ValidFromDate
--          GROUP BY
--                dv.VisitKey
--	) active ON [source].VisitKey = active.realmax

-- Update Counts
UPDATE DimVisit SET
	PeriodBranchVisitCount = Counts.PeriodBranchVisitCount
,	PeriodBranchCategoryVisitCount = Counts.PeriodBranchCategoryVisitCount
FROM
	DimVisit dv
	LEFT JOIN
		(
			SELECT	
				v.VisitUID	
			,	RANK() OVER(PARTITION BY v.Period, v.BranchUID, q.VisitType ORDER BY v.VisitDate)					AS PeriodBranchVisitCount
			,	RANK() OVER(PARTITION BY v.Period, v.BranchUID, q.CategoryUID ORDER BY v.VisitDate)	AS PeriodBranchCategoryVisitCount
			FROM
				MysteryDiningCopy.dbo.Visit v
				INNER JOIN MysteryDiningCopy.dbo.Questionnaire q ON v.QuestionnaireUID = q.QuestionnaireUID
			WHERE
				v.VisitStatusUID IN ('V','R','C')
		) Counts ON dv.VisitID = Counts.VisitUID
	

-- Test
-- EXEC usp_ETL_LegacyExpireDimVisitRecords
	





GO
