SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_LegacyExpireFactFeedbackResultRecords] 
AS

--Expired Records

UPDATE FactFeedbackResult
SET
	[Active] = 0
,	[ValidToDate] = SfR.ValidToDate

FROM
	FactFeedbackResult fR
INNER JOIN
	StagingFactFeedbackResult SfR	ON	fR.[Source]		= SfR.[Source]
									AND	fR.FeedbackResultKey = SfR.FeedbackResultKey
									
WHERE
--	fR.[Active] = 1
--AND	
fR.[ValidFromDate] <= SfR.ValidToDate
AND SfR.[Type] = 'Expired'
AND fR.[Source] = 'Legacy'


--Expired Records

UPDATE FactFeedbackResult
SET
	[FeedbackKey] = DfR.FeedbackKey
,	[TimeStarted] = DfR.TimeStarted
,	[TimeEnded]	  = DfR.TimeEnded
,	[IPAddress]   = DfR.IPAddress
,	[CookieGUID]  = DfR.CookieGUID
,	[CompletedDateKey]  = DfR.CompletedDateKey
,	[OfferedAdvocacy]	= DfR.OfferedAdvocacy
,	[AcceptedAdvocacy]	= Dfr.AcceptedAdvocacy
,	[PostedAdvocacy]	= Dfr.PostedAdvocacy
,	[SubscribeAdvocacy]	= Dfr.SubscribeAdvocacy

FROM
	FactFeedbackResult fR
INNER JOIN
	StagingFactFeedbackResult DfR	ON	fR.[Source]		= DfR.[Source]
									AND	fR.FeedbackResultID = DfR.FeedbackResultID									
WHERE
--	DfR.[Type] = 'SCD1'
--AND 
	fR.[Source] = 'Legacy'

-- Set Active flag Appropriately

--UPDATE
--    FactFeedbackResult
--SET
--    [Active] = 
--				CASE 
--					WHEN active.realmax IS NULL THEN 0
--					ELSE 1
--				END
--FROM
--	FactFeedbackResult [source]
--LEFT JOIN
--    (    
--          SELECT
--                MAX(fdr.FeedbackResultKey) realmax
--          FROM
--                (
--                  SELECT
--						VisitID,
--                        FeedbackID,
--                        [Source],
--                        MAX(ValidFromDate) maxdate
--                  FROM
--                        FactFeedbackResult
--                  WHERE
--						[Source] = 'Legacy'
--                  GROUP BY
--                        VisitID,
--                        FeedbackID,
--                        [Source]
--                ) x
--                INNER JOIN FactFeedbackResult fdr ON x.FeedbackID = fdr.FeedbackID
--												AND x.VisitID = fdr.VisitID
--												AND x.maxdate = fdr.ValidFromDate
--          GROUP BY
--                fdr.FeedbackID
--			,	fdr.VisitID
--			,	fdr.[Source]
--	) active ON [source].FeedbackResultKey = active.realmax

-- Test
-- EXEC usp_ETL_LegacyExpireFactFeedbackResultRecords
	
GO
