
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[DashboardGetLeagueTableScores]
-- Add the parameters for the stored procedure here

	@ClientID int = 0, 
	@BranchIDs nvarchar(max),
	@VisitTypes nvarchar(max),
	@HideCurrentPeriod bit,
    @DisplayBy nvarchar(30),
	@Count int = 300,
	@IsTop bit = 1,
	@IsNB bit = 0,
    @FilterBy nvarchar(30)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE 
	@OFPeriodType nvarchar(50),
	@LatestPeriodAgo int =0,
	@PeriodYear	nvarchar(50),
	@BranchIDInts AS IdListInt,
	@BranchIDIntsFiltered AS IdListInt,
	@VisitTypesStrings AS IdListString,
	@OFPFromDate int,
	@OFPToDate int,
	@OFYTDFromDate int,
	@OFYTDToDate int

	INSERT INTO @BranchIDInts
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@BranchIDs,',')

	INSERT INTO @VisitTypesStrings
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@VisitTypes,',')


	SELECT @OFPeriodType= PeriodType
		FROM DimVisit dv 
		INNER JOIN DimPeriod dp ON dv.PeriodKey=dp.PeriodKey
		INNER JOIN DimBranch db  ON dv.BranchKey = db.BranchKey

		WHERE

			db.ClientID = @ClientID	
		AND
			dv.VisitStatus IN ('Reviewed','Complete')
		AND
			dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)
		AND
			dp.ClientPeriodsAgo between @HideCurrentPeriod and @HideCurrentPeriod+1
		AND
			dp.IncludeInAnalysis='Include'
		AND
			dp.IncludeInPortal='Include'
		AND
			dv.type='Online Feedback'
		AND
			dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)

IF 
			@ClientID<>28647 OR 'Online Feedback' NOT IN (SELECT Id FROM @VisitTypesStrings)
	
	SELECT @LatestPeriodAgo= Min(dp.ClientPeriodsAgo) 
			FROM  DimVisit dv 
			INNER JOIN DimPeriod dp1  ON dv.PeriodKey=dp1.PeriodKey 
			INNER JOIN DimPeriod dp ON dp1.PeriodID=dp.periodID and dp.Active=1
		
			WHERE 
				dp.IncludeInAnalysis='Include' 
			AND
				dv.VisitStatus  IN ('Allocated','Unallocated','Reviewed','Visited','Complete')
			AND 
				dp.IncludeInPortal='Include' 
			AND 
				dp.ClientID=@ClientID
			AND
				dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)
			AND
				dp.ClientPeriodsAgo>=@HideCurrentPeriod 
			AND 
				dv.Active=1 
	ELSE
			SELECT
			@LatestPeriodAgo= Min(dp.ClientPeriodsAgo) 
			FROM  DimPeriod dp
		
			WHERE 
				dp.Active=1
			AND
				dp.IncludeInAnalysis='Include' 
			AND 
				dp.IncludeInPortal='Include' 
			AND 
				dp.ClientID=28647
			AND
				dp.PeriodType=@OFPeriodType
			AND
				dp.ClientPeriodsAgo>=@HideCurrentPeriod
		
IF 
			@ClientID<>28647 OR 'Online Feedback' NOT IN (SELECT Id FROM @VisitTypesStrings)
		
		SELECT @PeriodYear= dp.[Year] 

				FROM  DimVisit dv 
				INNER JOIN DimPeriod dp1  ON dv.PeriodKey=dp1.PeriodKey 
				INNER JOIN DimPeriod dp ON dp1.PeriodID=dp.periodID and dp.Active=1
		
				WHERE 
					dp.IncludeInAnalysis='Include' 
				AND
					dv.VisitStatus  IN ('Allocated','Unallocated','Reviewed','Visited','Complete')	
				AND 
					dp.IncludeInPortal='Include' 
				AND 
					dp.ClientID=@ClientID
				AND
					dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)
				AND
					dp.ClientPeriodsAgo=@LatestPeriodAgo
				AND dv.Active=1 
		ELSE
			SELECT
			@PeriodYear= dp.[Year] 
		,	@OFPFromDate=dp.StartDateKey
		,   @OFPToDate=dp.EndDateKey
		,	@OFYTDFromDate=dp.YearStartKey
		,	@OFYTDToDate=dp.YearEndKey

			FROM  DimPeriod dp
		
			WHERE 
				dp.Active=1
			AND
				dp.IncludeInAnalysis='Include' 
			AND 
				dp.IncludeInPortal='Include' 
			AND 
				dp.ClientID=28647
			AND
				dp.PeriodType=@OFPeriodType
			AND
				dp.ClientPeriodsAgo=@LatestPeriodAgo
	

	
	INSERT INTO @BranchIDIntsFiltered
	SELECT db.BranchID FROM DimBranch db 
		INNER JOIN  (select Distinct 
				IIF(@FilterBy='Region' AND @IsNB=0,db.CurrentRegion,'A') as Region
			,	IIF(@FilterBy='Area'  AND @IsNB=0,db.CurrentArea,'A') as Area
			,   IIF(@FilterBy='Region' AND @IsNB=1,db.CurrentNBRegion,'A') as NBRegion
			,	IIF(@FilterBy='Area'  AND @IsNB=1,db.CurrentNBArea,'A') as NBArea
			,	IIF(@FilterBy='Branch',db.BranchID,-1) as BranchID
				FROM DimBranch db 
				WHERE Active=1 AND BranchID = ANY (SELECT Id FROM @BranchIdInts) AND ClientID=@ClientID) BFilter ON BFilter.Area=IIF(@FilterBy='Area' AND @IsNB=0,db.CurrentArea,'A') 
																												AND Bfilter.Region=IIF(@FilterBy='Region' AND @IsNB=0,db.CurrentRegion,'A')
																												AND BFilter.NBArea=IIF(@FilterBy='Area' AND @IsNB=1,db.CurrentNBArea,'A') 
																												AND Bfilter.NBRegion=IIF(@FilterBy='Region' AND @IsNB=1,db.CurrentNBRegion,'A')
																												AND BFilter.BranchID=IIF(@FilterBy='Branch',db.BranchID,-1)
	WHERE ClientID=@ClientID  and db.active=1 
	and (ClosedBranch =0 or ClosedBranch is null)


	
	SELECT
		a.Site
	,	A.YTDScore
	,	a.PeriodScore
	
	FROM
	(
	SELECT
	case  when @DisplayBy='Branch' then db.CurrentBranch
		  when @DisplayBy='Area' AND @IsNB=0 then db.CurrentArea 
		  when @DisplayBy='Region' AND @IsNB=0 then db.CurrentRegion
		  when @DisplayBy='Area' AND @IsNB=1 then db.CurrentNBArea 
		  when @DisplayBy='Region' AND @IsNB=1 then db.CurrentNBRegion
		  when @DisplayBy='Brand'  then db.CurrentBrand
		end as Site
	,	round((cast(sum(fvr.Score) as decimal)/sum(fvr.MaxScore))*100,0) as YTDScore
	,	IIF(sum(fvr.MaxScore*iiF(dp.clientperiodsago=@LatestPeriodAgo,1,0))=0
		   ,null
		   ,ROUND((cast(sum(fvr.Score * iiF(dp.clientperiodsago=@LatestPeriodAgo,1,0)) as decimal)/sum(fvr.MaxScore * iiF(dp.clientperiodsago=@LatestPeriodAgo,1,0)))*100,0)
		 ) as PeriodScore
	,	RANK () OVER (order by 
		      case when @IsTop=1 then 
										IIF(sum(fvr.MaxScore*iiF(dp.clientperiodsago=@LatestPeriodAgo,1,0))=0
											,300
											,ROUND((cast(sum(fvr.Score * iiF(dp.clientperiodsago=@LatestPeriodAgo,1,0)) as decimal)/sum(fvr.MaxScore * iiF(dp.clientperiodsago=@LatestPeriodAgo,1,0)))*-100,0)
											) 
				                 else 
										IIF(sum(fvr.MaxScore*iiF(dp.clientperiodsago=@LatestPeriodAgo,1,0))=0
											,300
											,ROUND((cast(sum(fvr.Score * iiF(dp.clientperiodsago=@LatestPeriodAgo,1,0)) as decimal)/sum(fvr.MaxScore * iiF(dp.clientperiodsago=@LatestPeriodAgo,1,0)))*100,0)
										) 
				end 
		)  as SRank
		
	FROM FactVisitResult fvr

	INNER JOIN DimBranch db WITH (NOLOCK) ON db.BranchKey = fvr.Branchkey 
	INNER JOIN DimVisit dv WITH (NOLOCK) ON fvr.VisitKey	 = dv.VisitKey
	INNER JOIN DimPeriod dp WITH (NOLOCK) ON fvr.PeriodKey = dp.PeriodKey
	
	WHERE
			db.ClientID = @ClientID
		AND
			dv.VisitStatus IN ('Reviewed','Complete')
		AND
			dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)
		AND
			fvr.MaxScore > 0			
		AND
			fvr.Active = 1
		AND
			fvr.IncludeInAnalysis = 'Include'
		AND
			db.BranchID = ANY (SELECT Id FROM @BranchIDIntsFiltered)
		
		AND 
			dp.[Year]= @PeriodYear
		AND
			dp.ClientPeriodsAgo>=@HideCurrentPeriod 
		AND
			(dv.IsNBVisit=1 OR @IsNB=0)
		AND
			(db.ClientID<>28647 OR dv.Type<>'Online Feedback')
GROUP BY 
case  when @DisplayBy='Branch' then db.CurrentBranch
		  when @DisplayBy='Area' AND @IsNB=0 then db.CurrentArea 
		  when @DisplayBy='Region' AND @IsNB=0 then db.CurrentRegion
		  when @DisplayBy='Area' AND @IsNB=1 then db.CurrentNBArea 
		  when @DisplayBy='Region' AND @IsNB=1 then db.CurrentNBRegion
		  when @DisplayBy='Brand'  then db.CurrentBrand
		end
 
)A 
where SRank<=@Count AND PeriodScore is not null

UNION

	SELECT
		a.Site
	,	A.YTDScore
	,	a.PeriodScore

	
	FROM
	(
	SELECT
	case  when @DisplayBy='Branch' then db.CurrentBranch
		  when @DisplayBy='Area' AND @IsNB=0 then db.CurrentArea 
		  when @DisplayBy='Region' AND @IsNB=0 then db.CurrentRegion
		  when @DisplayBy='Area' AND @IsNB=1 then db.CurrentNBArea 
		  when @DisplayBy='Region' AND @IsNB=1 then db.CurrentNBRegion
		  when @DisplayBy='Brand'  then db.CurrentBrand
		end as Site
	,	round((cast(sum(fvr.Score) as decimal)/sum(fvr.MaxScore))*100,0) as YTDScore
	,	IIF(sum(fvr.MaxScore*iiF(ffr.CompletedDateKey >= @OFPFromDate,1,0))=0
		   ,null
		   ,ROUND((cast(sum(fvr.Score * iiF(ffr.CompletedDateKey >= @OFPFromDate,1,0)) as decimal)/sum(fvr.MaxScore * iiF(ffr.CompletedDateKey >= @OFPFromDate,1,0)))*100,0)
		 ) as PeriodScore
	,	RANK () OVER (order by 
		      case when @IsTop=1 then 
										IIF(sum(fvr.MaxScore*iiF(ffr.CompletedDateKey >= @OFPFromDate,1,0))=0
											,300
											,ROUND((cast(sum(fvr.Score * iiF(ffr.CompletedDateKey >= @OFPFromDate,1,0)) as decimal)/sum(fvr.MaxScore * iiF(ffr.CompletedDateKey >= @OFPFromDate,1,0)))*-100,0)
											) 
				                 else 
										IIF(sum(fvr.MaxScore*iiF(ffr.CompletedDateKey >= @OFPFromDate,1,0))=0
											,300
											,ROUND((cast(sum(fvr.Score * iiF(ffr.CompletedDateKey >= @OFPFromDate,1,0)) as decimal)/sum(fvr.MaxScore * iiF(ffr.CompletedDateKey >= @OFPFromDate,1,0)))*100,0)
										) 
				end 
		)  as SRank
		
	FROM FactVisitResult fvr

	INNER JOIN DimBranch db WITH (NOLOCK) ON db.BranchKey = fvr.Branchkey 
	INNER JOIN DimVisit dv WITH (NOLOCK) ON fvr.VisitKey	 = dv.VisitKey
	INNER JOIN FactFeedbackResult ffr ON ffr.FeedbackResultID=fvr.VisitID and ffr.active=1
	
	WHERE
			db.ClientID = @ClientID
		AND
			dv.VisitStatus IN ('Reviewed','Complete')
		AND
			dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)
		AND
			fvr.MaxScore > 0			
		AND
			fvr.Active = 1
		AND
			fvr.IncludeInAnalysis = 'Include'
		AND
			db.BranchID = ANY (SELECT Id FROM @BranchIDIntsFiltered)
		
		AND 
			ffr.CompletedDateKey >= @OFYTDFromDate 	AND ffr.CompletedDateKey <=  @OFPToDate

		AND
			(dv.IsNBVisit=1 OR @IsNB=0)
		AND
			db.ClientID=28647 
		AND
			dv.Type='Online Feedback'
GROUP BY 
case  when @DisplayBy='Branch' then db.CurrentBranch
		  when @DisplayBy='Area' AND @IsNB=0 then db.CurrentArea 
		  when @DisplayBy='Region' AND @IsNB=0 then db.CurrentRegion
		  when @DisplayBy='Area' AND @IsNB=1 then db.CurrentNBArea 
		  when @DisplayBy='Region' AND @IsNB=1 then db.CurrentNBRegion
		  when @DisplayBy='Brand'  then db.CurrentBrand
		end
 
)A 
where SRank<=@Count  AND PeriodScore is not null

order by PeriodScore desc

OPTION(OPTIMIZE FOR UNKNOWN)
END

-- TEXT CASE:EXEC [dbo].[DashboardGetLeagueTableScores] @ClientID=12,@BranchIDs= '2880,733,2967,550,94,238,2879,4669,1083,3711,966,5054,766,398,401,157', @VisitTypes ='Mystery Visit',@HideCurrentPeriod=1, @DisplayBy ='Area',@FilterBy='Region',@IsNB=0,@Count=100,@IsTop=0


GO
