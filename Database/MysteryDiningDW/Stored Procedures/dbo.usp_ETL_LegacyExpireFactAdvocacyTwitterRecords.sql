SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_LegacyExpireFactAdvocacyTwitterRecords] 
AS

--Expired Records

UPDATE FactAdvocacyTwitter
SET
	[Active] = 0
,	[ValidToDate] = SfAt.ValidToDate

FROM
	FactAdvocacyTwitter fAt
INNER JOIN
	StagingFactAdvocacyTwitter SfAt ON	fAt.AdvocacyTwitterKey = SfAt.AdvocacyTwitterKey
WHERE
	SfAt.[Source] = 'Legacy'
AND SfAt.[Type] = 'Expired'


-- Update SCD 1 records

UPDATE FactAdvocacyTwitter
SET
	[FollowingAfter]		= SfAt.FollowingAfter
,	[ConvertedToFollower]	= SfAt.ConvertedToFollower
,	[Tweeted]				= SfAt.Tweeted
,	[Tweet]					= SfAt.Tweet

FROM
	FactAdvocacyTwitter fAt
INNER JOIN
	StagingFactAdvocacyTwitter SfAt ON	fAt.AdvocacyTwitterID = SfAt.AdvocacyTwitterID
WHERE
	SfAt.[Source] = 'Legacy'
--AND 
--	SfAt.[Type]	= 'SCD1'

-- Set Active flag Appropriately

--UPDATE
--    FactAdvocacyTwitter
--SET
--    [Active] = 
--				CASE 
--					WHEN active.realmax IS NULL THEN 0
--					ELSE 1
--				END
--FROM
--	FactAdvocacyTwitter [source]
--LEFT JOIN
--    (    
--          SELECT
--                MAX(fdr.AdvocacyTwitterKey) realmax
--          FROM
--                (
--                  SELECT
--						VisitID,
--                        CategoryID,
--                        [Source],
--                        MAX(ValidFromDate) maxdate
--                  FROM
--                        FactAdvocacyTwitter
--                  WHERE
--						[Source] = 'Legacy'
--                  GROUP BY
--                        VisitID,
--                        CategoryID,
--                        [Source]
--                ) x
--                INNER JOIN FactAdvocacyTwitter fdr ON x.CategoryID = fdr.CategoryID
--												AND x.VisitID = fdr.VisitID
--												AND x.maxdate = fdr.ValidFromDate
--          GROUP BY
--                fdr.CategoryID
--			,	fdr.VisitID
--			,	fdr.[Source]
--	) active ON [source].AdvocacyTwitterKey = active.realmax

-- Test
-- EXEC usp_ETL_LegacyExpireFactAdvocacyTwitterRecords

	
GO
