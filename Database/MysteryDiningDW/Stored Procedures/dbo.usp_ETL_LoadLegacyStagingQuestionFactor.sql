SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_LoadLegacyStagingQuestionFactor] 
AS

TRUNCATE TABLE StagingQuestionFactor

INSERT INTO StagingQuestionFactor (QuestionUID, Factor)
SELECT
	q.QuestionUID
,	CONVERT(NVARCHAR(100),
		(Substring((
					CASE 
						WHEN (Substring(q.Question,1, CHARINDEX(' ', q.Question))) IN ('WELCOME','RECOMMEND','PACE','INTEREST','CONFIDENCE','KNOWLEDGE','VALUE','WARMTH','TRAINING ')
							THEN (Substring(q.Question,1, CHARINDEX(' ', q.Question)))
						ELSE 'NONE'
					END),1,1) 
		+ Lower(Substring((
					CASE 
						WHEN (Substring(q.Question,1, CHARINDEX(' ', q.Question))) IN ('WELCOME','RECOMMEND','PACE','INTEREST','CONFIDENCE','KNOWLEDGE','VALUE','WARMTH','TRAINING ')
							THEN (Substring(q.Question,1, CHARINDEX(' ', q.Question)))
						ELSE 'NONE'
					END),2,
		Len((
					CASE 
						WHEN (Substring(q.Question,1, CHARINDEX(' ', q.Question))) IN ('WELCOME','RECOMMEND','PACE','INTEREST','CONFIDENCE','KNOWLEDGE','VALUE','WARMTH','TRAINING ')
							THEN (Substring(q.Question,1, CHARINDEX(' ', q.Question)))
						ELSE 'NONE'
					END)))))) AS Factor

FROM
        MysteryDiningCopy.dbo.Question q
 
 -- Test Case
 -- EXEC usp_ETL_LoadLegacyStagingQuestionFactor
GO
