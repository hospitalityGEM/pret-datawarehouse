SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_DimAssessorTruncateStagingTable] 
AS
	-- DimAssessor
	TRUNCATE TABLE StagingDimAssessor

-- Test Case
-- EXEC usp_ETL_DimAssessorTruncateStagingTable
GO
