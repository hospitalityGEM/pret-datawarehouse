SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_Minerva_GetAllReportTypes]
	@IncludeInactive	BIT = 0
AS

DECLARE @ActiveCondition NVARCHAR(1)

IF @IncludeInactive = 1
	SET @ActiveCondition = '%'
ELSE
	SET @ActiveCondition = 1

SELECT
	ReportTypeID
,	TypeName
FROM
	ReportType
WHERE
	Active LIKE @ActiveCondition
GO
