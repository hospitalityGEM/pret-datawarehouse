SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_UpdateFactsEarliestDate] 
AS

DECLARE @EarliestDate DATETIME

SELECT @EarliestDate =		MAX([TimeStamp])
						FROM
							MysteryDiningCopy.dbo.VisitUpdate
						

UPDATE 
	ConfigInfo
SET 
	Value = @EarliestDate
,	LastUpdated = @EarliestDate
WHERE
	ConfigInfoID IN (
		4
	,	5
	,	6
	,	7
	,	8
	,	9
	,	10
	,	12
	,	18
	,	11
			  )
			  
DECLARE @MaxLogID INT

SELECT @MaxLogID = MAX(AssessorHistoryID) FROM MysteryDiningDW.dbo.FactAssessorHistory

UPDATE 
	ConfigInfo
SET 
	Value = @MaxLogID
,	LastUpdated = GETDATE()
WHERE
	ConfigInfoID = 21
			  
	
-- EXEC usp_ETL_UpdateFactsEarliestDate
GO
