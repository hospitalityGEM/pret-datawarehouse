SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_FactSectionResultTruncateStagingTable] 
AS
	-- FactSectionResult
	TRUNCATE TABLE StagingFactSectionResult

-- Test Case
-- EXEC usp_ETL_FactSectionResultTruncateStagingTable
GO
