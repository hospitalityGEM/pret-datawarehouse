SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_FactLinkTrackingLatestDateLoaded] 
AS

UPDATE 
	ConfigInfo
SET 
	Value = ld.LatestDate
,	LastUpdated = ld.LatestDate

FROM
	ConfigInfo ci

INNER JOIN
	(
	SELECT
		MAX(DateTimeAdded)	AS LatestDate
	,	20					AS ConfigInfoID
	FROM
		MysteryDiningCopy.dbo.OnlineFeedbackLinkTracking
	) ld ON ci.ConfigInfoID = ld.ConfigInfoID
WHERE
	ld.LatestDate IS NOT NULL
	
-- EXEC usp_ETL_FactLinkTrackingLatestDateLoaded
GO
