SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[DashboardGetChartDataCounts]
-- Add the parameters for the stored procedure here
	@DateFrom datetime,
	@DateTo datetime,
	@ClientID int = 0, 
	@PeriodsAgo int = -1, -- -1 if using year to date 
	@BranchIDs nvarchar(max),
	@VisitTypes nvarchar(max),
	@HideCurrentPeriod bit,
	@Benchmark nvarchar(max) = '',
	@QuestionCategory nvarchar(max) = '',
	@QuestionnaireCategory nvarchar(max) = '',
	@Department nvarchar(max) = '',
	@Brand nvarchar(max) = '',
	@ShowCompanyScore bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE
		@DateKeyFrom	INT
	,	@DateKeyTo		INT
	,	@DateKeyCurrent	INT
		
	SELECT @DateKeyFrom		= DateKey FROM DimDate WHERE [Date] = CONVERT(DATE, @DateFrom)
	SELECT @DateKeyTo		= DateKey FROM DimDate WHERE [Date] = CONVERT(DATE, @DateTo)
	SELECT @DateKeyCurrent	= DateKey FROM DimDate WHERE [Date] = CONVERT(DATE, GETDATE()) 

	DECLARE @Results TABLE
	(
		BranchID	INT
	,	Label		NVARCHAR(200)
	,	StartDateKey INT
	,	Value		DECIMAL(18,2)
	)

	INSERT INTO @Results
	SELECT
		BranchID
	,	Visits.PeriodName		AS [Label]
	,	Visits.StartDateKey		AS StartDateKey
	,	COUNT(Visits.VisitID)	AS [Value]
	FROM
		(
		SELECT DISTINCT
			dp.PeriodName
		,	dp.StartDateKey
		,	db.BranchID
		,	dv.VisitID
		FROM
			FactQuestionResult fqr
			INNER JOIN DimBranch db ON fqr.BranchKey = db.BranchKey
			INNER JOIN DimBranch cdb ON db.BranchID = cdb.BranchID AND cdb.Active = 1
			INNER JOIN DimVisit dv	ON fqr.VisitKey	 = dv.VisitKey
			INNER JOIN DimPeriod dp ON fqr.PeriodKey = dp.PeriodKey
			INNER JOIN DimQuestion dq ON fqr.QuestionKey = dq.QuestionKey
		WHERE
				db.ClientID = @ClientID
			AND
				dv.VisitStatus IN ('Reviewed','Complete')
			AND
				cdb.BranchStatus = 'Live'
			AND
				dv.[Type] IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable( @VisitTypes, ','))
			AND
				(CASE
					WHEN @PeriodsAgo = -1 THEN
						CASE
							WHEN
									dp.StartDateKey >= @DateKeyFrom
								AND
									dp.StartDateKey <= @DateKeyTo
							THEN 1
							ELSE 0
						END
					WHEN @PeriodsAgo = 0 THEN
						CASE
							WHEN dp.ClientPeriodsAgo = @PeriodsAgo THEN 1
							ELSE 0
						END
					ELSE
						CASE
							WHEN 
								dp.ClientPeriodsAgo <= @PeriodsAgo 
							AND
								dp.ClientPeriodsAgo	>= 0
							THEN 1
							ELSE 0
						END
				END) = 1
			AND
				fqr.MaxScore > 0
			AND
				(CASE
					WHEN @HideCurrentPeriod = 1 AND dp.EndDateKey < @DateKeyCurrent THEN 1
					WHEN @HideCurrentPeriod = 0 THEN 1
					ELSE 0
				END) = 1
			AND
				fqr.Active = 1
			AND
				fqr.IncludeInAnalysis = 'Include'
			AND
				fqr.IncludeScoreInTotal = 1
			AND
				dp.IncludeInPortal = 'Include'
			AND
				dq.HiddenSection = 'Visible'
			--AND
			--	dq.MenuQuestion = 'N/A'
			AND
				dq.HiddenQuestion = 'Visible'
			AND
				(CASE
					WHEN LEN(@Benchmark) = 0 THEN 1
					WHEN dq.Benchmark IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@Benchmark,',')) THEN 1
					ELSE 0
				END) = 1
			AND
				(CASE
					WHEN LEN(@QuestionCategory) = 0 THEN 1
					WHEN dq.ManagementCategory IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@QuestionCategory,',')) THEN 1
					ELSE 0
				END) = 1
			AND
				(CASE
					WHEN LEN(@QuestionnaireCategory) = 0 THEN 1
					WHEN dq.QuestionnaireCategory IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@QuestionnaireCategory,',')) THEN 1
					ELSE 0
				END) = 1
			AND
				(CASE
					WHEN LEN(@Department) = 0 THEN 1
					WHEN dq.Department IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@Department,',')) THEN 1
					ELSE 0
				END) = 1
			AND
				(CASE
					WHEN LEN(@Brand) = 0 THEN 1
					WHEN db.Brand IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@Brand,',')) THEN 1
					ELSE 0
				END) = 1
		) Visits
	GROUP BY
		Visits.BranchID
	,	Visits.PeriodName
	,	Visits.StartDateKey
	ORDER BY
		Visits.StartDateKey
	

	SELECT
		r.Label							AS Label
	,	CONVERT(INT,SUM(r.Value))		AS Value
	,	CONVERT(DECIMAL(18,2),cv.CompanyValue)	AS CompanyValue
	FROM
		@Results r
		INNER JOIN 
		(
			SELECT
				Label
			,	CASE
					WHEN @ShowCompanyScore = 0 THEN 0
					ELSE SUM(Value)			
				END	AS [CompanyValue]
			FROM
				@Results				
			GROUP BY
				Label
		) cv ON r.Label = cv.Label
	WHERE
		BranchID IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@BranchIDs,','))
	GROUP BY
		r.Label
	,	r.StartDateKey
	,	cv.CompanyValue
	ORDER BY
		r.StartDateKey

END

-- Test Case
-- EXEC [dbo].[DashboardGetChartDataCounts] 	@DateFrom = '',	@DateTo = '',	@ClientID = 18867, 	@BranchIDs = '3775,3776,3778,5589', @VisitTypes = 'Mystery Visit', @HideCurrentPeriod = 0, @PeriodsAgo = 12





GO
