SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_FactFeedbackResultTruncateStagingTable] 
AS
	-- FactFeedbackResult
	TRUNCATE TABLE StagingFactFeedbackResult

-- Test Case
-- EXEC usp_ETL_FactFeedbackResultTruncateStagingTable
GO
