
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[DashboardGetCategoryTableScores]
       @DateFrom datetime, 
	   @DateTo datetime, 
       @ClientID int = 0, 
       @BranchIDs nvarchar(max),
	   @GroupBy nvarchar(30)='Period',
       @VisitTypes nvarchar(max),
       @HideCurrentPeriod bit,
       @QuestionCategory nvarchar(max) = '', 
       @Questionnaire nvarchar(max) = '',   
	   @IsNB bit =0

AS BEGIN

SET NOCOUNT ON;

DECLARE
		@DateKeyFrom	INT = cast(CONVERT(VARCHAR(19),@DateFrom,112) as int)
	,	@DateKeyTo		INT = cast(CONVERT(VARCHAR(19),@DateTo,112) as int)
    ,   @BranchIDInts AS IdListInt
    ,   @VisitTypesStrings AS IdListString
	,	@QuestionCategoriesString as IdListString
	,	@QuestionnaireString as IdListString

INSERT INTO @BranchIDInts
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@BranchIDs,',')

INSERT INTO @VisitTypesStrings
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@VisitTypes,',')

INSERT INTO @QuestionCategoriesString
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@QuestionCategory,',')

INSERT INTO @QuestionnaireString
	SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@Questionnaire,',')

	DECLARE @OFPeriodType nvarchar(50)

	SELECT @OFPeriodType= PeriodType
		FROM DimVisit dv 
		INNER JOIN DimPeriod dp ON dv.PeriodKey=dp.PeriodKey
		INNER JOIN DimBranch db  ON dv.BranchKey = db.BranchKey

		WHERE

			db.ClientID = @ClientID	
		AND
			dv.VisitStatus IN ('Reviewed','Complete')
		AND
			dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)
		AND
			dp.ClientPeriodsAgo between @HideCurrentPeriod and @HideCurrentPeriod+3
		AND
			dp.IncludeInAnalysis='Include'
		AND
			dp.IncludeInPortal='Include'
		AND
			dv.type='Online Feedback'
		AND
			dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)


SELECT C.CategoryName,P.PeriodName,P.PeriodsAgo,S.Score 

FROM 
		(SELECT DISTINCT dp.CurrentPeriodName as PeriodName,dp.ClientPeriodsAgo as PeriodsAgo,dp.PeriodID
		
		FROM  DimVisit dv 
		INNER JOIN DimPeriod dp  ON dv.PeriodKey=dp.PeriodKey 
		
		WHERE 
			@GroupBy='Period'
		AND
			dp.IncludeInAnalysis='Include' 
		AND
			dv.VisitStatus  IN ('Allocated','Unallocated','Reviewed','Visited','Complete')
		AND 
			dp.IncludeInPortal='Include' 
		AND 
			dp.ClientID=@ClientID
		AND
			dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)
		AND
			dp.ClientPeriodsAgo between @HideCurrentPeriod and @HideCurrentPeriod+3
		AND 
			dv.Active=1 
		AND
			(case when Len(@Questionnaire)=0 then 1 when  dv.Questionnaire = ANY (SELECT Id FROM @QuestionnaireString) then 1 else 0 end )=1	
		AND
			(dp.ClientID<>28647 OR dv.Type<>'Online Feedback')	
		
		UNION
		
		SELECT DISTINCT dp.CurrentPeriodName as PeriodName,dp.ClientPeriodsAgo as PeriodsAgo,dp.PeriodID	
	
		FROM  FactFeedbackResult ffr
		INNER JOIN DimPeriod dp  ON ffr.CompletedDateKey between dp.StartDateKey and dp.EndDateKey 
		INNER JOIN DimBranch db ON db.Branchkey=ffr.BranchKey

		WHERE 
			@GroupBy='Period'
		AND
			dp.IncludeInAnalysis='Include' 
		AND 
			dp.IncludeInPortal='Include' 
		AND 
			dp.ClientID=@ClientID
		AND
			db.ClientID=@ClientID
		AND
			dp.PeriodType = @OFPeriodType
		AND
			'Online Feedback' = ANY (SELECT Id FROM @VisitTypesStrings)
		AND
			dp.ClientPeriodsAgo between @HideCurrentPeriod and @HideCurrentPeriod+3
		AND 
			ffr.Active=1 
		AND
			(case when Len(@Questionnaire)=0 then 1 when  ffr.Questionnaire = ANY (SELECT Id FROM @QuestionnaireString) then 1 else 0 end )=1	
		AND
			dp.ClientID=28647 	

		)P

	CROSS JOIN 
 
		(SELECT DISTINCT  fcr.Category as CategoryName
		
		FROM FactCategoryResult fcr 
		INNER JOIN DimBranch db ON fcr.BranchKey = db.BranchKey 
		INNER JOIN DimPeriod dp ON fcr.PeriodKey = dp.PeriodKey
		INNER JOIN DimVisit dv ON fcr.VisitKey = dv.VisitKey
		
		WHERE
			@GroupBy='Period'
		AND
			db.ClientID = @ClientID
		AND
			dv.VisitStatus IN ('Reviewed','Complete')
		AND
			dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)
		AND
			db.BranchID = ANY (SELECT Id FROM @BranchIDInts)
		AND
			dp.ClientPeriodsAgo between @HideCurrentPeriod and @HideCurrentPeriod+3
		AND
			fcr.MaxScore > 0
		AND
			fcr.Active = 1
		AND
			fcr.IncludeInAnalysis='Include'	
		AND
			dp.IncludeInPortal='Include'
		AND
			dp.IncludeInAnalysis='Include'
		AND
			(dv.IsNBVisit=1 OR @IsNB=0)
		AND
			(case when Len(@Questionnaire)=0 then 1 when  dv.Questionnaire = ANY (SELECT Id FROM @QuestionnaireString) then 1 else 0 end )=1
		AND
			(case when Len(@QuestionCategory)=0 then 1 when  fcr.Category = ANY (SELECT Id FROM @QuestionCategoriesString) then 1 else 0 end)=1
		AND
			(db.ClientID<>28647 OR dv.Type<>'Online Feedback')

		UNION

		SELECT DISTINCT  fcr.Category as CategoryName
		
		FROM FactCategoryResult fcr 
		INNER JOIN DimBranch db ON fcr.BranchKey = db.BranchKey 
		INNER JOIN FactFeedbackResult ffr ON ffr.FeedbackResultID=fcr.VisitID and ffr.active=1
		INNER JOIN DimPeriod dp ON ffr.CompletedDateKey between dp.StartDateKey and dp.EndDateKey
		INNER JOIN DimVisit dv ON fcr.VisitKey = dv.VisitKey
		
		WHERE
			@GroupBy='Period'
		AND
			db.ClientID = @ClientID
		AND
			dv.VisitStatus IN ('Reviewed','Complete')
		AND
			dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)
		AND
			db.BranchID = ANY (SELECT Id FROM @BranchIDInts)
		AND
			dp.ClientPeriodsAgo between @HideCurrentPeriod and @HideCurrentPeriod+3
		AND
			dp.ClientID= @ClientID
		AND
			dp.PeriodType = @OFPeriodType
		AND
			fcr.MaxScore > 0
		AND
			fcr.Active = 1
		AND
			fcr.IncludeInAnalysis='Include'	
		AND
			dp.IncludeInPortal='Include'
		AND
			dp.IncludeInAnalysis='Include'
		AND
			(dv.IsNBVisit=1 OR @IsNB=0)
		AND
			(case when Len(@Questionnaire)=0 then 1 when  dv.Questionnaire = ANY (SELECT Id FROM @QuestionnaireString) then 1 else 0 end )=1
		AND
			(case when Len(@QuestionCategory)=0 then 1 when  fcr.Category = ANY (SELECT Id FROM @QuestionCategoriesString) then 1 else 0 end)=1
		AND
			db.ClientID=28647 
		AND
			dv.Type='Online Feedback'

		)C

	LEFT JOIN 

		(SELECT
				fcr.Category as CategoryName
			,	dp.PeriodID
			,	CONVERT(DECIMAL(5,0),(SUM(fcr.Score)/SUM(fcr.MaxScore))*100)  as Score

		FROM FactCategoryResult fcr 
	
			INNER JOIN DimBranch db ON fcr.BranchKey = db.BranchKey 
			INNER JOIN DimPeriod dp ON fcr.PeriodKey = dp.PeriodKey
			INNER JOIN DimVisit dv ON fcr.VisitKey = dv.VisitKey
			WHERE
					@GroupBy='Period'
				AND
					db.ClientID = @ClientID
				AND
					dv.VisitStatus IN ('Reviewed','Complete')
				AND
					dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)
				AND
					db.BranchID = ANY (SELECT Id FROM @BranchIDInts)
				AND
					dp.ClientPeriodsAgo between @HideCurrentPeriod and @HideCurrentPeriod+3
				AND
					fcr.MaxScore > 0
				AND
					fcr.Active = 1
				AND
					fcr.IncludeInAnalysis='Include'	
				AND
					dp.IncludeInPortal='Include'
				AND
					(dv.IsNBVisit=1 OR @IsNB=0)
				AND
					(case when Len(@Questionnaire)=0 then 1 when  dv.Questionnaire = ANY (SELECT Id FROM @QuestionnaireString) then 1 else 0 end )=1
				AND
					(case when Len(@QuestionCategory)=0 then 1 when  fcr.Category = ANY (SELECT Id FROM @QuestionCategoriesString) then 1 else 0 end)=1
				AND
					(db.ClientID<>28647 OR dv.Type<>'Online Feedback')
			GROUP BY 
				fcr.Category
			,	dp.PeriodID

			UNION

			SELECT
				fcr.Category as CategoryName
			,	dp.PeriodID
			,	CONVERT(DECIMAL(5,0),(SUM(fcr.Score)/SUM(fcr.MaxScore))*100)  as Score

			FROM FactCategoryResult fcr 
	
			INNER JOIN DimBranch db ON fcr.BranchKey = db.BranchKey 
			INNER JOIN FactFeedbackResult ffr ON ffr.FeedbackResultID=fcr.VisitID and ffr.active=1
			INNER JOIN DimPeriod dp ON ffr.CompletedDateKey between dp.StartDateKey and dp.EndDateKey
			INNER JOIN DimVisit dv ON fcr.VisitKey = dv.VisitKey
			WHERE
					@GroupBy='Period'
				AND
					db.ClientID = @ClientID
				AND
					dv.VisitStatus IN ('Reviewed','Complete')
				AND
					dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)
				AND
					db.BranchID = ANY (SELECT Id FROM @BranchIDInts)
				AND
					dp.ClientPeriodsAgo between @HideCurrentPeriod and @HideCurrentPeriod+3
				AND
					dp.ClientID = @ClientID
				AND
					fcr.MaxScore > 0
				AND
					fcr.Active = 1
				AND
					fcr.IncludeInAnalysis='Include'	
				AND
					dp.IncludeInPortal='Include'
				AND
					dp.IncludeInAnalysis='Include'
				AND
					dp.PeriodType = @OFPeriodType
				AND
					(dv.IsNBVisit=1 OR @IsNB=0)
				AND
					(case when Len(@Questionnaire)=0 then 1 when  dv.Questionnaire = ANY (SELECT Id FROM @QuestionnaireString) then 1 else 0 end )=1
				AND
					(case when Len(@QuestionCategory)=0 then 1 when  fcr.Category = ANY (SELECT Id FROM @QuestionCategoriesString) then 1 else 0 end)=1
				AND
					db.ClientID=28647
				AND
					dv.Type='Online Feedback'
			GROUP BY 
				fcr.Category
			,	dp.PeriodID
			)S ON S.CategoryName=C.CategoryName and S.PeriodID=P.PeriodID

UNION

 
SELECT
	fcr.Category as CategoryName
,	case When @GroupBy='Area' then db.CurrentArea 
	else 'A' 
	end as PeriodName
,	-1 as PeriodsAgo
,	CONVERT(DECIMAL(5,0),(SUM(fcr.Score)/SUM(fcr.MaxScore))*100)  as Score

FROM FactCategoryResult fcr 
	
	INNER JOIN DimBranch db ON fcr.BranchKey = db.BranchKey 
	INNER JOIN DimPeriod dp ON fcr.PeriodKey = dp.PeriodKey
	INNER JOIN DimVisit dv ON fcr.VisitKey = dv.VisitKey
	WHERE
			@GroupBy<>'Period'
		AND
			db.ClientID = @ClientID
		AND
			dv.VisitStatus IN ('Reviewed','Complete')
		AND
			dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)
		AND
			db.BranchID = ANY (SELECT Id FROM @BranchIDInts)
		AND	
			dp.StartDateKey >= @DateKeyFrom
		AND
			dp.StartDateKey <= @DateKeyTo
		AND
			fcr.MaxScore > 0
		AND
			fcr.Active = 1
		AND
			fcr.IncludeInAnalysis='Include'	
		AND
			dp.IncludeInPortal='Include'
		AND
			(dv.IsNBVisit=1 OR @IsNB=0)
		AND
			(case when Len(@Questionnaire)=0 then 1 when  dv.Questionnaire = ANY (SELECT Id FROM @QuestionnaireString) then 1 else 0 end )=1
		AND
			(case when Len(@QuestionCategory)=0 then 1 when  fcr.Category = ANY (SELECT Id FROM @QuestionCategoriesString) then 1 else 0 end)=1
		AND
			(db.ClientID<>28647 OR dv.Type<>'Online Feedback')
	
	GROUP BY 
		fcr.Category 
,	case When @GroupBy='Area' then db.CurrentArea 
	else 'A' 
	end

UNION

SELECT
	fcr.Category as CategoryName
,	case When @GroupBy='Area' then db.CurrentArea 
	else 'A' 
	end as PeriodName
,	-1 as PeriodsAgo
,	CONVERT(DECIMAL(5,0),(SUM(fcr.Score)/SUM(fcr.MaxScore))*100)  as Score

FROM FactCategoryResult fcr 
	
	INNER JOIN DimBranch db ON fcr.BranchKey = db.BranchKey 
	INNER JOIN FactFeedbackResult ffr ON ffr.FeedbackResultID=fcr.VisitID and ffr.active=1
	INNER JOIN DimPeriod dp ON ffr.CompletedDateKey between dp.StartDateKey and dp.EndDateKey
	INNER JOIN DimVisit dv ON fcr.VisitKey = dv.VisitKey
	WHERE
			@GroupBy<>'Period'
		AND
			db.ClientID = @ClientID
		AND
			dv.VisitStatus IN ('Reviewed','Complete')
		AND
			dv.Type = ANY (SELECT Id FROM @VisitTypesStrings)
		AND
			db.BranchID = ANY (SELECT Id FROM @BranchIDInts)
		AND
			dp.ClientPeriodsAgo between @HideCurrentPeriod and @HideCurrentPeriod+3
		AND
			dp.PeriodType = @OFPeriodType
		AND
			dp.ClientID=@ClientID
		AND
			dp.IncludeInPortal='Include'
		AND
			dp.IncludeInAnalysis='Include'
		AND
			fcr.MaxScore > 0
		AND
			fcr.Active = 1
		AND
			fcr.IncludeInAnalysis='Include'	
		AND
			(dv.IsNBVisit=1 OR @IsNB=0)
		AND
			(case when Len(@Questionnaire)=0 then 1 when  dv.Questionnaire = ANY (SELECT Id FROM @QuestionnaireString) then 1 else 0 end )=1
		AND
			(case when Len(@QuestionCategory)=0 then 1 when  fcr.Category = ANY (SELECT Id FROM @QuestionCategoriesString) then 1 else 0 end)=1
		AND
			db.ClientID=28647 
		AND
		    dv.Type='Online Feedback'

	
	GROUP BY 
	fcr.Category 
,	case When @GroupBy='Area' then db.CurrentArea 
	else 'A' 
	end

	
order by CategoryName, PeriodsAgo ,PeriodName

OPTION(OPTIMIZE FOR UNKNOWN)
		
END

-- Test Case
-- EXEC [dbo].[DashboardGetCategoryTableScores]  @ClientID = 209, @BranchIDs = '50,51,118,120,121,122,123,124,126,127,128,129,130,131,133,134,136,138,139,143,161,205,239,251,258,259,308,311,312,405,418,427,435,446,453,559,609,619,623,624,734,735,736,737' ,@VisitTypes = 'Mystery Visit', @HideCurrentPeriod = 0,@GroupBy='Area'



GO
