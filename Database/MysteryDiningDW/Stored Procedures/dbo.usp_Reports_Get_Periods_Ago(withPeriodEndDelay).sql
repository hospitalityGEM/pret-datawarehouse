SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Reports_Get_Periods_Ago(withPeriodEndDelay)]
	@Client			INT
,	@PeriodEndDelay BIT = 0
,	@NumberPeriods INT

AS

SELECT DISTINCT 
      dp.PeriodID  


FROM
FactVisitResult fvr
INNER JOIN DimPeriod dp ON fvr.PeriodKey = dp.PeriodKey

WHERE
      dp.ClientID = @Client		
AND
      fvr.Active = 1
AND dp.ClientPeriodsAgo BETWEEN  CAST(@PeriodEndDelay as int) AND (CAST(@PeriodEndDelay as int) + @NumberPeriods -1)


GO
