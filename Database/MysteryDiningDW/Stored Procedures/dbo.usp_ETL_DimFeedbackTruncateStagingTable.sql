SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_DimFeedbackTruncateStagingTable] 
AS
	-- DimFeedback
	TRUNCATE TABLE StagingDimFeedback

-- Test Case
-- EXEC usp_ETL_DimFeedbackTruncateStagingTable
GO
