SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_Minerva_GetManagersReports]
	@ManagerID			INT
,	@ReportTypeID		TINYINT = 0
,	@IncludeInactive	BIT = 1
AS

DECLARE	@ActiveCondition	NVARCHAR(1)
DECLARE @TypeCondition		NVARCHAR(5)

IF (@IncludeInactive = 1)
		SET @ActiveCondition = '%'
ELSE
		SET @ActiveCondition = 1
	
IF @ReportTypeID = 0
		SET @TypeCondition = '%'
ELSE
		SET @TypeCondition = @ReportTypeID

SELECT
		1 AS ReportID
	,	'ReportSection' AS SectionName
	,	'TypeName' AS TypeName
	,	'Ask Weekly' AS ReportTitle
	,	'AskWeekly' AS SSRSName
UNION
SELECT
		2 AS ReportID
	,	'ReportSection2' AS SectionName
	,	'TypeName2' AS TypeName
	,	'Report Title2' AS ReportTitle
	,	'SSRS Name2' AS SSRSName
UNION
SELECT
		3 AS ReportID
	,	'ReportSection2' AS SectionName
	,	'TypeName2' AS TypeName
	,	'Report Title3' AS ReportTitle
	,	'SSRS Name3' AS SSRSName
UNION
SELECT
		4 AS ReportID
	,	'ReportSection3' AS SectionName
	,	'TypeName' AS TypeName
	,	'Report Title4' AS ReportTitle
	,	'SSRS Name4' AS SSRSName


/*
BEGIN
	SELECT DISTINCT
		R.ReportID
	,	RS.SectionName
	,	RT.TypeName
	,	R.ReportTitle
	,	R.SSRSName
	FROM
		Report R
	INNER JOIN ReportSection	RS ON R.ReportSectionID = RS.ReportSectionID
	INNER JOIN ReportType		RT ON R.ReportTypeID	= RT.ReportTypeID
-- ReportSubscription table info needs to be added (but not possible until new database is in use)
-- Left join to ManagerBrand/ManagerClient table to check they have a subscription to the report	
	WHERE
		R.ReportTypeID	LIKE @TypeCondition
	AND	R.Active		LIKE @ActiveCondition
	AND R.ReportID		NOT IN	(
								SELECT 
									ReportID 
								FROM 
									ReportGroupException 
								WHERE 
									ManagerID = @ManagerID 
								AND	Allow = 0
								)
	AND R.ReportID		IN	(
								SELECT
									ReportID
								FROM
									ReportReportGroup
								WHERE
									ReportGroupID IN (
														SELECT
															ReportGroupID
														FROM
															ReportGroupManager
														WHERE
															ManagerID = @ManagerID
													 )
							)
		
UNION
	SELECT
		R.ReportID
	,	RS.SectionName
	,	RT.TypeName
	,	R.ReportTitle
	,	R.SSRSName
	FROM
		Report R
	INNER JOIN ReportSection	RS ON R.ReportSectionID = RS.ReportSectionID
	INNER JOIN ReportType		RT ON R.ReportTypeID	= RT.ReportTypeID
	WHERE
		R.ReportTypeID	LIKE @TypeCondition
	AND	R.Active		LIKE @ActiveCondition
	AND R.ReportID		IN	(
								SELECT 
									ReportID 
								FROM 
									ReportGroupException 
								WHERE 
									ManagerID = @ManagerID 
								AND	Allow = 1
							)

END
*/
GO
GRANT EXECUTE ON  [dbo].[usp_Minerva_GetManagersReports] TO [TMDCReporting]
GO
