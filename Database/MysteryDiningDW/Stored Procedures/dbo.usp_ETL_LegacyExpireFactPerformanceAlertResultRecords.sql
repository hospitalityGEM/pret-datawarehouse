SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_LegacyExpireFactPerformanceAlertResultRecords] 
AS

--Expired Records

UPDATE FactPerformanceAlertResult
SET
	[Active] = 0
,	[ValidToDate] = PfR.ValidToDate

FROM
	FactPerformanceAlertResult fR
INNER JOIN
	StagingFactPerformanceAlertResult PfR	ON	fR.[Source]		= PfR.[Source]
											AND	fR.AlertID		= PfR.AlertID
											AND fR.VisitID		= PfR.VisitID	
WHERE
	fR.[Active] = 1
AND	fR.[ValidFromDate] <= PfR.ValidToDate
AND fR.[Source] = 'Legacy'

-- Update SCD1 Records

UPDATE FactPerformanceAlertResult
SET
	Questionnaire		= PfR.Questionnaire
,	SubQuestionnaire	= PfR.SubQuestionnaire
,	IncludeInAnalysis	= PfR.IncludeInAnalysis
,	AlertName			= PfR.AlertName
,	AlertType			= PfR.AlertType
,	VisitStatus			= PfR.VisitStatus
,	CompletedDateKey	= PfR.CompletedDateKey
FROM
	FactPerformanceAlertResult fR
INNER JOIN
	StagingFactPerformanceAlertResult PfR	ON	fR.[Source]		= PfR.[Source]
											AND	fR.AlertID		= PfR.AlertID
											AND fR.VisitID		= PfR.VisitID	
WHERE
--	Pfr.[Type] = 'SCD1'
--AND 
	fR.[Source] = 'Legacy'

-- Update Current Score values on old records

--UPDATE FactSectionResult
--SET
--	[CurrentScore] = SfR.CurrentScore

--FROM
--	FactSectionResult fR
--INNER JOIN
--	StagingFactSectionResult SfR	ON	fR.[Source]		= SfR.[Source]
--									AND	fR.SectionID	= SfR.SectionID
--									AND fR.VisitID		= SfR.VisitID
--WHERE
--	fR.[Source] = 'Legacy'

-- Set Active flag Appropriately

--UPDATE
--    FactPerformanceAlertResult
--SET
--    [Active] = 
--				CASE 
--					WHEN active.realmax IS NULL THEN 0
--					ELSE 1
--				END
--FROM
--	FactPerformanceAlertResult [source]
--LEFT JOIN
--    (    
--          SELECT
--                MAX(fpar.PerformanceAlertResultKey) realmax
--          FROM
--                (
--                  SELECT
--						VisitID,
--                        AlertID,
--                        [Source],
--                        MAX(ValidFromDate) maxdate
--                  FROM
--                        FactPerformanceAlertResult
--                  WHERE
--						[Source] = 'Legacy'
--                  GROUP BY
--                        VisitID,
--                        AlertID,
--                        [Source]
--                ) x
--                INNER JOIN FactPerformanceAlertResult fpar ON x.AlertID = fpar.AlertID
--												AND x.VisitID = fpar.VisitID
--												AND x.maxdate = fpar.ValidFromDate
--          GROUP BY
--                fpar.AlertID
--			,	fpar.VisitID
--			,	fpar.[Source]
--	) active ON [source].PerformanceAlertResultKey = active.realmax

-- Test
-- EXEC usp_ETL_LegacyExpireFactPerformanceAlertResultRecords
	
GO
