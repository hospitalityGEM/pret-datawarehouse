SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_DimBranchTruncateStagingTable] 
AS
	-- DimBranch
	TRUNCATE TABLE StagingDimBranch

-- Test Case
-- EXEC usp_ETL_DimBranchTruncateStagingTable
GO
