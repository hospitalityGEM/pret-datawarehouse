SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_ETL_DWRunSummary]
	@AsHTML	 BIT = 0
AS

--DECLARE @AsHTML   BIT	  = 0	

DECLARE @SQL VARCHAR(MAX) 

SELECT @SQL = 

'
DECLARE @DWStart DATETIME

SELECT @DWStart = 
CASE
	WHEN @DWStart IS NULL 
		THEN (SELECT LastUpdated FROM ConfigInfo WHERE ConfigInfoID = 15)
	ELSE @DWStart
END

'
+
CASE
	WHEN @AsHTML = 1
	THEN
		'
		SELECT
			''
			<html><H1>DW Summary</H1><body><table border = 2><tr>
			<th>TimeStamp</th>
			<th>Table</th>
			<th>Start</th>
			<th>Existing</th>
			<th>Source</th>
			<th>Unchanged</th>
			<th>New</th>
			<th>SCD2 Expired</th>
			<th>SCD2 New</th>
			<th>SCD1 Update</th>
			<th>Invalid</th>
			<th>Errors</th>
			<th>Last Output</th>
			</tr>
			'' 
			+ dr.DataRows + 
			''</table></body></html>''
		FROM 
		(
		SELECT
		'
	ELSE
		'SELECT'
END
+ 
CASE
	WHEN @AsHTML = 1
	THEN
		'
		(CAST((
		SELECT
			CONVERT(TIME,[TimeStamp])	AS ''td'',''''
		,	alc.[Task]					AS ''td'',''''
		,	CONVERT(TIME,[StartTime])	AS ''td'',''''
		,	[ExistingInput]				AS ''td'',''''
		,	[SourceSystemInput]			AS ''td'',''''
		,	[UnchangedOutput]			AS ''td'',''''
		,	[NewOutput]					AS ''td'',''''
		,	[SCD2ExpiredOutput]			AS ''td'',''''
		,	[SCD2NewOutput]				AS ''td'',''''
		,	[SCD1UpdateOutput]			AS ''td'',''''
		,	[InvalidInputOutput]		AS ''td'',''''
		,	et.ErrorCount				AS ''td'',''''
		,	CONVERT(TIME,[LastOutputTime]) AS ''td'',''''
		'
	ELSE
		'
			CONVERT(TIME,[TimeStamp])	AS [TimeStamp]
		,	alc.[Task]					AS [Table]
		,	CONVERT(TIME,[StartTime])	AS [Start]
		,	[ExistingInput]				AS [Existing]
		,	[SourceSystemInput]			AS [Source]
		,	[UnchangedOutput]			AS Unchanged
		,	[NewOutput]					AS [New]
		,	[SCD2ExpiredOutput]			AS [SCD2Expired]
		,	[SCD2NewOutput]				AS [SCD2New]
		,	[SCD1UpdateOutput]			AS [SCD1Update]
		,	[InvalidInputOutput]		AS [Invalid]
		,	et.ErrorCount				AS [Error]
		,	CONVERT(TIME,[LastOutputTime]) AS LastOutput
		'
END
+
'
FROM 
	[AuditLoadCounts] alc
	INNER JOIN 
		(
			SELECT
				''DimAdmin''				AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorDimAdmin
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				''DimAssessor''			AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorDimAssessor
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				''DimBranch''				AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorDimBranch
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				''DimDish''				AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorDimDish
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				''DimFeedback''			AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorDimFeedback
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				''DimGeography''			AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorDimGeography
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				''DimGuestMarketing''	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorDimGuestMarketing
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				''DimPeriod''				AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorDimPeriod
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				''DimQuestion''			AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorDimQuestion
			WHERE
				[TimeStamp] >  @DWStart
			
			UNION

			SELECT
				''DimRecommendation''	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorDimRecommendation
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				''DimSocialChannel''	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorDimSocialChannel
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				''DimVisit''				AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorDimVisit
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				''FactAdvocacyFacebook''		AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactAdvocacyFacebook
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				''FactAdvocacyTwitter''		AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactAdvocacyTwitter
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				''FactAdvocacyEmail''	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactAdvocacyEmail
			WHERE
				[TimeStamp] >  @DWStart
			
			UNION

			SELECT
				''FactLinkTracking''	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactLinkTracking
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				''FactRecommendationView''	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactRecommendationView
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				''FactSocialChannelCount''	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactSocialChannelCount
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				''FactVisitResult''		AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactVisitResult
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				''FactAssessorHistory''	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactAssessorHistory
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				''FactBenchmarkResult''	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactBenchmarkResult
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				''FactCategoryResult''	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactCategoryResult
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				''FactDepartmentResult''	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactDepartmentResult
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				''FactFeedbackResult''	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactFeedbackResult
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				''FactPerformanceAlertResult''	AS Task
			,	COUNT(*)						AS ErrorCount
			FROM
				ErrorFactPerformanceAlertResult
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				''FactQuestionResult''	AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactQuestionResult
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				''FactSectionResult''		AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactSectionResult
			WHERE
				[TimeStamp] >  @DWStart

			UNION

			SELECT
				''FactVisitResult''		AS Task
			,	COUNT(*)				AS ErrorCount
			FROM
				ErrorFactVisitResult
			WHERE
				[TimeStamp] >  @DWStart
		) et ON alc.Task = et.Task
WHERE
	[TimeStamp] >  @DWStart
'
+
CASE
	WHEN @AsHTML = 1
	THEN
		'
		GROUP BY
			CONVERT(TIME,[TimeStamp])	
		,	CONVERT(TIME,[StartTime])	
		,	[ExistingInput]				
		,	[SourceSystemInput]			
		,	[UnchangedOutput]			
		,	[NewOutput]					
		,	[SCD2ExpiredOutput]			
		,	[SCD2NewOutput]				
		,	[SCD1UpdateOutput]			
		,	[InvalidInputOutput]		
		,	et.ErrorCount				
		,	CONVERT(TIME,[LastOutputTime])
		,	alc.Task FOR XML PATH(''tr''), ELEMENTS) AS NVARCHAR(MAX)
		)) AS DataRows
		) dr
		'
	ELSE
		''
END

EXEC (@SQL)

-- Test Case
-- EXEC [usp_ETL_DWRunSummary] 1
GO
