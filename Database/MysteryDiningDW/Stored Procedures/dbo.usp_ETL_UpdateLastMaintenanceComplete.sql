SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_UpdateLastMaintenanceComplete] 
AS
					
UPDATE 
	ConfigInfo
SET 
	Value = GETDATE()
,	LastUpdated = GETDATE()
WHERE
	[Key] = 'LastMaintenanceComplete'
	
-- EXEC usp_ETL_UpdateLastMaintenanceComplete
GO
