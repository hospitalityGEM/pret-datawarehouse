SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_TruncateStagingTables] 
AS
	-- DimAssessor
	TRUNCATE TABLE StagingDimAssessor
	
	-- DimBranch
	TRUNCATE TABLE StagingDimBranch
	
	-- DimVisit
	TRUNCATE TABLE StagingDimVisit

	-- DimQuestion
	TRUNCATE TABLE StagingDimQuestion

	-- DimGeography
	TRUNCATE TABLE StagingDimGeography

	-- DimDish
	TRUNCATE TABLE StagingDimDish

	-- DimFeedback
	TRUNCATE TABLE StagingDimFeedback

	-- DimPeriod
	TRUNCATE TABLE StagingDimPeriod

	-- DimRecommendation
	TRUNCATE TABLE StagingDimRecommendation

	-- FactSectionResult
	TRUNCATE TABLE StagingFactSectionResult

	-- FactQuestionResult
	TRUNCATE TABLE StagingFactQuestionResult

	-- FactDepartmentResult
	TRUNCATE TABLE StagingFactDepartmentResult

	-- FactCategoryResult
	TRUNCATE TABLE StagingFactCategoryResult

	-- FactBenchmarkResult
	TRUNCATE TABLE StagingFactBenchmarkResult

	-- FactPerformanceAlertResult
	TRUNCATE TABLE StagingFactPerformanceAlertResult

	-- FactFeedbackResult
	TRUNCATE TABLE StagingFactFeedbackResult

	-- FactVisitResult
	TRUNCATE TABLE StagingFactVisitResult

	-- FactAdvocacyTwitter
	TRUNCATE TABLE StagingFactAdvocacyTwitter

	-- FactAdvocacyFacebook
	TRUNCATE TABLE StagingFactAdvocacyFacebook

	-- FactAdvocacyEmail
	TRUNCATE TABLE StagingFactAdvocacyEmail

	-- FactRecommendationView
	TRUNCATE TABLE StagingFactRecommendationView

-- Test Case
-- EXEC usp_ETL_TruncateStagingTables
GO
