SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_LegacyExpireFactBenchmarkResultRecords] 
AS

--Expired Records

UPDATE FactBenchmarkResult
SET
	[Active] = 0
,	[ValidToDate] = SfB.ValidToDate

FROM
	FactBenchmarkResult fB
INNER JOIN
	StagingFactBenchmarkResult SfB	ON	fB.[Source]		= SfB.[Source]
									AND	fB.BenchmarkID	= SfB.BenchmarkID
									AND fB.VisitID		= SfB.VisitID	
WHERE
	fB.[ValidFromDate] <= SfB.ValidToDate
AND fB.[Source] = 'Legacy'
AND SfB.[Type] = 'Expired' 


 -- Update SCD1 Changes --
UPDATE FactBenchmarkResult
SET
	VisitScore			= SfB.VisitScore
,	VisitMaxScore		= SfB.VisitMaxScore
,	VisitScoreMethod	= SfB.VisitScoreMethod
FROM
	FactBenchmarkResult fB
INNER JOIN
	StagingFactBenchmarkResult SfB	ON	fB.[Source]		= SfB.[Source]
									AND fB.BenchmarkID	= SfB.BenchmarkID
									AND fB.VisitID		= SfB.VisitID
WHERE
--	SfB.[Type] = 'SCD1'
--AND 
	SfB.[Source] = 'Legacy'

-- Update most recent record to set it active (shouldn't be needed)
--UPDATE
--    FactBenchmarkResult
--SET
--    [Active] = 
--				CASE 
--					WHEN active.realmax IS NULL THEN 0
--					ELSE 1
--				END
--FROM
--	FactBenchmarkResult [source]
--LEFT JOIN
--    (    
--          SELECT
--                MAX(fdr.BenchmarkResultKey) realmax
--          FROM
--                (
--                  SELECT
--						VisitID,
--                        BenchmarkID,
--                        [Source],
--                        MAX(ValidFromDate) maxdate
--                  FROM
--                        FactBenchmarkResult
--                  WHERE
--						[Source] = 'Legacy'
--                  GROUP BY
--                        VisitID,
--                        BenchmarkID,
--                        [Source]
--                ) x
--                INNER JOIN FactBenchmarkResult fdr ON x.BenchmarkID = fdr.BenchmarkID
--												AND x.VisitID = fdr.VisitID
--												AND x.maxdate = fdr.ValidFromDate
--          GROUP BY
--                fdr.BenchmarkID
--			,	fdr.VisitID
--			,	fdr.[Source]
--	) active ON [source].BenchmarkResultKey = active.realmax

-- Test
-- EXEC usp_ETL_LegacyExpireFactBenchmarkResultRecords
	
GO
