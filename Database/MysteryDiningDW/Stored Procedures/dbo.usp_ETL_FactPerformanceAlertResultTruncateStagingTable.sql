SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_FactPerformanceAlertResultTruncateStagingTable] 
AS
	-- FactPerformanceAlertResult
	TRUNCATE TABLE StagingFactPerformanceAlertResult

-- Test Case
-- EXEC usp_ETL_FactPerformanceAlertResultTruncateStagingTable
GO
