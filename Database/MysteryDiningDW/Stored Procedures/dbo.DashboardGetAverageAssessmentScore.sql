
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[DashboardGetAverageAssessmentScore]
	-- Add the parameters for the stored procedure here
	@DateFrom datetime, 
	@DateTo datetime, 
	@ClientID int = 0, 
	@BranchIDs nvarchar(max),
	@VisitTypes nvarchar(max),
	@HideCurrentPeriod bit,
	@Benchmark nvarchar(max) = '',
	@QuestionCategory nvarchar(max) = '',
	@QuestionnaireCategory nvarchar(max) = '',
	@Department nvarchar(max) = '',
	@Brand nvarchar(max) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    DECLARE
		@DateKeyFrom	INT
	,	@DateKeyTo		INT
	,	@DateKeyCurrent	INT
		
	SELECT @DateKeyFrom	= DateKey FROM DimDate WHERE [Date] = @DateFrom
	SELECT @DateKeyTo	= DateKey FROM DimDate WHERE [Date] = @DateTo
	SELECT @DateKeyCurrent	= DateKey FROM DimDate WHERE [Date] = CONVERT(DATE, GETDATE())

	SELECT
		CASE 
			WHEN db.ClientID=35418 THEN CONVERT(DECIMAL(5,2),(SUM(fvr.AdjustedScore)/SUM(fvr.MaxScore))*100)
			ELSE  CONVERT(DECIMAL(5,2),(SUM(fvr.Score)/SUM(fvr.MaxScore))*100) END 

	FROM
		FactVisitResult fvr
		INNER JOIN DimBranch db ON fvr.BranchKey = db.BranchKey
		INNER JOIN DimVisit  dv	ON fvr.VisitKey	 = dv.VisitKey
		INNER JOIN DimPeriod dp ON fvr.PeriodKey = dp.PeriodKey
	WHERE
			db.BranchID IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@BranchIDs, ','))
		AND
			(CASE
				WHEN LEN(@QuestionnaireCategory) = 0 THEN 1
				WHEN dv.QuestionnaireCategory IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@QuestionnaireCategory,',')) THEN 1
				ELSE 0
			END) = 1
		AND
			(CASE
				WHEN LEN(@Brand) = 0 THEN 1
				WHEN db.Brand IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@Brand,',')) THEN 1
				ELSE 0
			END) = 1
		AND
			dv.VisitStatus IN ('Reviewed','Complete')
		AND
			dv.[Type] IN (SELECT * FROM dbo.ParseCommaSeparatedValuesToTable(@VisitTypes, ','))
		AND
			dp.StartDateKey >= @DateKeyFrom
		AND
			dp.StartDateKey <= @DateKeyTo
		AND
			fvr.MaxScore > 0
		AND
			fvr.Active = 1

		AND
			(CASE
				WHEN @HideCurrentPeriod = 1 AND dp.EndDateKey < @DateKeyCurrent THEN 1
				WHEN @HideCurrentPeriod = 0 THEN 1
				ELSE 0
			END) = 1
		AND
			fvr.IncludeInAnalysis = 'Include'
		AND
			dp.IncludeInPortal='Include'

		GROUP BY db.ClientID				
END

-- Test Case
-- EXEC [dbo].[DashboardGetAverageAssessmentScore] @DateFrom = '10-01-2012', @DateTo = '12-31-2012', @ClientID = 18867,  @BranchIDs = '5589,3776,3775,3778', @VisitTypes = 'Mystery Visit', @HideCurrentPeriod = 0




GO
