
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_UpdateFactVistResultEarliestDate] 
AS

DECLARE @EarliestDate DATETIME

SELECT @EarliestDate =		DATEADD(d,-5,MAX([TimeStamp]))
						FROM
							MysteryDiningCopy.dbo.Answer
						WHERE
							[TimeStamp] <= GETDATE()
UPDATE 
	ConfigInfo
SET 
	Value = @EarliestDate
,	LastUpdated = @EarliestDate
WHERE
	[Key] = 'FactVisitResultEarliestRecord'
	
-- EXEC usp_ETL_UpdateFactVistResultEarliestDate
GO
