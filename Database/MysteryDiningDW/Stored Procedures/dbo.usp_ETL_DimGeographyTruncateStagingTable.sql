SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ETL_DimGeographyTruncateStagingTable] 
AS
	-- DimGeography
	TRUNCATE TABLE StagingDimGeography

-- Test Case
-- EXEC usp_ETL_DimGeographyTruncateStagingTable
GO
