SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [RedGate].[MigrationHistory] ()
RETURNS @Tbl TABLE (PropertyKey VARCHAR(30) UNIQUE, PropertyValue NVARCHAR(MAX))
AS 
BEGIN
    
INSERT  @Tbl  VALUES  ('MigrationHistory' , N'[
  {
    "UpScript": "/*\r\nWrite the migration script to be included in the deployment script.\r\n\r\nMigration scripts are run at the beginning of the deployment. We \r\nrecommend you include guard clauses to make sure the objects you''re \r\nmodifying exist before the rest of the script runs.\r\n\r\nYou can see examples of migration scripts at http://documentation.red-gate.com/display/MV2.\r\n*/\r\n\r\nDECLARE @ShouldRunMigrationScript BIT  = 0\r\n\r\nIF EXISTS (SELECT 1 FROM Information_Schema.Columns WHERE table_schema = ''dbo'' AND TABLE_NAME = ''DimPeriod'' AND COLUMN_NAME=''YearStartKey'')\r\nBEGIN\r\n\tIF EXISTS (SELECT 1 FROM Information_Schema.Columns WHERE table_schema = ''dbo'' AND TABLE_NAME = ''DimPeriod'' AND COLUMN_NAME=''YearEndKey'')\r\n\tBEGIN\r\n\t\tIF EXISTS (SELECT COUNT(*) FROM DimPeriod WHERE YearEndKey IS NULL AND YearStartKey IS NULL)\r\n\t\tBEGIN\r\n\t\t\tSET @ShouldRunMigrationScript = 1\r\n\t\tEND\r\n\tEND\r\nEND\r\n\r\nIF @ShouldRunMigrationScript = 1\r\nBEGIN\r\n\r\nUPDATE\r\n       DimPeriod\r\nSET\r\n       YearStartKey=dd.DateKey\r\nFROM\r\n\tDimPeriod dp\r\n\tINNER JOIN [MysteryDiningCopy].[dbo].[ClientPeriodBrief] cpb on cpb.PeriodBriefUID=dp.PeriodID\r\n\tINNER JOIN [MysteryDiningCopy].[dbo].[ClientPeriodYear] cpy on cpy.PeriodYearUID=cpb.PeriodYearUID\r\n\tINNER JOIN DimDate dd ON dd.[Date]=cpy.PeriodYearStart\r\nWHERE\r\n       YearStartKey IS NULL\r\n\r\nUPDATE\r\n     DimPeriod\r\nSET\r\n     YearEndKey=dd.DateKey\r\nFROM\r\n\tDimPeriod dp\r\n\tINNER JOIN [MysteryDiningCopy].[dbo].[ClientPeriodBrief] cpb on cpb.PeriodBriefUID=dp.PeriodID\r\n\tINNER JOIN [MysteryDiningCopy].[dbo].[ClientPeriodYear] cpy on cpy.PeriodYearUID=cpb.PeriodYearUID\r\n\tINNER JOIN DimDate dd ON dd.[Date]=cpy.PeriodYearEnd\r\nWHERE\r\n     YearEndKey IS NULL\r\n\r\nEND\r\n",
    "DownScript": null,
    "Name": "Add Initial Values for DimPeriod Year Start & End Keys",
    "UniqueId": "a0df58d9-75a0-4f0f-8feb-57978da28d93",
    "Timestamp": "2014-03-06T09:17:09",
    "Order": 0,
    "Description": "Checks if columns have been added and that there are null values before adding an initial value to the YearStartKey and YearEndKey"
  }
]')
    RETURN
END
GO
