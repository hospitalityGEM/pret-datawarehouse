SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[udf_Minerva_PadNumeric] 
(
	@Number			INT
,	@RequiredLength INT
,	@PadWith		NCHAR(1)
)
RETURNS NVARCHAR(50)
AS
    BEGIN 
        DECLARE @Result VARCHAR(50)
      
        SELECT @Result = REPLACE(STR(@Number, @RequiredLength),' ',@PadWith)
        
        RETURN @Result
    END
GO
