SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[ParseCommaSeparatedValuesToTable] ( @Array NVARCHAR(max), @separator NVARCHAR(10)) 
RETURNS @resultTable TABLE 
	(parseValue NVARCHAR(100))
AS
BEGIN

	DECLARE @separator_position INT 
	DECLARE @array_value NVARCHAR(max) 
	
	SET @array = @array + @separator
	
	WHILE patindex('%' + @separator + '%' , @array) <> 0 
	BEGIN
	
	  SELECT @separator_position =  patindex('%' + @separator + '%', @array)
	  SELECT @array_value = left(@array, @separator_position - 1)
	
		INSERT @resultTable
		VALUES (Cast(@array_value AS nvarchar(max)))

	  SELECT @array = stuff(@array, 1, @separator_position, '')
	END

	RETURN
END


GO
