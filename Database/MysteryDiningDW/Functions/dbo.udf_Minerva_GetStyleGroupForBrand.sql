SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[udf_Minerva_GetStyleGroupForBrand] 
(
	@BrandID			INT
)
RETURNS NVARCHAR(50)
AS
    BEGIN 
        DECLARE @Result SMALLINT
      
      IF @BrandID = 1
		SELECT @Result = 1
	  ELSE
		SELECT @Result = 0
      
      --  SELECT @Result = ISNULL(
						--	(SELECT 1)
						--,1)
        
        -- Select data from Brand table in DB
        
        RETURN @Result
    END
GO
