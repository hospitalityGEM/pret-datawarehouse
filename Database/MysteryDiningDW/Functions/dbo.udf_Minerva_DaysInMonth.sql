SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[udf_Minerva_DaysInMonth] 
(
	@Date		DATETIME
)
RETURNS INT
AS
    BEGIN 

    RETURN CASE WHEN MONTH(@Date) IN (1, 3, 5, 7, 8, 10, 12) THEN 31
                WHEN MONTH(@Date) IN (4, 6, 9, 11) THEN 30
                ELSE CASE WHEN (YEAR(@Date) % 4    = 0 AND
                                YEAR(@Date) % 100 != 0) OR
                               (YEAR(@Date) % 400  = 0)
                          THEN 29
                          ELSE 28
                     END
           END

	END
GO
